<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Продажа автомобилей в Москве в автосалонах FAVORIT MOTORS. Купить новые и подержанные авто. Актуальный каталог и цены. Акции.");
$APPLICATION->SetPageProperty("keywords", "Главная страница");
$APPLICATION->SetPageProperty("title", "Продажа авто в Москве | Купить автомобиль в автосалоне дилера FAVORIT MOTORS, цены на машины");
$APPLICATION->SetTitle("Главная страница \" FAVORIT MOTORS\"");
?><div class="tab-bar">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"slider_main_page",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "infoblocks",
		"IBLOCK_ID" => "11",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DETAIL_PICTURE",3=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"BRAND",2=>"STOCK",3=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?><br>
	<div class="tab-bar-container">
		<div class="left-menu">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"brands_main_page",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "references",
		"IBLOCK_ID" => "10",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"CODE",1=>"NAME",2=>"PREVIEW_PICTURE",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	)
);?>
		</div>
		<div class="right-menu-container">
			<div class="tab-un-active tabs">
 <br>
				 &nbsp; &nbsp; <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.bigdata.products",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"RCM_TYPE" => "personal",
		"ID" => $_REQUEST["PRODUCT_ID"],
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "",
		"SHOW_FROM_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"TEMPLATE_THEME" => "blue",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"PRICE_CODE" => array(),
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"SHOW_PRODUCTS_13" => "N",
		"SHOW_PRODUCTS_17" => "N"
	)
);?><br>
 <br>
				 Новые
			</div>
			<div class="tab-semi-active tabs">
				 В наличии
			</div>
			<div class="tab-active tabs active">
				 С пробегом
			</div>
			<div class="right-menu">
				<div class="menu-container">
					<select class="sort-by__select select--styled-ui">
						<option>Марка ...</option>
						<option>Ford</option>
						<option>Skoda</option>
						<option>Volvo</option>
					</select>
				</div>
				<div class="menu-container">
					<select class="sort-by__select select--styled-ui">
						<option>Модель ...</option>
					</select>
				</div>
				<div class="menu-container cost roll active">
					<div class="info-block hidden">
						<div class="left">
							 Цена:
						</div>
						<div class="right">
							 10 000 – 100 000 <del>P</del>
						</div>
					</div>
					<div class="deployed">
						<div class="big-filter__label big-filter__label--widget" style="margin-top: 20px">
							 Цена:
							<div class="big-filter__price">
 <span id="price-first">10 000</span>
								– <span id="price-second">100 000</span> <del>P</del>
							</div>
						</div>
						<div class="big-fiter-price__inputs">
							<div class="big-fiter-price__input big-fiter-price__input--right">
 <input type="text" id="price-val-second">
							</div>
							<div class="big-fiter-price__input big-fiter-price__input--left">
 <input type="text" id="price-val-first">
							</div>
						</div>
						<div class="big-fiter-price__range" style="margin: 23px 0">
							<div id="price-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
 <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.4137931034483%;"></span> <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 73.9310344827586%;"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="splitter splitter_cost">
				</div>
				<div class="menu-container mileage roll">
					<div class="info-block">
						<div class="left">
							 Пробег:
						</div>
						<div class="right">
							 10 000 – 100 000 км
						</div>
					</div>
					<div class="deployed hidden">
						<div class="big-filter__label big-filter__label--widget" style="margin-top: 20px">
							 Пробег:
							<div class="big-filter__price">
 <span id="price-first-2">10 000</span>
								– <span id="price-second-2">100 000</span> км
							</div>
						</div>
						<div class="big-fiter-price__inputs">
							<div class="big-fiter-price__input big-fiter-price__input--right">
 <input type="text" id="price-val-second-2">
							</div>
							<div class="big-fiter-price__input big-fiter-price__input--left">
 <input type="text" id="price-val-first-2">
							</div>
						</div>
						<div class="big-fiter-price__range" style="margin: 23px 0">
							<div id="price-range-2" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
 <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.4137931034483%;"></span> <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 73.9310344827586%;"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="splitter splitter_mileage">
				</div>
				<div class="menu-container year roll">
					<div class="info-block">
						<div class="left">
							 Год:
						</div>
						<div class="right">
							 2010 – 2015 гг
						</div>
					</div>
					<div class="deployed hidden">
						<div class="big-filter__label big-filter__label--widget" style="margin-top: 20px">
							 Год:
							<div class="big-filter__price">
 <span id="price-first-3">2010</span>
								– <span id="price-second-3">2015</span> гг
							</div>
						</div>
						<div class="big-fiter-price__inputs">
							<div class="big-fiter-price__input big-fiter-price__input--right">
 <input type="text" id="price-val-second-3">
							</div>
							<div class="big-fiter-price__input big-fiter-price__input--left">
 <input type="text" id="price-val-first-3">
							</div>
						</div>
						<div class="big-fiter-price__range" style="margin: 23px 0">
							<div id="price-range-3" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
 <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.4137931034483%;"></span> <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 73.9310344827586%;"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="order-btn">
					 Подобрать
				</div>
			</div>
		</div>
	</div>
</div>
<div class="block-icons">
	<div class="container">
		<ul>
			<li class="tech"> <a href="#"> <span class="round-button"></span> <span class="title">Записаться в<br>
			 тех. центр</span> </a> </li>
			<li class="test"> <a href="#"> <span class="round-button"></span> <span class="title">ПРОЙТИ<br>
			 Тест-Драйв</span> </a> </li>
			<li class="sale"> <a href="#"> <span class="round-button"></span> <span class="title">Продать авто<br>
			 выгодно</span> </a> </li>
			<li class="calc"> <a href="#"> <span class="round-button"></span> <span class="title">Калькулятор<br>
			 каско</span> </a> </li>
		</ul>
	</div>
</div>
<div class="second-sub-menu">
	<div class="sub-menu-container">
 <a href="#" class="auto"> <span class="counter">4500</span> <span class="content">Новых<br>
		 автомобилей</span> </a> <a href="#" class="auto"> <span class="counter">1500</span> <span class="content">Автомобилей<br>
		 с пробегом</span> </a> <a href="#" class="auto"> <span class="counter">500</span> <span class="content">Коммерческих<br>
		 автомобилей</span> </a> <a href="#" class="percents"> <span class="img"></span> <span class="content">Рассчитать<br>
		 кредит</span> </a>
	</div>
</div>
<div class="why-favorit">
	<div class="container">
		<div class="title">
			<div class="text">
				 ПОЧЕМУ FAVORIT MOTORS
			</div>
		</div>
		<div class="left-block">
			<ul>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/why_favorit.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
			</ul>
			<div class="about-link">
 <a href="#">О компании</a>
			</div>
		</div>
		<div class="right-block">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"locations_main_page",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "references",
		"IBLOCK_ID" => "12",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"",),
		"PROPERTY_CODE" => array(0=>"METRO",1=>"ADDRESS",2=>"PHONES",3=>"SCHEDULE_SALON",4=>"SCHEDULE_TECHNICAL_CENTER",5=>"POSITION_LEFT",6=>"POSITION_TOP",7=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N"
	)
);?>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"testimonials",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "infoblocks",
		"IBLOCK_ID" => "9",
		"NEWS_COUNT" => "6",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DATE_ACTIVE_FROM",3=>"ACTIVE_FROM",4=>"",),
		"PROPERTY_CODE" => array(0=>"AUTHOR",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "300",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?>
<div class="popular-links">
	<div class="links-container">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/popular_links_1.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
	<div class="links-container">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/popular_links_2.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
	<div class="links-container">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/popular_links_3.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
</div>
<div class="info-container">
	<div class="info">
		<div class="about-container">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/about.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_news_list",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"DATE_ACTIVE_FROM",2=>"",),
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	)
);?>
		<div class="clear">
		</div>
	</div>
	<div class="clear">
	</div>
</div>
<div class="footer-menu-container">
	<div class="container">
		<div class="footer-menu">
			<div class="cabinet">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer_menu_personal.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
			</div>
			<div class="call">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer_menu_call.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
			</div>
			<div class="search">
 <span>
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer_menu_search.php",
		"EDIT_TEMPLATE" => ""
	)
);?> </span>
				<form action="#">
 <input type="text" class="input_text" name="q" value="" id="title-search-input" size="20" maxlength="30" autocomplete="off">
				</form>
			</div>
			<div class="consultant">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/footer_menu_online_consultation.php",
		"EDIT_TEMPLATE" => ""
	)
);?>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>