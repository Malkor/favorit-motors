<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Event");
?>

<!-- Content -->
    <section id="content">

        <div class="section section-red padding">
            <div class="box-container">
                <div class="title">
                    <h1>
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                           "AREA_FILE_SHOW" => "file",
                           "PATH" => SITE_TEMPLATE_PATH."/include/event_title.php",
                           "EDIT_TEMPLATE" => ""
                           ),
                           false
                        );?>
                    </h1>
                </div>
            </div>
        </div>

        <!-- Brands -->
        <div class="section section-sec padding">
            <div class="box-container medium">
                <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"stocks_catalog_section_list", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "stocks",
		"IBLOCK_ID" => "23",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "2",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"VIEW_MODE" => "LINE",
		"SHOW_PARENT_NAME" => "Y",
		"SECTION_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "N"
	),
	false
);?>
                <div class="text on-center">
                     <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                       "AREA_FILE_SHOW" => "file",
                       "PATH" => SITE_TEMPLATE_PATH."/include/event_text_under_sections.php",
                       "EDIT_TEMPLATE" => ""
                       ),
                       false
                    );?>
                </div>
            </div>
        </div><!-- /Brands -->

        <!-- Event Deals -->
        <div class="section section-sec section-gray">
            <div class="box-container medium">
                <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"icon_stocks_automobiles", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "stocks",
		"IBLOCK_ID" => "25",
		"NEWS_COUNT" => "6",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "ICON_FONT_AWESOME",
			1 => "LINK",
			2 => "",
			3 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
                
            </div>
        </div><!-- /Event Deals -->

        <!-- Phone -->
        <div id="add-your-phone" class="section section-sec padding">
            <div class="box-container medium">

                <div class="text on-center">
                    <p>Оставьте свой телефон и мы перезвоним вам в течении 15 минут</p>
                </div>

                <div class="filter"><form id="event" class="clearfix">

                    <div class="filter-item clearfix">
                        <label for="event-name" class="on-left">Ваше имя:</label>
                        <input id="event-name" class="on-left" type="text" />
                    </div>

                    <div class="filter-item on-center clearfix">
                        <label for="event-phone" class="on-left">Телефон:</label>
                        <input id="event-phone" class="on-left" type="text" />
                    </div>

                    <div class="filter-item align-right">
                        <input type="submit" class="animate-custom red-small-link" value="Персональное предложение" />
                    </div>

                </form></div>

            </div>
        </div><!-- /Phone -->

        <!-- Seo Text -->
        <div class="section section-sec section-gray padding">
            <div class="box-container">

                <div class="seo-text">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                       "AREA_FILE_SHOW" => "file",
                       "PATH" => SITE_TEMPLATE_PATH."/include/event_text_seo.php",
                       "EDIT_TEMPLATE" => ""
                       ),
                       false
                    );?>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>