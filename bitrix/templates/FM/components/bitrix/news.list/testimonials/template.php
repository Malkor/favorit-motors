<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="testimonials">
  <div class="testimonials-container">
    <div class="title">ОТЗЫВЫ</div>
    <div class="splitter">
        <div class="left"></div>
        <div class="right"></div>
        <div class="img"></div>
    </div>
    <ul class="bxslider">
      <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
          <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
            <div class="date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
          <?endif;?>
          <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
            <div class="title"><?echo $arItem["NAME"]?></div>
          <?endif;?>
          <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
            <div class="content"><?echo $arItem["PREVIEW_TEXT"];?></div>
          <?endif;?>
          <div class="author"><?echo $arItem["PROPERTIES"]["AUTHOR"]["VALUE"];?></div>
          
          <div class="testimonial-btn">Оставить свой отзыв</div>
        </li>
      <?endforeach;?>
    </ul>
  </div>
</div>
