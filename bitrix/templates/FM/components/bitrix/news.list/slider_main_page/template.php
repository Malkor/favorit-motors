<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$i = 1;?>
<?$c = count($arResult["ITEMS"]);?>
<?foreach($arResult["ITEMS"] as $arItem):?>
  <?
  $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
  $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
  ?>
  <div class="tab-bar-bg" id="promotion<?=$arItem["DISPLAY_PROPERTIES"]["BRAND"]["LINK_ELEMENT_VALUE"][$arItem["PROPERTIES"]["BRAND"]["VALUE"]]["CODE"]?>" style="background: url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>') top center no-repeat; background-size: cover; <?if($i == $c):?>visibility: visible<?endif;?>">
      <div class="wrap" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
          <div class="tab-info">
              <?if($arItem["PROPERTIES"]["HREF"]["VALUE"]):?>
                <a href="<?echo $arItem["PROPERTIES"]["HREF"]["VALUE"]?>" class="title">
              <?else:?>
                <a href="<?=$arItem["DISPLAY_PROPERTIES"]["STOCK"]["LINK_ELEMENT_VALUE"][$arItem["PROPERTIES"]["STOCK"]["VALUE"]]["DETAIL_PAGE_URL"]?>" class="title">
              <?endif?>
                <?if(is_array($arItem["DISPLAY_PROPERTIES"]["STOCK"]["LINK_ELEMENT_VALUE"]) && $arItem["DISPLAY_PROPERTIES"]["STOCK"]["LINK_ELEMENT_VALUE"][$arItem["PROPERTIES"]["STOCK"]["VALUE"]]["NAME"] != ""):?>
                  <?echo $arItem["DISPLAY_PROPERTIES"]["STOCK"]["LINK_ELEMENT_VALUE"][$arItem["PROPERTIES"]["STOCK"]["VALUE"]]["NAME"];?>
                <?else:?>
                  <?echo $arItem["NAME"]?>
                <?endif;?>
              </a>
              <div class="description">
                <?if($arItem["PREVIEW_TEXT"] != ""):?>
                  <?echo $arItem["PREVIEW_TEXT"];?>
                <?else:?>
                  <?echo $arItem["DISPLAY_PROPERTIES"]["STOCK"]["LINK_ELEMENT_VALUE"][$arItem["PROPERTIES"]["STOCK"]["VALUE"]]["NAME"];?>
                <?endif;?>
              </div>
          </div>
      </div>
  </div>
  <?$i++;?>
<?endforeach;?>

