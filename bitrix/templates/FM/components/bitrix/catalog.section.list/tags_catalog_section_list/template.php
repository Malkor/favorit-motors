<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"])
{
    $intCurrentDepth = 1;
    $boolFirst = true;
    $request_uri=strpos($_SERVER['REQUEST_URI'], "?");
    $path=substr($_SERVER['REQUEST_URI'], 0, $request_uri);?>
    <ul class="category-list">
        <?foreach ($arResult['SECTIONS'] as &$arSection){
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);?>

            <li id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                <a class="open"><?=$arSection["NAME"]?></a>

                <?if($arSection["ELEMENTS"]):?>
                    <ul>
                        <?foreach ($arSection["ELEMENTS"] as $key => $element) {?>
                              <li><a href="<?=$path?>?tag=<?=$element["CODE"]?>"><?=$element["NAME"]?></a></li>
                        <?}?>
                    </ul>
                <?endif;?>
            </li>
        <?}?>
    </ul>
<?
}
?>