<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID.'_pict',
    'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
    'STICKER_ID' => $strMainID.'_sticker',
    'BIG_SLIDER_ID' => $strMainID.'_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
    'SLIDER_LIST' => $strMainID.'_slider_list',
    'SLIDER_LEFT' => $strMainID.'_slider_left',
    'SLIDER_RIGHT' => $strMainID.'_slider_right',
    'OLD_PRICE' => $strMainID.'_old_price',
    'PRICE' => $strMainID.'_price',
    'DISCOUNT_PRICE' => $strMainID.'_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
    'QUANTITY' => $strMainID.'_quantity',
    'QUANTITY_DOWN' => $strMainID.'_quant_down',
    'QUANTITY_UP' => $strMainID.'_quant_up',
    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
    'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
    'BASIS_PRICE' => $strMainID.'_basis_price',
    'BUY_LINK' => $strMainID.'_buy_link',
    'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
    'BASKET_ACTIONS' => $strMainID.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
    'COMPARE_LINK' => $strMainID.'_compare_link',
    'PROP' => $strMainID.'_prop_',
    'PROP_DIV' => $strMainID.'_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
    'OFFER_GROUP' => $strMainID.'_set_group_',
    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?>


<!-- Content -->
<section id="content">
  <!-- Super Slider -->
  <div class="super-slider">
    <div class="box-container">
      <div class="left-side">
        <div class="this-car-logo bg skoda" style="background-image: url(images/skoda.png); "></div>
      </div>
      <div class="right-side nalichie">
        <div class="this-car-info">
          <div class="this-car-info-name">Skoda Fabia</div>
          <div class="this-car-info-price"><span>1 890 000</span> р.</div>
          <div class="this-car-info-price-old">2 190 000 р.</div>
        </div>
        <div class="this-car-some-links">
          <ul>
            <li><a href="#">Калькулятор КАСКО</a></li>
            <li><a href="#">Рассчитать кредит</a></li>
          </ul>
        </div>
        <div class="this-car-big-links">
          <ul>
            <li><a href="#">Конфигуратор</a></li>
          </ul>
        </div>
        <div class="this-car-buttons-links">
          <ul>
            <li><a class="red-small-link animate-custom" href="#">Тест-драйв</a></li>
            <li><a class="green-small-link animate-custom" href="#">Заказать автомобиль</a></li>
          </ul>
        </div>
        <div class="this-car-sell">
          <a href="#" class="opacity-custom-hover animate-custom clearfix">
            <span class="circle-icon active on-left"><i class="fa fa-thumbs-up"></i></span>
            <span class="on-right align-left">продать авто выгодно</span>
          </a>
        </div>
      </div>

      <div class="breadcrumbs clearfix">
        <div class="on-left">
          <ul class="breadcrumbs-list clearfix">
            <li><a href="#">Каталог</a></li>
            <li>/</li>
            <li><a href="#">В наличии</a></li>
            <li>/</li>
            <li><a href="#">Skoda</a></li>
            <li>/</li>
            <li><a href="#">Fabia</a></li>
          </ul>
        </div>
        <div class="addres-block on-right">
          <div class="address-button animate-custom opacity-custom-hover">
            <div class="address-button-icon bg" style="background-image: url(images/f.png); "></div>
            <div class="address-button-text">Адреса салонов</div>
          </div>
          <div class="address-map">
            <ul class="salon-list">
              <li style="top: 70px; left: 90px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
              <li style="top: 100px; left: 225px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
              <li style="top: 90px; left: 175px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
              <li style="top: 185px; left: 235px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
              <li style="top: 185px; left: 120px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
              <li style="top: 195px; left: 155px;"><a href="#">
                  <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                  <span class="salon-name animate-custom red-small-link">Коптево</span>
              </a></li>
            </ul>
            <div class="close-address-map"><i class="fa fa-times"></i>Закрыть</div>
          </div>
        </div>
      </div>
      <div class="super-slider-containers">
          <div id="gallery" class="super-slider-container">
              <div class="gallery-navigation tabs-inner-navigation">
                  <ul class="clearfix">
                      <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-exterior">Экстерьер</a></li>
                      <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-interior">Интерьер</a></li>
                  </ul>
              </div>
              <div id="slider-exterior" class="gallery-navigation-container">
                  <div class="body-slider gallery-exterior-top">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-4" class="colorbox"></a>
                          </div>
                      </div>
                  </div>
                  <div class="swiper-container gallery-rotation-slider gallery-exterior-thumbs">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                      </div>
                      <!-- Add Arrows -->
                      <div class="swiper-button-next"></div>
                      <div class="swiper-button-prev"></div>
                  </div>
              </div>
              <div id="slider-interior" class="gallery-navigation-container">
                  <div class="body-slider gallery-inter-top">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                              <a href="content/profile/slider/4.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                              <a href="content/profile/slider/5.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                              <a href="content/profile/slider/6.jpg" rel="gallery-5" class="colorbox"></a>
                          </div>
                      </div>
                  </div>
                  <div class="swiper-container gallery-rotation-slider gallery-inter-thumbs">
                      <div class="swiper-wrapper">
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); "></div>
                          <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); "></div>
                      </div>
                      <!-- Add Arrows -->
                      <div class="swiper-button-next"></div>
                      <div class="swiper-button-prev"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div><!-- /Super Slider -->



    <!-- Cars -->
    <div id="models-cars" class="section section-sec">
        <div class="box-container">
            <div class="title border-bottom">
                <h2>Skoda Fabia в наличии: <span class="red-text">91</span></h2>
            </div>

            <div class="new-auto-items four-in-line"><!--
                --><div class="new-auto-item">
                    <div class="new-auto-item-image">
                        <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                    </div>
                    <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                    <div class="new-auto-item-desc">
                        <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                        <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                    </div>
                    <div class="new-auto-item-info clearfix">
                        <div class="on-left">
                            <div class="new-auto-item-price-new">1 019 900</div>
                            <div class="new-auto-item-price-old">1 320 000</div>
                        </div>
                        <div class="on-right">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>
                    </div>
                </div><!--
                --><div class="new-auto-item">
                    <div class="new-auto-item-image">
                        <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                    </div>
                    <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                    <div class="new-auto-item-desc">
                        <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                        <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                    </div>
                    <div class="new-auto-item-info clearfix">
                        <div class="on-left">
                            <div class="new-auto-item-price-new">1 019 900</div>
                            <div class="new-auto-item-price-old">1 320 000</div>
                        </div>
                        <div class="on-right">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>
                    </div>
                </div><!--
                --><div class="new-auto-item">
                    <div class="new-auto-item-image">
                        <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                    </div>
                    <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                    <div class="new-auto-item-desc">
                        <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                        <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                    </div>
                    <div class="new-auto-item-info clearfix">
                        <div class="on-left">
                            <div class="new-auto-item-price-new">1 019 900</div>
                            <div class="new-auto-item-price-old">1 320 000</div>
                        </div>
                        <div class="on-right">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>
                    </div>
                </div><!--
                --><div class="new-auto-item">
                    <div class="new-auto-item-image">
                        <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                    </div>
                    <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                    <div class="new-auto-item-desc">
                        <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                        <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                    </div>
                    <div class="new-auto-item-info clearfix">
                        <div class="on-left">
                            <div class="new-auto-item-price-new">1 019 900</div>
                            <div class="new-auto-item-price-old">1 320 000</div>
                        </div>
                        <div class="on-right">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>
                    </div>
                </div><!--
            --></div>

            <div class="bottom-part clearfix">
                <div class="on-right"><a href="#">Смотреть все</a></div>
            </div>

        </div>
    </div><!-- /Cars -->

    <!-- Tabs Container -->
    <div class="section-tabs">
        <div class="tabs-navigation section-gray section-sec">
            <div class="box-container">
                <ul class="tabs five-tabs clearfix">
                    <li class="active"><a class="animate-custom" data-block="#description">Описание</a></li>
                    <li><a class="animate-custom" data-block="#characteristics">Характеристики</a></li>
                    <li><a class="animate-custom" data-block="#picking">Коплектации и цены</a></li>
                    <li><a class="animate-custom" data-block="#reviews">Отзывы</a></li>
                    <li><a class="animate-custom" data-block="#stock">Акции</a></li>
                </ul>
            </div>
        </div>

        <div id="description" class="section tab-container section-sec">
            <div class="box-container">
                <div class="description-items"><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/1.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>Седан КИА Оптима бизнес-класса был представлен публике на Международном автосалоне в Нью-Йорке и стал воплощением инновационного подхода компании KIA Motors к производству новых автомобилей. KIA Optima создан совместными усилиями немецких и американских конструкторских центров для замены KIA Magentis. Конструкторам удалось достичь оптимального баланса габаритов и пропорций благодаря внедрению универсальной платформы.</p>
                        </div>
                    </div><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/2.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>КИА Оптима 2013-2014 года отличается превосходной динамикой благодаря использованию линейки новых силовых агрегатов — одновременно мощных и экономичных. Так, вы можете купить KIA Optima с бензиновым двигателем объемом 2,0 л и 2,4 л. Что касается трансмиссий, то автомобиль КИА Оптима комплектуется шестиступенчатой автоматической или механической коробкой передач. К слову, «автомат» оснащен системой ручного перехода с передачи на передачу Sportmatic.</p>
                        </div>
                    </div><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/3.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>Всем внешним видом KIA Optima производители словно хотели сказать: это динамичный и современный автомобиль для человека, который не стоит на месте. Агрессивная решетка радиатора, большие боковые фары заходящие на бока авто, широкий кузов — перед нами отличный представитель нового поколения автомобилей КИА, продажа которых с каждым годом становится все более популярной. Профиль кузова Киа Оптима очерчен хромированной дугой, продолжающейся от передних стоек к задним. </p>
                        </div>
                    </div><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/4.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>В интерьере КИА Оптима все продумано для комфортной езды и не вызывает сомнений в том, что это бизнес-класс. Во всех комплектациях авто покупателям доступны лепестковые переключатели скоростей, разместившиеся за рулевым колесом. В базовой версии можно встретить мультиформатную аудиосистему с восемью динамиками и широкими возможностями подключения внешних устройств. Просторность салона достигается не только благодаря обилию свободного места, но и визуально, при помощи панорамного люка из солнцезащитного стекла.</p>
                        </div>
                    </div><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/5.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>Конструкторы KIA Optima не забыли и о безопасности. В базовой комплектации автомобиль оснащен шестью подушками, активными подголовниками передних сидений, системой крепления детского кресла и противоударными балками в дверях. В отдельных версиях можно встретить ABS, электронную систему стабилизации движения, систему помощи при экстренном торможении и многие другие полезные опции.</p>
                        </div>
                    </div><!--
                    --><div class="description-item">
                        <div class="description-item-image bg" style="background-image: url(content/models/desc/6.jpg)"></div>
                        <div class="description-item-info align-justify">
                            <p>Купить KIA Optima New вы можете в автосалонах FAVORIT MOTORS. Мы предлагаем большое разнообразие комплектаций и цветовых решений, а также выгодные программы кредитования. Новый автомобиль КИА Оптима в салонах FAVORIT MOTORS — всегда выгодная покупка! </p>
                        </div>
                    </div><!--
                --></div>

                <div class="bottom-part clearfix">
                    <div class="pdf-link on-right clearfix align-left">
                        <div class="on-left">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                        <div class="on-left">
                            <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                            <div class="pdf-link-format">формат PDF</div>
                        </div>
                    </div>
                </div>

                <div class="bottom-part clearfix">
                    <div class="on-center"><a href="#" class="red-big-link animate-custom">Задать вопрос</a></div>
                </div>

            </div>
        </div>

        <div id="characteristics" class="section tab-container section-sec">
            <div class="box-container">

                <div class="characteristics-navigation clearfix">
                    <span class="on-left">Кузов: </span>
                    <ul class="on-left clearfix">
                        <li class="on-left active"><a data-block="#sedan">седан</a></li>
                        <li class="on-left"><a data-block="#hatchback">хэчбэк</a></li>
                        <li class="on-left"><a data-block="#wagon">универсал</a></li>
                    </ul>
                </div>

                <div class="char-tabs-container" id="sedan">

                    <div class="body-arrangement bg" style="background-image: url(content/models/schema.jpg);"></div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип топлива</td>
                                        <td colspan="2">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td class="border-right">2.0 NU CVVL MPI</td>
                                        <td>2.4 Theta CVVT MPI</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Кол-во/расположение цилиндров</td>
                                        <td colspan="2">4, рядное</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Рабочий объем, см3</td>
                                        <td class="border-right">2000</td>
                                        <td>2400</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество клапанов</td>
                                        <td colspan="2">16</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                        <td class="border-right">150 (6500)</td>
                                        <td>180 (6000)</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                        <td class="border-right">196 @ 4800</td>
                                        <td>231 @ 4000</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Требования к топливу</td>
                                        <td colspan="2">бензин RON95</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">MT</td>
                                        <td>AT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип привода</td>
                                        <td colspan="2">передний</td>
                                    </tr>

                                    <tr class="border-bottom">
                                        <td class="border-right">Количество передач</td>
                                        <td colspan="2">6</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип кузова</td>
                                        <td colspan="3">седан</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество дверей/мест</td>
                                        <td colspan="3">4/5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                        <td colspan="3">4845 / 1830 / 1455</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колесная база, мм</td>
                                        <td colspan="3">2795</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колея (передняя, задняя), мм</td>
                                        <td colspan="3">1591-1601 / 1591-1601</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Дорожный просвет, мм</td>
                                        <td colspan="3">145</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём багажника, л (VDA)</td>
                                        <td colspan="3">505</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подвеска (передняя/задняя)</td>
                                        <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормоза (передние/задние)</td>
                                        <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                        <td class="border-right">1423/1535</td>
                                        <td class="border-right">1450/1550</td>
                                        <td>480/1580</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Динамические характеристики</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 0-100 км/ч, c</td>
                                        <td class="border-right">9.5</td>
                                        <td class="border-right">10.6</td>
                                        <td>9.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 60-100 км/ч, c</td>
                                        <td class="border-right">9.8</td>
                                        <td class="border-right">5.7</td>
                                        <td>5.4</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная скорость, км/ч</td>
                                        <td class="border-right">210</td>
                                        <td class="border-right">208</td>
                                        <td>210</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                        <td colspan="3">35,8</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Расход топлива*</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём топливного бака, л</td>
                                        <td colspan="3">70</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Город, л/100км</td>
                                        <td class="border-right">9.3</td>
                                        <td class="border-right">10.3</td>
                                        <td>11.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Трасса, л/100км</td>
                                        <td class="border-right">5.6</td>
                                        <td class="border-right">6.1</td>
                                        <td>6.2</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Смешанный, л/100км</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right">7.6</td>
                                        <td>8.1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-body">
                            <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                            <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Кузов</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                            <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="char-tabs-container" id="hatchback">

                    <div class="body-arrangement bg" style="background-image: url(content/models/schema.jpg);"></div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип топлива</td>
                                    <td colspan="2">бензиновый</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип двигателя</td>
                                    <td class="border-right">2.0 NU CVVL MPI</td>
                                    <td>2.4 Theta CVVT MPI</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Кол-во/расположение цилиндров</td>
                                    <td colspan="2">4, рядное</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Рабочий объем, см3</td>
                                    <td class="border-right">2000</td>
                                    <td>2400</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Количество клапанов</td>
                                    <td colspan="2">16</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                    <td class="border-right">150 (6500)</td>
                                    <td>180 (6000)</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                    <td class="border-right">196 @ 4800</td>
                                    <td>231 @ 4000</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Требования к топливу</td>
                                    <td colspan="2">бензин RON95</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип трансмиссии</td>
                                    <td class="border-right">MT</td>
                                    <td>AT</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип привода</td>
                                    <td colspan="2">передний</td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="border-right">Количество передач</td>
                                    <td colspan="2">6</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип кузова</td>
                                    <td colspan="3">седан</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Количество дверей/мест</td>
                                    <td colspan="3">4/5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                    <td colspan="3">4845 / 1830 / 1455</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Колесная база, мм</td>
                                    <td colspan="3">2795</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Колея (передняя, задняя), мм</td>
                                    <td colspan="3">1591-1601 / 1591-1601</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Дорожный просвет, мм</td>
                                    <td colspan="3">145</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Объём багажника, л (VDA)</td>
                                    <td colspan="3">505</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Подвеска (передняя/задняя)</td>
                                    <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тормоза (передние/задние)</td>
                                    <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                    <td class="border-right">1423/1535</td>
                                    <td class="border-right">1450/1550</td>
                                    <td>480/1580</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Динамические характеристики</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Разгон 0-100 км/ч, c</td>
                                    <td class="border-right">9.5</td>
                                    <td class="border-right">10.6</td>
                                    <td>9.5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Разгон 60-100 км/ч, c</td>
                                    <td class="border-right">9.8</td>
                                    <td class="border-right">5.7</td>
                                    <td>5.4</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальная скорость, км/ч</td>
                                    <td class="border-right">210</td>
                                    <td class="border-right">208</td>
                                    <td>210</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                    <td colspan="3">35,8</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Расход топлива*</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Объём топливного бака, л</td>
                                    <td colspan="3">70</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Город, л/100км</td>
                                    <td class="border-right">9.3</td>
                                    <td class="border-right">10.3</td>
                                    <td>11.5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Трасса, л/100км</td>
                                    <td class="border-right">5.6</td>
                                    <td class="border-right">6.1</td>
                                    <td>6.2</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Смешанный, л/100км</td>
                                    <td class="border-right">7.0</td>
                                    <td class="border-right">7.6</td>
                                    <td>8.1</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-body">
                            <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                            <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Кузов</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                            <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="char-tabs-container" id="wagon">

                    <div class="body-arrangement bg" style="background-image: url(content/models/schema.jpg);"></div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип топлива</td>
                                    <td colspan="2">бензиновый</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип двигателя</td>
                                    <td class="border-right">2.0 NU CVVL MPI</td>
                                    <td>2.4 Theta CVVT MPI</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Кол-во/расположение цилиндров</td>
                                    <td colspan="2">4, рядное</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Рабочий объем, см3</td>
                                    <td class="border-right">2000</td>
                                    <td>2400</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Количество клапанов</td>
                                    <td colspan="2">16</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                    <td class="border-right">150 (6500)</td>
                                    <td>180 (6000)</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                    <td class="border-right">196 @ 4800</td>
                                    <td>231 @ 4000</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Требования к топливу</td>
                                    <td colspan="2">бензин RON95</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип трансмиссии</td>
                                    <td class="border-right">MT</td>
                                    <td>AT</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип привода</td>
                                    <td colspan="2">передний</td>
                                </tr>

                                <tr class="border-bottom">
                                    <td class="border-right">Количество передач</td>
                                    <td colspan="2">6</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип кузова</td>
                                    <td colspan="3">седан</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Количество дверей/мест</td>
                                    <td colspan="3">4/5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                    <td colspan="3">4845 / 1830 / 1455</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Колесная база, мм</td>
                                    <td colspan="3">2795</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Колея (передняя, задняя), мм</td>
                                    <td colspan="3">1591-1601 / 1591-1601</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Дорожный просвет, мм</td>
                                    <td colspan="3">145</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Объём багажника, л (VDA)</td>
                                    <td colspan="3">505</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Подвеска (передняя/задняя)</td>
                                    <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тормоза (передние/задние)</td>
                                    <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                    <td class="border-right">1423/1535</td>
                                    <td class="border-right">1450/1550</td>
                                    <td>480/1580</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Динамические характеристики</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Разгон 0-100 км/ч, c</td>
                                    <td class="border-right">9.5</td>
                                    <td class="border-right">10.6</td>
                                    <td>9.5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Разгон 60-100 км/ч, c</td>
                                    <td class="border-right">9.8</td>
                                    <td class="border-right">5.7</td>
                                    <td>5.4</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Максимальная скорость, км/ч</td>
                                    <td class="border-right">210</td>
                                    <td class="border-right">208</td>
                                    <td>210</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                    <td colspan="3">35,8</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Расход топлива*</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                    <col width="20%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Объём топливного бака, л</td>
                                    <td colspan="3">70</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Город, л/100км</td>
                                    <td class="border-right">9.3</td>
                                    <td class="border-right">10.3</td>
                                    <td>11.5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Трасса, л/100км</td>
                                    <td class="border-right">5.6</td>
                                    <td class="border-right">6.1</td>
                                    <td>6.2</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Смешанный, л/100км</td>
                                    <td class="border-right">7.0</td>
                                    <td class="border-right">7.6</td>
                                    <td>8.1</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-body">
                            <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Двигатель</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                            <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Трансмиссия</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-title">
                            <h2>Кузов</h2>
                        </div>
                        <div class="text-body">
                            <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                            <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div id="picking" class="section tab-container section-sec">
            <div class="box-container">

                <div class="title">
                    <h2>Стандартное оборудование</h2>
                </div>

                <div class="standard-equipment-items">

                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Экстерьер</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <li>Полноразмерное легкосплавное запасное колесо</li>
                                <li>Cтеклоочистители "Aero blade"</li>
                                <li>Омыватель фар</li>
                                <li>Поворотный свет</li>
                                <li>Светодиодные дневные ходовые огни (LED DRL)</li>
                                <li>Дополнительный стоп-сигнал</li>
                                <li>Электропривод складывания зеркал заднего вида и повторители указателя поворота</li>
                            </ul>
                        </div>
                    </div>

                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Интерьер</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <li>Задние сиденья со спинками, складывающимися в соотношении 60/40</li>
                                <li>Ручки дверей с отделкой хромом</li>
                                <li>Ящик в переднем центральном подлокотнике</li>
                                <li>Задний подлокотник с подстаканниками</li>
                                <li>Охлаждаемый перчаточный ящик</li>
                                <li>Прикуриватель и пепельница</li>
                                <li>Сетка для крепления багажа</li>
                            </ul>
                        </div>
                    </div>

                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Безопасность</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <li>Фронтальные подушки безопасности</li>
                                <li>Боковые подушки и шторки безопасности</li>
                                <li>Активные подголовники передних сидений</li>
                                <li>Антиблокировочная система тормозов (ABS)</li>
                                <li>Система предупреждения об экстренном торможении (ESS)</li>
                                <li>Автоматическое запирание дверей при движении</li>
                                <li>Крепление для детского кресла ISOFIX</li>
                            </ul>
                        </div>
                    </div>

                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Комфорт</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <li>Круиз-контроль</li>
                                <li>Адаптивные спортивные амортизаторы переменной жёсткости</li>
                                <li>Функция "Эскорт" (Задержка выключения фар при закрывании замков)</li>
                                <li>Передние и задние стеклоподъёмники с электроприводом и функцией Auto</li>
                                <li>Раздельный климат-контроль</li>
                                <li>Мультифункциональное рулевое колесо</li>
                                <li>Регулировка рулевой колонки по высоте и по вылету</li>
                                <li>Поясничный подпор водительского сиденья с электроприводом регулирово</li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="table-element">
                    <div class="table-title border-bottom">
                        <h2>Различающиеся опции</h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Объем и мощность двигателя (л / л.с.)</td>
                                    <td class="border-right" colspan="4">2.0 / 150</td>
                                    <td colspan="3">2.4 / 180</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип двигателя</td>
                                    <td colspan="7">бензиновый</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Тип трансмиссии</td>
                                    <td class="border-right">6AT</td>
                                    <td colspan="6">6MT</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Ускорение 0-100 км/ч (сек.)</td>
                                    <td class="border-right">10.5</td>
                                    <td class="border-right" colspan="3">9.6</td>
                                    <td colspan="3">10.5</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Расход топлива комбинированный (л/100 км) ***</td>
                                    <td class="border-right">7.0</td>
                                    <td class="border-right" colspan="3">7.6</td>
                                    <td colspan="3">8.1</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Название комплектации</td>
                                    <td class="border-right" colspan="2">Comfort</td>
                                    <td class="border-right">Luxe</td>
                                    <td class="border-right">Prestige</td>
                                    <td class="border-right">Luxe</td>
                                    <td class="border-right">Prestige</td>
                                    <td>Premium</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">Код модели</td>
                                    <td class="border-right">TFS42G617</td>
                                    <td class="border-right" colspan="3">TFS42G61F</td>
                                    <td colspan="3">TFS4L661F</td>
                                </tr>
                                <tr class="border-bottom">
                                    <td class="border-right">OCN</td>
                                    <td class="border-right" colspan="2">D565</td>
                                    <td class="border-right">GI73</td>
                                    <td class="border-right">GJS9/GK26</td>
                                    <td class="border-right">GI77</td>
                                    <td class="border-right">GJT0/GK27</td>
                                    <td>GJT1/GK28</td>
                                </tr>
                                <tr class="border-bottom strong">
                                    <td class="border-right">Максимальная цена перепродажи на автомобили 2015 производственного года*</td>
                                    <td class="border-right">1 049 900</td>
                                    <td class="border-right">1 099 900</td>
                                    <td class="border-right">1 189 900</td>
                                    <td class="border-right">1 299 900</td>
                                    <td class="border-right">1 249 900</td>
                                    <td class="border-right">1 359 900</td>
                                    <td>1 459 900</td>
                                </tr>
                                <tr class="border-bottom strong">
                                    <td class="border-right">Максимальная цена перепродажи на автомобили 2014 производственного года*</td>
                                    <td class="border-right">989 900</td>
                                    <td class="border-right">1 039 900</td>
                                    <td class="border-right">1 129 900</td>
                                    <td class="border-right">1 239 900</td>
                                    <td class="border-right">1 189 900</td>
                                    <td class="border-right">1 299 900</td>
                                    <td>1 399 900</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-element collapsed">
                    <div class="table-title animate-custom opacity-custom-hover">
                        <h2>Пакет <span class="red-text">«Теплые опции»</span></h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right">Подогрев Руля</td>
                                    <td class="border-right">-</td>
                                    <td class="border-right">-</td>
                                    <td class="border-right">-</td>
                                    <td class="border-right">+</td>
                                    <td class="border-right">+</td>
                                    <td class="border-right">+</td>
                                    <td>+</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-element collapsed">
                    <div class="table-title animate-custom opacity-custom-hover">
                        <h2>Экстерьер</h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                            <tr class="border-bottom">
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-element collapsed">
                    <div class="table-title animate-custom opacity-custom-hover">
                        <h2>Интерьер</h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                            <tr class="border-bottom">
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-element collapsed">
                    <div class="table-title animate-custom opacity-custom-hover">
                        <h2>Безопасность</h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                            <tr class="border-bottom">
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-element collapsed">
                    <div class="table-title animate-custom opacity-custom-hover">
                        <h2>Комфорт</h2>
                    </div>
                    <div class="table-body">
                        <table>
                            <colgroup>
                                <col width="30%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <tbody>
                            <tr class="border-bottom">
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td class="border-right"></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-element">
                    <div class="text-body">
                        <p>* Цены на продукцию KIA. Сведения о ценах, содержащиеся на сайте, носят исключительно информационный характер. Указанные цены могут отличаться от действительных цен уполномоченных дилеров KIA. Для получения подробной информации об актуальных ценах на продукцию KIA обращайтесь к уполномоченным дилерам KIA. Приобретение любой продукции KIA осуществляется в соответствии с условиями индивидуального договора купли-продажи. </p>
                        <p>* Prices for KIA products. Information about prices, placed at this web-site, has only informational purposes. The indicated prices may be different from actual prices of authorized KIA dealers. To receive detailed information on actual prices for KIA products please refer to authorized KIA dealers. Purchase of any of KIA products is made according to provisions of individual sale and purchase contracts.</p>
                        <p>*** Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                    </div>
                </div>

                <div class="text-element">
                    <div class="text-title">
                        <h2>Цены на KIA Optima 2014-2015</h2>
                    </div>
                    <div class="text-body">
                        <p>Новый седан бизнес-класса корейского производителя дебютировал на российском рынке в 2010 году. За прошедшее время Киа Оптима зарекомендовал себя как очень комфортный и представительный автомобиль. Богатое оснащение дополнительными опциями, мощные двигатели и высокий уровень безопасности — вот сильные стороны этой модели. Уже в базовой комплектации автомобиль оснащается раздельным климат-контролем, дистанционным управлением центральным замком и легкосплавными дисками. </p>
                        <p>Доступны два бензиновых двигателя (2,0 л и 2,4 л), на которые устанавливается механическая или автоматическая трансмиссия (6 ступеней). Механика доступна только в базовой комплектации, в остальных случаях устанавливается «автомат». Автомобиль поставляется в следующих комплектациях: Comfort, Luxe, Prestige и Premium. Они доступны как для базового двигателя (2,0 л), так и для самого мощного (2,4 л). Это позволяет водителям выбрать машину под свой темперамент и стиль вождения. Именно этого и ожидаешь от бизнес-класса — максимальный выбор и комфорт. Машина полностью соответствует своему классу. За безопасность отвечают антиблокировочная система, система курсовой устойчивости, интегрированная система активного управления, система помощи при трогании на подъеме, система контроля слепых зон, фронтальные и боковые подушки безопасности, ремни с преднатяжителями.</p>
                        <p>Комфорт обеспечивает современная аудиосистема с внешним сабвуфером и усилителем, система Drive Mode Select, приборная панель Supervision 4.3 дюйма и вентилируемые сидения. Получить более точную информацию о комплектациях и ценах на новый Киа Оптима 2014-2015 можно узнать из таблицы на данной странице. </p>
                    </div>
                </div>

                <div class="bottom-part clearfix">
                    <div class="pdf-link on-right clearfix align-left">
                        <div class="on-left">
                            <i class="fa fa-file-pdf-o"></i>
                        </div>
                        <div class="on-left">
                            <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                            <div class="pdf-link-format">формат PDF</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="reviews" class="section tab-container section-sec">

            <div class="reviews-slider-block">
                <div class="box-container">

                    <div class="reviews-slider-header opacity-custom"><i class="fa fa-pencil"></i></div>

                    <div class="reviews-slider swiper-container">

                        <div class="swiper-wrapper">

                            <div class="swiper-slide">

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                            </div>

                            <div class="swiper-slide">

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                            </div>

                            <div class="swiper-slide">

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                                <div class="reviews-slider-item">
                                    <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                    <div class="reviews-slider-item-text">
                                        <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                    </div>
                                    <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                </div>

                            </div>

                        </div>

                        <div class="swiper-pagination"></div>

                    </div>

                </div>
            </div>

            <div class="reviews-slider-form section section-gray section-sec">
                <div class="box-container">
                    <div class="title">
                        <h2>Оставить свой отзыв</h2>
                    </div>

                    <p class="gray-text">Для того, чтобы оставить отзыв, Вам необходимо  <a href="#" class="animate-custom red-text">Зарегистрироваться</a>  или войдите через: <a class="fb social-icon-link" href="#"><i class="fa fa-facebook animate-custom"></i></a> <a class="vk social-icon-link" href="#"><i class="fa fa-vk animate-custom"></i></a></p>

                    <form id="reviews-form" class="page-form">

                        <div class="form-item">
                            <label for="review-theme">Выбирите тему отзыва:</label>
                            <select id="review-theme">
                                <option>Продажа новых автомобилей</option>
                                <option>Ещё пункт 1</option>
                                <option>Ещё пункт 2</option>
                                <option>Ещё пункт 3</option>
                                <option>Ещё пункт 4</option>
                            </select>
                        </div>

                        <div class="form-item">
                            <label for="review-text">Текст Вашего сообщения:</label>
                            <textarea id="review-text"></textarea>
                        </div>

                        <div class="form-item clearfix">
                            <input type="submit" class="on-right animate-custom red-big-link" value="Оставить отзыв" />
                        </div>

                    </form>

                </div>
            </div>

            <div class="section section-sec"></div>

        </div>

        <div id="stock" class="section tab-container section-sec">
            <div class="box-container">

            </div>
        </div>

    </div><!-- /Tabs Container -->

</section><!-- /Content -->


<div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>" id="<? echo $arItemIDs['ID']; ?>">
<?
if ('Y' == $arParams['DISPLAY_NAME'])
{
?>
<div class="bx_item_title"><h1><span><?
    echo (
        isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
        ? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
        : $arResult["NAME"]
    ); ?>
</span></h1></div>
<?
}
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
    <div class="bx_item_container">
        <div class="bx_lt">
<div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
    <div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
    <div class="bx_bigimages_imgcontainer">
    <span class="bx_bigimages_aligner"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>"></span>
<?
if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
{
    if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
    {
        if (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF'])
        {
?>
    <div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>"><? echo -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
<?
        }
    }
    else
    {
?>
    <div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" style="display: none;"></div>
<?
    }
}
if ($arResult['LABEL'])
{
?>
    <div class="bx_stick average left top" id="<? echo $arItemIDs['STICKER_ID'] ?>" title="<? echo $arResult['LABEL_VALUE']; ?>"><? echo $arResult['LABEL_VALUE']; ?></div>
<?
}
?>
    </div>
    </div>
<?
if ($arResult['SHOW_SLIDER'])
{
    if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
    {
        if (5 < $arResult['MORE_PHOTO_COUNT'])
        {
            $strClass = 'bx_slider_conteiner full';
            $strOneWidth = (100/$arResult['MORE_PHOTO_COUNT']).'%';
            $strWidth = (20*$arResult['MORE_PHOTO_COUNT']).'%';
            $strSlideStyle = '';
        }
        else
        {
            $strClass = 'bx_slider_conteiner';
            $strOneWidth = '20%';
            $strWidth = '100%';
            $strSlideStyle = 'display: none;';
        }
?>
    <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
    <div class="bx_slider_scroller_container">
    <div class="bx_slide">
    <ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
<?
        foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
        {
?>
    <li data-value="<? echo $arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
        }
        unset($arOnePhoto);
?>
    </ul>
    </div>
    <div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
    <div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
    </div>
    </div>
<?
    }
    else
    {
        foreach ($arResult['OFFERS'] as $key => $arOneOffer)
        {
            if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
                continue;
            $strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
            if (5 < $arOneOffer['MORE_PHOTO_COUNT'])
            {
                $strClass = 'bx_slider_conteiner full';
                $strOneWidth = (100/$arOneOffer['MORE_PHOTO_COUNT']).'%';
                $strWidth = (20*$arOneOffer['MORE_PHOTO_COUNT']).'%';
                $strSlideStyle = '';
            }
            else
            {
                $strClass = 'bx_slider_conteiner';
                $strOneWidth = '20%';
                $strWidth = '100%';
                $strSlideStyle = 'display: none;';
            }
?>
    <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>" style="display: <? echo $strVisible; ?>;">
    <div class="bx_slider_scroller_container">
    <div class="bx_slide">
    <ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>">
<?
            foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto)
            {
?>
    <li data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
            }
            unset($arOnePhoto);
?>
    </ul>
    </div>
    <div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
    <div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
    </div>
    </div>
<?
        }
    }
}
?>
</div>
        </div>
        <div class="bx_rt">
<?
$useBrands = ('Y' == $arParams['BRAND_USE']);
$useVoteRating = ('Y' == $arParams['USE_VOTE_RATING']);
if ($useBrands || $useVoteRating)
{
?>
    <div class="bx_optionblock">
<?
    if ($useVoteRating)
    {
        ?><?$APPLICATION->IncludeComponent(
            "bitrix:iblock.vote",
            "stars",
            array(
                "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                "ELEMENT_ID" => $arResult['ID'],
                "ELEMENT_CODE" => "",
                "MAX_VOTE" => "5",
                "VOTE_NAMES" => array("1", "2", "3", "4", "5"),
                "SET_STATUS_404" => "N",
                "DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
                "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                "CACHE_TIME" => $arParams['CACHE_TIME']
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );?><?
    }
    if ($useBrands)
    {
        ?><?$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", ".default", array(
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "ELEMENT_ID" => $arResult['ID'],
            "ELEMENT_CODE" => "",
            "PROP_CODE" => $arParams['BRAND_PROP_CODE'],
            "CACHE_TYPE" => $arParams['CACHE_TYPE'],
            "CACHE_TIME" => $arParams['CACHE_TIME'],
            "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
            "WIDTH" => "",
            "HEIGHT" => ""
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );?><?
    }
?>
    </div>
<?
}
unset($useVoteRating, $useBrands);
?>
<div class="item_price">
<?
$boolDiscountShow = (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF']);
?>
    <div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? $arResult['MIN_PRICE']['PRINT_VALUE'] : ''); ?></div>
    <div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
    <div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? GetMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $arResult['MIN_PRICE']['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
</div>
<?
if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
{
?>
<div class="item_info_section">
<?
    if (!empty($arResult['DISPLAY_PROPERTIES']))
    {
?>
    <dl>
<?
        foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
        {
?>
        <dt><? echo $arOneProp['NAME']; ?></dt><dd><?
            echo (
                is_array($arOneProp['DISPLAY_VALUE'])
                ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                : $arOneProp['DISPLAY_VALUE']
            ); ?></dd><?
        }
        unset($arOneProp);
?>
    </dl>
<?
    }
    if ($arResult['SHOW_OFFERS_PROPS'])
    {
?>
    <dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl>
<?
    }
?>
</div>
<?
}
if ('' != $arResult['PREVIEW_TEXT'])
{
    if (
        'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
        || ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
    )
    {
?>
<div class="item_info_section">
<?
        echo ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>');
?>
</div>
<?
    }
}
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
    $arSkuProps = array();
?>
<div class="item_info_section" style="padding-right:150px;" id="<? echo $arItemIDs['PROP_DIV']; ?>">
<?
    foreach ($arResult['SKU_PROPS'] as &$arProp)
    {
        if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
            continue;
        $arSkuProps[] = array(
            'ID' => $arProp['ID'],
            'SHOW_MODE' => $arProp['SHOW_MODE'],
            'VALUES_COUNT' => $arProp['VALUES_COUNT']
        );
        if ('TEXT' == $arProp['SHOW_MODE'])
        {
            if (5 < $arProp['VALUES_COUNT'])
            {
                $strClass = 'bx_item_detail_size full';
                $strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
                $strWidth = (20*$arProp['VALUES_COUNT']).'%';
                $strSlideStyle = '';
            }
            else
            {
                $strClass = 'bx_item_detail_size';
                $strOneWidth = '20%';
                $strWidth = '100%';
                $strSlideStyle = 'display: none;';
            }
?>
    <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
        <span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
        <div class="bx_size_scroller_container"><div class="bx_size">
            <ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
            foreach ($arProp['VALUES'] as $arOneValue)
            {
                $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; display: none;">
<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
<?
            }
?>
            </ul>
            </div>
            <div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
            <div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
        </div>
    </div>
<?
        }
        elseif ('PICT' == $arProp['SHOW_MODE'])
        {
            if (5 < $arProp['VALUES_COUNT'])
            {
                $strClass = 'bx_item_detail_scu full';
                $strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
                $strWidth = (20*$arProp['VALUES_COUNT']).'%';
                $strSlideStyle = '';
            }
            else
            {
                $strClass = 'bx_item_detail_scu';
                $strOneWidth = '20%';
                $strWidth = '100%';
                $strSlideStyle = 'display: none;';
            }
?>
    <div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
        <span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
        <div class="bx_scu_scroller_container"><div class="bx_scu">
            <ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
            foreach ($arProp['VALUES'] as $arOneValue)
            {
                $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>; display: none;" >
<i title="<? echo $arOneValue['NAME']; ?>"></i>
<span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo $arOneValue['NAME']; ?>"></span></span></li>
<?
            }
?>
            </ul>
            </div>
            <div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
            <div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
        </div>
    </div>
<?
        }
    }
    unset($arProp);
?>
</div>
<?
}
?>
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
    $canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
}
else
{
    $canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
{
    if ($arParams['SHOW_BASIS_PRICE'] == 'Y')
    {
        $basisPriceInfo = array(
            '#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
            '#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
        );
?>
        <p id="<? echo $arItemIDs['BASIS_PRICE']; ?>" class="item_section_name_gray"><? echo GetMessage('CT_BCE_CATALOG_MESS_BASIS_PRICE', $basisPriceInfo); ?></p>
<?
    }
?>
    <span class="item_section_name_gray"><? echo GetMessage('CATALOG_QUANTITY'); ?></span>
    <div class="item_buttons vam">
        <span class="item_buttons_counter_block">
            <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
            <input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
                    ? 1
                    : $arResult['CATALOG_MEASURE_RATIO']
                ); ?>">
            <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
            <span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
        </span>
        <span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
    if ($showBuyBtn)
    {
?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
    }
    if ($showAddBtn)
    {
?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
    }
?>
        </span>
        <span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
    if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
    {
?>
        <span class="item_buttons_counter_block">
<?
        if ($arParams['DISPLAY_COMPARE'])
        {
?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
<?
        }
        if ($showSubscribeBtn)
        {

        }
?>
        </span>
<?
    }
?>
    </div>
<?
    if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
    {
        if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
        {
?>
    <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
<?
        }
        else
        {
            if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
            {
?>
    <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
<?
            }
        }
    }
}
else
{
?>
    <div class="item_buttons vam">
        <span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
    if ($showBuyBtn)
    {
?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
    }
    if ($showAddBtn)
    {
?>
        <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
    }
?>
        </span>
        <span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
    </div>
<?
}
unset($showAddBtn, $showBuyBtn);
?>
</div>
            <div class="clb"></div>
        </div>

        <div class="bx_md">
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
    if ($arResult['OFFER_GROUP'])
    {
        foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
        {
?>
    <span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
    ".default",
    array(
        "IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
        "ELEMENT_ID" => $offerID,
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
        "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
        "CURRENCY_ID" => $arParams["CURRENCY_ID"]
    ),
    $component,
    array("HIDE_ICONS" => "Y")
);?><?
?>
    </span>
<?
        }
    }
}
else
{
    if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
    {
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
    ".default",
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_ID" => $arResult["ID"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
        "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
        "CURRENCY_ID" => $arParams["CURRENCY_ID"]
    ),
    $component,
    array("HIDE_ICONS" => "Y")
);?><?
    }
}
?>
</div>
        </div>
        <div class="bx_rb">
<div class="item_info_section">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
    <div class="bx_item_description">
        <div class="bx_item_section_name_gray" style="border-bottom: 1px solid #f2f2f2;"><? echo GetMessage('FULL_DESCRIPTION'); ?></div>
<?
    if ('html' == $arResult['DETAIL_TEXT_TYPE'])
    {
        echo $arResult['DETAIL_TEXT'];
    }
    else
    {
        ?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
    }
?>
    </div>
<?
}
?>
</div>
        </div>
        <div class="bx_lb">
<div class="tac ovh">
</div>
<div class="tab-section-container">
<?
if ('Y' == $arParams['USE_COMMENTS'])
{
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.comments",
    "",
    array(
        "ELEMENT_ID" => $arResult['ID'],
        "ELEMENT_CODE" => "",
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "URL_TO_COMMENT" => "",
        "WIDTH" => "",
        "COMMENTS_COUNT" => "5",
        "BLOG_USE" => $arParams['BLOG_USE'],
        "FB_USE" => $arParams['FB_USE'],
        "FB_APP_ID" => $arParams['FB_APP_ID'],
        "VK_USE" => $arParams['VK_USE'],
        "VK_API_ID" => $arParams['VK_API_ID'],
        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
        "CACHE_TIME" => $arParams['CACHE_TIME'],
        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
        "BLOG_TITLE" => "",
        "BLOG_URL" => $arParams['BLOG_URL'],
        "PATH_TO_SMILE" => "",
        "EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
        "AJAX_POST" => "Y",
        "SHOW_SPAM" => "Y",
        "SHOW_RATING" => "N",
        "FB_TITLE" => "",
        "FB_USER_ADMIN_ID" => "",
        "FB_COLORSCHEME" => "light",
        "FB_ORDER_BY" => "reverse_time",
        "VK_TITLE" => "",
        "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
    ),
    $component,
    array("HIDE_ICONS" => "Y")
);?>
<?
}
?>
</div>
        </div>
            <div style="clear: both;"></div>
    </div>
    <div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
    foreach ($arResult['JS_OFFERS'] as &$arOneJS)
    {
        if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
        {
            $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
            $arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
        }
        $strProps = '';
        if ($arResult['SHOW_OFFERS_PROPS'])
        {
            if (!empty($arOneJS['DISPLAY_PROPERTIES']))
            {
                foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
                {
                    $strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
                        is_array($arOneProp['VALUE'])
                        ? implode(' / ', $arOneProp['VALUE'])
                        : $arOneProp['VALUE']
                    ).'</dd>';
                }
            }
        }
        $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
    }
    if (isset($arOneJS))
        unset($arOneJS);
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'NAME' => $arResult['~NAME']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $arSkuProps
    );
    if ($arParams['DISPLAY_COMPARE'])
    {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
}
else
{
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
    {
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
        if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
        {
            foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
            {
?>
    <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
                if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                    unset($arResult['PRODUCT_PROPERTIES'][$propID]);
            }
        }
        $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
        if (!$emptyProductProperties)
        {
?>
    <table>
<?
            foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
            {
?>
    <tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
    <td>
<?
                if(
                    'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
                    && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
                )
                {
                    foreach($propInfo['VALUES'] as $valueID => $value)
                    {
                        ?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
                    }
                }
                else
                {
                    ?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                    foreach($propInfo['VALUES'] as $valueID => $value)
                    {
                        ?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
                    }
                    ?></select><?
                }
?>
    </td></tr>
<?
            }
?>
    </table>
<?
        }
?>
</div>
<?
    }
    if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
    {
        $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
        $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
    }
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
        ),
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'PICT' => $arFirstPhoto,
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'PRICE' => $arResult['MIN_PRICE'],
            'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
        ),
        'BASKET' => array(
            'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    if ($arParams['DISPLAY_COMPARE'])
    {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
    unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
    ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
    BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
    TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
    TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
    BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
    BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
    BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
    BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
    BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
    TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
    COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
    COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
    COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
    BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
    SITE_ID: '<? echo SITE_ID; ?>'
});
</script>