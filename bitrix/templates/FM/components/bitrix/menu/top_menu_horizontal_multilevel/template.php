<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<ul class="nav<?if($arParams["ROOT_MENU_TYPE"]=="stocks"):?> stocks<?endif;?>">
	<?$i = 1;?>
	<?$previousLevel = 0;?>
	<?foreach($arResult as $arItem):?>
		<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?endif?>
		<?if ($arItem["IS_PARENT"]):?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="dropdown"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>active dropdown-toggle<?else:?>dropdown-toggle<?endif?>"><?=$arItem["TEXT"]?></a>
					<ul class="dropdown-menu">
			<?else:?>
				<li class="dropdown-submenu <?if ($arItem["SELECTED"]):?>active<?endif?>" data-submenu-id="<?=$i?>" data-submenuId="<?=$i?>"><a href="<?=$arItem["LINK"]?>" class="dropdown-toggle"><?=$arItem["TEXT"]?><span class="arrow"></span></a>
					<ul class="dropdown-menu">
			<?endif?>
		<?else:?>
			<?if ($arItem["PERMISSION"] > "D"):?>
				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>active<?endif?>"><?=$arItem["TEXT"]?></a></li>
				<?else:?>
					<li<?if ($arItem["SELECTED"]):?> class="active"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?endif?>
			<?else:?>
				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
				<?else:?>
					<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
				<?endif?>
			<?endif?>
		<?endif?>
		<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
		<?$i++;?>
	<?endforeach?>
	<?if ($previousLevel > 1)://close last item tags?>
		<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
	<?endif?>
	</ul>
<?endif?>