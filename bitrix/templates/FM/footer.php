


<?if(($curDir != '/catalog/') && ($curDir != '/models/')):?>

      </div>
      
      <footer>
        <div class="top">
          <div class="container">
            <div class="footer-nav">
              <h3>
                  <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                   "AREA_FILE_SHOW" => "file",
                   "PATH" => SITE_TEMPLATE_PATH."/include/footer_nav_company.php",
                   "EDIT_TEMPLATE" => ""
                   ),
                   false
                );?>
              </h3>
              <?$APPLICATION->IncludeComponent(
                "bitrix:menu", 
                "footer_nav", 
                array(
                  "COMPONENT_TEMPLATE" => ".default",
                  "ROOT_MENU_TYPE" => "company",
                  "MENU_CACHE_TYPE" => "N",
                  "MENU_CACHE_TIME" => "3600",
                  "MENU_CACHE_USE_GROUPS" => "Y",
                  "MENU_CACHE_GET_VARS" => array(
                  ),
                  "MAX_LEVEL" => "1",
                  "CHILD_MENU_TYPE" => "company",
                  "USE_EXT" => "Y",
                  "DELAY" => "N",
                  "ALLOW_MULTI_SELECT" => "N"
                ),
                false
              );?>
            </div>
            <div class="footer-nav">
              <h3>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                   "AREA_FILE_SHOW" => "file",
                   "PATH" => SITE_TEMPLATE_PATH."/include/footer_nav_brands.php",
                   "EDIT_TEMPLATE" => ""
                   ),
                   false
                );?>
              </h3>
              <?$APPLICATION->IncludeComponent(
                "bitrix:menu", 
                "footer_nav", 
                array(
                  "COMPONENT_TEMPLATE" => ".default",
                  "ROOT_MENU_TYPE" => "brands",
                  "MENU_CACHE_TYPE" => "N",
                  "MENU_CACHE_TIME" => "3600",
                  "MENU_CACHE_USE_GROUPS" => "Y",
                  "MENU_CACHE_GET_VARS" => array(
                  ),
                  "MAX_LEVEL" => "1",
                  "CHILD_MENU_TYPE" => "brands",
                  "USE_EXT" => "Y",
                  "DELAY" => "N",
                  "ALLOW_MULTI_SELECT" => "N"
                ),
                false
              );?>
            </div>

            <div class="footer-nav">
              <h3>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                   "AREA_FILE_SHOW" => "file",
                   "PATH" => SITE_TEMPLATE_PATH."/include/footer_nav_clients.php",
                   "EDIT_TEMPLATE" => ""
                   ),
                   false
                );?>
              </h3>
              <?$APPLICATION->IncludeComponent(
                "bitrix:menu", 
                "footer_nav", 
                array(
                  "COMPONENT_TEMPLATE" => ".default",
                  "ROOT_MENU_TYPE" => "clients",
                  "MENU_CACHE_TYPE" => "N",
                  "MENU_CACHE_TIME" => "3600",
                  "MENU_CACHE_USE_GROUPS" => "Y",
                  "MENU_CACHE_GET_VARS" => array(
                  ),
                  "MAX_LEVEL" => "1",
                  "CHILD_MENU_TYPE" => "clients",
                  "USE_EXT" => "Y",
                  "DELAY" => "N",
                  "ALLOW_MULTI_SELECT" => "N"
                ),
                false
              );?>
            </div>

            <div class="footer-nav addresses">
              <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                 "AREA_FILE_SHOW" => "file",
                 "PATH" => SITE_TEMPLATE_PATH."/include/addresses.php",
                 "EDIT_TEMPLATE" => ""
                 ),
                 false
              );?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="middle">
          <div class="container">
            <div class="middle-center">
              <div class="padding-center">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                  "AREA_FILE_SHOW" => "file",
                  "PATH" => SITE_TEMPLATE_PATH."/include/footer_mobile.php",
                  "EDIT_TEMPLATE" => ""
                  ),
                  false
                );?>
              </div>
            </div>
            <div class="middle-left">
              <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH."/include/footer_social.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
              );?>
            </div>
            <div class="middle-right">
              <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_TEMPLATE_PATH."/include/footer_rss.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
              );?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="bottom">
          <div class="container">
            <div class="bottom-center">
              <div class="padding-center">
                 <span>
                   <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                   "AREA_FILE_SHOW" => "file",
                   "PATH" => SITE_TEMPLATE_PATH."/include/footer_favorit_motors.php",
                   "EDIT_TEMPLATE" => ""
                   ),
                   false
                );?>
              </span>
              <span class="current_year">2015</span>
              </div>
            </div>
            <div class="bottom-left">
               <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                 "AREA_FILE_SHOW" => "file",
                 "PATH" => SITE_TEMPLATE_PATH."/include/footer_copyrights.php",
                 "EDIT_TEMPLATE" => ""
                 ),
                 false
              );?>
            </div>
            <div class="bottom-right">
               <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                 "AREA_FILE_SHOW" => "file",
                 "PATH" => SITE_TEMPLATE_PATH."/include/footer_development.php",
                 "EDIT_TEMPLATE" => ""
                 ),
                 false
              );?>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </footer>

<?if($catalog_stock):?>
<div style="display:none;">
  <?$APPLICATION->IncludeComponent(
  	"bitrix:catalog.viewed.products", 
  	"catalog.viewed.products", 
  	array(
  		"COMPONENT_TEMPLATE" => "vertical",
  		"IBLOCK_TYPE" => "catalog",
  		"IBLOCK_ID" => "13",
  		"SHOW_FROM_SECTION" => "N",
  		"HIDE_NOT_AVAILABLE" => "N",
  		"SHOW_DISCOUNT_PERCENT" => "Y",
  		"PRODUCT_SUBSCRIPTION" => "N",
  		"SHOW_NAME" => "Y",
  		"SHOW_IMAGE" => "Y",
  		"MESS_BTN_BUY" => "Купить",
  		"MESS_BTN_DETAIL" => "Подробнее",
  		"MESS_BTN_SUBSCRIBE" => "Подписаться",
  		"PAGE_ELEMENT_COUNT" => "5",
  		"LINE_ELEMENT_COUNT" => "3",
  		"TEMPLATE_THEME" => "blue",
  		"DETAIL_URL" => "",
  		"CACHE_TYPE" => "A",
  		"CACHE_TIME" => "36000000",
  		"CACHE_GROUPS" => "Y",
  		"SHOW_OLD_PRICE" => "N",
  		"PRICE_CODE" => array(
  		),
  		"SHOW_PRICE_COUNT" => "1",
  		"PRICE_VAT_INCLUDE" => "Y",
  		"CONVERT_CURRENCY" => "N",
  		"BASKET_URL" => "/personal/basket.php",
  		"ACTION_VARIABLE" => "action",
  		"PRODUCT_ID_VARIABLE" => "id",
  		"ADD_PROPERTIES_TO_BASKET" => "Y",
  		"PRODUCT_PROPS_VARIABLE" => "prop",
  		"PARTIAL_PRODUCT_PROPERTIES" => "N",
  		"USE_PRODUCT_QUANTITY" => "N",
  		"SHOW_PRODUCTS_13" => "Y",
  		"SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
  		"SECTION_CODE" => "",
  		"SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
  		"SECTION_ELEMENT_CODE" => "",
  		"DEPTH" => "2",
  		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
  		"PROPERTY_CODE_13" => array(
  			0 => "PRICE",
			1 => "CARBRAND",
			3 => "CARMODEL"
  		),
  		"CART_PROPERTIES_13" => array(
  			0 => ""
  		),
  		"ADDITIONAL_PICT_PROP_13" => "",
  		"LABEL_PROP_13" => "-"
  	),
  	false
  );?>
</div>
  <?endif;?>


    </div> <!-- wrapper --> 

<?endif?>
  </body>
</html>