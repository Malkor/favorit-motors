/**
 * Created by Znoy on 3/12/2015.
 */
$(document).ready(function(){
    /* Vetraz */

    var options_str = "";
    var count_of_models;
    var count_of_select;
    //var array_of_options_rel = array();

    $('.tab1').find('.widget__content').append($('#box_CARBRAND'));
    $('#box_CARBRAND').find('.big-filter__label--widget').css('display', 'none');
    $('.tab1').find('.widget__content').append($('#box_CARMODEL'));
    $('#box_CARMODEL').find('.big-filter__label--widget').css('display', 'none');
    $('#box_CARMODEL').css('display', 'none');

    $('.tab2').find('.widget__content').append($('#box_PRICE'));
    $('.tab2').find('.widget__content').append($('#box_TRANSMISSION'));
    $('.tab2').find('.widget__content').append($('#box_FUEL_TYPE'));
    $('.tab2').find('.widget__content').append($('#box_DRIVE'));
    $('.tab2').find('.widget__content').append($('#box_CARYEAR'));

    $('.tab3').find('.widget__content').append($('#box_VOLUMEREAL'));
    $('.tab3').find('.widget__content').append($('#box_TOP100'));
    $('.tab3').find('.widget__content').append($('#box_POWER'));
    $('.tab3').find('.widget__content').append($('#box_SPEED'));

    $('.tab4').find('.widget__content').append($('#box_BODY'));
    $('.tab4').find('.widget__content').append($('#box_VEHICLETYPE'));
    $('.tab4').find('.widget__content').append($('#box_CLEARANCE'));
    $('.tab4').find('.widget__content').append($('#box_TRUNK_SIZE'));
    $('.tab4').find('.widget__content').append($('#box_EQUIPMENT_NAME'));

    $('.tab5').find('.widget__content').append($('#box_FUEL_TYPE'));
    $('.tab5').find('.widget__content').append($('#box_FUEL_CONSUMPTION_TRACK'));
    $('.tab5').find('.widget__content').append($('#box_ABS'));

    $('#box_CARBRAND').find('input[type="radio"]').on('click', function(){
        $('#box_CARMODEL').css('display', 'block');
        //count_of_models = $('#box_CARMODEL .bx_filter_parameters_box_container label').length;
        //count_of_select = $(this).find('#box_CARBRAND .CARMODEL').length;
        //$(this).find('.CARMODEL option:checked').each(function(){
        //    //array_of_options_rel[] =
        //});

        $('.bx_filter_parameters_box_container').change(function(){
            $('#box_CARMODEL .bx_filter_parameters_box_container label').each(function(){
                if ($(this).hasClass('disabled') != true){
                    options_str = options_str+'<option rel="'+$(this).attr('for')+'">'+$(this).find('.bx_filter_param_text').text()+'</option>';
                }
            });
            $(this).parents('.bx_filter_param_label').append('<div class=\"subselect\"><select class="CARMODEL">'+options_str+'</select></div>');
            options_str = "";
        });

        //$(this).find('.CARMODEL').append(options_str);
    });

    $('#box_CARMODEL select').change(function(){

    });

    var $price_slider, $price_slider2, $price_slider3;

    $('.select--styled-ui').selectmenu({
        width: function(){
            return $(this).parent().width();
        }
    });

    $('.menu-element').click(function() {
        $('.tab-bar-bg').css({ visibility: 'hidden' });
        $('#' + $(this).attr('data-link')).css({ visibility: 'visible' });
        $('.menu-element').css({ 'background-color': 'rgba(0,0,0,0.4)'});
        $(this).css({ 'background-color': 'rgba(255,255,255,0.5)'});
    });

    $('.menu-element').each(function() {
        if ($(this).attr('data-link') == 'promotionvolvo') {
            $(this).css({ 'background-color': 'rgba(255,255,255,0.5)'});
        }
    });

    $( "#price-range" ).slider({
        range: true,
        min: 10000,
        max: 100000,
        values: [ 10000, 100000 ],
        slide: function( event, ui ) {
            $( "#price-first" ).html( ui.values[ 0 ] );
            $( "#price-second" ).html( ui.values[ 1 ] );
            $( "#price-val-first" ).val( ui.values[ 0 ] );
            $( "#price-val-second" ).val( ui.values[ 1 ] );
        }
    });

    $( "#price-range-2" ).slider({
        range: true,
        min: 10000,
        max: 100000,
        values: [ 10000, 100000 ],
        slide: function( event, ui ) {
            $( "#price-first-2" ).html( ui.values[ 0 ] );
            $( "#price-second-2" ).html( ui.values[ 1 ] );
            $( "#price-val-first-2" ).val( ui.values[ 0 ] );
            $( "#price-val-second-2" ).val( ui.values[ 1 ] );
        }
    });

    $( "#price-val-second-3" ).change(function(){
        $( "#price-range-3" ).slider({
            range: true,
            min: 2000,
            max: 2015,
            values: [ 2000, 2015 ],
            slide: function( event, ui ) {
                $( "#price-first-3" ).html( ui.values[ 0 ] );
                $( "#price-second-3" ).html( ui.values[ 1 ] );
                $( "#price-val-first-3" ).val( ui.values[ 0 ] );
                $( "#price-val-second-3" ).val( ui.values[ 1 ] );
            }
        });

    });

    $( "#price-range-3" ).slider({
        range: true,
        min: 2000,
        max: 2015,
        values: [ 2000, 2015 ],
        slide: function( event, ui ) {
            $( "#price-first-3" ).html( ui.values[ 0 ] );
            $( "#price-second-3" ).html( ui.values[ 1 ] );
            $( "#price-val-first-3" ).val( ui.values[ 0 ] );
            $( "#price-val-second-3" ).val( ui.values[ 1 ] );
        }
    });

    $('#slider').slick({
        dots: true,
        //infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        //lazyLoad: 'progressive',
        speed: 500,
        cssEase: 'linear'
    });

    // $('.location').click(function() {
    //     $(this).find('.info').show();
    // }, function() {
    //     $(this).find('.info').hide();
    // });

    $price_slider = $( "#price-range" );
    $( "#price-first" ).html( $price_slider.slider( "values", 0 ) );
    $( "#price-second" ).html( $price_slider.slider( "values", 1 ) );
    $( "#price-val-first" ).val( $price_slider.slider( "values", 0 ) );
    $( "#price-val-second" ).val( $price_slider.slider( "values", 1 ) );

    $price_slider2 = $( "#price-range-2" );
    $( "#price-first-2" ).html( $price_slider2.slider( "values", 0 ) );
    $( "#price-second-2" ).html( $price_slider2.slider( "values", 1 ) );
    $( "#price-val-first-2" ).val( $price_slider2.slider( "values", 0 ) );
    $( "#price-val-second-2" ).val( $price_slider2.slider( "values", 1 ) );

    $price_slider3 = $( "#price-range-3" );
    $( "#price-first-3" ).html( $price_slider3.slider( "values", 0 ) );
    $( "#price-second-3" ).html( $price_slider3.slider( "values", 1 ) );
    $( "#price-val-first-3" ).val( $price_slider3.slider( "values", 0 ) );
    $( "#price-val-second-3" ).val( $price_slider3.slider( "values", 1 ) );

    $( ".big-filter" ).accordion({
        header: ".widget__title",
        heightStyle: "content"
    });

    if (window.location.href.indexOf('catalog3') != -1 || window.location.href.indexOf('catalog_brand') != -1) {
        $( ".big-filter" ).accordion( "option", "active", 1 );
    }

    $( ".slide-range" ).slider({
        range: true,
        min: 1,
        max: 1000,
        values: [ 1, 1000 ],
        slide: function( event, ui ) {
            var $parent = $(this).parents('.widget__content');
            $parent.find('.input-slider-l').val( ui.values[ 0 ] );
            $parent.find('.input-slider-r').val( ui.values[ 1 ] );
        }
    });

    $( ".slide-range" ).slider({
        range: true,
        min: 1,
        max: 1000,
        values: [ 1, 1000 ],
        slide: function( event, ui ) {
            var $parent = $(this).parents('.widget__content');
            $parent.find('.input-slider-l').val( ui.values[ 0 ] );
            $parent.find('.input-slider-r').val( ui.values[ 1 ] );
        }
    });

    $('.input-slider-l, .input-slider-r').on('blur', changeSliderValues);

    function changeSliderValues() {
        console.log(123);
        var leftval = $('.input-slider-l').val(),
            rightval = $('.input-slider-r').val();
        $('.input-slider-l').parents('.widget__content').find( ".slide-range" ).slider("values", [ leftval, rightval ]);
    }

    $('input[type="checkbox"]', '.marks__item').on('change', function(){
        var $parent = $(this).parents('.marks__item'),
            $selects = $parent.find('.select--styled-ui');
        $selects.selectmenu( "destroy" );
        $parent.children('.mark__subselect').toggle();
        $selects.selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });
    });

    $('.add-more-select').click(function(){
        var $parent = $(this).parent(),
            $selects = $parent.find('.select--styled-ui'),
            select_qauntity = $(this).data('quantity');
        $selects.selectmenu( "destroy" );
        $parent.find('.remove-select').before($parent.find('.select--styled-ui').first().clone());
        $parent.find('.select--styled-ui').selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });
        $(this).data('quantity', ++select_qauntity);
        if (select_qauntity > 1) $parent.find('.remove-select').show();
    });

    $('.widget__content').on('click', '.remove-select', function(){
        var $parent = $(this).parent(),
            $add_select = $parent.find('.add-more-select'),
            $selects = $parent.find('.select--styled-ui'),
            select_qauntity = $add_select.data('quantity');
        $selects.selectmenu( "destroy" );
        $selects.last().detach();
        $parent.find('.select--styled-ui').selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });
        $add_select.data('quantity', --select_qauntity);
        if (select_qauntity === 1) $(this).hide();
    });

    $('.add-more-mark-model').click(function(){
        var $parent = $(this).parents('.widget__allcontent'),
            models_quantity = $(this).data('quantity'),
            $clone;
        if (models_quantity < 3) {
            $parent.find('.select--styled-ui').selectmenu("destroy");
            $clone = $parent.children('.widget__content').first().clone();
            $clone.append('<div class="remove-link">х <span>Удалить</span></div>');
            $(this).parent().before($clone);
            $parent.find('.select--styled-ui').selectmenu({
                width: function () {
                    return $(this).parent().width();
                }
            });
            $(this).data('quantity', ++models_quantity);
            if (models_quantity === 3) $(this).parent().hide();
        }
    });

    $('.widget__allcontent').on('click', '.remove-link', function(){
        var $mark_model = $('.add-more-mark-model'),
            models_qauntity = $mark_model.data('quantity');
        $(this).parent().detach();
        $mark_model.data('quantity', --models_qauntity);
        if (models_qauntity === 2) $mark_model.parent().show();
    });

    $('.open').click(function(){
	if($(this).is('active')){
		$(this).removeClass('active');
	}
	else{
		$(this).addClass('active');
	}
    });

    var app = {
        scrolled: 0,
        footerOffset: 0,
        menuOffset: 0
    };

    function adjustFooterBar() {
        app.scrolled = window.pageYOffset || document.documentElement.scrollTop;
        app.footerOffset = $('footer').offset().top;
        if (app.scrolled + window.innerHeight >= app.footerOffset) {
            $('.footer-menu-container').css({ bottom: app.scrolled + window.innerHeight - app.footerOffset });
        } else {
            $('.footer-menu-container').css({ bottom: 0 });
        }
    };

    adjustFooterBar();

    window.onscroll = function() {
        adjustFooterBar();
    };
});