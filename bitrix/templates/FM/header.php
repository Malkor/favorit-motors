<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$curPage = $APPLICATION->GetCurPage(true);
$curDir = $APPLICATION->GetCurDir();
$catalog = preg_match("/catalog/", $curDir, $matches);
$catalog_stock = preg_match("/catalog\/stock/", $curDir, $matches);
$catalog_order = preg_match("/catalog\/order/", $curDir, $matches);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
  <head>
    <?
    echo '<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'"'.(true ? ' /':'').'>'."\n";
    $APPLICATION->ShowMeta("robots", false, true);
    $APPLICATION->ShowMeta("description", false, true);
    $APPLICATION->ShowCSS(true, true);
    $APPLICATION->ShowHeadStrings();
    $APPLICATION->ShowHeadScripts();
    $APPLICATION->ShowViewContent('catalog_show_paginator_links');
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
   
    <?if(($curDir == '/catalog/') || ($curDir == '/models/')):?>
      <link rel="stylesheet" href="/v4/stylesheets/screen.css" media="all" />
      <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/screen.css" media="all" />
      <link rel="stylesheet" href="/v4/stylesheets/jmenu.css" media="all" />
      <link rel="stylesheet" href="/v4/stylesheets/jquery.rating.css" media="all" />
      <link rel="stylesheet" href="/v4/stylesheets/style.css"  media="all">
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/jquery.bxslider.css")?>" />
    <?else:?>
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/normalize.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/screen.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/slick.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/slick-theme.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/style.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/jquery.bxslider.css")?>" />
      <link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/styles/styles.css")?>" />
    <?endif?>
  <?if(($curDir == '/catalog/') || ($curDir == '/models/')):?>
      <script type="text/javascript" src="/v4/javascripts/jquery.min.js"></script>
      <script type="text/javascript" src="/v4/javascripts/jquery.bxslider.js"></script>
      <script type="text/javascript" src="/v4/javascripts/jquery-ui.min.js"></script>
      <script type="text/javascript" src="/v4/javascripts/jMenu.jquery.min.js"></script>
      <script type="text/javascript" src="/v4/javascripts/slick.min.js"></script>
      <script type="text/javascript" src="/v4/javascripts/jquery.rating.pack.js"></script>
      <script type="text/javascript" src="/v4/javascripts/app.js"></script>
  <?else:
    $APPLICATION->AddHeadScript("https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/jquery.bxslider.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/jquery-ui.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/slick.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/app.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/main.min.js");?>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.pjax.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			//$('a').pjax('#pjax-container');
			$('a[data-pjax]').pjax();
		});
	</script>
 <? endif;?>
<title><?$APPLICATION->ShowTitle()?></title>
  </head>
  <body>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?if(($curDir != '/catalog/') && ($curDir != '/models/')):?>
    <div id="wrapper">
      <nav>
        <div class="container">
          <a class="logo" href="/">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
               "AREA_FILE_SHOW" => "file",
               "PATH" => SITE_TEMPLATE_PATH."/include/logo.php",
               "EDIT_TEMPLATE" => ""
               ),
               false
            );?>
          </a>
         
          <div class="menu-wrapper">
            <?$APPLICATION->IncludeComponent(
              "bitrix:menu", 
              "top_menu_horizontal_multilevel", 
              array(
                "COMPONENT_TEMPLATE" => ".default",
                "ROOT_MENU_TYPE" => "top",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MAX_LEVEL" => "3",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
              ),
              false
            );?>
          </div>

          <?$APPLICATION->IncludeComponent(
            "bitrix:menu", 
            "top_menu_horizontal_multilevel", 
            array(
              "COMPONENT_TEMPLATE" => ".default",
              "ROOT_MENU_TYPE" => "stocks",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "MAX_LEVEL" => "3",
              "CHILD_MENU_TYPE" => "left",
              "USE_EXT" => "Y",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
            ),
            false
          );?>

          <div class="contacts">
            <div class="phone">
              <div class="wrap">
                <div class="text">
                  <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                     "AREA_FILE_SHOW" => "file",
                     "PATH" => SITE_TEMPLATE_PATH."/include/nav_top_phone.php",
                     "EDIT_TEMPLATE" => ""
                     ),
                     false
                  );?>
                </div>
              </div>
            </div>
            <div class="info">
              <div class="wrap">
                <div class="text">
                  <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH."/include/nav_top_info.php",
                    "EDIT_TEMPLATE" => ""
                    ),
                    false
                  );?>
                </div>
              </div>
            </div>
          </div>
        </div><!-- container -->
      </nav>

      <div class="content">

<?endif;?>

        