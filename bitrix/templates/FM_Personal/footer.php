        </section><!-- /Main Content -->

        <!-- Footer -->
        <footer id="footer">

            <div class="moving-bar border-top">
                <div class="box-container">

                    <div class="moving-bar-link">
                        <a href="#"><i class="fa fa-file-text-o"></i><span class="display-desktop">Написать отзыв</span></a>
                    </div>

                    <div class="moving-bar-link on-center">
                        <a class="use-popup" data-block="thanks"><i class="fa fa-phone"></i><span class="display-desktop">Обратный звонок</span></a>
                    </div>

                    <div class="moving-bar-link align-right">
                        <a href="#"><i class="fa fa-microphone"></i><span class="red-text display-desktop">on-line</span><span class="display-desktop">консультант</span></a>
                    </div>

                </div>
            </div>

            <div class="section section-gray section-sec">
                <div class="box-container clearfix">

                    <div class="on-left clearfix">

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>О компании</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Наша миссия</a></li>
                                <li><a href="#">Наши награды</a></li>
                                <li><a href="#">Блог президента</a></li>
                                <li><a href="#">Отзывы</a></li>
                                <li><a href="#">Партнёрам</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>Наши бренды</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Citroen</a></li>
                                <li><a href="#">Chevrolet</a></li>
                                <li><a href="#">Ford</a></li>
                                <li><a href="#">Peugeot</a></li>
                                <li><a href="#">Kia</a></li>
                                <li><a href="#">Seat</a></li>
                                <li><a href="#">Ssangyong</a></li>
                                <li><a href="#">Skoda</a></li>
                                <li><a href="#">Opel</a></li>
                                <li><a href="#">Volvo</a></li>
                            </ul>
                        </div>

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>Клиентам</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Личный кабинет</a></li>
                                <li><a href="#">Оставить отзыв</a></li>
                                <li><a href="#">Подать жалобу</a></li>
                                <li><a href="#">Корпоративным клиентам</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="on-right footer-address left-margin">
                        <div class="title-footer">
                            <h2>Адреса салонов</h2>
                        </div>
                        <ul class="footer-menu">
                            <li><a href="#">м. Войковская, Москва, ул. Коптевская, д.69-71</a></li>
                            <li><a href="#">м. Семеновская, Москва, ул. Большая Семеновская, д.42</a></li>
                            <li><a href="#">м. Новогиреево, Выхино, Москва, МКАД, 2 км,</a></li>
                            <li><a href="#">внешний радиус, г. Реутов</a></li>
                            <li><a href="#">Московская область, г. Люберцы, Рязанское шоссе, д. 46</a></li>
                            <li><a href="#">м. Юго-Западная, Москва, Ленинский проспект, д. 146</a></li>
                            <li><a href="#">м. Южная, Пражская, Москва, Дорожный 1-й проезд, д.4</a></li>
                        </ul>
                    </div>

                    <div class="footer-search on-right">

                        <div class="title-footer">
                            <h2 class="clearfix"><label class="on-left" for="search">Поиск по сайту:</label></h2>
                        </div>

                        <form id="search-form">
                            <input type="text" name="search" id="search" class="animate-custom" />
                            <input type="submit" value="" />
                        </form>

                        <a href="#" class="display-mobile full-site-link">Перейти на полную версию сайта</a>

                    </div>

                </div>
            </div>

            <div class="footer-bar">
                <div class="box-container">

                    <div class="on-left clearfix">
                        <span class="on-left">
                            <span>Мы в социальных сетях</span>
                        </span>
                        <ul class="footer-social-links on-left clearfix">
                            <li class="on-left"><a class="fb social-icon-link" href="#"><i class="fa fa-facebook animate-custom"></i></a></li>
                            <li class="on-left"><a class="tw social-icon-link" href="#"><i class="fa fa-twitter animate-custom"></i></a></li>
                            <li class="on-left"><a class="in social-icon-link" href="#"><i class="fa fa-instagram animate-custom"></i></a></li>
                            <li class="on-left"><a class="gp social-icon-link" href="#"><i class="fa fa-google-plus animate-custom"></i></a></li>
                            <li class="on-left"><a class="vk social-icon-link" href="#"><i class="fa fa-vk animate-custom"></i></a></li>
                        </ul>
                    </div>

                    <div class="on-center clearfix">
                        <span class=" left-margin on-left display-desktop">Мобильные приложения</span>
                        <span class="display-mobile">Скачать приложения:</span>
                        <a href="#" class="bg on-left app-logo animate-custom opacity-custom-hover" style="background-image: url(images/appstore.png);"></a>
                        <a href="#" class="bg on-left app-logo animate-custom opacity-custom-hover" style="background-image: url(images/googleplay.png);"></a></div>

                    <div class="on-right clearfix"><a class="on-right rss-button animate-custom" href="#"><i class="fa fa-rss"></i></a><span class="on-right">Подписаться на рассылку</span></div>

                </div>
            </div>

            <div class="copyright">
                <div class="box-container clearfix">

                    <div class="on-left">Все права защищены</div>

                    <div class="on-center"><a href="#">Фаворит-Моторс 1994–2015</a></div>

                    <div class="on-right"><a href="#">Разработка</a></div>

                </div>
            </div>

        </footer><!-- /Footer -->

    </div><!-- /Main Container -->

        <!-- Popups -->
        <div class="popups-container">

            <div class="popup medium-popup" id="call-back">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Заказ обратного звонка</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="call-back-name">Ваше имя:</label>
                                        <input type="text" name="call-back-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="call-back-service">Департамент:</label>
                                        <select id="call-back-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <div class="form-item">
                                        <label for="call-back-phone">Ваш телефон:</label>
                                        <input type="text" name="call-back-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="call-back-theme">Тема обращения:</label>
                                        <select id="call-back-theme">
                                            <option>Выбрать тему</option>
                                            <option>Тема 1</option>
                                            <option>Тема 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <input type="submit" class="red-small-link animate-custom" value="Заказать звонок" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="test-drive">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Запись на тест–драйв: Skoda Fabia</h2>
                            </div>
                            <div class="form-items">
                                <div class="clearfix">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="test-drive-name">Ваше имя:</label>
                                            <input type="text" name="test-drive-name" id="call-back-name" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="test-drive-phone">Ваш телефон:</label>
                                            <input type="text" name="test-drive-phone" id="call-back-phone" class="phone-mask" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="test-drive-mark">Марка:</label>
                                        <select id="test-drive-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="test-drive-model">Модель:</label>
                                        <select id="test-drive-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="test-drive-service">Департамент:</label>
                                        <select id="test-drive-service">
                                            <option>Выбрать</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="test-drive-image">
                            <div class="test-drive-image-container"><img src="content/models/popup-car.png" alt="model" /></div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container"><input type="checkbox" name="agreement-checkbox" id="agreement-checkbox" /><label for="agreement-checkbox">Я согласен на <a href="#">использование моих персональных данных</a></label></div>
                            <input type="submit" class="red-small-link animate-custom" value="Заказать звонок" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="book-car">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Резервирование автомобиля</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="book-car-name">Ваше имя:</label>
                                        <input type="text" name="book-car-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="book-car-phone">Ваш телефон:</label>
                                        <input type="text" name="book-car-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="book-car-service">Департамент:</label>
                                        <select id="book-car-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <div class="popup-info-fields">
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Марка:</div>
                                            <div class="popup-info-field-text on-left">KIA</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Модель:</div>
                                            <div class="popup-info-field-text on-left">QUORIS</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">VIN:</div>
                                            <div class="popup-info-field-text on-left">XWELW412BD0000612</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Комплектация:</div>
                                            <div class="popup-info-field-text on-left">LUX 1.6 AT</div>
                                        </div>
                                        <div class="popup-info-field padding clearfix">
                                            <div class="popup-info-field-label on-left">Название акции:</div>
                                            <div class="popup-info-field-text on-left">«Успей купить»</div>
                                        </div>
                                        <div class="popup-info-field padding clearfix">
                                            <div class="popup-info-field-label on-left">Цена:</div>
                                            <div class="popup-info-field-text on-left"><strong>2 159 900</strong> р.</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">В кредит:</div>
                                            <div class="popup-info-field-text on-left">от <strong>12 500</strong> р./мес.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container">Вам перзвонит менеджер и уточнит детали и удобное для Вас время, чтобы подготовить документы</div>
                            <input type="submit" class="red-small-link animate-custom" value="Зарезервировать" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="event-info">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Узнать подробности предложения</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="event-info-name">Ваше имя:</label>
                                        <input type="text" name="event-info-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="event-info-phone">Ваш телефон:</label>
                                        <input type="text" name="event-info-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="event-info-service">Департамент:</label>
                                        <select id="event-info-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <textarea name="event-info-text" id="event-info-text" placeholder="Название акции и/или идентификатор автомобиля (vin):"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container">Менеджер перезвонит Вам, чтобы уточнить подробности данной акции</div>
                            <input type="submit" class="red-small-link animate-custom" value="Зарезервировать" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="add-service">
                <div class="popup-container">
<?
        $APPLICATION->IncludeFile(
        SITE_DIR."include/forms/entry_techcenter.php",
        Array(),
        Array("SHOW_BORDER"=>false));
?>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="car-sell">
                <div class="popup-container">
<?
        $APPLICATION->IncludeFile(
        SITE_DIR."include/forms/request_sale.php",
        Array(),
        Array("SHOW_BORDER"=>false));
?>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="car-insurance">
                <div class="popup-container">

<?
        $APPLICATION->IncludeFile(
        SITE_DIR."include/forms/insurance.php",
        Array(),
        Array("SHOW_BORDER"=>false));
?>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <!--div class="popup medium-popup" id="thanks">
                <div class="popup-container">
<?
        $APPLICATION->IncludeFile(
        SITE_DIR."include/forms/feedback_form.php",
        Array(),
        Array("SHOW_BORDER"=>false));
?> 
                    

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div-->
            
            <div class="popup medium-popup" id="call-back">
                <div class="popup-container">
<?
        $APPLICATION->IncludeFile(
        SITE_DIR."include/forms/feedback_form.php",
        Array(),
        Array("SHOW_BORDER"=>false));
?> 
                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup-shadow"></div>

        </div><!-- /Popups -->

        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript" src="/anh/scripts/jquery.formstyler.min.js"></script>
        <script type="text/javascript" src="/anh/scripts/jquery.smoothscroll.js"></script>
        <script type="text/javascript" src="/anh/scripts/jquery.colorbox.js"></script>
        <script type="text/javascript" src="/anh/scripts/jquery.inputmask.js"></script>
        <script type="text/javascript" src="/anh/scripts/jquery.slicknav.min.js"></script>
        <script type="text/javascript" src="/anh/scripts/swiper.min.js"></script>
        <script type="text/javascript" src="/anh/scripts/response.min.js"></script>
        <script type="text/javascript" src="/anh/scripts/interface.js"></script>
        <script type="text/javascript" src="/anh/scripts/main.js"></script>
        <script type="text/javascript" src="/anh/scripts/applications.js"></script>

</body>
</html>