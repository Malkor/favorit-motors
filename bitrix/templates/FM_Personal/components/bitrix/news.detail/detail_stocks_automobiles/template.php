
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="section-red">
	<div class="box-container">
		 <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "stocks",
            Array(
            )
        );?>
		<div class="title-gray clearfix event-section">
			<div class="on-left">
				<h1><?=$arResult["NAME"]?></h1>
			</div>
			<div class="on-right">
				<h2 class="align-left">Адреса салонов:</h2>
			</div>
		</div>
	</div>
</div>

 <!-- Banners Header -->
<div id="banner-header" class="section-sec section-gray">
	<div class="box-container clearfix">
		<div class="on-left bg" style="background-image: url(content/events/banner-1.jpg); ">
		</div>
		<div class="on-right">
			<div class="event-address-items align-left">
				<div class="event-address-item">
 <span class="red-text">м.</span> Войковская, Москва, ул. Коптевская, д. 69-71
				</div>
				<div class="event-address-item">
 <span class="red-text">м.</span> Семеновская, Москва, ул. Большая Семеновская, д.42
				</div>
				<div class="event-address-item">
 <span class="red-text">м.</span> Новогиреево, Выхино, Москва, МКАД, 2 км, внешний радиус, г. Реутов
				</div>
				<div class="event-address-item">
 <span class="red-text">м.</span> Лермонтовский проспект, Московская обл., г. Люберцы, Рязанское шоссе, д. 46
				</div>
				<div class="event-address-item">
 <span class="red-text">м.</span> Юго-Западная, Москва, Ленинский проспект, д. 146
				</div>
				<div class="event-address-item">
 <span class="red-text">м.</span> Южная, Пражская, Москва, Дорожный 1-й проезд, д.4
				</div>
			</div>
		</div>
	</div>
</div>
 <!-- /Banners Header --> 

<!-- Events Body -->
	<?=$arResult["DETAIL_TEXT"]?>
<!-- /Events Body --> 