<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

 <div class="section section-sec">
    <div class="box-container">
        <div class="title clearfix">
            <h2 class="on-left"><?=$arResult["NAME"]?></h2>
            
           	<?if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"){?>
			<?$APPLICATION->IncludeComponent(
	"bitrix:asd.share.buttons", 
	"share", 
	array(
		"ASD_ID" => $_REQUEST["id"],
		"ASD_TITLE" => $arResult["NAME"],
		"ASD_URL" => $arResult["DETAIL_PAGE_URL"],
		"ASD_PICTURE" => $arResult["PREVIEW_PICTURE"]["SRC"],
		"ASD_TEXT" => $arResult["PREVIEW_TEXT"],
		"ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
		"ASD_SITE_NAME" => "",
		"ASD_INCLUDE_SCRIPTS" => array(
		)
	),
	false
);?>
				<noindex>
					<?/*$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
						"HANDLERS" => array("facebook","twitter","vk"),
						"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
						"PAGE_TITLE" => $arResult["~NAME"],
						"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"HIDE" => $arParams["SHARE_HIDE"],
						),
						$component,
						array("HIDE_ICONS" => "Y")
					);*/?>
				</noindex>
			<?}?>
        </div>

        <div class="body clearfix align-justify border-bottom">

            <a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="body-image colorbox animate-custom opacity-custom-hover show-on-left">
                <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" />
            </a>

            <?=$arResult["DETAIL_TEXT"];?>

        </div>

         <?$APPLICATION->IncludeComponent(
		"bitrix:iblock.vote", 
		"articles_votes", 
		array(
			"COMPONENT_TEMPLATE" => ".default",
			"IBLOCK_TYPE" => "news",
			"IBLOCK_ID" => "29",
			"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
			"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"MAX_VOTE" => "5",
			"VOTE_NAMES" => array(
				0 => "",
				1 => "",
			),
			"SET_STATUS_404" => "N"
		),
		false
	);?>

    </div>
</div>