<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID.'_pict',
    'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
    'STICKER_ID' => $strMainID.'_sticker',
    'BIG_SLIDER_ID' => $strMainID.'_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
    'SLIDER_LIST' => $strMainID.'_slider_list',
    'SLIDER_LEFT' => $strMainID.'_slider_left',
    'SLIDER_RIGHT' => $strMainID.'_slider_right',
    'OLD_PRICE' => $strMainID.'_old_price',
    'PRICE' => $strMainID.'_price',
    'DISCOUNT_PRICE' => $strMainID.'_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
    'QUANTITY' => $strMainID.'_quantity',
    'QUANTITY_DOWN' => $strMainID.'_quant_down',
    'QUANTITY_UP' => $strMainID.'_quant_up',
    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
    'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
    'BASIS_PRICE' => $strMainID.'_basis_price',
    'BUY_LINK' => $strMainID.'_buy_link',
    'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
    'BASKET_ACTIONS' => $strMainID.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
    'COMPARE_LINK' => $strMainID.'_compare_link',
    'PROP' => $strMainID.'_prop_',
    'PROP_DIV' => $strMainID.'_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
    'OFFER_GROUP' => $strMainID.'_set_group_',
    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?>


<!-- Content -->
<section id="content">
  <!-- Super Slider -->
  <div class="super-slider">
    <div class="box-container">
      <div class="left-side">
        <div class="this-car-logo bg <?=strtolower($arResult['PROPERTIES']['CARBRAND']['VALUE'])?>"></div>
      </div>
      <div class="right-side nalichie">
        <div class="this-car-info">
          <div class="this-car-info-name"><?=$arResult['PROPERTIES']['CARBRAND']['VALUE']?> <?=$arResult['PROPERTIES']['CARMODEL']['VALUE']?></div>
          <div class="this-car-info-price"><span><?=$arResult['PROPERTIES']['PRICE']['VALUE']?></span> р.</div>
          <div class="this-car-info-price-old">2 190 000 р.</div>
        </div>
        <div class="this-car-some-links">
          <ul>
            <li><a href="#">Калькулятор КАСКО</a></li>
            <li><a href="#">Рассчитать кредит</a></li>
          </ul>
        </div>
        <div class="this-car-big-links">
          <ul>
            <li><a href="#">Конфигуратор</a></li>
          </ul>
        </div>
        <div class="this-car-buttons-links">
          <ul>
            <li><a class="red-small-link animate-custom" href="#">Тест-драйв</a></li>
            <li><a class="green-small-link animate-custom" href="#">Заказать автомобиль</a></li>
          </ul>
        </div>
        <div class="this-car-sell">
          <a href="#" class="opacity-custom-hover animate-custom clearfix">
            <span class="circle-icon active on-left"><i class="fa fa-thumbs-up"></i></span>
            <span class="on-right align-left">продать авто выгодно</span>
          </a>
        </div>
      </div>

      <div class="breadcrumbs clearfix">
        <div class="on-left">
          <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "",
            Array(
              "COMPONENT_TEMPLATE" => ".default",
              "START_FROM" => "1",
              "PATH" => "",
              "SITE_ID" => "-"
            ),
            $component
          );?>
        </div>

        <?
        global $arrFilter;
        $arrFilter = array("PROPERTY_BRAND_VALUE"=>strtolower($arResult["PROPERTIES"]["CARBRAND"]["VALUE"]));
        ?>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "model_salons_list", Array(
          "COMPONENT_TEMPLATE" => ".default",
            "IBLOCK_TYPE" => "references",  // Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => "12",  // Код информационного блока
            "NEWS_COUNT" => "20", // Количество новостей на странице
            "SORT_BY1" => "ACTIVE_FROM",  // Поле для первой сортировки новостей
            "SORT_ORDER1" => "DESC",  // Направление для первой сортировки новостей
            "SORT_BY2" => "SORT", // Поле для второй сортировки новостей
            "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
            "FILTER_NAME" => "arrFilter", // Фильтр
            "FIELD_CODE" => array(  // Поля
              0 => "NAME",
              1 => "",
            ),
            "PROPERTY_CODE" => array( // Свойства
              0 => "METRO",
              1 => "BRAND",
              2 => "",
            ),
            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "AJAX_MODE" => "N", // Включить режим AJAX
            "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "A",  // Тип кеширования
            "CACHE_TIME" => "36000000", // Время кеширования (сек.)
            "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",  // Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "", // Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "d.m.Y",  // Формат показа даты
            "SET_TITLE" => "N", // Устанавливать заголовок страницы
            "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
            "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
            "SET_META_DESCRIPTION" => "N",  // Устанавливать описание страницы
            "SET_STATUS_404" => "N",  // Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
            "PARENT_SECTION" => "", // ID раздела
            "PARENT_SECTION_CODE" => "",  // Код раздела
            "INCLUDE_SUBSECTIONS" => "N", // Показывать элементы подразделов раздела
            "DISPLAY_DATE" => "N",  // Выводить дату элемента
            "DISPLAY_NAME" => "N",  // Выводить название элемента
            "DISPLAY_PICTURE" => "N", // Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N",  // Выводить текст анонса
            "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
            "PAGER_TITLE" => "Новости", // Название категорий
            "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
            "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",  // Показывать ссылку "Все"
          ),
          false
        );?>
      </div>

      <div class="super-slider-containers">
          <div id="gallery" class="super-slider-container">
              <div class="gallery-navigation tabs-inner-navigation">
                  <ul class="clearfix">
                      <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-exterior">Экстерьер</a></li>
                      <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-interior">Интерьер</a></li>
                  </ul>
              </div>
              <?
                $exterior_arr = array();
                foreach($arResult["PROPERTIES"]["SLIDER_EXTERIOR"]["VALUE"] as $exterior){
                  $exterior_arr[] = $exterior;
                }

                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>18, "ID"=>$exterior_arr, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
                $exterior_arr_res = array();
                while($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $exterior_arr_res[$arFields["ID"]] = $arFields;
                }
              ?>

              <div id="slider-exterior" class="gallery-navigation-container">
                <div class="body-slider gallery-exterior-top">
                    <div class="swiper-wrapper">
                      <?foreach($exterior_arr_res as $exterior):?>
                        <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($exterior["DETAIL_PICTURE"])?>');">
                            <a href="<?=CFile::GetPath($exterior["DETAIL_PICTURE"])?>" rel="gallery-5" class="colorbox"></a>
                        </div>
                      <?endforeach;?>
                    </div>
                </div>
                <div class="swiper-container gallery-rotation-slider gallery-exterior-thumbs">
                    <div class="swiper-wrapper">
                        <?foreach($exterior_arr_res as $exterior):?>
                          <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($exterior["DETAIL_PICTURE"])?>'); "></div>
                        <?endforeach;?>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
              </div>

              <?
                $interior_arr = array();
                foreach($arResult["PROPERTIES"]["SLIDER_INTERIOR"]["VALUE"] as $interior){
                  $interior_arr[] = $interior;
                }

                $arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>18, "ID"=>$interior_arr, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
                $interior_arr_res = array();
                while($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $interior_arr_res[$arFields["ID"]] = $arFields;
                }
              ?>

              <div id="slider-interior" class="gallery-navigation-container">
                <div class="body-slider gallery-inter-top">
                    <div class="swiper-wrapper">
                      <?foreach($interior_arr_res as $interior):?>
                        <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($interior["DETAIL_PICTURE"])?>');">
                            <a href="<?=CFile::GetPath($interior["DETAIL_PICTURE"])?>" rel="gallery-5" class="colorbox"></a>
                        </div>
                      <?endforeach;?>
                       
                    </div>
                </div>
                <div class="swiper-container gallery-rotation-slider gallery-inter-thumbs">
                    <div class="swiper-wrapper">
                        <?foreach($interior_arr_res as $interior):?>
                          <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($interior["DETAIL_PICTURE"])?>'); "></div>
                        <?endforeach;?>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
              </div>

          </div>
      </div>
  </div>
</div><!-- /Super Slider -->

<?
$arCars = array();
$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PROPERTY_CARBRAND", "PROPERTY_CARMODEL", "PROPERTY_YEAR", "PROPERTY_CARNAME", "PROPERTY_PRICE");
$arFilter = Array("IBLOCK_ID"=>28, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CARBRAND_VALUE"=>$arResult["PROPERTIES"]["CARBRAND"]["VALUE"], "PROPERTY_CARMODEL_VALUE"=>$arResult["PROPERTIES"]["CARMODEL"]["VALUE"]);
$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
while($ob = $res->GetNextElement())
{
  $arFields = $ob->GetFields();
  $arCars[$arFields["ID"]] = $arFields;
}
?>
<?if(count($arCars) > 0):?>
  <!-- Cars -->
  <div id="models-cars" class="section section-sec">
    <div class="box-container">
     <div class="title border-bottom">
        <h2><?=$arResult['PROPERTIES']['CARBRAND']['VALUE']?> <?=$arResult['PROPERTIES']['CARMODEL']['VALUE']?> в наличии: <span class="red-text"><?=count($arCars);?></span></h2>
      </div>
      <?echo '<div class="new-auto-items four-in-line"><!--';?>
        <?$c = 1;?>
        <?foreach($arCars as $car):?>
          <?if($c > 4):break;endif;?>
          <?echo '--><div class="new-auto-item">';?>
            <div class="new-auto-item-image">
               <a href="<?=$car["DETAIL_PAGE_URL"]?>" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?if($car["PREVIEW_PICTURE"]):echo $car["PREVIEW_PICTURE"];else:echo SITE_TEMPLATE_PATH."/images/no_photo.png";endif;?>');"></a>
            </div>
            <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom"><?=$car['PROPERTY_CARBRAND_VALUE']?> <?=$car['PROPERTY_CARMODEL_VALUE']?></a></div>
            <div class="new-auto-item-desc">
              <div class="new-auto-item-desc-field"><?if($car['PROPERTY_CARYEAR_VALUE']):echo $car['PROPERTY_CARYEAR_VALUE'].", ";endif;?><?=$car['PROPERTY_CARNAME_VALUE']?></div>
              <!--<div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>-->
            </div>
            <div class="new-auto-item-info clearfix">
              <div class="on-left">
                <div class="new-auto-item-price-new"><?=$car['PROPERTY_PRICE_VALUE']?></div>
                <div class="new-auto-item-price-old">1 320 000</div>
              </div>
              <div class="on-right">
                <a href="<?=$car["DETAIL_PAGE_URL"]?>" class="animate-custom gray-small-link">Подробнее</a>
              </div>
            </div>
          <?echo '</div><!--';?>
          <?$c++;?>
        <?endforeach;?>
      <?echo '--></div>';?>
      <div class="bottom-part clearfix">
        <div class="on-right"><a href="#">Смотреть все</a></div>
      </div>
    </div>
  </div><!-- /Cars -->
<?endif;?>
               
    <!-- Tabs Container -->
    <div class="section-tabs">
        <div class="tabs-navigation section-gray section-sec">
            <div class="box-container">
                <ul class="tabs five-tabs clearfix">
                  <li class="active"><a href="detail.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Описание</a></li>
                  <li><a href="characteristics.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Характеристики</a></li>
                  <li><a href="picking.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Коплектации и цены</a></li>
                  <li><a href="reviews.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Отзывы</a></li>
                  <li><a href="stock.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Акции</a></li>
                </ul>
            </div>
        </div>
        
         <div id="pjax-container">
              <div id="section">
                <div class="section section-sec">
                    <div class="box-container">
                        <div class="description-items"><!---->
                            <?
                            if ($arResult["PROPERTIES"]["TAB_DESCRIPTION"]["~VALUE"]["TEXT"] != ""):
                              echo $arResult["PROPERTIES"]["TAB_DESCRIPTION"]["~VALUE"]["TEXT"];
                            endif;
                            ?>
                            <!----></div>

                        <?if ($arResult["PROPERTIES"]["TAB_DESCRIPTION_FILES"]["VALUE"] != ""):?>
                          <div class="bottom-part clearfix">
                              <div class="pdf-link on-right clearfix align-left">
                                  <div class="on-left">
                                      <i class="fa fa-file-pdf-o"></i>
                                  </div>
                                  <div class="on-left">
                                      <div class="pdf-link-url"><a href="<?=CFile::GetPath($arResult["PROPERTIES"]["TAB_DESCRIPTION_FILES"]["VALUE"]);?>" download>Загрузить файл</a></div>
                                      <div class="pdf-link-format">формат PDF</div>
                                  </div>
                              </div>
                          </div>
                        <?endif;?>

                        <div class="bottom-part clearfix">
                            <div class="on-center"><a href="#" class="red-big-link animate-custom">Задать вопрос</a></div>
                        </div>
                    </div>
                </div>
            </div>
      </div>

    </div><!-- /Tabs Container -->

</section><!-- /Content -->