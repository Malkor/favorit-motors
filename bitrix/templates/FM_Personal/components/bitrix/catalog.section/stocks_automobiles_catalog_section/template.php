<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="section-red">
    <div class="box-container">
         <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "stocks",
            Array(
            )
        );?>
        <div class="title-gray">
            <h1><?=$arResult["UF_TITLE_STOCKS"]?></h1>
        </div>
    </div>
</div>

<!-- Banners Header -->
<div id="banner-header" class="section-sec section-gray">
    <div class="box-container clearfix">
        <?if($arResult["UF_BIG_IMG_POS_LEFT"] == 4):?>
            <div class="on-left">
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_1"])?>'); "></a>
            </div>

            <div class="on-right">
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_2"])?>'); "></a>
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_3"])?>'); "></a>
            </div>
        <?endif;?>
        <?if($arResult["UF_BIG_IMG_POS_LEFT"] == 5):?>
             <div class="on-left">
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_1"])?>'); "></a>
            </div>

            <div class="on-right">
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_2"])?>'); "></a>
                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=CFile::GetPath($arResult["UF_IMG_3"])?>'); "></a>
            </div>
        <?endif;?>
    </div>
</div>
<!-- /Banners Header -->

<?if (!empty($arResult['ITEMS'])):?>
    <!-- Events List -->
    <div class="section section-sec">
        <div class="box-container">
            <?foreach ($arResult['ITEMS'] as $key => $arItem):?>
                <div class="event-list-items">
                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></a>
                        </div>
                        <div class="accessories-item-info">
                            <?$mf = getRusMonth(date("m", mktime($arItem["DATE_ACTIVE_FROM"])));?>
                            <?$mt = getRusMonth(date("m", mktime($arItem["DATE_ACTIVE_TO"])));?>
                            <div class="accessories-item-name"><?echo date("d", mktime($arItem["DATE_ACTIVE_FROM"]));?> <?=$mf?> <?echo date("Y", mktime($arItem["DATE_ACTIVE_FROM"]));?> – <?echo date("d", mktime($arItem["DATE_ACTIVE_TO"]));?> <?=$mt?> <?echo date("Y", mktime($arItem["DATE_ACTIVE_TO"]));?></div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover"><?=$arItem["NAME"]?></a>
                            </div>
                            <div class="event-list-item-desc">
                                <?=$arItem["PREVIEW_TEXT"]?>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="animate-custom gray-small-link">Подробнее</a>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
            <div class="bottom-part clearfix">
                <a class="on-right" href="#">Архив акций</a>
            </div>
        </div>
    </div><!-- /Events List -->
<?endif;?>

<?
    function getRusMonth($month){
      if($month > 12 || $month < 1) return FALSE;
      $aMonth = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
      return $aMonth[$month - 1];
    }
?>