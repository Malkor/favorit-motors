<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<div class="tabs-navigation section-gray">
		<div class="box-container">
			<ul class="tabs three-tabs clearfix active-gray">
				<?foreach($arResult as $arItem):
					if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
						continue;
				?>
					<?if($arItem["SELECTED"]):?>
						<li class="active"><a href="<?=$arItem["LINK"]?>" class="animate-custom"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li><a href="<?=$arItem["LINK"]?>" class="animate-custom"><?=$arItem["TEXT"]?></a></li>
					<?endif?>
					
				<?endforeach?>
			</ul>
	    </div>
	</div>
<?endif?>