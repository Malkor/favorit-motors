<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<?$this->SetViewTarget('catalog_show_paginator_links');?>
  <?$request_uri=strpos($_SERVER['REQUEST_URI'], "?");?>
  <?$path=substr($_SERVER['REQUEST_URI'], 0, $request_uri);?>
	<?if($arResult["NavPageCount"] > 1 && $arResult["NavPageNomer"] == 1):?>
		<link rel="next" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=2">
	<?endif;?>
	<?if($arResult["NavPageCount"] > 1 && $arResult["NavPageNomer"] > 1):?>
		<meta name="robots" content="noindex, follow"/>
		<link rel="prev" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=<?=$arResult["NavPageNomer"]-1?>">
		<?if($arResult["NavPageCount"] != $arResult["NavPageNomer"]):?>
			<link rel="next" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=<?=$arResult["NavPageNomer"]+1?>">
		<?endif;?>
	<?endif;?>
<?$this->EndViewTarget();?> 

<div class="paginator">
	<?if($arResult["bDescPageNumbering"] === true):?>
		<div style="display:none;"><?=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]?><br /></div>
		<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["bSavePage"]):?>
				<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_begin")?></a>
			<?else:?>
				<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
					<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"></a>
				<?else:?>
					<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
				<?endif?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_begin")?></a>
			<?endif?>
		<?endif?>
		<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
			<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<b class="paginator__link paginator__link--current"><?=$NavRecordGroupPrint?></b>
			<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a>
			<?else:?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
			<?endif?>
			<?$arResult["nStartPage"]--?>
		<?endwhile?>
		<?if ($arResult["NavPageNomer"] > 1):?>
			<a class="paginator__arr paginator__arr--right" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
			<a class="paginator__arr paginator__arr--right" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_end")?></a>
		<?endif?>
	<?else:?>
		<div style="display:none;"><?=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]?><br /></div>
		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["bSavePage"]):?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_begin")?></a>
				<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
			<?else:?>
				<?if ($arResult["NavPageNomer"] > 2):?>
					<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
				<?else:?>
					<a class="paginator__arr paginator__arr--left" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"></a>
				<?endif?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_begin")?></a>
			<?endif?>
		<?endif?>
		<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<?if($arResult["NavPageCount"] != 1):?>
					<b class="paginator__link paginator__link--current"><?=$arResult["nStartPage"]?></b>
				<?endif;?>
			<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
			<?else:?>
				<a class="paginator__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
			<?endif?>
			<?$arResult["nStartPage"]++?>
			<?endwhile?>
		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<a class="paginator__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_end")?></a>
			<a class="paginator__arr paginator__arr--right" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
		<?endif?>
	<?endif?>

	<?if ($arResult["bShowAll"]):?>
	<noindex>
		<?if ($arResult["NavShowAll"]):?>
			<!-- <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><?=GetMessage("nav_paged")?></a> -->
		<?else:?>
			<a class="show-all-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><?=GetMessage("nav_all")?></a>
		<?endif?>
	</noindex>
	<?endif?>
</div>