<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<?$this->SetViewTarget('articles_show_paginator_links');?>
  <?$request_uri=strpos($_SERVER['REQUEST_URI'], "?");?>
  <?$path=substr($_SERVER['REQUEST_URI'], 0, $request_uri);?>
	<?if($arResult["NavPageCount"] > 1 && $arResult["NavPageNomer"] == 1):?>
		<link rel="next" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=2">
	<?endif;?>
	<?if($arResult["NavPageCount"] > 1 && $arResult["NavPageNomer"] > 1):?>
		<meta name="robots" content="noindex, follow"/>
		<link rel="prev" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=<?=$arResult["NavPageNomer"]-1?>">
		<?if($arResult["NavPageCount"] != $arResult["NavPageNomer"]):?>
			<link rel="next" href="<?=$_SERVER['SERVER_NAME']?><?=$path?>?PAGEN_1=<?=$arResult["NavPageNomer"]+1?>">
		<?endif;?>
	<?endif;?>
<?$this->EndViewTarget();?> 

<div class="pagination">
	<ul class="pagination-list">
	<?if($arResult["bDescPageNumbering"] === true):?>
		<div style="display:none;"><?=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]?><br /></div>
		<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["bSavePage"]):?>
				<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><i class="fa fa-angle-left"></i></a></li>
				<li><a class="animate-custom" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_begin")?></a></li>
			<?else:?>
				<?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
					<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><i class="fa fa-angle-right"></i></a></li>
				<?else:?>
					<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><i class="fa fa-angle-left"></i></a></li>
				<?endif?>
				<li><a class="animate-custom pagination-firsl" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_begin")?></a></li>
			<?endif?>
		<?endif?>
		<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
			<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<li><a class="animate-custom active"><?=$NavRecordGroupPrint?></a></li>
			<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
				<li><a class="animate-custom" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a></li>
			<?else:?>
				<li><a class="animate-custom" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a></li>
			<?endif?>
			<?$arResult["nStartPage"]--?>
		<?endwhile?>
		<?if ($arResult["NavPageNomer"] > 1):?>
			<li><a class="animate-custom pagination-next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="fa fa-angle-right"></i></a></li>
			<li><a class="animate-custom pagination-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_end")?></a></li>
		<?endif?>
	<?else:?>
		<div style="display:none;"><?=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]?><br /></div>
		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["bSavePage"]):?>
				<li><a class="animate-custom pagination-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_begin")?></a></li>
				<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="fa fa-angle-left"></a></li>
			<?else:?>
				<?if ($arResult["NavPageNomer"] > 2):?>
					<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><i class="fa fa-angle-left"></i></a></li>
				<?else:?>
					<li><a class="animate-custom pagination-prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><i class="fa fa-angle-left"></i></a></li>
				<?endif?>
				<li><a class="animate-custom pagination-last" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_begin")?></a></li>
			<?endif?>
		<?endif?>
		<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<?if($arResult["NavPageCount"] != 1):?>
					<li><a class="animate-custom active"><?=$arResult["nStartPage"]?></a></li>
				<?endif;?>
			<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
				<li><a class="animate-custom" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></li>
			<?else:?>
				<li><a class="animate-custom" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></li>
			<?endif?>
			<?$arResult["nStartPage"]++?>
			<?endwhile?>
		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<li><a class="animate-custom pagination-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_end")?></a></li>
			<li><a class="animate-custom pagination-next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><i class="fa fa-angle-right"></i></a></li>
		<?endif?>
	<?endif?>

	<?if ($arResult["bShowAll"]):?>
	<noindex>
		<?if ($arResult["NavShowAll"]):?>
			<!-- <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><?=GetMessage("nav_paged")?></a> -->
		<?else:?>
			<li><a class="show-all-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><?=GetMessage("nav_all")?></a></li>
		<?endif?>
	</noindex>
	<?endif?>
	</ul>
</div>

<div><a href="#" class="animate-custom">Архив статей</a></div>