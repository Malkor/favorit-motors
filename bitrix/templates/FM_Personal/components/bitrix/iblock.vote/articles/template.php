<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

 <div class="likes-buttons on-center gray-text">
	<form method="post" action="<?=POST_FORM_ACTION_URI?>">
		<select name="rating" style="display:none;">
			<option value="1">1</option>
		</select>
		<?echo bitrix_sessid_post();?>
		<input type="hidden" name="rating" value="1" />
		<input type="hidden" name="back_page" value="<?=$arResult["BACK_PAGE_URL"]?>" />
		<input type="hidden" name="vote_id" value="<?=$arResult["ID"]?>" />
		<input type="submit"  class="animate-custom" name="vote" value="">Статья была полезна</a><span>(<?$arResult["VOTES"]?> человек)</span></input></div>
	</form>
</div>

<?/*<div class="dislike-button"><a href="#" class="animate-custom"><i class="fa fa-thumbs-down animate-custom"></i>Статья не понравилась</a><span>(1 упырь)</span></div>*/?>