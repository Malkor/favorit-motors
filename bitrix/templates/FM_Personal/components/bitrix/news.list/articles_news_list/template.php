<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="section section-sec">
    <div class="box-container">
        <div class="post-items">
        	<?echo "<!--";?>
	        	<?foreach($arResult["ITEMS"] as $arItem):?>
	        		<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
	        		<?echo "--><div class=\"post-item\" id=\"".$this->GetEditAreaId($arItem['ID'])."\">";?>
	                <div class="post-item-image">
	                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>'); "></a>
	                </div>
	                <div class="post-item-title">
	                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="red-hover animate-custom"><?=$arItem["NAME"]?></a>
	                </div>
	                <div class="post-item-desc align-justify"><?=$arItem["PREVIEW_TEXT"]?></div>
	                <span class="post-item-info clearfix border-bottom animate-custom">
	                    <span class="on-left">просмотров</span>
	                    <span class="on-right"><?if($arItem["SHOW_COUNTER"] != ""):echo $arItem["SHOW_COUNTER"];else:echo "0";endif;?></span>
	                </span>
	            	<?echo "</div><!--";?>
	            <?endforeach;?>
	        <?echo "--></div>";?>
  			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<?=$arResult["NAV_STRING"]?><br /><br />
			<?endif;?>
       
    </div>
</div>