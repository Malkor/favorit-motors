<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="addres-block on-right">
  <div class="address-button animate-custom opacity-custom-hover">
    <div class="address-button-icon bg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/f.png'); "></div>
    <div class="address-button-text">Адреса салонов</div>
  </div>
  <div class="address-map">
    <ul class="salon-list">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<li style="top: <?=$arItem["PROPERTIES"]["POSITION_MODELS_TOP"]["VALUE"]?>px; left: <?=$arItem["PROPERTIES"]["POSITION_MODELS_LEFT"]["VALUE"]?>px;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a href="#">
			      <span class="salon-icon bg animate-custom" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/f.png'); "></span>
			      <span class="salon-name animate-custom red-small-link"><?=$arItem["PROPERTIES"]["METRO"]["VALUE"]?></span>
			  	</a>
			 	</li>
			<?endforeach;?>
		</ul>
	<div class="close-address-map"><i class="fa fa-times"></i>Закрыть</div>
	</div>
</div>