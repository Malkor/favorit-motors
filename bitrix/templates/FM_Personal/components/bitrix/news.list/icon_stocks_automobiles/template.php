<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?echo '<div class="event-deal-items"><!--';?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    
    <?echo '--><a id="'.$this->GetEditAreaId($arItem['ID']).'" href="'.$arItem["PROPERTIES"]["LINK"]["VALUE"].'" class="event-deal-item clearfix">
                    <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-'.$arItem["PROPERTIES"]["ICON_FONT_AWESOME"]["VALUE"].'"></i></span>
                    <span class="event-deal-item-text on-left animate-custom">'.$arItem["~NAME"].'</span>
                </a><!--';?>
<?endforeach;?>

<?echo '--></div>';?>