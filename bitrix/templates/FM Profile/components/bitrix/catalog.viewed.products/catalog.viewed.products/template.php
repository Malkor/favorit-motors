<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main;
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;
use \Bitrix\Main\Text\String as String;
use \Bitrix\Main\Localization\Loc as Loc;
use \Bitrix\Main\SystemException as SystemException;

/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

   global $APPLICATION;
   global $USER;
   $user_id = CSaleBasket::GetBasketUserID();
    if($user_id  > 0)
   {
      $product_id = $arResult['ID'];
      CatalogViewedProductTable::refresh($product_id, $user_id);
   }

$frame = $this->createFrame()->begin();

if (!empty($arResult['ITEMS']))
{
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
	);

	$i = 0;?>
  <div class="watched-late">
      Недавно просмотренные: [ <span class="red-text"><?=count($arResult['ITEMS'])?></span> ]
      <div class="watched-late__pop">
          <table class="watched-late__listtable">
						<?foreach ($arResult['ITEMS'] as $key => $arItem)
						{
							if($i>4)
								break;
							$i++;
							$strMainID = $this->GetEditAreaId($arItem['ID'] . $key);
							$arItemIDs = array(
								'ID' => $strMainID,
								'PICT' => $strMainID . '_pict',
								'SECOND_PICT' => $strMainID . '_secondpict',
								'MAIN_PROPS' => $strMainID . '_main_props',

								'QUANTITY' => $strMainID . '_quantity',
								'QUANTITY_DOWN' => $strMainID . '_quant_down',
								'QUANTITY_UP' => $strMainID . '_quant_up',
								'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
								'BUY_LINK' => $strMainID . '_buy_link',
								'SUBSCRIBE_LINK' => $strMainID . '_subscribe',

								'PRICE' => $strMainID . '_price',
								'DSC_PERC' => $strMainID . '_dsc_perc',
								'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

								'PROP_DIV' => $strMainID . '_sku_tree',
								'PROP' => $strMainID . '_prop_',
								'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
								'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
							);

							$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
							$strTitle = (
							isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
								? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
								: $arItem['NAME']
							);
							$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";
							?>

							<tr>

					      <td class="watched-late__listtable__ltd"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?=$arItem['PROPERTIES']['CARBRAND']['VALUE']?> <?=$arItem['PROPERTIES']['CARMODEL']['VALUE'];?></a></td>
					      <td class="watched-late__listtable__rtd"><?=$arItem['PROPERTIES']['PRICE']['VALUE']?></td>

					  	</tr>

						
							<?
								if (!empty($arItem['MIN_PRICE']))
								{
									if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
									{
										echo GetMessage(
											'CVP_TPL_MESS_PRICE_SIMPLE_MODE',
											array(
												'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
												'#MEASURE#' => GetMessage(
													'CVP_TPL_MESS_MEASURE_SIMPLE_MODE',
													array(
														'#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
														'#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
													)
												)
											)
										);
									}
									else
									{
										echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
									}
									if ('Y' == $arParams['SHOW_OLD_PRICE'] && $arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE'])
									{
										?> <span style="color: #a5a5a5;font-size: 12px;font-weight: normal;white-space: nowrap;text-decoration: line-through;"><? echo $arItem['MIN_PRICE']['PRINT_VALUE']; ?></span><?
									}
								}
								?>
							<?
						}
						?>
					</table>
          <a class="watched-late__show-all-link" href="#">Показать все</a>
      </div>
  </div>

	<script type="text/javascript">
		BX.message({
			MESS_BTN_BUY: '<? echo ('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CVP_TPL_MESS_BTN_BUY')); ?>',
			MESS_BTN_ADD_TO_BASKET: '<? echo ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CVP_TPL_MESS_BTN_ADD_TO_BASKET')); ?>',

			MESS_BTN_DETAIL: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',

			MESS_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',
			BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			ADD_TO_BASKET_OK: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			TITLE_ERROR: '<? echo GetMessageJS('CVP_CATALOG_TITLE_ERROR') ?>',
			TITLE_BASKET_PROPS: '<? echo GetMessageJS('CVP_CATALOG_TITLE_BASKET_PROPS') ?>',
			TITLE_SUCCESSFUL: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CVP_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_CLOSE') ?>'
		});
	</script>
<?}?>
<?$frame->beginStub();?>
<?$frame->end();?>