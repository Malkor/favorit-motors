<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (strlen($arResult["PAGE_URL"]) > 0):?>
	<div class="share-post on-right clearfix">
		<span class="on-left gray-text">Поделиться статьёй</span>
		<?if (is_array($arResult["BOOKMARKS"]) && count($arResult["BOOKMARKS"]) > 0){?>
			<?foreach($arResult["BOOKMARKS"] as $name => $arBookmark){?>
				<span class="social-icon-link on-left"><?=$arBookmark["ICON"]?></span>
			<?}?>
		<?}?>
	</div>		
<?else:?>
	<?=GetMessage("SHARE_ERROR_EMPTY_SERVER")?>
<?endif;?>