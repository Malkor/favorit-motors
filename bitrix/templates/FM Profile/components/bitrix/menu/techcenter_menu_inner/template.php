<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="tech-center-menu">
        <ul class="tech-center-menu-list align-left">

        <?
        foreach($arResult as $arItem):
            if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                continue;
            ?>
            <?if($arItem["SELECTED"]):?>
            <li  class="active"><a href="<?=$arItem["LINK"]?>" class="animate-custom"><span><?=$arItem["TEXT"]?></span></a></li>
        <?else:?>
            <li><a href="<?=$arItem["LINK"]?>" class="animate-custom"><span><?=$arItem["TEXT"]?></span></a></li>
        <?endif?>

        <?endforeach?>

    </ul>
</div>
<?endif?>