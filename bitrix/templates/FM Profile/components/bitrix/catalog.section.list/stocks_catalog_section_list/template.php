<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<?echo '<div class="brand-items"><!--';

if (0 < $arResult["SECTIONS_COUNT"])
{
	$intCurrentDepth = 1;
	$boolFirst = true;
	foreach ($arResult['SECTIONS'] as &$arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
		
		echo '--><a id="'.$this->GetEditAreaId($arSection["ID"]).'" href="'.$arSection["SECTION_PAGE_URL"].'" class="animate-custom opacity-custom-hover brand-item"><img src="'.$arSection["PICTURE"]["SRC"].'" alt="'.$arSection["NAME"].'"/></a><!--';
		 
	}
}
?>

<?echo '--></div>';?>
