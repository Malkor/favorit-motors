<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

        <?echo '--><div class="accessories-thumb-item"  id="'.$this->GetEditAreaId($arItem['ID']).'">';?>
            <?echo '<div class="accessories-thumb-item-image">';?>
                <?echo '<a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url('.$arItem["PREVIEW_PICTURE"]["SRC"].')"></a>';?>
            <?echo '</div>';?>
            <?echo '<div class="accessories-thumb-item-title">';?>
                <?echo '<a href="#" class="animate-custom red-hover">'.$arItem['NAME'].'</a>';?>
            <?echo '</div>';?>
            <?echo '<div class="accessories-thumb-item-desc gray-text">';?>
                <?echo $arItem['PREVIEW_TEXT'];?>
            <?echo '</div>';?>
            <?echo '<div class="accessories-thumb-item-info clearfix">';?>
            <?echo '<div class="on-left">';?>
                <?echo '<div class="accessories-thumb-item-price">'.$arItem['PROPERTIES']['COST']['VALUE'].' р.</div>';?>
            <?echo '</div>';?>
            <?echo '<div class="on-right">';?>
                <?echo '<a href="#" class="animate-custom red-small-link">Заказать</a>';?>
             <?echo '</div>';?>
            <?echo '</div>';?>
        <?echo '</div><!--';?>

    <?endforeach;?>
