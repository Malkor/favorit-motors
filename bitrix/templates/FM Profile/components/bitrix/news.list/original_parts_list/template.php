<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div id="accessories-items" class="accessories-items">

    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

        <div class="accessories-item section-small border-top border-bottom clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="accessories-item-image">
                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></a>
            </div>
            <div class="accessories-item-info">
                <div class="accessories-item-category">
                    <a class="animate-custom red-hover" href="?SECTION_ID=<?=$arItem["IBLOCK_SECTION_ID"]?>"><?=$arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"];?></a>
                </div>
                <div class="accessories-item-name clearfix">
                    <a class="on-left animate-custom red-hover" href="#"><?=$arItem["NAME"]?></a> <span class="on-right accessories-item-price"><?=$arItem["PROPERTIES"]["COST"]["VALUE"]?> р.</span>
                </div>
                <div class="accessories-item-desc">
                    <p>
                        <?=$arItem["PREVIEW_TEXT"]?>
                    </p>
                </div>
            </div>
            <div class="accessories-item-button">
                <a class="animate-custom red-small-link" href="#">Заказать</a>
            </div>
        </div>

    <?endforeach;?>

</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>



