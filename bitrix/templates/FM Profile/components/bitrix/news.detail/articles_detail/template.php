<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

 <div class="section section-sec">
    <div class="box-container">
        <div class="title clearfix">
            <h2 class="on-left"><?=$arResult["NAME"]?></h2>
            
            <?if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y"){?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:asd.share.buttons", 
                    "share", 
                    array(
                        "ASD_ID" => $_REQUEST["id"],
                        "ASD_TITLE" => $arResult["NAME"],
                        "ASD_URL" => $arResult["DETAIL_PAGE_URL"],
                        "ASD_PICTURE" => $arResult["PREVIEW_PICTURE"]["SRC"],
                        "ASD_TEXT" => $arResult["PREVIEW_TEXT"],
                        "ASD_LINK_TITLE" => "Расшарить в #SERVICE#",
                        "ASD_SITE_NAME" => "",
                        "ASD_INCLUDE_SCRIPTS" => array(
                        )
                    ),
                    false
                );?>
            <?}?>
        </div>

        <div class="body clearfix align-justify border-bottom">

            <a href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="body-image colorbox animate-custom opacity-custom-hover show-on-left">
                <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" />
            </a>

            <?=$arResult["DETAIL_TEXT"];?>
        </div>

        <?
        $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_TYPE_VOTE"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>32, "PROPERTY_ARTICLE_ID"=>$arResult["ID"], "ACTIVE"=>"Y");
        $ar_res = array();
        $res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
        while($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            if ($arFields["PROPERTY_TYPE_VOTE_ENUM_ID"] == 8232) {
                $ar_res["GOOD"][$arFields["ID"]] = $arFields;
            }
            if ($arFields["PROPERTY_TYPE_VOTE_ENUM_ID"] == 8233) {
                $ar_res["BAD"][$arFields["ID"]] = $arFields;
            }
        }

        ?>

         <div class="likes-buttons on-center gray-text">
            <div id="good_votes" class="like-button right-margin">
                <a href="javascript:void(0)" class="animate-custom" data-vote-type="good" data-article-id="<?=$arResult["ID"]?>" data-count="<?=count($ar_res["GOOD"])?>">
                    <i class="fa fa-thumbs-up animate-custom"></i>Статья была полезна
                </a>
                <span>(<?echo get_correct_str(count($ar_res["BAD"]), "человеку", "людям", "человек")?>)</span>
            </div>
            <div id="bad_votes" class="dislike-button">
                <a href="javascript:void(0)" class="animate-custom" data-vote-type="bad" data-article-id="<?=$arResult["ID"]?>" data-count="<?=count($ar_res["BAD"])?>">
                    <i class="fa fa-thumbs-down animate-custom"></i>Статья не понравилась</a>
                <span>(<?echo get_correct_str(count($ar_res["BAD"]), "человеку", "людям", "человек")?>)</span>
            </div>
        </div>
    </div>
</div>