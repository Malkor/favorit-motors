<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
?>

<?
CModule::IncludeModule('iblock');

$ar_model = 0;
$rs_brand_model = CIBlockSection::GetList(
	array('DEPTH_LEVEL' => 'asc'),
	array('IBLOCK_ID' => 26, "NAME" => array("FABIA")),
	false,
	array('ID')
);
if($ar_brand_model = $rs_brand_model->GetNext())
{
	$ar_model = $ar_brand_model["ID"];
}

$rsParentSection = CIBlockSection::GetByID($ar_model);
if ($arParentSection = $rsParentSection->GetNext())
{
	$arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
	$rs_Section = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'desc', 'sort'=>'asc'),$arFilter,false,array('ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'SORT', 'PICTURE', 'UF_VIDEO_ID', 'UF_VIDEO_TITLE', 'UF_VIDEO_DESC'));
   	$ar_SectionList = array();
	$ar_DepthLavel = array();
	while($ar_Section = $rs_Section->GetNext(true, false))
	{
		$ar_SectionList[$ar_Section['ID']] = $ar_Section;
		$ar_DepthLavel[] = $ar_Section['DEPTH_LEVEL'];
	}

	$ar_DepthLavelResult = array_unique($ar_DepthLavel);
	rsort($ar_DepthLavelResult);

	$i_MaxDepthLevel = $ar_DepthLavelResult[0];

	for( $i = $i_MaxDepthLevel; $i > 1; $i-- )
	{
		foreach ( $ar_SectionList as $i_SectionID => $ar_Value )
		{
		 	if($ar_Value['DEPTH_LEVEL'] == $i){
				$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]] = $ar_Value;
				$res = CIBlockElement::GetList(array("SORT"=>"ASC"), array('IBLOCK_ID' => 26, "SECTION_ID"=>$ar_Value['ID']), false, array(), array("ID", "NAME", "CODE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PICTURE", "PROPERTY_COLOR"));
				
				while($ar_fields = $res->GetNextElement()){
					$ar_res = $ar_fields->GetFields();
					$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]]['ELEMENTS'][$ar_res["ID"]] = $ar_res;
					
					if($ar_res["PROPERTY_COLOR_VALUE"] != ""){
						$color_res = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 22, "ID" => $ar_res["PROPERTY_COLOR_VALUE"]), false, array(), array("ID", "NAME", "CODE", "PROPERTY_HEXCODE", "PROPERTY_DESC"));

						while($ar_color_res = $color_res->GetNextElement()){
							$arFields = $ar_color_res->GetFields();  
							$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]]['ELEMENTS'][$ar_res["ID"]]["COLOR_NAME"] = $arFields["NAME"];
							$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]]['ELEMENTS'][$ar_res["ID"]]["COLOR_CODE"] = $arFields["CODE"];
							$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]]['ELEMENTS'][$ar_res["ID"]]["COLOR_HEXCODE"] = $arFields["PROPERTY_HEXCODE_VALUE"];
							$ar_SectionList[$ar_Value['IBLOCK_SECTION_ID']]['SUB_SECTION'][$ar_Value["ID"]]['ELEMENTS'][$ar_res["ID"]]["COLOR_DESC"] = $arFields["PROPERTY_DESC_VALUE"];
						}
					}
				}
				unset( $ar_SectionList[$i_SectionID] );
			}
		}
	}

	function __sectionSort($a, $b) {
		if ($a['SORT'] == $b['SORT']) {
			return 0;
		}
		return ($a['SORT'] < $b['SORT']) ? -1 : 1;
	}

	usort($ar_SectionList, "__sectionSort");
}?>

<pre><?//print_r($ar_SectionList)?></pre>

    <!-- Content -->
    <section id="content">
        <!-- Super Slider -->
        <div class="super-slider">
            <div class="box-container">
                <div class="left-side">
                    <div class="super-slider-nav-tabs">
                        <ul>
                        	<?foreach ($ar_SectionList[0]["SUB_SECTION"] as $key => $value) {?>
                    			<li <?if(strtolower($value["NAME"]) == "кузов"):?>class="active"<?endif;?>>
	                                <a class="animate-custom" data-block="#<?=$value["CODE"]?>">

	                        		<?
	                        		switch (strtolower($value["NAME"])) {
									    case "кузов":
									        echo '<span class="circle-icon"><i class="fa fa-car animate-custom"></i></span>';
									        break;
									    case "галерея":
									        echo '<span class="circle-icon"><i class="fa fa-picture-o animate-custom"></i></span>';
									        break;
									    case "цвета":
									        echo '<span class="circle-icon"><i class="fa fa-eyedropper animate-custom"></i></span>';
									        break;
									    case "видео":
									    	echo '<span class="circle-icon"><i class="fa fa-play animate-custom"></i></span>';
									        break;
									}
									?>

								 	<span><?=strtolower($value["NAME"])?></span>
	                                </a>
	                            </li>
	                        <?}?>
                        </ul>
                    </div>
                </div>

                <div class="right-side">
                    <div class="this-car-info">
                        <div class="this-car-info-name"><?=$arResult['PROPERTIES']['CARBRAND']['VALUE']?> <?=$arResult['PROPERTIES']['CARMODEL']['VALUE']?></div>
                        <div class="this-car-info-price"><span><?=$arResult['PROPERTIES']['PRICE']['VALUE']?></span> р.</div>
                    </div>
                    <div class="this-car-some-links">
                        <ul>
                            <li><a href="#">Калькулятор КАСКО</a></li>
                            <li><a href="#">Рассчитать кредит</a></li>
                        </ul>
                    </div>
                    <div class="this-car-big-links">
                        <ul>
                            <li><a href="#">Конфигуратор</a></li>
                        </ul>
                    </div>
                    <div class="this-car-buttons-links">
                        <ul>
                            <li><a class="red-small-link animate-custom use-popup" href="#" data-block="test-drive">Тест-драйв</a></li>
                            <li><a class="green-small-link animate-custom" href="#">Заказать автомобиль</a></li>
                        </ul>
                    </div>
                    <div class="this-car-sell">
                        <a href="#" class="animate-custom clearfix">
                            <span class="circle-icon on-left"><i class="fa fa-thumbs-up animate-custom"></i></span>
                            <span class="on-right align-left">продать авто выгодно</span>
                        </a>
                    </div>
                </div>

                <div class="breadcrumbs clearfix">
                    <div class="on-left">
                       	<?$APPLICATION->IncludeComponent(
				            "bitrix:breadcrumb",
				            "",
				            Array(
				              "COMPONENT_TEMPLATE" => ".default",
				              "START_FROM" => "1",
				              "PATH" => "",
				              "SITE_ID" => "-"
				            ),
				            $component
				        );?>
                    </div>

                    <?
			        global $arrFilter;
			        $arrFilter = array("PROPERTY_BRAND_VALUE"=>strtolower($arResult["PROPERTIES"]["CARBRAND"]["VALUE"]));
			        ?>

			        <?$APPLICATION->IncludeComponent("bitrix:news.list", "model_salons_list", Array(
			          "COMPONENT_TEMPLATE" => ".default",
			            "IBLOCK_TYPE" => "references",  // Тип информационного блока (используется только для проверки)
			            "IBLOCK_ID" => "12",  // Код информационного блока
			            "NEWS_COUNT" => "20", // Количество новостей на странице
			            "SORT_BY1" => "ACTIVE_FROM",  // Поле для первой сортировки новостей
			            "SORT_ORDER1" => "DESC",  // Направление для первой сортировки новостей
			            "SORT_BY2" => "SORT", // Поле для второй сортировки новостей
			            "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
			            "FILTER_NAME" => "arrFilter", // Фильтр
			            "FIELD_CODE" => array(  // Поля
			              0 => "NAME",
			              1 => "",
			            ),
			            "PROPERTY_CODE" => array( // Свойства
			              0 => "METRO",
			              1 => "BRAND",
			              2 => "",
			            ),
			            "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
			            "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			            "AJAX_MODE" => "N", // Включить режим AJAX
			            "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
			            "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
			            "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
			            "CACHE_TYPE" => "A",  // Тип кеширования
			            "CACHE_TIME" => "36000000", // Время кеширования (сек.)
			            "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
			            "CACHE_GROUPS" => "Y",  // Учитывать права доступа
			            "PREVIEW_TRUNCATE_LEN" => "", // Максимальная длина анонса для вывода (только для типа текст)
			            "ACTIVE_DATE_FORMAT" => "d.m.Y",  // Формат показа даты
			            "SET_TITLE" => "N", // Устанавливать заголовок страницы
			            "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
			            "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
			            "SET_META_DESCRIPTION" => "N",  // Устанавливать описание страницы
			            "SET_STATUS_404" => "N",  // Устанавливать статус 404, если не найдены элемент или раздел
			            "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
			            "ADD_SECTIONS_CHAIN" => "N",  // Включать раздел в цепочку навигации
			            "HIDE_LINK_WHEN_NO_DETAIL" => "N",  // Скрывать ссылку, если нет детального описания
			            "PARENT_SECTION" => "", // ID раздела
			            "PARENT_SECTION_CODE" => "",  // Код раздела
			            "INCLUDE_SUBSECTIONS" => "N", // Показывать элементы подразделов раздела
			            "DISPLAY_DATE" => "N",  // Выводить дату элемента
			            "DISPLAY_NAME" => "N",  // Выводить название элемента
			            "DISPLAY_PICTURE" => "N", // Выводить изображение для анонса
			            "DISPLAY_PREVIEW_TEXT" => "N",  // Выводить текст анонса
			            "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
			            "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
			            "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
			            "PAGER_TITLE" => "Новости", // Название категорий
			            "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
			            "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
			            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
			            "PAGER_SHOW_ALL" => "N",  // Показывать ссылку "Все"
			          ),
			          false
			        );?>
                </div>

                <div class="super-slider-containers">
                    <?foreach ($ar_SectionList[0]["SUB_SECTION"] as $section) {?>
                       	<?if(strtolower($section["NAME"]) == "кузов"):?>
                       		<div id="body" class="super-slider-container">
	                       		<div class="body-navigation tabs-inner-navigation">
		                            <ul class="clearfix">
		                            	<?$a = 1;?>
		                            	<?foreach ($section["SUB_SECTION"] as $sub_section_value) {?>
			                                <li <?if($a == 1):?>class="active"<?endif;?>><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-<?=$sub_section_value['CODE']?>"><?=$sub_section_value['NAME']?></a></li>
			                               	<?$a++;?>
		                                <?}?>
		                            </ul>
		                        </div>
		                        <?foreach ($section["SUB_SECTION"] as $sub_section_value) {?>
			                        <div id="slider-<?=$sub_section_value["CODE"]?>" class="body-navigation-container">
			                            <div class="body-<?=$sub_section_value["CODE"]?>-slider body-slider swiper-container">
			                                <div class="swiper-wrapper">
			                                	<?foreach ($sub_section_value['ELEMENTS'] as $element) {?>
				                                    <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($element["PREVIEW_PICTURE"])?>'); ">
				                                        <a href="<?=CFile::GetPath($element["PREVIEW_PICTURE"])?>" rel="gallery-sedan" class="colorbox"></a>
				                                    </div>
			                                    <?}?>
			                                </div>
			                                <!-- Add Arrows -->
			                                <div class="swiper-button-next"></div>
			                                <div class="swiper-button-prev"></div>
			                            </div>
			                        </div>
	                        	<?}?>
	                        </div>
                       	<?endif;?>
	                       	<?if(strtolower($section["NAME"]) == "галерея"):?>
	                       		<div id="gallery" class="super-slider-container">
	                       			<?foreach ($section["SUB_SECTION"] as $sub_section_value):?>
	                       				<?if (strtolower($sub_section_value["NAME"]) == "экстерьер"):?>
	                       					<div id="slider-exterior" class="gallery-navigation-container">
					                            <div class="body-slider gallery-exterior-top">
					                                <div class="swiper-wrapper">
					                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
						                                    <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($element["DETAIL_PICTURE"])?>'); ">
						                                        <a href="<?=CFile::GetPath($element["DETAIL_PICTURE"])?>" rel="gallery-exterior" class="colorbox"></a>
						                                    </div>
					                                   <?endforeach;?>
					                                </div>
					                            </div>
					                            <div class="swiper-container gallery-rotation-slider with-text gallery-exterior-thumbs">
					                                <div class="swiper-wrapper">
					                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
						                                    <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($element["DETAIL_PICTURE"])?>'); ">
						                                        <div class="stider-text">
						                                            <h3><?=$element["NAME"]?></h3>
						                                            <p><?=$element["PREVIEW_TEXT"]?></p>
						                                        </div>
						                                    </div>
					                                    <?endforeach;?>
					                                </div>
					                                <!-- Add Arrows -->
					                                <div class="swiper-button-next"></div>
					                                <div class="swiper-button-prev"></div>
					                            </div>
					                        </div>
	                       				<?endif?>
	                       				<?if (strtolower($sub_section_value["NAME"]) == "интерьер"):?>
	                       					<div id="slider-interior" class="gallery-navigation-container">
					                            <div class="body-slider gallery-inter-top">
					                                <div class="swiper-wrapper">
					                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
						                                    <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($element["DETAIL_PICTURE"])?>'); ">
						                                        <a href="<?=CFile::GetPath($element["DETAIL_PICTURE"])?>" rel="gallery-interior" class="colorbox"></a>
						                                    </div>
					                                    <?endforeach;?>
					                                </div>
					                            </div>
					                            <div class="swiper-container gallery-rotation-slider with-text gallery-inter-thumbs">
					                                <div class="swiper-wrapper">
					                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
						                                    <div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($element["DETAIL_PICTURE"])?>'); ">
						                                        <div class="stider-text">
						                                            <h3><?=$element["NAME"]?></h3>
						                                            <p><?=$element["PREVIEW_TEXT"]?></p>
						                                        </div>
						                                    </div>
					                                    <?endforeach;?>
					                                   
					                                </div>
					                                <!-- Add Arrows -->
					                                <div class="swiper-button-next"></div>
					                                <div class="swiper-button-prev"></div>
					                            </div>
					                        </div>
	                       				<?endif?>
	                       			<?endforeach;?>

			                        <div class="gallery-navigation tabs-inner-navigation">
			                            <ul class="clearfix">
			                                <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-exterior">Экстерьер</a></li>
			                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-interior">Интерьер</a></li>
			                            </ul>
			                        </div>

			                    </div>
	                       	<?endif;?>
	                       	<?if(strtolower($section["NAME"]) == "цвета"):?>
	                       		<div id="colors" class="super-slider-container">
	                       			 <div class="colors-navigation tabs-inner-navigation">
	                       			 	<ul class="clearfix">
	                       			 		<li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-all">Все</a></li>
			                       			<?foreach ($section["SUB_SECTION"] as $sub_section_value):?>
			                       				<li><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-<?=$sub_section_value["CODE"]?>"><?=$sub_section_value["NAME"]?></a></li>
			                       			<?endforeach;?>
			                            </ul>
			                        </div>

			                        <div id="colors-all" class="colors-navigation-container">
			                            <div class="colors-all-slider body-slider swiper-container">
			                                <div class="swiper-wrapper">
			                                	<?foreach ($section["SUB_SECTION"] as $sub_section_value):?>
				                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
				                                    	<div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($section["PICTURE"])?>'); background-color: #<?=$element['COLOR_HEXCODE']?>;"></div>
				                                    <?endforeach;?>
				                                <?endforeach;?>
			                                </div>
			                            </div>

			                            <div class="colors-all-slider-thumb colors-thumb swiper-container">
			                                <div class="swiper-wrapper">
			                                    <?foreach ($section["SUB_SECTION"] as $sub_section_value):?>
				                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
					                                    <div class="swiper-slide">
					                                        <div class="color-square animate-custom" style="background: #<?=$element['COLOR_HEXCODE']?>"></div>
					                                        <div class="color-description"><?=$element["COLOR_DESC"]?></div>
					                                    </div>
					                                <?endforeach;?>
				                                <?endforeach;?>
			                                </div>
			                            </div>
			                        </div>

			                        <?foreach ($section["SUB_SECTION"] as $sub_section_value):?>
			                        	<div id="colors-<?=$sub_section_value['CODE']?>" class="colors-navigation-container">
				                            <div class="colors-<?=$sub_section_value['CODE']?>-slider body-slider swiper-container">
				                                <div class="swiper-wrapper">
				                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
				                                    	<div class="swiper-slide bg" style="background-image: url('<?=CFile::GetPath($section["PICTURE"])?>'); background-color: #<?=$element['COLOR_HEXCODE']?>;"></div>
				                                    <?endforeach;?>
				                                </div>
				                            </div>
				                            <div class="colors-<?=$sub_section_value['CODE']?>-slider-thumb colors-thumb swiper-container">
				                                <div class="swiper-wrapper">
				                                	<?foreach ($sub_section_value['ELEMENTS'] as $element):?>
					                                    <div class="swiper-slide">
					                                        <div class="color-square animate-custom" style="background: #<?=$element['COLOR_HEXCODE']?>"></div>
					                                        <div class="color-description"><?=$element["COLOR_DESC"]?></div>
					                                    </div>
				                                   	<?endforeach;?>
				                                </div>
				                            </div>
				                        </div>
			                        <?endforeach;?>
			                    </div>
	                       	<?endif;?>
	                       	<?if(strtolower($section["NAME"]) == "видео"):?>
	                       		<div id="video" class="super-slider-container">
			                        <div class="youtube animate-custom opacity-custom-hover" id="<?=$section["UF_VIDEO_ID"]?>"></div>
			                        <div class="text-element">
			                            <div class="text-title border-bottom">
			                                <h2><?=$section["UF_VIDEO_TITLE"]?></h2>
			                            </div>
			                            <div class="text-body">
			                                <p><?=$section["UF_VIDEO_DESC"]?></p>
			                            </div>
			                        </div>
			                    </div>
	                       	<?endif?>
	                    <?}?>
                </div>
            </div>
        </div><!-- /Super Slider -->

        <?
		$arCars = array();
		$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PROPERTY_CARBRAND", "PROPERTY_CARMODEL", "PROPERTY_YEAR", "PROPERTY_CARNAME", "PROPERTY_PRICE");
		$arFilter = Array("IBLOCK_ID"=>13, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_CARBRAND_VALUE"=>$arResult["PROPERTIES"]["CARBRAND"]["VALUE"], "PROPERTY_CARMODEL_VALUE"=>$arResult["PROPERTIES"]["CARMODEL"]["VALUE"]);
		$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
		while($ob = $res->GetNextElement())
		{
		  $arFields = $ob->GetFields();
		  $arCars[$arFields["ID"]] = $arFields;
		}
		?>
		<?if(count($arCars) > 0):?>
		  <!-- Cars -->
		  <div id="models-cars" class="section section-sec">
		    <div class="box-container">
		     <div class="title border-bottom">
		        <h2><?=$arResult['PROPERTIES']['CARBRAND']['VALUE']?> <?=$arResult['PROPERTIES']['CARMODEL']['VALUE']?> в наличии: <span class="red-text"><?=count($arCars);?></span></h2>
		      </div>
		      <?echo '<div class="new-auto-items four-in-line"><!--';?>
		      	<?$c = 1;?>
		        <?foreach($arCars as $car):?>
		        	<?if($c > 4):break;endif;?>
		          <?echo '--><div class="new-auto-item">';?>
		            <div class="new-auto-item-image">
		               <a href="<?=$car["DETAIL_PAGE_URL"]?>" class="animate-custom opacity-custom-hover bg" style="background-image: url('<?if($car["PREVIEW_PICTURE"]):echo $car["PREVIEW_PICTURE"];else:echo SITE_TEMPLATE_PATH."/images/no_photo.png";endif;?>');"></a>
		            </div>
		            <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom"><?=$car['PROPERTY_CARBRAND_VALUE']?> <?=$car['PROPERTY_CARMODEL_VALUE']?></a></div>
		            <div class="new-auto-item-desc">
		              <div class="new-auto-item-desc-field"><?if($car['PROPERTY_CARYEAR_VALUE']):echo $car['PROPERTY_CARYEAR_VALUE'].", ";endif;?><?=$car['PROPERTY_CARNAME_VALUE']?></div>
		              <!--<div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>-->
		            </div>
		            <div class="new-auto-item-info clearfix">
		              <div class="on-left">
		                <div class="new-auto-item-price-new"><?=$car['PROPERTY_PRICE_VALUE']?></div>
		                <div class="new-auto-item-price-old">1 320 000</div>
		              </div>
		              <div class="on-right">
		                <a href="<?=$car["DETAIL_PAGE_URL"]?>" class="animate-custom gray-small-link">Подробнее</a>
		              </div>
		            </div>
		          <?echo '</div><!--';?>
		          <?$c++;?>
		        <?endforeach;?>
		      <?echo '--></div>';?>
		      <div class="bottom-part clearfix">
		        <div class="on-right"><a href="#">Смотреть все</a></div>
		      </div>
		    </div>
		  </div><!-- /Cars -->
		<?endif;?>

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray section-sec">
                <div class="box-container">
                    <ul class="tabs five-tabs clearfix">
                        <li class="active"><a href="detail.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Описание</a></li>
                        <li><a href="characteristics.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Характеристики</a></li>
                        <li><a href="picking.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Коплектации и цены</a></li>
                        <li><a href="reviews.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Отзывы</a></li>
                        <li><a href="stock.php?ID=<?=$_REQUEST['ID']?>" data-pjax="#pjax-container">Акции</a></li>
                    </ul>
                </div>
            </div>
            <?
	            $tab_descr = "";
	            $tab_descr_file = "";
				$rsModel = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "NAME" => $arResult["PROPERTIES"]["CARMODEL"]["VALUE"]), false, array("DESCRIPTION", "UF_TAB_DESC_FILE"));
				if ($arModel = $rsModel->Fetch())
				{
				    $tab_descr = $arModel['DESCRIPTION'];
				    $tab_descr_file = $arModel['UF_TAB_DESC_FILE'];
				}
				else{
					$rsBrand = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "NAME" => $arResult["PROPERTIES"]["CARMODEL"]["VALUE"]), false, array("DESCRIPTION", "UF_TAB_DESC_FILE"));
					if ($arBrand = $rsBrand->Fetch())
					{
					    $tab_descr = $arBrand['DESCRIPTION'];
					    $tab_descr_file = $arModel['UF_TAB_DESC_FILE'];
					}
				}
			?>
            <div id="pjax-container">
            	<div id="section">
		            <div class="section section-sec">
		                <div class="box-container">
		                    <div class="description-items"><!--
		                        -->
									<?
									if ($tab_descr != ""):
										echo $tab_descr;
									endif;
									?>

									<?=$arResult["PROPERTIES"]["CARBRAND"]["VALUE"]?>
									<?=$arResult["PROPERTIES"]["CARMODEL"]["VALUE"]?>

		                        <!--
		                    --></div>

		                    <?if ($tab_descr != ""):?>
			                    <div class="bottom-part clearfix">
			                        <div class="pdf-link on-right clearfix align-left">
			                            <div class="on-left">
			                                <i class="fa fa-file-pdf-o"></i>
			                            </div>
			                            <div class="on-left">
			                                <div class="pdf-link-url"><a href="<?=CFile::GetPath($tab_descr_file);?>" download>Загрузить файл</a></div>
			                                <div class="pdf-link-format">формат PDF</div>
			                            </div>
			                        </div>
			                    </div>
		                    <?endif;?>

		                    <div class="bottom-part clearfix">
		                        <div class="on-center"><a href="#" class="red-big-link animate-custom">Задать вопрос</a></div>
		                    </div>
		                </div>
		            </div>
	        	</div>
			</div>
        </div><!-- /Tabs Container -->
    </section><!-- /Content -->


<?/*
<div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>" id="<? echo $arItemIDs['ID']; ?>">
<?
if ('Y' == $arParams['DISPLAY_NAME'])
{
?>
<div class="bx_item_title"><h1><span><?
	echo (
		isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
		? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
		: $arResult["NAME"]
	); ?>
</span></h1></div>
<?
}
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
	<div class="bx_item_container">
		<div class="bx_lt">
<div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
	<div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
	<div class="bx_bigimages_imgcontainer">
	<span class="bx_bigimages_aligner"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>"></span>
<?
if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF'])
		{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>"><? echo -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
<?
		}
	}
	else
	{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" style="display: none;"></div>
<?
	}
}
if ($arResult['LABEL'])
{
?>
	<div class="bx_stick average left top" id="<? echo $arItemIDs['STICKER_ID'] ?>" title="<? echo $arResult['LABEL_VALUE']; ?>"><? echo $arResult['LABEL_VALUE']; ?></div>
<?
}
?>
	</div>
	</div>
<?
if ($arResult['SHOW_SLIDER'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (5 < $arResult['MORE_PHOTO_COUNT'])
		{
			$strClass = 'bx_slider_conteiner full';
			$strOneWidth = (100/$arResult['MORE_PHOTO_COUNT']).'%';
			$strWidth = (20*$arResult['MORE_PHOTO_COUNT']).'%';
			$strSlideStyle = '';
		}
		else
		{
			$strClass = 'bx_slider_conteiner';
			$strOneWidth = '20%';
			$strWidth = '100%';
			$strSlideStyle = 'display: none;';
		}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
<?
		foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
		{
?>
	<li data-value="<? echo $arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
		}
		unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	</div>
	</div>
<?
	}
	else
	{
		foreach ($arResult['OFFERS'] as $key => $arOneOffer)
		{
			if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
				continue;
			$strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
			if (5 < $arOneOffer['MORE_PHOTO_COUNT'])
			{
				$strClass = 'bx_slider_conteiner full';
				$strOneWidth = (100/$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strWidth = (20*$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_slider_conteiner';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>" style="display: <? echo $strVisible; ?>;">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>">
<?
			foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto)
			{
?>
	<li data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
			}
			unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	</div>
	</div>
<?
		}
	}
}
?>
</div>
		</div>
		<div class="bx_rt">
<?
$useBrands = ('Y' == $arParams['BRAND_USE']);
$useVoteRating = ('Y' == $arParams['USE_VOTE_RATING']);
if ($useBrands || $useVoteRating)
{
?>
	<div class="bx_optionblock">
<?
	if ($useVoteRating)
	{
		?><?$APPLICATION->IncludeComponent(
			"bitrix:iblock.vote",
			"stars",
			array(
				"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arResult['ID'],
				"ELEMENT_CODE" => "",
				"MAX_VOTE" => "5",
				"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
				"SET_STATUS_404" => "N",
				"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
				"CACHE_TYPE" => $arParams['CACHE_TYPE'],
				"CACHE_TIME" => $arParams['CACHE_TIME']
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
	if ($useBrands)
	{
		?><?$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", ".default", array(
			"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"ELEMENT_ID" => $arResult['ID'],
			"ELEMENT_CODE" => "",
			"PROP_CODE" => $arParams['BRAND_PROP_CODE'],
			"CACHE_TYPE" => $arParams['CACHE_TYPE'],
			"CACHE_TIME" => $arParams['CACHE_TIME'],
			"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
			"WIDTH" => "",
			"HEIGHT" => ""
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
?>
	</div>
<?
}
unset($useVoteRating, $useBrands);
?>
<div class="item_price">
<?
$boolDiscountShow = (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF']);
?>
	<div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? $arResult['MIN_PRICE']['PRINT_VALUE'] : ''); ?></div>
	<div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
	<div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? GetMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $arResult['MIN_PRICE']['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
</div>
<?
if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
{
?>
<div class="item_info_section">
<?
	if (!empty($arResult['DISPLAY_PROPERTIES']))
	{
?>
	<dl>
<?
		foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
		{
?>
		<dt><? echo $arOneProp['NAME']; ?></dt><dd><?
			echo (
				is_array($arOneProp['DISPLAY_VALUE'])
				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
				: $arOneProp['DISPLAY_VALUE']
			); ?></dd><?
		}
		unset($arOneProp);
?>
	</dl>
<?
	}
	if ($arResult['SHOW_OFFERS_PROPS'])
	{
?>
	<dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl>
<?
	}
?>
</div>
<?
}
if ('' != $arResult['PREVIEW_TEXT'])
{
	if (
		'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
		|| ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
	)
	{
?>
<div class="item_info_section">
<?
		echo ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>');
?>
</div>
<?
	}
}
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
	$arSkuProps = array();
?>
<div class="item_info_section" style="padding-right:150px;" id="<? echo $arItemIDs['PROP_DIV']; ?>">
<?
	foreach ($arResult['SKU_PROPS'] as &$arProp)
	{
		if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
			continue;
		$arSkuProps[] = array(
			'ID' => $arProp['ID'],
			'SHOW_MODE' => $arProp['SHOW_MODE'],
			'VALUES_COUNT' => $arProp['VALUES_COUNT']
		);
		if ('TEXT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_size full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_size';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
		<div class="bx_size_scroller_container"><div class="bx_size">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; display: none;">
<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
<?
		}
		elseif ('PICT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_scu full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_scu';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
		<div class="bx_scu_scroller_container"><div class="bx_scu">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>; display: none;" >
<i title="<? echo $arOneValue['NAME']; ?>"></i>
<span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo $arOneValue['NAME']; ?>"></span></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
<?
		}
	}
	unset($arProp);
?>
</div>
<?
}
?>
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
}
else
{
	$canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
{
	if ($arParams['SHOW_BASIS_PRICE'] == 'Y')
	{
		$basisPriceInfo = array(
			'#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
			'#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
		);
?>
		<p id="<? echo $arItemIDs['BASIS_PRICE']; ?>" class="item_section_name_gray"><? echo GetMessage('CT_BCE_CATALOG_MESS_BASIS_PRICE', $basisPriceInfo); ?></p>
<?
	}
?>
	<span class="item_section_name_gray"><? echo GetMessage('CATALOG_QUANTITY'); ?></span>
	<div class="item_buttons vam">
		<span class="item_buttons_counter_block">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
			<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
					? 1
					: $arResult['CATALOG_MEASURE_RATIO']
				); ?>">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
			<span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
		</span>
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
	if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
	{
?>
		<span class="item_buttons_counter_block">
<?
		if ($arParams['DISPLAY_COMPARE'])
		{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
<?
		}
		if ($showSubscribeBtn)
		{

		}
?>
		</span>
<?
	}
?>
	</div>
<?
	if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
	{
		if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
		{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
<?
		}
		else
		{
			if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
			{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
<?
			}
		}
	}
}
else
{
?>
	<div class="item_buttons vam">
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
	</div>
<?
}
unset($showAddBtn, $showBuyBtn);
?>
</div>
			<div class="clb"></div>
		</div>

		<div class="bx_md">
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	if ($arResult['OFFER_GROUP'])
	{
		foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
		{
?>
	<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
		"ELEMENT_ID" => $offerID,
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
?>
	</span>
<?
		}
	}
}
else
{
	if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
	{
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
	}
}
?>
</div>
		</div>
		<div class="bx_rb">
<div class="item_info_section">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
	<div class="bx_item_description">
		<div class="bx_item_section_name_gray" style="border-bottom: 1px solid #f2f2f2;"><? echo GetMessage('FULL_DESCRIPTION'); ?></div>
<?
	if ('html' == $arResult['DETAIL_TEXT_TYPE'])
	{
		echo $arResult['DETAIL_TEXT'];
	}
	else
	{
		?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
	}
?>
	</div>
<?
}
?>
</div>
		</div>
		<div class="bx_lb">
<div class="tac ovh">
</div>
<div class="tab-section-container">
<?
if ('Y' == $arParams['USE_COMMENTS'])
{
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.comments",
	"",
	array(
		"ELEMENT_ID" => $arResult['ID'],
		"ELEMENT_CODE" => "",
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"URL_TO_COMMENT" => "",
		"WIDTH" => "",
		"COMMENTS_COUNT" => "5",
		"BLOG_USE" => $arParams['BLOG_USE'],
		"FB_USE" => $arParams['FB_USE'],
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"VK_USE" => $arParams['VK_USE'],
		"VK_API_ID" => $arParams['VK_API_ID'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
		"BLOG_TITLE" => "",
		"BLOG_URL" => $arParams['BLOG_URL'],
		"PATH_TO_SMILE" => "",
		"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
		"AJAX_POST" => "Y",
		"SHOW_SPAM" => "Y",
		"SHOW_RATING" => "N",
		"FB_TITLE" => "",
		"FB_USER_ADMIN_ID" => "",
		"FB_COLORSCHEME" => "light",
		"FB_ORDER_BY" => "reverse_time",
		"VK_TITLE" => "",
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>
<?
}
?>
</div>
		</div>
			<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOneJS)
	{
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
		{
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
			$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($arOneJS['DISPLAY_PROPERTIES']))
			{
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
				{
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	{
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
		if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
?>
	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
				if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
					unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			}
		}
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties)
		{
?>
	<table>
<?
			foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
			{
?>
	<tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	<td>
<?
				if(
					'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
					&& 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
				)
				{
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
					}
				}
				else
				{
					?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
					}
					?></select><?
				}
?>
	</td></tr>
<?
			}
?>
	</table>
<?
		}
?>
</div>
<?
	}
	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
	{
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>

*/?>