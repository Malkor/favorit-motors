<?$bg = $APPLICATION->GetDirProperty("bg");?>
<?$dir = $APPLICATION->GetCurDir();?>
<?$page = $APPLICATION->GetCurPage();?>
<?CModule::IncludeModule('iblock');?>
<!-- Content -->
<section id="content">
    <div id="accessories-header" class="section section-gray section-sec section-top">
        <div class="box-container clearfix"  <?if($bg):?>style="background-image: url('<?=$bg?>');"<?endif;?>>
            <div class="on-left">
                <div class="title-post">
                    <h1><?$APPLICATION->ShowTitle()?></h1>
                </div>

                <?if(!$bg && $page == "/technical_center/index.php"):
                    $section_list = array();
                    $brands_list = array();
                    $if_exists_brands = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>27), false, array(), array("ID", "NAME", "PROPERTY_BRAND", "IBLOCK_SECTION_ID" ));
                    while($ob = $if_exists_brands->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $brands_list[] = $arFields["PROPERTY_BRAND_VALUE"];
                        $section_list[] = $arFields["IBLOCK_SECTION_ID"];
                    }
                    $brands_list = array_unique($brands_list);
                    $section_list = array_unique($section_list);

                    $brands = array();
                    $get_brands_list = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>10, "ID"=>$brands_list), false, array(), array("ID", "NAME" ));
                    while($ob = $get_brands_list->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $brands[$arFields["ID"]] = $arFields["NAME"];
                    }

                    $type_of_work_list = array();
                    $if_exists_type_of_work = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>33), false, array(), array("ID", "NAME"));
                    while($ob = $if_exists_type_of_work->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $type_of_work_list[$arFields["ID"]] = $arFields["NAME"];
                    }
                    ?>

                    <div class="filter">
                        <form id="models-works-filter" class="clearfix">
                            <div class="filter-item clearfix">
                                <label for="brand">Марка автомобиля:</label>
                                <select id="brand">
                                    <option value="0">Все марки</option>
                                    <?foreach ($brands as $id => $name) :?>
                                        <option value="<?=$id?>"><?=$name?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                            <div class="filter-item clearfix">
                                <label for="type_of_work">Вид работ:</label>
                                <select id="type_of_work" disabled>
                                    <option value="0">Все виды</option>
                                    <?foreach ($type_of_work_list as $id => $name) :?>
                                        <option value="<?=$id?>"><?=$name?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </form>
                    </div>

                    <div class="techcenter-inner clearfix">
                        <div class="on-left">
                            <div class="body">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "AREA_FILE_RECURSIVE" => "Y",
                                        "EDIT_MODE" => "html",
                                    )
                                );?>
                            </div>
                        </div>

                        <div class="on-right">
                            <div id="big-center-map">
                                <?
                                $main_departments_id = array();
                                $rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>27), false, array("ID", "IBLOCK_ID", "UF_MAIN_DEPARTMENT"));
                                while ($arSection = $rsSections->Fetch())
                                {
                                    $main_departments_id[] = $arSection["UF_MAIN_DEPARTMENT"];
                                }

                                $main_departments_id = array_unique($main_departments_id);

                                $main_departments = array();
                                $rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>12, "ID"=>$main_departments_id), false, array("ID", "IBLOCK_ID", "NAME", "UF_METRO", "UF_ADDRESS", "UF_PHONE", "UF_SALON_TIME", "UF_TECH_TIME", "UF_SQUARE_NUM"));
                                while ($arSection = $rsSections->Fetch())
                                {
                                    $main_departments[$arSection["ID"]] = $arSection;
                                }
                                ?>

                                <?foreach ($main_departments as $department):?>
                                    <div class="tech-center tech-center-<?=$department["UF_SQUARE_NUM"]?> animate-custom">
                                        <div class="tech-center-icon"></div>
                                        <div class="tech-center-info">
                                            <div class="tech-center-info-name red-text"><?=$department["NAME"]?></div>
                                            <div class="tech-center-info-address"><span class="red-text">М</span> <?=$department["UF_ADDRESS"]?><br/><?=$department["UF_PHONE"]?></div>
                                            <div class="tech-center-info-worktime red-text">График работы:</div>
                                            <div class="tech-center-info-worktime-line clearfix">
                                                <div class="tech-center-info-worktime-line-salone">Автосалон:<br/><?=$department["UF_SALON_TIME"]?></div>
                                                <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/><?=$department["UF_TECH_TIME"]?></div>
                                            </div>
                                            <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                            <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                        </div>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                <?endif;?>

                <?if($dir == "/technical_center/original_parts/"):?>
                    <div class="gray-text">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc",
                                "AREA_FILE_RECURSIVE" => "Y",
                                "EDIT_MODE" => "html",
                            )
                        );?>
                    </div>

                    <?global $arrFilter;
                    $arrFilter = array("PROPERTY_TOP_VALUE" => "Да");?>
                    <div class="accessories-thumb-items"><!--
                        <?$APPLICATION->IncludeComponent("bitrix:news.list", "original_parts_top_list", Array(
                            "COMPONENT_TEMPLATE" => ".default",
                                "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
                                "IBLOCK_ID" => "34",	// Код информационного блока
                                "NEWS_COUNT" => "3",	// Количество новостей на странице
                                "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                                "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                                "SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
                                "SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
                                "FILTER_NAME" => "arrFilter",	// Фильтр
                                "FIELD_CODE" => array(	// Поля
                                    0 => "NAME",
                                    1 => "PREVIEW_TEXT",
                                    2 => "PREVIEW_PICTURE",
                                    3 => "",
                                ),
                                "PROPERTY_CODE" => array(	// Свойства
                                    0 => "COST",
                                    1 => "TOP",
                                    2 => "BRAND",
                                ),
                                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                "AJAX_MODE" => "N",	// Включить режим AJAX
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                "PARENT_SECTION" => "",	// ID раздела
                                "PARENT_SECTION_CODE" => "",	// Код раздела
                                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                "DISPLAY_DATE" => "N",	// Выводить дату элемента
                                "DISPLAY_NAME" => "N",	// Выводить название элемента
                                "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                                "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                "PAGER_TITLE" => "Новости",	// Название категорий
                                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                            ),
                            false
                        );?>
                    --></div>
                <?endif;?>

                <?if(($dir != "/technical_center/original_parts/") && ($dir != "/technical_center/additional_equipments/") && ($page != "/technical_center/index.php")):?>
                    <div class="body">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "inc",
                                "AREA_FILE_RECURSIVE" => "Y",
                                "EDIT_MODE" => "html",
                            )
                        );?>
                    </div>
                <?endif;?>
            </div>

            <div class="on-right">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "techcenter_menu_inner", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "ROOT_MENU_TYPE" => "techcenter",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "3",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    ),
                    false
                );?>
            </div>
            <?if(($dir != "/technical_center/original_parts/") && ($dir != "/technical_center/additional_equipments/")):?>
                <div class="accessories-button">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "form_header",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "EDIT_MODE" => "html",
                        )
                    );?>
                </div>
            <?endif;?>
        </div>
    </div>

    <div class="section section-sec padding">
        <div class="box-container">

            <div class="body">