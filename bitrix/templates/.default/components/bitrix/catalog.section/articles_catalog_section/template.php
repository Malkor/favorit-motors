<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<?if($_REQUEST["SECTION_CODE"] != ""):?>
	<div class="section section-gray section-sec section-top">
	    <div class="box-container">
			<div class="title-post">
        		<h1><?=$arResult["NAME"];?></h1>
   			</div>
			<div class="gray-text">
				<?=$arResult["DESCRIPTION"];?>
			</div>
	    </div>
	</div>
<?endif;?>

<?return 2;?>