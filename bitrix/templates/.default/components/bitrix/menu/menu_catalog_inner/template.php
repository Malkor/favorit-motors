<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<?$c = count($arResult)?>
	<?$i = 1;?>
	<div class="cat_nav">
		<?foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
			continue;
		?>
		<?if($arItem["SELECTED"]):?>
			<a href="<?=$arItem["LINK"]?>" class="cat_nav__link cat_nav__link--active <?if($i == $c):?>cat_nav__link--last<?endif?>"><?=$arItem["TEXT"]?></a>
		<?else:?>
			<a href="<?=$arItem["LINK"]?>" class="cat_nav__link <?if($i == $c):?>cat_nav__link--last<?endif?>" ><?=$arItem["TEXT"]?></a>
		<?endif?>
		<?$i++;?>
		<?endforeach?>
	</div>
<?endif?>