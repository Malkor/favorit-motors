<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTransParams = array(
	'INIT_MAP_TYPE' => $arParams['INIT_MAP_TYPE'],
	'INIT_MAP_LON' => $arResult['POSITION']['google_lon'],
	'INIT_MAP_LAT' => $arResult['POSITION']['google_lat'],
	'INIT_MAP_SCALE' => $arResult['POSITION']['google_scale'],
	'MAP_WIDTH' => $arParams['MAP_WIDTH'],
	'MAP_HEIGHT' => $arParams['MAP_HEIGHT'],
	'CONTROLS' => $arParams['CONTROLS'],
	'OPTIONS' => $arParams['OPTIONS'],
	'MAP_ID' => $arParams['MAP_ID'],
);

if ($arParams['DEV_MODE'] == 'Y')
{
	$arTransParams['DEV_MODE'] = 'Y';
	if ($arParams['WAIT_FOR_EVENT'])
		$arTransParams['WAIT_FOR_EVENT'] = $arParams['WAIT_FOR_EVENT'];
}
?>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
		var map;
		var il7pro = new google.maps.LatLng(<?=$arResult['POSITION']['PLACEMARKS']['0']['LAT']?>, <?=$arResult['POSITION']['PLACEMARKS']['0']['LON']?>);
		var mapCoordinates = new google.maps.LatLng(<?=$arResult['POSITION']['PLACEMARKS']['0']['LAT']?>, <?=$arResult['POSITION']['PLACEMARKS']['0']['LON']?>);

		var markers = [];
		var image = new google.maps.MarkerImage(
		    '<?=$arParams['LOGO_PATH']?>',
		    new google.maps.Size(84,44),
		    new google.maps.Point(0,0),
		    new google.maps.Point(<?=$arResult['POSITION']['PLACEMARKS']['0']['LAT']?>, <?=$arResult['POSITION']['PLACEMARKS']['0']['LON']?>)
		  );

		function addMarker()
		{
		      markers.push(new google.maps.Marker({
		      position: il7pro,
		      raiseOnDrag: false,
			  icon: image,
		      map: map,
		      draggable: false
		      }));

		}

		function initialize() {
		  var mapOptions = {
			backgroundColor: "#ffffff",
		    zoom: <?=$arTransParams['INIT_MAP_SCALE']?>,
			disableDefaultUI: true,
		    center: mapCoordinates,
		    mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [
					  {
					    "featureType": "landscape.natural",
					    "elementType": "geometry.fill",
					    "stylers": [
					      { "color": "#ffffff" }
					    ]
					  },
					  {
						    "featureType": "landscape.man_made",
						    "stylers": [
						      { "color": "#ffffff" },
						      { "visibility": "off" }
						    ]
					  },
					  {
						    "featureType": "water",
						    "stylers": [
						       { "color": "#80C8E5" },
						      { "saturation": 0 }
						    ]
					  },
					  {
						    "featureType": "road.arterial",
						    "elementType": "geometry",
						    "stylers": [
						      { "color": "#999999" }
						    ]
					  }
					 ,{
						    "elementType": "labels.text.stroke",
						    "stylers": [
						      { "visibility": "off" }
						    ]
					  }
						,{
						    "elementType": "labels.text",
						    "stylers": [
						      { "color": "#333333" }
						    ]
						  }

						,{
						    "featureType": "road.local",
						    "stylers": [
						      { "color": "#dedede" }
						    ]
						  }
						,{
						    "featureType": "road.local",
						    "elementType": "labels.text",
						    "stylers": [
						      { "color": "#666666" }
						    ]
						  }
						,{
						    "featureType": "transit.station.bus",
						    "stylers": [
						      { "saturation": -57 }
						    ]
						  }
						,{
						    "featureType": "road.highway",
						    "elementType": "labels.icon",
						    "stylers": [
						      { "visibility": "off" }
						    ]
						  },{
						    "featureType": "poi",
						    "stylers": [
						      { "visibility": "off" }
						    ]
						  }

					]

		  };
		map = new google.maps.Map(document.getElementById('map1'),mapOptions);
		addMarker();

		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<div id="map1" style="width:<?=$arTransParams['MAP_WIDTH']?>px; height:<?=$arTransParams['MAP_HEIGHT']?>px;"></div>