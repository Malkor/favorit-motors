<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(function_exists('yenisite_GetCompositeLoader')){global $MESS;$MESS ['COMPOSITE_LOADING'] = yenisite_GetCompositeLoader();}

if(method_exists($this, 'setFrameMode')) $this->createFrame()->begin(GetMessage('COMPOSITE_LOADING'));
function flag_to_bool ($flag){
	if($flag == "Y")
		return "true";
	return "false";
}
?>
 <style type="text/css">
	.fb_iframe_widget>span { width: <?=$arParams["WIDTH"];?>px !important; }
	.fb-like-box iframe { width: <?=$arParams["WIDTH"];?>px !important; }
 </style>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div 
	class="fb-like-box" 
	data-href="<?=$arParams["PAGE_URL"];?>"
	data-width="<?=$arParams["WIDTH"];?>"
	<?if($arParams["HEIGHT"] != ""):?>
		data-height="<?=$arParams["HEIGHT"];?>"
	<?endif;?>
	data-colorscheme="<?=$arParams["COLOR_SCHEME"];?>"
	data-show-faces="<?=flag_to_bool($arParams["FACES"]);?>"
	data-stream="<?=flag_to_bool($arParams["STREAM"]);?>"
	data-show-border="<?=flag_to_bool($arParams["BORDER"]);?>"
	data-header="<?=flag_to_bool($arParams["HEADER"]);?>"
>
</div>

