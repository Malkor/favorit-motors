<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$arComponentDescription = array(
	'NAME' => GetMessage("NSANDREY_RESIZE_RESAYZER_IZOBRAJENIY"),
	'DESCRIPTION' => GetMessage("NSANDREY_RESIZE_POZVOLAET_POKAZATQ_I"),
	'ICON' => '/images/resizer.png',
	'PATH' => array('ID' => GetMessage("NSANDREY_RESIZE_POLEZNYE_KOMPONENTY"))
);