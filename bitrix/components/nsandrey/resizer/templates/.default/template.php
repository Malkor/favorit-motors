<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
	
$this->setFrameMode(true);
?>
<img<?= (!empty($arResult['IMAGE_CSS_CLASSES']) ? ' class="' . $arResult['IMAGE_CSS_CLASSES'] . '"' : '') ?> src="<?= $arResult['IMAGE_RESIZED'] ?>" alt="<?= $arResult['IMAGE_ALT'] ?>">