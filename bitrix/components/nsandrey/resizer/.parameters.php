<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

// ������������� ��������������� ��������
$arEvent = $arFields = $arEventTypes = Array();

// ���� �������
$resizeTypes = array(
	'NONE' => GetMessage("NSANDREY_RESIZE_NE_IZMENATQ"),
	'CROP' => GetMessage("NSANDREY_RESIZE_OBREZATQ"),
	'IN' => GetMessage("NSANDREY_RESIZE_VPISATQ_V_OBLASTQ"),
	'RESIZE_WIDTH' => GetMessage("NSANDREY_RESIZE_IZMENITQ_PO_SIRINE"),
	'RESIZE_HEIGHT' => GetMessage("NSANDREY_RESIZE_IZMENITQ_PO_VYSOTE")
);

// ���� �������� �����
$watermarkTypes = array(
	'NONE' => GetMessage("NSANDREY_RESIZE_NE_ISPOLQZOVATQ"),
	'IMAGE' => GetMessage("NSANDREY_RESIZE_IZOBRAJENIE"),
	'TEXT' => GetMessage("NSANDREY_RESIZE_TEKST")
);

// ������������ �������� �����
$watermarkFillType = array(
	'FILL' => GetMessage("NSANDREY_RESIZE_ZAPOLNITQ_IZOBRAJENI"),
	'ROW' => GetMessage("NSANDREY_RESIZE_ZAPOLNITQ_PO_GORIZON"),
	'COORDS' => GetMessage("NSANDREY_RESIZE_RASPOLOJITQ_PO_KOORD")
);
$watermarkPositionX = array(
	'LEFT' => GetMessage("NSANDREY_RESIZE_SLEVA"),
	'CENTER' => GetMessage("NSANDREY_RESIZE_PO_CENTRU"),
	'RIGHT' => GetMessage("NSANDREY_RESIZE_SPRAVA")
);
$watermarkPositionY = array(
	'TOP' => GetMessage("NSANDREY_RESIZE_SVERHU"),
	'CENTER' => GetMessage("NSANDREY_RESIZE_PO_CENTRU"),
	'BOTTOM' => GetMessage("NSANDREY_RESIZE_SNIZU")
);

// ������ �����
$arMGroups = array(
	'RESIZE_SETTINGS' => array('NAME' => GetMessage("NSANDREY_RESIZE_NASTROYKI_IZMENENIA")),
	'WATERMARK_SETTINGS' => array('NAME' => GetMessage("NSANDREY_RESIZE_VODANOY_ZNAK"))
);

// ������ ����������
$arMParams = array(
	'IMAGE' => array(
		'NAME' => 'ID '.GetMessage("NSANDREY_RESIZE_IZOBRAJENIA_PUTQ_K"),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
		'PARENT' => 'BASE'
	),
	'IMAGE_ALT' => array(
		'NAME' => 'ALT '.GetMessage("NSANDREY_RESIZE_IZOBRAJENJA"),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
		'PARENT' => 'BASE'
	),
	'IMAGE_CSS_CLASSES' => array(
		'NAME' => 'CSS-'.GetMessage("NSANDREY_RESIZE_KLASSY_IZOBRAJENJA"),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
		'PARENT' => 'BASE',
		'MULTIPLE' => 'Y'
	),
	'NO_IMAGE' => array(
		'NAME' => GetMessage("NSANDREY_RESIZE_ZAGLUSKA_IZOBRAJENI"),
		'TYPE' => 'FILE',
		'DEFAULT' => '',
		'PARENT' => 'BASE'
	),
	'RESIZE_TYPE' => array(
		'NAME' => 'ID '.GetMessage("NSANDREY_RESIZE_IZOBRAJENIA_PUTQ_K"),
		'TYPE' => 'LIST',
		'VALUES' => $resizeTypes,
		'DEFAULT' => 'NONE',
		'PARENT' => 'RESIZE_SETTINGS',
		'REFRESH' => 'Y'
	)
);

if (in_array($arCurrentValues['RESIZE_TYPE'], array('CROP', 'IN', 'RESIZE_WIDTH')))
{
	$arMParams['RESIZE_WIDTH'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_SIRINA"),
		'TYPE' => 'STRING',
		'DEFAULT' => '0',
		'PARENT' => 'RESIZE_SETTINGS'
	);
}

if (in_array($arCurrentValues['RESIZE_TYPE'], array('CROP', 'IN', 'RESIZE_HEIGHT')))
{
	$arMParams['RESIZE_HEIGHT'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_VYSOTA"),
		'TYPE' => 'STRING',
		'DEFAULT' => '0',
		'PARENT' => 'RESIZE_SETTINGS'
	);
}

if (in_array($arCurrentValues['RESIZE_TYPE'], array('IN')))
{
	$arMParams['RESIZE_BACKGROUND'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_CVET_FONA"),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#ffffff',
		'PARENT' => 'RESIZE_SETTINGS'
	);
}

$arMParams['WATERMARK_TYPE'] = array(
	'NAME' => GetMessage("NSANDREY_RESIZE_TIP_VODANOGO_ZNAKA"),
	'TYPE' => 'LIST',
	'VALUES' => $watermarkTypes,
	'DEFAULT' => 'NONE',
	'PARENT' => 'WATERMARK_SETTINGS',
	'REFRESH' => 'Y'
);

if ($arCurrentValues['WATERMARK_TYPE'] == 'IMAGE')
{
	$arMParams['WATERMARK_IMAGE'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_PUTQ_K_VODANOMU_ZNAK"),
		'TYPE' => 'FILE',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
}
else if ($arCurrentValues['WATERMARK_TYPE'] == 'TEXT')
{
	$arMParams['WATERMARK_TEXT'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_TEKST_NA_VODANOM_ZNA"),
		'TYPE' => 'STRING',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
	$arMParams['WATERMARK_FONT'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_PUTQ_K_FAYLU_SRIFTOV"),
		'TYPE' => 'FILE',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
	$arMParams['WATERMARK_FONT_COLOR'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_CVET_SRIFTA"),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#000000',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
	$arMParams['WATERMARK_FONT_SIZE'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_RAZMER_SRIFTA"),
		'TYPE' => 'STRING',
		'DEFAULT' => '14',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
}

if (isset($arCurrentValues['WATERMARK_TYPE']) && $arCurrentValues['WATERMARK_TYPE'] != 'NONE')
{
	$arMParams['WATERMARK_FILL'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_PARAMETRY_NALOJENIA"),
		'TYPE' => 'LIST',
		'VALUES' => $watermarkFillType,
		'DEFAULT' => 'FILL',
		'PARENT' => 'WATERMARK_SETTINGS',
		'REFRESH' => 'Y'
	);

	if ($arCurrentValues['WATERMARK_FILL'] == 'COORDS')
	{
		$arMParams['WATERMARK_POSITION_X'] = array(
			'NAME' => GetMessage("NSANDREY_RESIZE_POLOJENIE_PO_GORIZON"),
			'TYPE' => 'LIST',
			'VALUES' => $watermarkPositionX,
			'PARENT' => 'WATERMARK_SETTINGS'
		);
	}

	if ($arCurrentValues['WATERMARK_FILL'] == 'ROW')
	{
		$arMParams['WATERMARK_POSITION_Y'] = array(
			'NAME' => GetMessage("NSANDREY_RESIZE_POLOJENIE_PO_VERTIKA"),
			'TYPE' => 'LIST',
			'VALUES' => $watermarkPositionY,
			'PARENT' => 'WATERMARK_SETTINGS'
		);
	}

	$arMParams['WATERMARK_OPACITY'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_PROZRACNOSTQ_VODANOG"),
		'TYPE' => 'STRING',
		'DEFAULT' => '100',
		'PARENT' => 'WATERMARK_SETTINGS'
	);

	$arMParams['WATERMARK_ANGLE'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_UGOL_POVOROTA_VODANO"),
		'TYPE' => 'STRING',
		'DEFAULT' => '0',
		'PARENT' => 'WATERMARK_SETTINGS'
	);

	$arMParams['WATERMARK_ON_SOURCE'] = array(
		'NAME' => GetMessage("NSANDREY_RESIZE_NAKLADYVATQ_NA_OSNOV"),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
		'PARENT' => 'WATERMARK_SETTINGS'
	);
}


$arComponentParameters = array('GROUPS' => $arMGroups, 'PARAMETERS' => $arMParams);