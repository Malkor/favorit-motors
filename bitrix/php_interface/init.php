<?

// Выводим правильные окончания в словах в зависимости от числа
// $num - число
// $str1 - текст для варианта «один»:
// $str1 - текст для варианта «несколько» (2–4)
// $str3 - текст для варианта «много»
function get_correct_str($num, $str1, $str2, $str3) {
    
    
    $val = $num % 100;

    if ($val > 10 && $val < 20) return $num .' '. $str3;
    else {
        $val = $num % 10;
        if ($val == 1) return $num .' '. $str1;
        elseif ($val > 1 && $val < 5) return $num .' '. $str2;
        else return $num .' '. $str3;
    }
}

function pre($text){
    echo '<pre>';
        print_r($text);
    echo '</pre>';
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Подключение индивидуального шаблона
function IncludePersonalTemplate($FILE, $bMainpage=true)
{
    global $APPLICATION;
    if ($bMainpage && $APPLICATION->GetCurPage() == SITE_DIR)
    {
        $personalTemplate = "mainpage"; // шаблон для главной
    }

    $personalTemplate = (strlen($personalTemplate)>0 && !empty($personalTemplate))? $personalTemplate : $APPLICATION->GetDirProperty("personal_template");
    $templateFile = dirname($FILE)."/"."tpl_".$personalTemplate."_".basename($FILE);

    if (strlen($personalTemplate) > 0
        && !empty($personalTemplate)
        && file_exists($templateFile)
    )
    {
        require_once($templateFile);
        return true;
    }
    else
        return false;
}

/*
=======
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyPicture", "GetUserTypeDescription"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", array("CIBlockPropertyPicture", "OnBeforeIBlockElementDelete"));

AddEventHandler("main", "OnBeforeChangeFile", "CallGitHook");

function CallGitHook($abs_path, $content)
{
    $cnt = file_get_contents("http://".$_SERVER['HTTP_HOST']."/deploy.php");
    return true;
}

function pre($text){
    echo '<pre>';
        print_r($text);
    echo '</pre>';
}

class CIBlockPropertyPicture
{
   function GetUserTypeDescription()
   {
      return array(
         "PROPERTY_TYPE"      =>"E",
         "USER_TYPE"      =>"Picture",
         "DESCRIPTION"      =>"Картинка",
         "GetPropertyFieldHtml" =>array("CIBlockPropertyPicture", "GetPropertyFieldHtml"),
         "GetPublicViewHTML" =>array("CIBlockPropertyPicture", "GetPublicViewHTML"),
         "ConvertToDB" =>array("CIBlockPropertyPicture", "ConvertToDB"),

         //"GetPublicEditHTML" =>array("CIBlockPropertyPicture","GetPublicEditHTML"),
         //"GetAdminListViewHTML" =>array("CIBlockPropertyPicture","GetAdminListViewHTML"),
         //"CheckFields" =>array("CIBlockPropertyPicture","CheckFields"),
         //"ConvertFromDB" =>array("CIBlockPropertyPicture","ConvertFromDB"),
         //"GetLength" =>array("CIBlockPropertyPicture","GetLength"),
      );
   }

   function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
   {
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         $ELEMENT_ID = intval($value["VALUE"]);
         if($ELEMENT_ID)
         {
            $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"],
                          "ID" => $value["VALUE"]), false, false, array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
            $arElement = $rsElement->Fetch();
            if(is_array($arElement))
               $file_id = $arElement["DETAIL_PICTURE"];
            else
               $file_id = 0;
         }
         else
         {
            $file_id = 0;
         }

         if($file_id)
         {
            $db_img = CFile::GetByID($file_id);
            $db_img_arr = $db_img->Fetch();
            if($db_img_arr)
            {
               $strImageStorePath = COption::GetOptionString("main", "upload_dir", "upload");
               $sImagePath = "/".$strImageStorePath."/".$db_img_arr["SUBDIR"]."/".$db_img_arr["FILE_NAME"];
               return '<label><input name="'.$strHTMLControlName["VALUE"].'[del]" value="Y" type="checkbox"> 
                        Удалить файл '.$sImagePath.'</label>'
               .'<input name="'.$strHTMLControlName["VALUE"].'[old]" value="'.$ELEMENT_ID.'" type="hidden">';
            }
         }
         return '<input type="file" size="'.$arProperty["COL_COUNT"].'" name="'.$strHTMLControlName["VALUE"].'"/>';
      }
      else
      {
         return "Ошибка настройки свойства. Укажите инфоблок в котором будут храниться картинки.";
      }
   }

   function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
   {
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         $ELEMENT_ID = intval($value["VALUE"]);
         if($ELEMENT_ID)
         {
            $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"], 
                          "ID" => $value["VALUE"]), false, false, array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
            $arElement = $rsElement->Fetch();
            if(is_array($arElement))
               return CFile::Show2Images($arElement["PREVIEW_PICTURE"], $arElement["DETAIL_PICTURE"]);
         }
      }
      return "";
   }

   function ConvertToDB($arProperty, $value)
   {
      $arResult = array("VALUE" => "", "DESCRIPTION" => "");
      $LINK_IBLOCK_ID = intval($arProperty["LINK_IBLOCK_ID"]);
      if($LINK_IBLOCK_ID)
      {
         if(
            is_array($value["VALUE"])
            && is_array($value["VALUE"]["error"])
            && $value["VALUE"]["error"]["VALUE"] == 0
            && $value["VALUE"]["size"]["VALUE"] > 0
         )
         {
            $arDetailPicture =  array(
               "name" => $value["VALUE"]["name"]["VALUE"],
               "type" => $value["VALUE"]["type"]["VALUE"],
               "tmp_name" => $value["VALUE"]["tmp_name"]["VALUE"],
               "error" => $value["VALUE"]["error"]["VALUE"],
               "size" => $value["VALUE"]["size"]["VALUE"],
            );
            $obElement = new CIBlockElement;
            $arResult["VALUE"] = $obElement->Add(array(
               "IBLOCK_ID" => $LINK_IBLOCK_ID,
               "NAME" => $arDetailPicture["name"],
               "DETAIL_PICTURE" => $arDetailPicture,
            ), false, false, true);
         }
         elseif(
            is_array($value["VALUE"])
            && isset($value["VALUE"]["size"])
            && !is_array($value["VALUE"]["size"])
            && $value["VALUE"]["size"] > 0
         )
         {
            $arDetailPicture =  array(
               "name" => $value["VALUE"]["name"],
               "type" => $value["VALUE"]["type"],
               "tmp_name" => $value["VALUE"]["tmp_name"],
               "error" => intval($value["VALUE"]["error"]),
               "size" => $value["VALUE"]["size"],
            );
            $obElement = new CIBlockElement;
            $arResult["VALUE"] = $obElement->Add(array(
               "IBLOCK_ID" => $LINK_IBLOCK_ID,
               "NAME" => $arDetailPicture["name"],
               "DETAIL_PICTURE" => $arDetailPicture,
            ), false, false, true);
         }
         elseif($value["VALUE"]["del"])
         {
            $obElement = new CIBlockElement;
            $obElement->Delete($value["VALUE"]["old"]);
         }
         elseif($value["VALUE"]["old"])
         {
            $arResult["VALUE"] = $value["VALUE"]["old"];
         }
         elseif(!is_array($value["VALUE"]) && intval($value["VALUE"]))
         {
            $arResult["VALUE"] = $value["VALUE"];
         }
      }
      return $arResult;
   }

   function OnBeforeIBlockElementDelete($ELEMENT_ID)
   {
      $arProperties = array();
      $rsElement = CIBlockElement::GetList(array(), array("ID" => $ELEMENT_ID), false, false, array("ID", "IBLOCK_ID"));
      $arElement = $rsElement->Fetch();
      if($arElement)
      {
         $rsProperties = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arElement["IBLOCK_ID"], "USER_TYPE" => "Picture"));
         while($arProperty = $rsProperties->Fetch())
            $arProperties[] = $arProperty;
      }

      $arElements = array();
      foreach($arProperties as $arProperty)
      {
         $rsPropValues = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array(), array(
            "EMPTY" => "N",
            "ID" => $arProperty["ID"],
         ));
         while($arPropValue = $rsPropValues->Fetch())
         {
            $ID = intval($arPropValue["VALUE"]);
            if($ID > 0)
               $arElements[$ID] = $ID;
         }
      }

      foreach($arElements as $to_delete)
      {
         CIBlockElement::Delete($to_delete);
      }
   }
}

//Функция для резервного копирования базы данных
>>>>>>> c0a3bfa9641a8c2cafb9e3da3bca1c5f077ee02f
function backup_database_tables()
{
   $host = "89.253.222.42:3306";
   $user = "b_admin_bMfZrn2";
   $pass = "O9b2^jc4";
   $name = "b_db_bMfZrn_tortodom_ru";
   $tables = '*';

   $Name = "Mailer"; //senders name
   $email = "informer@mydomain.ru"; //senders e-mail adress
   $recipient = "myemail@mydomain.ru"; //recipient
   $mail_body = "Database dump for mydomain.ru created. \n\nFile - ".$backup_name; //mail body
   $subject = "Backup mydomain.ru database"; //subject
   $header = "From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields

   mail($recipient, $subject, $mail_body, $header); //mail command :);
   *
    */

    /*
    $host = "89.253.222.42:3306";
    $user = "b_admin_bMfZrn2";
    $pass = "O9b2^jc4";
    $name = "b_db_bMfZrn_tortodom_ru";
    $tables = '*';

    $link = mysql_connect($host,$user,$pass);
    mysql_select_db($name,$link);

    //Получаем все таблицы
    if($tables == '*')
    { $tables = array();
        $result = mysql_query('SHOW TABLES');
        while($row = mysql_fetch_row($result))
        {
            $tables[] = $row[0];
        }
    }
    else
    {
        $tables = is_array($tables) ? $tables : explode(',',$tables);
    }
    foreach($tables as $table)
    { $result = mysql_query('SELECT * FROM '.$table);
        $num_fields = mysql_num_fields($result);
        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";
        for ($i = 0; $i < $num_fields; $i++)
        { while($row = mysql_fetch_row($result))
        { $return.= 'INSERT INTO '.$table.' VALUES(';
            for($j=0; $j<$num_fields; $j++)
            { $row[$j] = mysql_real_escape_string($row[$j]);
                if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                if ($j<($num_fields-1)) { $return.= ','; }
            }
            $return.= ");\n";
        }
        }
        $return.="\n\n\n";
    }

    //Сохраняем в файл
    $handle = fopen('../../db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
    fwrite($handle,$return);
    fclose($handle);

    $handle = fopen('http://localhost:6448/develop/database_backup/db-backup_'.time().'_.sql','w+');
    fwrite($handle,"\n\n\n");
    fclose($handle);

    return "backup_database_tables();";
}
<<<<<<< HEAD

*/
