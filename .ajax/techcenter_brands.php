<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

$arr_json = array();

$section_list = array();
$type_of_work = array();
$if_exists_brands = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>27, "PROPERTY_BRAND"=>$_REQUEST["BRAND_ID"]), false, array(), array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_TYPE_OF_WORK"));
while($ob = $if_exists_brands->GetNextElement())
{
    $arFields = $ob->GetFields();
    $section_list[] = $arFields["IBLOCK_SECTION_ID"];
    $type_of_work[] = $arFields["PROPERTY_TYPE_OF_WORK_VALUE"];
}
$section_list = array_unique($section_list);
$type_of_work = array_unique($type_of_work);

$type_of_work_list = array();
$if_exists_type_of_work = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>33, "ID"=>$type_of_work), false, array(), array("ID", "NAME"));
while($ob = $if_exists_type_of_work->GetNextElement())
{
    $arFields = $ob->GetFields();
    $type_of_work_list[$arFields["ID"]] = $arFields["NAME"];
}
$arr_json["type_of_work_list"] = $type_of_work_list;

$main_departments_id = array();

$rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>27, "ID"=>$section_list), false, array("ID", "IBLOCK_ID", "UF_MAIN_DEPARTMENT"));
while ($arSection = $rsSections->Fetch())
{
    if ($arSection["UF_MAIN_DEPARTMENT"] != "")
        $main_departments_id[] = $arSection["UF_MAIN_DEPARTMENT"];
}
$main_departments_id = array_unique($main_departments_id);

$main_departments = array();
if(count($main_departments_id) > 0):
    $rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>12, "ID"=>$main_departments_id ), false, array("ID", "IBLOCK_ID", "NAME", "UF_METRO", "UF_ADDRESS", "UF_PHONE", "UF_SALON_TIME", "UF_TECH_TIME", "UF_SQUARE_NUM"));
    while ($arSection = $rsSections->Fetch())
    {
        $main_departments[$arSection["ID"]] = $arSection;
    }
endif;
$arr_json["main_departments"] = $main_departments;

echo json_encode($arr_json);

?>