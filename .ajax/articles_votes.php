<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

$iblock_id = 32; // id инфоблока с голосами для статей
$type_vote_good_value = 8232; // id значения свойства типа голоса "good"
$type_vote_bad_value = 8233; // id значения типа голоса "bad"

$ar_res_user = array();
$good_votes = (int)$_REQUEST["GOOD_VOTES"];
$bad_votes = (int)$_REQUEST["BAD_VOTES"];
$type_vote = $_REQUEST["TYPE_VOTE"];
$arr_json = array();

// выберем все сессии посетителя
$arFilter = array(
    "GUEST_ID" => $_SESSION["SESS_GUEST_ID"]
);

// получим список записей
$rs = CSession::GetList(
    ($by = "s_id"),
    ($order = "desc"),
    $arFilter,
    $is_filtered
);

// выведем все записи
while ($ar = $rs->Fetch())
{
    $ar_res_user = $ar;
}

$is_good = false;
$vote_exist = false;
$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_TYPE_VOTE");
$arFilter = Array("IBLOCK_ID"=>$iblock_id, "PROPERTY_PHPSESSID_VALUE"=>$ar_res_user["PHPSESSID"], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
// проверяем оставлял ли ранее данный пользователь голос за эту статью, если да - удаляем его.
if($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $vote_exist = true;
    if($arFields["PROPERTY_TYPE_VOTE_VALUE"] == "good"){
        $is_good = true;
        $good_votes--;
    }
    else{
        $bad_votes--;
    }
    $DB->StartTransaction();
    if(!CIBlockElement::Delete($arFields["ID"]))
    {
        $DB->Rollback();
    }
    else{
        $DB->Commit();
    }
}


if($type_vote == "good")
{
    if(!$vote_exist) {
        $good_votes++;
        addVote($iblock_id, $type_vote_good_value, $good_votes, $ar_res_user["PHPSESSID"]);
    }else{
        if ($vote_exist && !$is_good){
            addVote($iblock_id, $type_vote_good_value, $good_votes, $ar_res_user["PHPSESSID"]);
            $good_votes++;
        }
    }

}

if($type_vote == "bad")
{
    if(!$vote_exist) {
        $bad_votes++;
        addVote($iblock_id, $type_vote_bad_value, $bad_votes, $ar_res_user["PHPSESSID"]);
    }else{
        if ($vote_exist && $is_good){
            addVote($iblock_id, $type_vote_bad_value, $bad_votes, $ar_res_user["PHPSESSID"]);
            $bad_votes++;
        }

    }
}

$arr_json["good_votes"] = $good_votes;
$arr_json["bad_votes"] = $bad_votes;

echo json_encode($arr_json);

function addVote($iblock_id, $type_vote, $curr, $phpSessId){
    $el = new CIBlockElement;

    $PROP = array();
    $PROP["GUEST_ID"] = $_SESSION["SESS_GUEST_ID"];
    $PROP["PHPSESSID"] = $phpSessId;

    $PROP["ARTICLE_ID"] = $_REQUEST["ARTICLE_ID"];
    $PROP["TYPE_VOTE"] = $type_vote;

    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID" => $iblock_id,
        "PROPERTY_VALUES" => $PROP,
        "NAME" => $PROP["PHPSESSID"],
        "ACTIVE" => "Y",            // активен
    );

    if ($PRODUCT_ID = $el->Add($arLoadProductArray))
        $curr++;
}
?>






