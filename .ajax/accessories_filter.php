<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');

$ar_res = array();
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "PROPERTY_COST", "PROPERTY_TOP");
$arFilter = Array("IBLOCK_ID"=>34, "NAME"=>'%'.$_REQUEST['q'].'%');
$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $ar_res[$arFields["ID"]] = $arFields;

}

$sections = array();
$arFilter = Array("IBLOCK_ID"=>34);
$db_list = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter, false);
while ($arr = $db_list->GetNext()) {
    $sections["SECTIONS"][$arr["ID"]]["NAME"] = $arr["NAME"];
}

?>

<?foreach ($ar_res as $id => $values):?>

    <div class="accessories-item section-small border-top border-bottom clearfix">
        <div class="accessories-item-image">
            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url('<?=CFile::GetPath($values["PREVIEW_PICTURE"])?>');"></a>
        </div>
        <div class="accessories-item-info">
            <div class="accessories-item-category">
                <a class="animate-custom red-hover" href="?SECTION_ID=<?=$values["IBLOCK_SECTION_ID"]?>"><?=$sections["SECTIONS"][$values["IBLOCK_SECTION_ID"]]["NAME"];?></a>
            </div>
            <div class="accessories-item-name clearfix">
                <a class="on-left animate-custom red-hover" href="#"><?=$values["NAME"]?></a> <span class="on-right accessories-item-price"><?=$values["PROPERTY_COST_VALUE"]?> р.</span>
            </div>
            <div class="accessories-item-desc">
                <p>
                    <?=$values["PREVIEW_TEXT"]?>
                </p>
            </div>
        </div>
        <div class="accessories-item-button">
            <a class="animate-custom red-small-link" href="#">Заказать</a>
        </div>
    </div>

<?endforeach;?>