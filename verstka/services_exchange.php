<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Выкуп и обмен Вашего автомобиля</h1>
                    </div>

                    <div class="exchange-your-car clearfix">

                        <form>

                            <div class="clearfix">

                                <div class="on-left">

                                    <div class="service-form-item">
                                        <label for="exchange-your-car-mark">Марка автомобиля:</label>
                                        <select id="exchange-your-car-mark">
                                            <option>Выберите марку</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>

                                    <div class="service-form-item">
                                        <label for="exchange-your-car-model">Модель автомобиля:</label>
                                        <select id="exchange-your-car-model">
                                            <option>Выберите модель</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="on-right">

                                    <div class="exchange-your-car-image">
                                        <img src="content/models/popup-car.png" alt="car" />
                                    </div>

                                </div>

                            </div>

                            <input type="submit" name="exchange-your-car-submit" class="red-small-link animate-custom" value="Отправить" />

                            <div class="body"><p>Наш менеджер свяжется с вами в ближайшее время с уже готовым предложением</p></div>

                        </form>

                    </div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Услуги</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кредитование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Страхование</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Выкуп и обмен Вашего автомобиля</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Тест–драйв</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Удалённое урегулирование убытков</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Гарантия</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Genius</span></a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>