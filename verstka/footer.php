        </section><!-- /Main Content -->

        <!-- Footer -->
        <footer id="footer">

            <div class="moving-bar border-top">
                <div class="box-container">

                    <div class="moving-bar-link">
                        <i class="fa fa-file-text-o"></i>
                        <a href="#">Написать отзыв</a>
                    </div>

                    <div class="moving-bar-link on-center">
                        <i class="fa fa-phone"></i>
                        <a class="use-popup" data-block="call-back">Обратный звонок</a>
                    </div>

                    <div class="moving-bar-link align-right">
                        <i class="fa fa-microphone"></i>
                        <a href="#"><span class="red-text">on-line</span> консультант</a>
                    </div>

                </div>
            </div>

            <div class="section section-gray section-sec">
                <div class="box-container clearfix">

                    <div class="on-left clearfix">

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>О компании</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Наша миссия</a></li>
                                <li><a href="#">Наши награды</a></li>
                                <li><a href="#">Блог президента</a></li>
                                <li><a href="#">Отзывы</a></li>
                                <li><a href="#">Партнёрам</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>Наши бренды</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Citroen</a></li>
                                <li><a href="#">Chevrolet</a></li>
                                <li><a href="#">Ford</a></li>
                                <li><a href="#">Peugeot</a></li>
                                <li><a href="#">Kia</a></li>
                                <li><a href="#">Seat</a></li>
                                <li><a href="#">Ssangyong</a></li>
                                <li><a href="#">Skoda</a></li>
                                <li><a href="#">Opel</a></li>
                                <li><a href="#">Volvo</a></li>
                            </ul>
                        </div>

                        <div class="on-left footer-menu-container">
                            <div class="title-footer">
                                <h2>Клиентам</h2>
                            </div>
                            <ul class="footer-menu">
                                <li><a href="#">Личный кабинет</a></li>
                                <li><a href="#">Оставить отзыв</a></li>
                                <li><a href="#">Подать жалобу</a></li>
                                <li><a href="#">Корпоративным клиентам</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="on-right footer-address left-margin">
                        <div class="title-footer">
                            <h2>Адреса салонов</h2>
                        </div>
                        <ul class="footer-menu">
                            <li><a href="#">м. Войковская, Москва, ул. Коптевская, д.69-71</a></li>
                            <li><a href="#">м. Семеновская, Москва, ул. Большая Семеновская, д.42</a></li>
                            <li><a href="#">м. Новогиреево, Выхино, Москва, МКАД, 2 км,</a></li>
                            <li><a href="#">внешний радиус, г. Реутов</a></li>
                            <li><a href="#">Московская область, г. Люберцы, Рязанское шоссе, д. 46</a></li>
                            <li><a href="#">м. Юго-Западная, Москва, Ленинский проспект, д. 146</a></li>
                            <li><a href="#">м. Южная, Пражская, Москва, Дорожный 1-й проезд, д.4</a></li>
                        </ul>
                    </div>

                    <div class="footer-search on-right">

                        <div class="title-footer">
                            <h2 class="clearfix"><label class="on-left" for="search">Поиск по сайту:</label></h2>
                        </div>

                        <form id="search-form">
                            <input type="text" name="search" id="search" class="animate-custom" />
                            <input type="submit" value="" />
                        </form>

                    </div>

                </div>
            </div>

            <div class="footer-bar">
                <div class="box-container">

                    <div class="on-left clearfix">
                        <span class="on-left">Мы в социальных сетях</span>
                        <ul class="footer-social-links on-left clearfix">
                            <li class="on-left"><a class="fb social-icon-link" href="#"><i class="fa fa-facebook animate-custom"></i></a></li>
                            <li class="on-left"><a class="tw social-icon-link" href="#"><i class="fa fa-twitter animate-custom"></i></a></li>
                            <li class="on-left"><a class="in social-icon-link" href="#"><i class="fa fa-instagram animate-custom"></i></a></li>
                            <li class="on-left"><a class="gp social-icon-link" href="#"><i class="fa fa-google-plus animate-custom"></i></a></li>
                            <li class="on-left"><a class="vk social-icon-link" href="#"><i class="fa fa-vk animate-custom"></i></a></li>
                        </ul>
                    </div>

                    <div class="on-center clearfix">
                        <span class=" left-margin on-left">Мобильные пришложения</span>
                        <a href="#" class="bg on-left app-logo animate-custom opacity-custom-hover" style="background-image: url(images/appstore.png);"></a>
                        <a href="#" class="bg on-left app-logo animate-custom opacity-custom-hover" style="background-image: url(images/googleplay.png);"></a></div>

                    <div class="on-right clearfix"><a class="on-right rss-button animate-custom" href="#"><i class="fa fa-rss"></i></a><span class="on-right">Подписаться на рассылку</span></div>

                </div>
            </div>

            <div class="copyright">
                <div class="box-container clearfix">

                    <div class="on-left">Все права защищены</div>

                    <div class="on-center"><a href="#">Фаворит-Моторс 1994–2015</a></div>

                    <div class="on-right"><a href="#">Разработка</a></div>

                </div>
            </div>

        </footer><!-- /Footer -->

    </div><!-- /Main Container -->

        <!-- Popups -->
        <div class="popups-container">

            <div class="popup medium-popup" id="call-back">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Заказ обратного звонка</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="call-back-name">Ваше имя:</label>
                                        <input type="text" name="call-back-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="call-back-service">Департамент:</label>
                                        <select id="call-back-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <div class="form-item">
                                        <label for="call-back-phone">Ваш телефон:</label>
                                        <input type="text" name="call-back-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="call-back-theme">Тема обращения:</label>
                                        <select id="call-back-theme">
                                            <option>Выбрать тему</option>
                                            <option>Тема 1</option>
                                            <option>Тема 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <input type="submit" class="red-small-link animate-custom" value="Заказать звонок" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="test-drive">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Запись на тест–драйв: Skoda Fabia</h2>
                            </div>
                            <div class="form-items">
                                <div class="clearfix">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="test-drive-name">Ваше имя:</label>
                                            <input type="text" name="test-drive-name" id="call-back-name" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="test-drive-phone">Ваш телефон:</label>
                                            <input type="text" name="test-drive-phone" id="call-back-phone" class="phone-mask" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="test-drive-mark">Марка:</label>
                                        <select id="test-drive-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="test-drive-model">Модель:</label>
                                        <select id="test-drive-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="test-drive-service">Департамент:</label>
                                        <select id="test-drive-service">
                                            <option>Выбрать</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="test-drive-image">
                            <div class="test-drive-image-container"><img src="content/models/popup-car.png" alt="model" /></div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container"><input type="checkbox" name="agreement-checkbox" id="agreement-checkbox" /><label for="agreement-checkbox">Я согласен на <a href="#">использование моих персональных данных</a></label></div>
                            <input type="submit" class="red-small-link animate-custom" value="Заказать звонок" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="book-car">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Резервирование автомобиля</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="book-car-name">Ваше имя:</label>
                                        <input type="text" name="book-car-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="book-car-phone">Ваш телефон:</label>
                                        <input type="text" name="book-car-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="book-car-service">Департамент:</label>
                                        <select id="book-car-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <div class="popup-info-fields">
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Марка:</div>
                                            <div class="popup-info-field-text on-left">KIA</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Модель:</div>
                                            <div class="popup-info-field-text on-left">QUORIS</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">VIN:</div>
                                            <div class="popup-info-field-text on-left">XWELW412BD0000612</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">Комплектация:</div>
                                            <div class="popup-info-field-text on-left">LUX 1.6 AT</div>
                                        </div>
                                        <div class="popup-info-field padding clearfix">
                                            <div class="popup-info-field-label on-left">Название акции:</div>
                                            <div class="popup-info-field-text on-left">«Успей купить»</div>
                                        </div>
                                        <div class="popup-info-field padding clearfix">
                                            <div class="popup-info-field-label on-left">Цена:</div>
                                            <div class="popup-info-field-text on-left"><strong>2 159 900</strong> р.</div>
                                        </div>
                                        <div class="popup-info-field clearfix">
                                            <div class="popup-info-field-label on-left">В кредит:</div>
                                            <div class="popup-info-field-text on-left">от <strong>12 500</strong> р./мес.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container">Вам перзвонит менеджер и уточнит детали и удобное для Вас время, чтобы подготовить документы</div>
                            <input type="submit" class="red-small-link animate-custom" value="Зарезервировать" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="event-info">
                <div class="popup-container">

                    <form class="popup-form">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Узнать подробности предложения</h2>
                            </div>
                            <div class="clearfix form-items">
                                <div class="on-left">
                                    <div class="form-item">
                                        <label for="event-info-name">Ваше имя:</label>
                                        <input type="text" name="event-info-name" id="call-back-name" />
                                    </div>
                                    <div class="form-item">
                                        <label for="event-info-phone">Ваш телефон:</label>
                                        <input type="text" name="event-info-phone" id="call-back-phone" class="phone-mask" />
                                    </div>
                                    <div class="form-item">
                                        <label for="event-info-service">Департамент:</label>
                                        <select id="event-info-service">
                                            <option>Выбрать департамент</option>
                                            <option>Департамент 1</option>
                                            <option>Департамент 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="on-right align-left">
                                    <textarea name="event-info-text" id="event-info-text" placeholder="Название акции и/или идентификатор автомобиля (vin):"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="agreement-checkbox-container">Менеджер перезвонит Вам, чтобы уточнить подробности данной акции</div>
                            <input type="submit" class="red-small-link animate-custom" value="Зарезервировать" />
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="add-service">
                <div class="popup-container">

                    <form class="popup-form action-popup">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Запись в сервисный центр</h2>
                            </div>
                            <div class="action-line">
                                <ul class="action-line-list clearfix" data-block="1">
                                    <li class="animate-custom action-line-1 active">1</li>
                                    <li class="animate-custom action-line-2">2</li>
                                    <li class="animate-custom action-line-3">3</li>
                                    <li class="animate-custom action-line-4">4</li>
                                    <li class="animate-custom action-line-5">5</li>
                                </ul>
                            </div>
                            <div class="popup-action-1 popup-action active">
                                <div class="title">
                                    <h2>Запишитесь самостоятельно - получите скидку!</h2>
                                </div>
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="add-service-name">Ваше имя:</label>
                                            <input type="text" name="add-service-name" id="add-service-name" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="add-service-phone">Ваш телефон:</label>
                                            <input type="text" name="add-service-phone" id="add-service-phone" class="phone-mask" />
                                        </div>
                                    </div>
                                </div>
                                <div class="agreement-checkbox-container"><a href="#">Запишите меня по телефону</a></div>
                            </div>
                            <div class="popup-action-2 popup-action">
                                <div class="clearfix car-line-custom-widths">
                                    <div class="form-item">
                                        <label for="add-service-mark">Марка:</label>
                                        <select id="add-service-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-model">Модель:</label>
                                        <select id="add-service-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-year">Год:</label>
                                        <select id="add-service-year">
                                            <option>Год</option>
                                            <option>2000</option>
                                            <option>2001</option>
                                            <option>2002</option>
                                            <option>2003</option>
                                            <option>2004</option>
                                            <option>2005</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-mod">Модификация:</label>
                                        <select id="add-service-mod">
                                            <option>Выбрать</option>
                                            <option>Модификация 1</option>
                                            <option>Модификация 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="test-drive-image">
                                    <div class="test-drive-image-container"><img src="content/models/popup-car.png" alt="model" /></div>
                                </div>
                            </div>
                            <div class="popup-action-3 popup-action">
                                <div class="clearfix car-line-custom-widths-2">
                                    <div class="form-item">
                                        <label for="add-service-dist">Пробег:</label>
                                        <input type="text" name="add-service-dist" id="add-service-dist" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-number">Гос.Номер:</label>
                                        <input type="text" name="add-service-number" id="add-service-number" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-region">Регион:</label>
                                        <input type="text" name="add-service-region" id="add-service-region" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-region-sel"> </label>
                                        <select id="add-service-region-sel">
                                            <option>RUS</option>
                                            <option>BEL</option>
                                            <option>EUR</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-4 popup-action">
                                <div class="clearfix form-items">
                                    <div class="form-item on-left">
                                        <label for="add-service-work">Вид работ:</label>
                                        <select id="add-service-work">
                                            <option>Выбрать</option>
                                            <option>Вид работ 1</option>
                                            <option>Вид работ 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item on-right align-left">
                                        <label for="add-service-to">Номер ТО:</label>
                                        <select id="add-service-to">
                                            <option>Выбрать</option>
                                            <option>ТО 1</option>
                                            <option>ТО 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-item full-text">
                                    <label for="add-service-desc">Описание:</label>
                                    <textarea id="add-service-desc" placeholder="Опишите проблемы, если таковые имеются"></textarea>
                                </div>
                                <div class="agreement-checkbox-container"><strong>ТО - 30</strong>. Ориентировочная стоимость <strong>11 000</strong> р.</div>
                                <div class="agreement-checkbox-container"><a href="#" class="show-hidden-desc">Подробное описание работ</a></div>
                                <div class="add-service-to-desc">
                                    <p><strong>ТО - 30</strong>. Ориентировочная стоимость <strong>11 000</strong> р.</p>
                                    <div class="agreement-checkbox-container">
                                        <p>Контроль уровня жидкости: системы охлаждения двигателя; тормозной и привода сцепления. Визуальный осмотр: Лакокрасочного покрытия; Наружных световых приборов и индикаторов; Вакуумные трубки и шланги; Валы приводов колёс, ШРУС; Герметичность системы выпуска; Приводные ремни; Радиаторы охлаждения и кондиционера; Топливные, шланги и соединения; Трубки, шланги и соединения тормозной системы; Шланги вентиляции.</p>
                                    </div>
                                    <div class="add-service-to-desc-buttons clearfix">
                                        <div class="on-left clearfix">
                                            <div class="on-left"><a href="#" class="animate-custom red-hover"><i class="fa fa-file-pdf-o red-text"></i>Скачать PDF</a></div>
                                            <div class="on-left red-hover"><a href="#" class="animate-custom red-hover"><i class="fa fa-print red-text"></i>Распечатать</a></div>
                                        </div>
                                        <div class="on-right on-center">
                                            <a href="#" class="hide-hidden-desc gray-small-link grayer animate-custom">Закрыть</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-5 popup-action">
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="add-service-date">Дата:</label>
                                            <input type="text" name="add-service-date" id="add-service-date" class="datepicker" />
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="add-service-location">Ближайший сервисный центр:</label>
                                            <select id="add-service-location" class="reload-map">
                                                <option>Выбрать центр</option>
                                                <option value="ул. Коптевская, д. 69 А">ул. Коптевская, д. 69 А</option>
                                                <option value="Выхино, МКАД, 2 км">Выхино, МКАД, 2 км</option>
                                                <option value="ул. Б. Семеновская, д. 42">ул. Б. Семеновская, д. 42</option>
                                                <option value="м. Войковская, ул. Коптевская, д. 79-81" selected>м. Войковская, ул. Коптевская, д. 79-81</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="date-area clearfix">
                                    <div class="on-left">

                                    </div>
                                    <div class="on-right">
                                        <div class="map-container">
                                            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=kP7S117fmGML_wbYmCEsYO5sqoTHXPuW&width=240&height=120"></script>
                                        </div>
                                        <div class="address-container">м. Войковская, ул. Коптевская, д. 79-81</div>
                                    </div>
                                </div>
                                <div class="time-line clearfix">
                                    <div class="time-line-label">Время:</div>
                                    <div class="time-line-buttons clearfix">
                                        <div class="time-line-button animate-custom" data-block="morning">Утро 7:00 - 12:00</div>
                                        <div class="time-line-button animate-custom" data-block="daytime">День 12:00 - 17:00</div>
                                        <div class="time-line-button animate-custom" data-block="evening">Вечер 17:00 - 22:00</div>
                                    </div>
                                    <div class="time-line-schema time-line-schema-morning" data-block="утро">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">7:00</li>
                                            <li class="animate-custom">7:30</li>
                                            <li class="animate-custom">8:00</li>
                                            <li class="animate-custom">8:30</li>
                                            <li class="animate-custom">9:00</li>
                                            <li class="animate-custom">9:30</li>
                                            <li class="animate-custom">10:00</li>
                                            <li class="animate-custom">10:30</li>
                                            <li class="animate-custom">11:00</li>
                                            <li class="animate-custom">11:30</li>
                                        </ul>
                                    </div>
                                    <div class="time-line-schema time-line-schema-daytime" data-block="день">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">12:00</li>
                                            <li class="animate-custom">12:30</li>
                                            <li class="animate-custom">13:00</li>
                                            <li class="animate-custom">13:30</li>
                                            <li class="animate-custom">14:00</li>
                                            <li class="animate-custom">14:30</li>
                                            <li class="animate-custom">15:00</li>
                                            <li class="animate-custom">15:30</li>
                                            <li class="animate-custom">16:00</li>
                                            <li class="animate-custom">16:30</li>
                                        </ul>
                                    </div>
                                    <div class="time-line-schema time-line-schema-evening" data-block="вечер">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">17:00</li>
                                            <li class="animate-custom">17:30</li>
                                            <li class="animate-custom">18:00</li>
                                            <li class="animate-custom">18:30</li>
                                            <li class="animate-custom">19:00</li>
                                            <li class="animate-custom">19:30</li>
                                            <li class="animate-custom">20:00</li>
                                            <li class="animate-custom">20:30</li>
                                            <li class="animate-custom">21:00</li>
                                            <li class="animate-custom">21:30</li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="add-service-time" id="add-service-time" />
                                    <div class="time-line-text"><span>Вы выбрали:</span><span class="time-line-text-daytime">день</span><span class="time-line-text-time">14:30</span><i class="fa fa-times animate-custom opacity-custom-hover"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom gray-small-link grayer prev">Назад</a></div>
                                <div class="on-right">
                                    <a class="animate-custom red-small-link next active">Далее</a>
                                    <input type="submit" name="add-service-submit" id="add-service-submit" class="animate-custom red-small-link" value="Записаться" />
                                </div>
                                <div class="block"></div>
                            </div>
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="car-sell">
                <div class="popup-container">

                    <form class="popup-form action-popup">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Заявка на продажу Вашего автомобиля</h2>
                            </div>
                            <div class="action-line">
                                <ul class="action-line-list clearfix" data-block="1">
                                    <li class="animate-custom action-line-1 active">1</li>
                                    <li class="animate-custom action-line-2">2</li>
                                    <li class="animate-custom action-line-3">3</li>
                                    <li class="animate-custom action-line-4">4</li>
                                    <li class="animate-custom action-line-5">5</li>
                                </ul>
                            </div>
                            <div class="title">
                                <h2>Самый просто способ продать автомобиль</h2>
                            </div>
                            <div class="action-line-text">Осталося последний шаг</div>
                            <div class="popup-action-1 popup-action active">
                                <div class="car-sell-line clearfix">
                                    <div class="form-item">
                                        <label for="car-sell-mark">Марка:</label>
                                        <select id="car-sell-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-model">Модель:</label>
                                        <select id="car-sell-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-year">Год:</label>
                                        <select id="car-sell-year">
                                            <option>Год</option>
                                            <option>Год 1</option>
                                            <option>Год 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="for-lazy-button">Я не хочу заполнять форму, <a href="#" class="animate-custom">позвонить мне</a></div>
                            </div>
                            <div class="popup-action-2 popup-action">
                                <div class="car-sell-line clearfix">
                                    <div class="form-item">
                                        <label for="car-sell-drive">Привод:</label>
                                        <select id="car-sell-drive">
                                            <option>Выбрать</option>
                                            <option>Передний</option>
                                            <option>Задний</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-box">Коробка:</label>
                                        <select id="car-sell-box">
                                            <option>Выбрать</option>
                                            <option>Коробка 1</option>
                                            <option>Коробка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-mileage">Пробег:</label>
                                        <input type="text" name="car-sell-mileage" id="car-sell-mileage" placeholder="км" />
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-3 popup-action">
                                <div class="car-sell-line clearfix">
                                    <div class="form-item">
                                        <label for="car-sell-mod">Модификация:</label>
                                        <select id="car-sell-mod">
                                            <option>Выбрать</option>
                                            <option>Модификация 1</option>
                                            <option>Модификация 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-engine">Двигатель:</label>
                                        <select id="car-sell-engine">
                                            <option>Выбрать</option>
                                            <option>Двигатель 1</option>
                                            <option>Двигатель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-volume">Объем:</label>
                                        <input type="text" name="car-sell-volume" id="car-sell-volume" placeholder="л" />
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-4 popup-action">
                                <div class="car-sell-line clearfix">
                                    <div class="form-item">
                                        <label for="car-sell-vin">VIN номер:</label>
                                        <input type="text" name="car-sell-vin" id="car-sell-volume" placeholder="Номер" />
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-condition">Состояние:</label>
                                        <select id="car-sell-condition">
                                            <option>Выбрать</option>
                                            <option>Состояние 1</option>
                                            <option>Состояние 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-sell-engine">Владельцев:</label>
                                        <select id="car-sell-engine">
                                            <option>По ПТС</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-5 popup-action">
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="car-sell-name">Ваше имя:</label>
                                            <input type="text" name="car-sell-name" id="car-sell-name" />
                                        </div>
                                        <div class="form-item">
                                            <label for="car-sell-phone">Ваш телефон:</label>
                                            <input type="text" name="car-sell-phone" id="car-sell-phone" class="phone-mask" />
                                        </div>
                                        <div class="form-item">
                                            <label for="car-sell-salon">Ближайший салон:</label>
                                            <select id="car-sell-salon">
                                                <option>Выбрать</option>
                                                <option>Салон 1</option>
                                                <option>Салон 2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="map-container with-margin">
                                            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=kP7S117fmGML_wbYmCEsYO5sqoTHXPuW&width=240&height=120"></script>
                                        </div>
                                        <div class="address-container">м. Войковская, ул. Коптевская, д. 79-81</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom gray-small-link grayer prev">Назад</a></div>
                                <div class="on-right">
                                    <a class="animate-custom red-small-link next active">Далее</a>
                                    <input type="submit" name="car-sell-submit" id="car-sell-submit" class="animate-custom red-small-link" value="Отправить заявку" />
                                </div>
                                <div class="block"></div>
                            </div>
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup medium-popup" id="car-insurance">
                <div class="popup-container">

                    <form class="popup-form action-popup">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Заявка на расчёт Страхования</h2>
                            </div>
                            <div class="action-line">
                                <ul class="action-line-list clearfix" data-block="1">
                                    <li class="animate-custom action-line-1 active">1</li>
                                    <li class="animate-custom action-line-2">2</li>
                                    <li class="animate-custom action-line-3">3</li>
                                    <li class="animate-custom action-line-4">4</li>
                                    <li class="animate-custom action-line-5">5</li>
                                </ul>
                            </div>
                            <div class="popup-action-1 popup-action active">
                                <div class="title">
                                    <h2 style="margin: 0 -10px 0 0;">На какой автомобиль Вы хотите рассчитать страховку?</h2>
                                </div>
                                <div class="form-items">
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" id="car-insurance-what-1" /><label for="car-insurance-what-1">На выбранный автомобиль: Skoda Superb Elegance 3.6 Дизель 4WD AT6 (260 л.с.)</label>
                                    </div>
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" id="car-insurance-what-2" /><label for="car-insurance-what-2">Новый автомобиль, кубленный в салоне FAVORIT MOTORS</label>
                                    </div>
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" id="car-insurance-what-3" /><label for="car-insurance-what-3">На другой автомобиль</label>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-2 popup-action">
                                <div class="title">
                                    <h2>Данные о вашем автомобиле</h2>
                                </div>
                                <div class="car-sell-line clearfix">
                                    <div class="form-item">
                                        <label for="car-insurance-mark">Марка:</label>
                                        <select id="car-insurance-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-model">Модель:</label>
                                        <select id="car-insurance-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-year">Год:</label>
                                        <select id="car-insurance-year">
                                            <option>Год</option>
                                            <option>Год 1</option>
                                            <option>Год 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="car-insurance-mileage">Пробег:</label>
                                            <input type="text" name="car-insurance-mileage" id="car-insurance-mileage" placeholder="км" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="car-insurance-price">Стоимость:</label>
                                            <input type="text" name="car-insurance-price" id="car-insurance-price" placeholder="рублей" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-3 popup-action">
                                <div class="title">
                                    <h2>Вид и особенности страхования</h2>
                                </div>
                                <div class="three-form-items clearfix">
                                    <div class="form-item on-left clearfix">
                                        <input type="checkbox" name="car-insurance-casko" id="car-insurance-casko" /><label for="car-insurance-casko">Полное КАСКО</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" name="car-insurance-casko-type" id="car-insurance-casko-type-1" /><label for="car-insurance-casko-type-1">Только УГОН</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" name="car-insurance-casko-type" id="car-insurance-casko-type-2" /><label for="car-insurance-casko-type-2">Только УЩЕРБ</label>
                                    </div>
                                </div>
                                <div class="three-form-items clearfix">
                                    <div class="form-item on-left clearfix">
                                        <input type="checkbox" name="car-insurance-osago" id="car-insurance-osago" /><label for="car-insurance-osago">ОСАГО</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" name="car-insurance-dosago" id="car-insurance-dosago" /><label for="car-insurance-dosago">ДОСАГО на сумму:</label>
                                    </div>
                                    <div class="form-item on-left">
                                        <input type="text" name="car-insurance-dosago-price" id="car-insurance-dosago-price" placeholder="Укажите сумму, руб." /><label for="car-insurance-dosago-price"></label>
                                    </div>
                                </div>
                                <div class="form-items clearfix">
                                    <div class="on-left">
                                        <div class="form-item clearfix">
                                            <input type="radio" name="car-insurance-car-price" id="car-insurance-car-price-1" /><label for="car-insurance-car-price-1">Автомобиль оплачен полностью</label>
                                        </div>
                                        <div class="form-item clearfix">
                                            <input type="radio" name="car-insurance-car-price" id="car-insurance-car-price-2" /><label for="car-insurance-car-price-2">Автомобиль куплен в кредит</label>
                                        </div>
                                        <div class="form-item clearfix">
                                            <input type="radio" name="car-insurance-car-price" id="car-insurance-car-price-3" /><label for="car-insurance-car-price-3">Автомобиль взят в лизинг</label>
                                        </div>
                                    </div>
                                    <div class="on-right">
                                        <div class="form-item">
                                            <input type="text" name="car-insurance-bank" id="car-insurance-bank" placeholder="Название банка" /><label for="car-insurance-bank"></label>
                                        </div>
                                        <div class="form-item">
                                            <input type="text" name="car-insurance-company" id="car-insurance-company" placeholder="Название компании" /><label for="car-insurance-company"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-4 popup-action">
                                <div class="title">
                                    <h2>Данные о владельце</h2>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="car-insurance-last-name-1">Фамилия:</label>
                                        <input type="text" name="car-insurance-last-name-1" id="car-insurance-last-name-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-first-name-1">Имя:</label>
                                        <input type="text" name="car-insurance-first-name-1" id="car-insurance-first-name-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-name-1">Отчество:</label>
                                        <input type="text" name="car-insurance-name-1" id="car-insurance-name-1" />
                                    </div>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="car-insurance-old-1">Возраст:</label>
                                        <input type="text" name="car-insurance-old-1" id="car-insurance-old-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-phone-1">Телефон:</label>
                                        <input type="text" name="car-insurance-phone-1" id="car-insurance-phone-1" class="phone-mask" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-email-1">Email:</label>
                                        <input type="text" name="car-insurance-email-1" id="car-insurance-email-1" class="email-mask" />
                                    </div>
                                </div>
                                <div class="clearfix car-insurance-line">
                                    <div class="form-item">
                                        <label for="car-insurance-sex">Ваш пол:</label>
                                        <select id="car-insurance-sex">
                                            <option>Выбрать</option>
                                            <option>Мужской</option>
                                            <option>Женский</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-experience">Стаж вождения:</label>
                                        <select id="car-insurance-experience">
                                            <option>Выбрать</option>
                                            <option>до 5 лет</option>
                                            <option>больше 5 лет</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-status">Семейное пол.:</label>
                                        <select id="car-insurance-status">
                                            <option>Выбрать</option>
                                            <option>Холост</option>
                                            <option>Женат</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-children">Дети:</label>
                                        <select id="car-insurance-children">
                                            <option>Выбрать</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="add-driver-line"><i class="fa fa-plus"></i><a href="#">Добавить водителя</a></div>
                            </div>
                            <div class="popup-action-5 popup-action">
                                <div class="title">
                                    <h2>Загрузите сканы документов</h2>
                                </div>
                                <div class="car-insurance-upload">
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ПАСПОРТА – все страницы </a></div>
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ПТС – все страницы</a></div>
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ВОДИТЕЛЬСКОГО УДОСТОВЕРЕНИЯ – с двух сторон</a></div>
                                    <div class="car-insurance-upload-desc">Файлы можно загружать в форматах: JPEG, PNG, PDF.<br/>Скан желательно делать в разрешении: 2000х2000 рх.<br/>Размер одного файла не должен превышать: 2 mb</div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom gray-small-link grayer prev">Назад</a></div>
                                <div class="on-right">
                                    <a class="animate-custom red-small-link next active">Далее</a>
                                    <input type="submit" name="car-insurance-submit" id="car-insurance-submit" class="animate-custom red-small-link" value="Отправить данные" />
                                </div>
                                <div class="block"></div>
                            </div>
                        </div>
                    </form>

                    <div class="popup-close animate-custom"><i class="fa fa-times"></i></div>

                </div>
            </div>

            <div class="popup-shadow"></div>

        </div><!-- /Popups -->

        <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript" src="/verstka/scripts/jquery.formstyler.min.js"></script>
        <script type="text/javascript" src="/verstka/scripts/jquery.smoothscroll.js"></script>
        <script type="text/javascript" src="/verstka/scripts/jquery.colorbox.js"></script>
        <script type="text/javascript" src="/verstka/scripts/jquery.inputmask.js"></script>
		<script type="text/javascript" src="/verstka/scripts/jquery.slicknav.min.js"></script>
        <script type="text/javascript" src="/verstka/scripts/swiper.min.js"></script>
		<script type="text/javascript" src="/verstka/scripts/response.min.js"></script>
        <script type="text/javascript" src="/verstka/scripts/interface.js"></script>
        <script type="text/javascript" src="/verstka/scripts/applications.js"></script>
		<!--script type="text/javascript" src="/verstka/scripts/jquery.pjax.js"></script-->

</body>
</html>