<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="login-block">
            <div class="box-container">

                <form id="login" class="on-center">
                    <div class="title on-center">
                        <h1>Вход в Личный кабинет</h1>
                    </div>
                    <div class="form-item clearfix">
                        <label for="user-login" class="align-right">Ваш e-mail</label>
                        <input type="text" name="user-login" id="user-login" class="email-mask" />
                        <div class="login-link align-left">
                            <a href="#" class="animate-custom red-text">Зарегистрироваться</a>
                        </div>
                    </div>
                    <div class="form-item">
                        <label for="user-pass" class="align-right">Пароль</label>
                        <input type="password" name="user-pass" id="user-pass" />
                        <div class="login-link align-left">
                            <a href="#" class="animate-custom">Забыли пароль?</a>
                        </div>
                    </div>
                    <div class="form-item">
                        <input type="submit" class="animate-custom red-big-link" value="Вход" />
                    </div>
                </form>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>