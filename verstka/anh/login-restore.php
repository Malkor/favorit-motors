<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="login-block">
            <div class="box-container">

                <form id="login" class="on-center">
                    <div class="title on-center">
                        <h1>Восстановление пароля</h1>
                    </div>
                    <div class="form-item">
                        <label for="user-email" class="align-right">Ваш e-mail</label>
                        <input type="text" name="user-email" id="user-email" placeholder="email@site.com" class="email-mask" />
                        <div class="login-link align-left">
                            <a href="#" class="animate-custom red-text">Зарегистрироваться</a>
                        </div>
                    </div>
                    <div class="form-item with-or gray-text">или</div>
                    <div class="form-item">
                        <label for="user-phone" class="align-right">Телефон</label>
                        <input type="text" name="user-phone" id="user-phone" class="phone-mask" />
                        <div></div>
                    </div>
                    <div class="form-item">
                        <input type="submit" class="animate-custom red-big-link" value="Восстановить" />
                    </div>
                    <div class="back-button">
                        <a href="#"><i class="fa fa-angle-left"></i>Вход</a>
                    </div>
                </form>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>