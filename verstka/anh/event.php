<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="section section-red padding">
            <div class="box-container">
                <div class="title">
                    <h1>Сервисные акции</h1>
                </div>
            </div>
        </div>

        <!-- Brands -->
        <div class="section section-sec section-gray padding">
            <div class="box-container medium">

                <div class="brand-items"><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/chevrolet.png" alt="chevrolet"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/citroen.png" alt="citroen"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/skoda.png" alt="skoda"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/seat.png" alt="seat"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/ford.png" alt="ford"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/volvo.png" alt="volvo"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/kia.png" alt="kia"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/peugeot.png" alt="peugeot"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/ssangyong.png" alt="ssangyong"/>
                    </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                        <img src="images/brands/opel.png" alt="opel"/>
                    </a><!--
                --></div>

                <div class="text on-center">
                    <p>Тут только приятные предложения<br/>Мы гордимся своими классными акциями, скорей-скорей жмите, кликайте, звоните. Только не забывайте скупой платит дважды.</p>
                </div>

            </div>
        </div><!-- /Brands -->

        <!-- Event Deals -->
        <div class="section section-sec">
            <div class="box-container medium">

                <div class="event-deal-items"><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-exclamation"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Акции на новые<br/>автомобили</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-car"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог подержанных<br/>авто со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-rub"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Кредитные<br/>предложения</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-umbrella"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Страхование<br/>Genius</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-file-text-o"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог автомобилей<br/>в наличии со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-expand"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Выгода при сдаче<br/>авто в Trade-In</span>
                    </a><!--
                --></div>

            </div>
        </div><!-- /Event Deals -->

        <!-- Phone -->
        <div id="add-your-phone" class="section section-sec padding section-gray">
            <div class="box-container medium">

                <div class="text on-center">
                    <p>Оставьте свой телефон и мы перезвоним вам в течении 15 минут</p>
                </div>

                <div class="filter"><form id="event" class="clearfix">

                    <div class="filter-item clearfix">
                        <label for="event-name" class="on-left">Ваше имя:</label>
                        <input id="event-name" class="on-left" type="text" />
                    </div>

                    <div class="filter-item on-center clearfix">
                        <label for="event-phone" class="on-left">Телефон:</label>
                        <input id="event-phone" class="on-left phone-mask" type="text" />
                    </div>

                    <div class="filter-item align-right">
                        <input type="submit" class="animate-custom red-small-link" value="Персональное предложение" />
                    </div>

                </form></div>

            </div>
        </div><!-- /Phone -->

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>