<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Тест-драйв</h1>
                    </div>

                    <div class="test-drive-page-form">

                        <form>

                            <div class="clearfix">

                                <div class="on-left">

                                    <div class="page-form-item">
                                        <label for="car-test-drive-mark">Марка:</label>
                                        <select id="car-test-drive-mark">
                                            <option>Выбрать</option>
                                            <option>Марка 1</option>
                                            <option>Марка 2</option>
                                        </select>
                                    </div>

                                    <div class="page-form-item">
                                        <label for="car-test-drive-model">Модель:</label>
                                        <select id="car-test-drive-model">
                                            <option>Выбрать</option>
                                            <option>Модель 1</option>
                                            <option>Модель 2</option>
                                        </select>
                                    </div>

                                    <div class="page-form-item">
                                        <label for="car-test-drive-name">Ваше имя:</label>
                                        <input type="text" name="car-test-drive-name" id="car-test-drive-name" />
                                    </div>

                                    <div class="page-form-item">
                                        <label for="car-test-drive-phone">Ваш телефон:</label>
                                        <input type="text" name="car-test-drive-phone" id="car-test-drive-phone" class="phone-mask" />
                                    </div>

                                </div>

                                <div class="on-right">

                                    <div class="brand-logo-container on-center">
                                        <img src="images/brands/large/kia.png" alt="logo" />
                                    </div>

                                </div>

                            </div>

                            <div class="title">
                                <h2>Где Вам будет удобно пройти Тест–Драйв?</h2>
                            </div>

                            <div class="clearfix">

                                <div class="on-left">

                                    <div class="checkbox-map">



                                    </div>

                                </div>

                                <div class="on-right align-left">

                                    <div class="page-form-item date-field">
                                        <label for="car-test-drive-date">Выберите дату:</label>
                                        <input type="text" name="car-test-drive-date" id="car-test-drive-date" class="datepicker" />
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <div class="page-form-item">
                                        <label for="car-test-drive-time">Выберите время:</label>
                                        <input type="hidden" name="car-test-drive-time" id="car-test-drive-time" />
                                        <div class="test-drive-time">
                                            <div class="test-drive-time-min">с <span>9:00</span></div>
                                            <div class="test-drive-time-max">до <span>21:00</span></div>
                                            <div class="test-drive-time-label">15:00 - 16:00</div>
                                            <div class="test-drive-time-line"></div>
                                        </div>
                                    </div>

                                    <div class="page-form-item">
                                        <input type="checkbox" name="car-test-drive-want" id="car-test-drive-want" /><label for="car-test-drive-want">Я хочу чтобы мне перезвонили и уточнили дату и время</label>
                                    </div>

                                    <div class="page-form-item align-right">
                                        <input type="submit" name="car-test-drive-submit" value="Записаться на тест–драйв" class="animate-custom red-small-link" />
                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Услуги</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кредитование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Страхование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Выкуп и обмен Вашего автомобиля</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Тест–драйв</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Удалённое урегулирование убытков</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Гарантия</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Genius</span></a></li>
                        </ul>
                    </div>

                    <div class="accent-button">
                        <a href="#" class="animate-custom red-small-link">Кнопка с акцентом</a>
                    </div>

                </div>

            </div>
        </div>

        <!-- Reviews -->
        <div class="section section-sec padding section-gray">
            <div class="box-container">
                <div class="title">
                    <h2>Последний отзыв о продаже своего авто в FAVORIT MOTORS</h2>
                </div>

                <div class="reviews-items">
                    <div class="reviews-item">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">Сегодня 13:54:46</div>
                                <div class="reviews-item-status positive">позитивный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /Reviews -->

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>