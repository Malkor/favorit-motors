<?php $user_bar = true; include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <!-- Service Block -->
        <div id="service-block" class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title-gray">
                    <h2>Обслуживание</h2>
                </div>

                <div class="service-work-name">Слесарные работы по переднему бамперу</div>

                <div class="service-work-block-status section-small border-top border-bottom clearfix">

                    <div class="service-work-block-line on-left">Готово</div>

                    <div class="service-work-block-line on-left">Слесарные работы</div>

                    <div class="service-work-block-line on-left">
                        <a href="#">Ланской И.А.</a>
                    </div>

                    <div class="service-work-block-line on-left"><strong>20 000 р.</strong></div>

                    <div class="service-work-block-line on-left on-center">
                        <a href="#" class="red-big-link animate-custom">Оценить работу</a>
                    </div>

                </div>

                <div class="service-work-block-code">
                    <div>Работы</div>
                    <div class="red-text"><i class="fa fa-tag"></i><strong>3H-2235466</strong></div>
                </div>

                <div class="service-work-block-items border-bottom">

                    <div class="service-work-block-item clearfix">

                        <div class="service-work-block-item-info on-left">
                            <div class="service-work-block-item-name">Установка сигнализации</div>
                            <div class="service-work-block-item-desc">Краткое описание выполненной работы, если таковое есть.</div>
                        </div>

                        <div class="service-work-block-item-author on-left">
                            <a href="#">Ланской И.А.</a>
                        </div>

                    </div>

                    <div class="service-work-block-item clearfix">

                        <div class="service-work-block-item-info on-left">
                            <div class="service-work-block-item-name">Установка ПО</div>
                            <div class="service-work-block-item-desc">Краткое описание выполненной работы, если таковое есть.</div>
                        </div>

                        <div class="service-work-block-item-author on-left">
                            <a href="#">Ланской И.А.</a>
                        </div>

                    </div>

                    <div class="service-work-block-item clearfix">

                        <div class="service-work-block-item-info on-left">
                            <div class="service-work-block-item-name">Мойка автомобиля</div>
                            <div class="service-work-block-item-desc">Краткое описание выполненной работы, если таковое есть.</div>
                        </div>

                        <div class="service-work-block-item-author on-left">
                            <a href="#">Ланской И.А.</a>
                        </div>

                    </div>

                </div>

            </div>
        </div><!-- /Service Block -->

        <!-- Map Block -->
        <div id="map-block" class="section section-sec">
            <div class="box-container">
                <div class="title-gray">
                    <h2 class="clearfix"><span class="on-left">Коптево Volvo. Технический центр: ежедневно 07:00 - 22:00</span><span class="on-right">8 (495) 786 25 25</span></h2>
                </div>

                <div class="map-block-container clearfix">

                    <div class="on-left">

                        <div id="center-map" class="map-block-map map-full">

                            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=Ah4pPb1gmj0wf4q62-VuWLRVSnfLCMp0&width=780&height=630"></script>

                        </div>

                    </div>

                    <div class="on-right">
                        <span class="red-text">Адрес:</span>
                        <p>Москва, ул. Коптевская 71.<br/>Главный офис, в здании Автосалона Volvo, подъезд №2 (между Автосалоном Volvo и Skoda) ОТДЕЛЬНЫЙ ВХОД (см. указатель)</p>
                        <span class="red-text">1 вариант:</span>
                        <p><span class="red-text">м.</span> Войковская и т.д.</p>
                    </div>

                </div>

            </div>
        </div><!-- /Map Block -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>