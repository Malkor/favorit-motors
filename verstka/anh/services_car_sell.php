<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Пять простых шагов для оценки Вашего автомобля</h1>
                    </div>

                    <form>

                        <div class="page-form-item">
                            <input type="checkbox" name="want-to-offer" id="want-to-offer" /><label for="want-to-offer" class="red-text">Я хочу обменять свой автомобиль на новой со скидкой 2%</label>
                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">1</div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-mark">Марка:</label>
                                    <select id="page-car-sell-mark">
                                        <option>Выбрать</option>
                                        <option>Марка 1</option>
                                        <option>Марка 2</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-model">Модель:</label>
                                    <select id="page-car-sell-model">
                                        <option>Выбрать</option>
                                        <option>Модель 1</option>
                                        <option>Модель 2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-year">Год выпуска:</label>
                                    <input type="text" name="page-car-sell-year" id="page-car-sell-year" placeholder="Укажите год" />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">2</div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-drive">Привод:</label>
                                    <select id="page-car-sell-drive">
                                        <option>Выбрать тип</option>
                                        <option>Привод 1</option>
                                        <option>Привод 2</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-box">Коробка:</label>
                                    <select id="page-car-sell-box">
                                        <option>Выбрать КПП</option>
                                        <option>Коробка 1</option>
                                        <option>Коробка 2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-mileage">Пробег:</label>
                                    <input type="text" name="page-car-sell-mileage" id="page-car-sell-mileage" placeholder="Км" />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">3</div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-engine">Тип двигателя:</label>
                                    <select id="page-car-sell-engine">
                                        <option>Выбрать тип</option>
                                        <option>Тип 1</option>
                                        <option>Тип 2</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-volume">Объем двигателя:</label>
                                    <input type="text" name="page-car-sell-volume" id="page-car-sell-volume" placeholder="Например, 1,6 л" />
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-mod">Модификация:</label>
                                    <input type="text" name="page-car-sell-mod" id="page-car-sell-mod" placeholder="Наример, Executive, Titanium, R8NYUGH (необязательно) " />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">4</div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-vin">VIN номер:</label>
                                    <input type="text" name="page-car-sell-vin" id="page-car-sell-vin" placeholder="Наример, YV1FS40LDF1341359" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-condition">Состояние:</label>
                                    <select id="page-car-sell-condition">
                                        <option>Выберите вариант</option>
                                        <option>Состояние 1</option>
                                        <option>Состояние 2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-owners">Владельцев по ПТС:</label>
                                    <input type="text" name="page-car-sell-owners" id="page-car-sell-owners" placeholder="Наример, 17" />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">5</div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-name">Ваше имя:</label>
                                    <input type="text" name="page-car-sell-name" id="page-car-sell-name" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-saloon">Выбрать автосалон:</label>
                                    <select id="page-car-sell-saloon">
                                        <option>Выберите вариант</option>
                                        <option>Автосалон 1</option>
                                        <option>Автосалон 2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="two-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-sell-phone">Ваш телефон:</label>
                                    <input type="text" name="page-car-sell-phone" id="page-car-sell-phone" class="phone-mask" />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section on-center">
                            <input type="submit" name="page-car-sell-submit" value="Перейти к подбору автомобиля" class="animate-custom red-small-link" />
                        </div>

                    </form>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Услуги</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кредитование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Страхование</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Выкуп и обмен Вашего автомобиля</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Тест–драйв</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Удалённое урегулирование убытков</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Гарантия</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Genius</span></a></li>
                        </ul>
                    </div>

                    <div class="accent-button">
                        <a href="#" class="animate-custom red-small-link">Кнопка с акцентом</a>
                    </div>

                </div>

            </div>
        </div>

        <!-- Reviews -->
        <div class="section section-sec padding section-gray">
            <div class="box-container">
                <div class="title">
                    <h2>Последний отзыв о продаже своего авто в FAVORIT MOTORS</h2>
                </div>

                <div class="reviews-items">
                    <div class="reviews-item">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">Сегодня 13:54:46</div>
                                <div class="reviews-item-status positive">позитивный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /Reviews -->

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>