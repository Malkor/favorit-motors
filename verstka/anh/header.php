<!DOCTYPE html><html lang="ru">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Favorit Motors</title>
    <link rel="shortcut icon" href="/images/favicon.ico">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="css/reset.css" type="text/css"/>
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css"/>
    <link rel="stylesheet" href="css/fonts.css" type="text/css"/>
    <link rel="stylesheet" href="css/swiper.css" type="text/css"/>
    <link rel="stylesheet" href="css/colorbox.css" type="text/css"/>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
    <link rel="stylesheet" href="css/response.css" type="text/css"/>
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<!-- Main Container -->
<div id="main-container">

    <!-- Main Content -->
    <section id="main-content">

        <!-- Header -->
        <header id="header" class="clearfix">

            <?php if ($user_bar): ?>
                <!-- User Bar -->
                <div class="user-bar">
                    <div class="box-container clearfix">

                        <nav class="user-menu-container on-left">
                            <ul class="user-menu navigation clearfix">
                                <li class="active"><a href="#" class="animate-custom">Личный кабинет</a></li>
                                <li><a href="#" class="animate-custom">Отзывы</a></li>
                            </ul>
                        </nav>

                        <div class="user-status on-right left-margin"><span>Favorit-VIP</span></div>

                        <div class="user-notifications on-right left-margin">
                            <ul class="user-notifications-list clearfix">
                                <li class="user-message">
                                    <a class="notification-link opacity-custom-hover animate-custom" href="#"><i class="fa fa-comment"></i></a>
                                    <a class="common-link opacity-custom-hover animate-custom" href="#">8</a>
                                </li>
                                <li class="user-notification">
                                    <a class="notification-link opacity-custom-hover animate-custom" href="#"><i class="fa fa-bell"></i></a>
                                    <a class="common-link opacity-custom-hover animate-custom" href="#">16</a>
                                </li>
                                <li class="user-pay">
                                    <a class="notification-link opacity-custom-hover animate-custom" href="#"><i class="fa fa-credit-card"></i></a>
                                    <a class="common-link opacity-custom-hover animate-custom" href="#">2</a>
                                </li>
                            </ul>
                        </div>

                        <div class="user-name on-right"><span><i class="fa fa-user"></i><a href="#">Николай Алексеевич К.</a></span></div>

                    </div>
                </div><!-- /User Bar -->
            <?php endif; ?>

            <!-- Navigation Bar -->
            <div class="navigation-bar">
                <div class="box-container clearfix">

                    <div id="logo" class="on-left"><a href="#" class="opacity-custom-hover animate-custom" style="background-image: url(images/logo.png);"></a></div>

                    <nav id="navigation" class="on-left">
                        <ul class="navigation clearfix">
                            <li class="active dropdown">
                                <a href="#" class="animate-custom">Автомобили</a>
                                <ul>
                                    <li><a href="#" class="animate-custom">Авто в наличии</a></li>
                                    <li><a href="#" class="animate-custom">Подержанные авто</a></li>
                                    <li><a href="#" class="animate-custom">На заказ</a></li>
                                    <li class="active dropdown">
                                        <a href="#" class="animate-custom">Коммерческий транспорт</a>
                                        <ul>
                                            <li><a href="#" class="animate-custom">Citroen</a></li>
                                            <li><a href="#" class="animate-custom">Peugeot</a></li>
                                            <li class="active"><a href="#" class="animate-custom">Volvo</a></li>
                                            <li><a href="#" class="animate-custom">Kia</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#" class="animate-custom">Наши марки</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="animate-custom">Тех. центр</a></li>
                            <li><a href="#" class="animate-custom">Услуги</a></li>
                            <li><a href="#" class="animate-custom">Контакты</a></li>
                            <li><a href="#" class="animate-custom">Акции</a></li>
                        </ul>
                    </nav>

                    <div class="info-block on-right">
                        <div class="work-time on-left on-center right-margin"><span>Справочная<br/>09:00 – 20:00</span></div>
                        <div class="site-phone on-left">
                            <i class="fa fa-phone"></i><a href="tel:8 (495) 786 25 25" class="opacity-custom-hover animate-custom">8 (495) 786 25 25</a>
                        </div>
                    </div>

                    <div class="login-button on-right right-margin">

                        <a href="profile.php" class="animate-custom opacity-custom-hover"><i class="fa fa-key"></i><span>Вход</span></a>

                    </div>

                </div>
            </div><!-- /Navigation Bar -->

        </header><!-- /Header -->