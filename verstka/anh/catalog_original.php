<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-gray section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Оригинальные запчасти и раходники</h1>
                    </div>

                    <div class="gray-text">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, nsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                    </div>

                    <div class="accessories-thumb-items"><!--
                        --><div class="accessories-thumb-item">
                            <div class="accessories-thumb-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/accessories/accessories-2.png); "></a>
                            </div>
                            <div class="accessories-thumb-item-title">
                                <a href="#" class="animate-custom red-hover">Набор тормозных колодок KIA</a>
                            </div>
                            <div class="accessories-thumb-item-desc gray-text">
                                <p>Устанавливаются на модель RIO 2013-2014 года выпуска</p>
                            </div>
                            <div class="accessories-thumb-item-info clearfix">
                                <div class="on-left">
                                    <div class="accessories-thumb-item-price">1 890 р.</div>
                                </div>
                                <div class="on-right">
                                    <a href="#" class="animate-custom red-small-link">Заказать</a>
                                </div>
                            </div>
                        </div><!--
                        --><div class="accessories-thumb-item">
                            <div class="accessories-thumb-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/accessories/accessories-2.png); "></a>
                            </div>
                            <div class="accessories-thumb-item-title">
                                <a href="#" class="animate-custom red-hover">Набор тормозных колодок KIA</a>
                            </div>
                            <div class="accessories-thumb-item-desc gray-text">
                                <p>Устанавливаются на модель RIO 2013-2014 года выпуска</p>
                            </div>
                            <div class="accessories-thumb-item-info clearfix">
                                <div class="on-left">
                                    <div class="accessories-thumb-item-price">1 890 р.</div>
                                </div>
                                <div class="on-right">
                                    <a href="#" class="animate-custom red-small-link">Заказать</a>
                                </div>
                            </div>
                        </div><!--
                        --><div class="accessories-thumb-item">
                            <div class="accessories-thumb-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/accessories/accessories-2.png); "></a>
                            </div>
                            <div class="accessories-thumb-item-title">
                                <a href="#" class="animate-custom red-hover">Набор тормозных колодок KIA</a>
                            </div>
                            <div class="accessories-thumb-item-desc gray-text">
                                <p>Устанавливаются на модель RIO 2013-2014 года выпуска</p>
                            </div>
                            <div class="accessories-thumb-item-info clearfix">
                                <div class="on-left">
                                    <div class="accessories-thumb-item-price">1 890 р.</div>
                                </div>
                                <div class="on-right">
                                    <a href="#" class="animate-custom red-small-link">Заказать</a>
                                </div>
                            </div>
                        </div><!--
                    --></div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Технический центр</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Сервисные акции</span></a></li>
                            <li><a href="#" class="animate-custom"><span>ТО и слесарный ремонт</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кузовной ремон</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Оригинальные запчасти</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Дополнительное оборудование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Регламент ТО</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Каталог оригинальных аксессуаров</span></a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>

        <div class="section section-sec padding">
            <div class="box-container">

                <div class="filter"><form id="accessories-filter" class="clearfix">

                        <div class="filter-item clearfix">
                            <label for="brand">Выбирите марку:</label>
                            <select id="brand">
                                <option>Все марки</option>
                                <option>Марка 1</option>
                                <option>Марка 2</option>
                            </select>
                        </div>

                        <div class="filter-item clearfix">
                            <label for="sorting">Сортировать по:</label>
                            <select id="sorting">
                                <option>Умолчанию</option>
                                <option>Параметр 1</option>
                                <option>Параметр 1</option>
                            </select>
                        </div>

                        <div class="filter-item clearfix">
                            <div class="on-left">
                                <input id="lets-search" type="submit" class="hidden" value="" />
                                <label for="lets-search"></label>
                            </div>
                            <div class="on-right">
                                <label for="search-text"></label>
                                <input id="search-text" type="text" class="large" placeholder="Поиск по каталогу запчастей" />
                            </div>
                        </div>

                    </form></div>

                <div class="accessories-items">
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                    <div class="accessories-item section-small border-top border-bottom clearfix">
                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/accessories/accessories-3.png);"></a>
                        </div>
                        <div class="accessories-item-info">
                            <div class="accessories-item-category">
                                <a class="animate-custom red-hover" href="#">Универсальная смазка</a>
                            </div>
                            <div class="accessories-item-name clearfix">
                                <a class="on-left animate-custom red-hover" href="#">WD-40</a>
                                <span class="on-right accessories-item-price">1 890 р.</span>
                            </div>
                            <div class="accessories-item-desc">
                                <p>Смазка с гидрофобным эффектом избавит от скрипов и поможет воткнуть ключ даже в сильный мороз</p>
                            </div>
                        </div>
                        <div class="accessories-item-button">
                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                        </div>
                    </div>
                </div>

                <div class="pagination">

                    <ul class="pagination-list">
                        <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                        <li><a href="#" class="animate-custom">3</a></li>
                        <li><a class="animate-custom active">4</a></li>
                        <li><a href="#" class="animate-custom">5</a></li>
                        <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                        <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                    </ul>

                    <div class="pagination-show-all"><a href="#" class="animate-custom">Показать все</a></div>

                </div>

                <div class="mail-as bottom-part on-center">
                    <span>Не нашли то, что искали?</span> <i class="fa fa-envelope"></i> <a href="#" class="animate-custom red-link">Напишите нам</a>
                </div>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>