<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="post-title">
                    <h1>ГК FAVORIT MOTORS</h1>
                </div>

                <div class="gray-text">
                    <p>Наша компания за двацатилетнию историю накопила богатый опыт на авторынке Москвы. Мы являемся офциальным дилером а/м Опель с их безумными ОРС, старейшего корейского автоконцерна СанЙонг представлеными моделями К... и далее начинаем перечислять все наши марки и модели легко и просто пишем о том какие мы разностороние какой классный у нас персонал как мы рады развиваться и прочее. Доводим требуемый Антоном до длины в 500 символов. Sed ut perspiciatis, unde omnis iste Sed ut perspiciatis, unde Amen</p>
                </div>

            </div>
        </div>

        <div class="tabs-navigation section-gray catalog-page">
            <div class="box-container clearfix">

                <div class="on-left">
                    <ul class="tabs three-tabs clearfix">
                        <li><a href="#" class="animate-custom">Автомобили в наличии</a></li>
                        <li><a href="#" class="animate-custom">Подержанные автомобили</a></li>
                        <li class="active"><a href="#" class="animate-custom">Авто на заказ</a></li>
                    </ul>
                </div>

                <div class="on-right">

                    <div class="catalog-filter-results">
                        <span>Недавно просмотренные: [ <a href="#" class="red-text animate-custom">7</a> ]</span>
                    </div>

                </div>

            </div>
        </div>

        <div class="section section-sec catalog-page">

            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="brand-items"><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/chevrolet.png" alt="chevrolet"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/citroen.png" alt="citroen"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/skoda.png" alt="skoda"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/seat.png" alt="seat"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/ford.png" alt="ford"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/volvo.png" alt="volvo"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/kia.png" alt="kia"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/peugeot.png" alt="peugeot"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/ssangyong.png" alt="ssangyong"/>
                        </a><!--
                        --><a href="#" class="animate-custom opacity-custom-hover brand-item">
                            <img src="images/brands/opel.png" alt="opel"/>
                        </a><!--
                    --></div>

                    <div class="car-type-items"><!--
                        --><a href="#" class="car-type-item animate-custom">
                            <span class="car-type-item-icon">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/1.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/1.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Седан</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/2.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/2.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Хэтчбэк</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/3.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/3.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Универсал</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/4.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/4.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Кроссовер</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/5.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/5.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Внедорожник</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/6.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/6.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Купе</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/7.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/7.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Кабриолет</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/8.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/8.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Пикап</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/9.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/9.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Минивэн</span>
                        </a><!--
                        --><a href="#" class="car-type-item">
                            <span class="car-type-item-icon animate-custom">
                                <img class="car-type-item-icon-blur animate-custom" src="images/icons/type/blur/10.png" alt="icon" />
                                <img class="car-type-item-icon-hover animate-custom" src="images/icons/type/hover/10.png" alt="icon" />
                            </span>
                            <span class="car-type-item-text animate-custom">Ком. транс.</span>
                        </a><!--
                    --></div>

                </div>

                <div class="on-right">

                    <div class="catalog-filter">
                        <form>
                            <div class="catalog-filter-item accordion active">
                                <div class="catalog-filter-title animate-custom">Марки</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Основные параметры</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Динамика и мощность</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Расход</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Двигатель</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item">
                                <div class="catalog-filter-title animate-custom">Категории</div>
                                <div class="catalog-filter-container">
                                    <ul class="catalog-filter-list">
                                        <li><a href="#">Кроссоверы до 600 т.р.</a></li>
                                        <li><a href="#">Семейные</a></li>
                                        <li><a href="#">Для девушек</a></li>
                                        <li><a href="#">Экономичные</a></li>
                                        <li><a href="#">Для поездок на дачу</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

        <div class="section section-sec catalog-page padding">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="catalog-page-cars-block">

                        <div class="title border-bottom">
                            <h2 class="red-text">Предложение дня</h2>
                        </div>

                        <div class="new-auto-items three-in-line"><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                    --></div>

                        <div class="bottom-part clearfix">
                            <div class="on-right text-body"><a href="#">Смотреть все: 76</a></div>
                        </div>

                    </div>

                    <div class="catalog-page-cars-block">

                        <div class="title border-bottom">
                            <h2>Автомобили в наличии</h2>
                        </div>

                        <div class="new-auto-items three-in-line"><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                    --></div>

                        <div class="bottom-part clearfix">
                            <div class="on-right text-body"><a href="#">Смотреть все: 88</a></div>
                        </div>

                    </div>

                    <div class="catalog-page-cars-block">

                        <div class="title border-bottom">
                            <h2>Подержанные автомобили</h2>
                        </div>

                        <div class="new-auto-items three-in-line"><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                        --><div class="new-auto-item">
                                <div class="new-auto-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                                </div>
                                <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                                <div class="new-auto-item-desc">
                                    <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                                    <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                                </div>
                                <div class="new-auto-item-info clearfix">
                                    <div class="on-left">
                                        <div class="new-auto-item-price-new">1 019 900</div>
                                        <div class="new-auto-item-price-old">1 320 000</div>
                                    </div>
                                    <div class="on-right">
                                        <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                    </div>
                                </div>
                            </div><!--
                    --></div>

                        <div class="bottom-part clearfix">
                            <div class="on-right text-body"><a href="#">Смотреть все: 420</a></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="title">
                    <h2>Заголовок</h2>
                </div>

                <div class="seo-text gray-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>