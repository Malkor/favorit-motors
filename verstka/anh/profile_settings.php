<?php $user_bar = true; include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="profile-settings">
            <!-- User Settings -->
            <div class="section section-gray section-sec section-top">
                <div class="box-container">
                    <div class="title">
                        <h2>Настройки</h2>
                    </div>
                    <div class="user-settings">
                        <div class="user-settings-inner">
                            <form>
                                <div class="photo">
                                    <img src="images/no-avatar.gif" height="200">
                                    <a href="#">изменить фото</a>
                                </div>
                                <div class="user-settings-info">
                                    <div class="name">
                                        <span>Николай Иванович Петров</span>
                                        <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                                    </div>
                                    <div class="communicate">
                                        <div class="phone">
                                            <div class="communicate-inner">
                                                <span class="communicate-title">Телефон:</span>
                                                <div class="communicate-existing">
                                                    <span>+7 (495) 786-25-25</span>
                                                    <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                                                </div>
                                                <div class="communicate-add">
                                                    <a href="#"><span class="fa fa-plus"></span> добавить ещё</a>
                                                </div>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="communicateOptions" id="communicateOptions1" value="phone" checked>
                                                <label for="communicateOptions1">предпочтительный способ связи</label>
                                            </div>
                                        </div>
                                        <div class="email">
                                            <div class="communicate-inner">
                                                <span class="communicate-title">e-mail:</span>
                                                <div class="communicate-existing">
                                                    <span>mail@mail.com</span>
                                                    <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                                                </div>
                                                <div class="communicate-add">
                                                    <a href="#"><span class="fa fa-plus"></span> добавить ещё</a>
                                                </div>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="communicateOptions" id="communicateOptions2" value="email">
                                                <label for="communicateOptions2">предпочтительный способ связи</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="user-settings-description">
                            <h3>Анкета клиента</h3>
                            <p>Текст описывающий почему важно и полезно заполнить эту важную для клиента анкету.</p>
                            <p>Плюс описать зачем нужно добавлять близкого человека.</p>
                        </div>
                    </div>
                </div>
            </div><!-- /User Settings -->
            <!-- Drivers Settings -->
            <div class="drivers-settings section-tabs">
                <div class="tabs-navigation section-gray">
                    <div class="box-container">
                        <ul class="tabs clearfix">
                            <li class="active"><a class="animate-custom" data-block="#nikolay">Николай Николаевич</a></li>
                            <li><a class="animate-custom" data-block="#addPerson">Добавить близкого человека</a></li>
                        </ul>
                    </div>
                </div>
                <div id="nikolay" class="section tab-container section-sec">
                    <div class="box-container">
                        <form>
                            <div class="address form-group clearfix">
                                <h3>Адрес проживания</h3>
                                <select id="country" class="address-select">
                                    <option>Страна</option>
                                    <option>Беларусь</option>
                                    <option>Россия</option>
                                </select>
                                <select id="city" class="address-select">
                                    <option>Город</option>
                                    <option>Минск</option>
                                    <option>Москва</option>
                                </select>
                                <input type="text" id="street" class="address-input" placeholder="Улица">
                                <input type="text" id="house" class="address-input" placeholder="Дом/корпус">
                                <input type="text" id="apartment" class="address-input" placeholder="Квартира">
                            </div>
                            <div class="birthday form-group clearfix">
                                <h3>Дата рождения</h3>
                                <select id="year" class="birthday-select">
                                    <option>Год</option>
                                    <option>1980</option>
                                    <option>1990</option>
                                </select>
                                <select id="month" class="birthday-select">
                                    <option>Месяц</option>
                                    <option>Май</option>
                                    <option>Июль</option>
                                </select>
                                <select id="day" class="birthday-select">
                                    <option>День</option>
                                    <option>11</option>
                                    <option>22</option>
                                </select>
                            </div>
                            <div class="radio-options">
                                <div class="marriage form-group form-group-radial">
                                    <h3>Семейное положение</h3>
                                    <div>
                                        <input type="radio" name="marriageOptions" id="marriageOptionsTrue" value="marriage" checked>
                                        <label for="marriageOptionsTrue">Состою в браке</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="marriageOptions" id="marriageOptionsFalse" value="marriage">
                                        <label for="marriageOptionsFalse">Не состою в браке</label>
                                    </div>
                                </div>
                                <div class="children form-group form-group-radial">
                                    <h3>Дети</h3>
                                    <div>
                                        <input type="radio" name="childrenOptions" id="childrenOptionsFalse" value="children" checked>
                                        <label for="childrenOptionsFalse">Нет детей</label>
                                    </div>
                                    <div class="children-have">
                                        <input type="radio" name="childrenOptions" id="childrenOptionsTrue" value="children">
                                        <label for="childrenOptionsTrue">Есть дети</label>
                                    </div>
                                    <input type="text" class="children-input-age" placeholder="Укажите возраст Ваших детей">
                                </div>
                            </div>
                            <div class="driving-experience form-group">
                                <h3>Водительский стаж</h3>
                                <input type="text" id="drivingExperience" placeholder="Укажите стаж">
                                <label>лет</label>
                            </div>
                            <div class="dc form-group">
                                <h3>Предпочитаемый ДЦ</h3>
                                <select id="dc" class="dc-select">
                                    <option>Выбрать ДЦ</option>
                                    <option>Выбрать ДЦ 1</option>
                                    <option>Выбрать ДЦ 2</option>
                                </select>
                            </div>
                            <div>
                                <button type="submit" class="animate-custom red-small-link">Сохранить анкету</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="addPerson" class="section tab-container section-sec">
                    <div class="box-container">
                        <form id="addPersonForm">
                            <a href="#" id="addPersonCancel"><span class="fa fa-times"></span>Отменить добавление</a>
                            <div class="person-relationship form-group">
                                <h3>Близкий человек</h3>
                                <select id="relationship" class="person-relationship-select">
                                    <option>Супруга / Супруг</option>
                                    <option>Супруга / Супруг 2</option>
                                    <option>Супруга / Супруг 3</option>
                                </select>
                            </div>
                            <div class="person-name form-group">
                                <div class="person-name-group">
                                    <label for="personName">Имя</label>
                                    <input type="text" id="personName" placeholder="Имя">
                                </div>
                                <div class="person-name-group">
                                    <label for="personPatronymic">Отчество</label>
                                    <input type="text" id="personPatronymic" placeholder="Отчество">
                                </div>
                                <div class="person-name-group">
                                    <label for="personLastName">Фамилия</label>
                                    <input type="text" id="personLastName" placeholder="Фамилия">
                                </div>
                            </div>
                            <div class="address form-group clearfix">
                                <h3>Адрес проживания</h3>
                                <div class="address-list">
                                    <select id="country" class="address-select">
                                        <option>Страна</option>
                                        <option>Беларусь</option>
                                        <option>Россия</option>
                                    </select>
                                    <select id="city" class="address-select">
                                        <option>Город</option>
                                        <option>Минск</option>
                                        <option>Москва</option>
                                    </select>
                                    <input type="text" id="street" class="address-input" placeholder="Улица">
                                    <input type="text" id="house" class="address-input" placeholder="Дом/корпус">
                                    <input type="text" id="apartment" class="address-input" placeholder="Квартира">
                                </div>
                                <div>
                                    <input type="checkbox" id="addressMine" checked>
                                    <label for="addressMine">Аналогичный моему</label>
                                </div>
                            </div>
                            <div class="birthday form-group clearfix">
                                <h3>Дата рождения</h3>
                                <select id="year" class="birthday-select">
                                    <option>Год</option>
                                    <option>1980</option>
                                    <option>1990</option>
                                </select>
                                <select id="month" class="birthday-select">
                                    <option>Месяц</option>
                                    <option>Май</option>
                                    <option>Июль</option>
                                </select>
                                <select id="day" class="birthday-select">
                                    <option>День</option>
                                    <option>11</option>
                                    <option>22</option>
                                </select>
                            </div>
                            <div class="driving-experience form-group">
                                <h3>Водительский стаж</h3>
                                <input type="text" id="drivingExperience" placeholder="Укажите стаж">
                                <label>лет</label>
                            </div>
                            <div>
                                <button type="submit" class="animate-custom red-small-link">Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /Drivers Settings -->
            <!-- Password Settings -->
            <div class="password-settings section section-gray section-sec section-sec">
                <div class="box-container">
                    <form>
                        <label for="inputNewPassword">Изменить пароль</label>
                        <div class="form-control">
                            <div class="password-settings-input">
                                <input type="password" placeholder="Новый пароль" id="inputNewPassword">
                            </div>
                            <a href="#">Напомнить старый пароль по СМС</a>
                        </div>
                        <span class="helper">Минимальная длина пароля 4 символа</span>
                        <div>
                            <button type="submit" class="animate-custom red-small-link">Изменить пароль</button>
                        </div>
                    </form>
                </div>
            </div><!-- /Password Settings -->
        </div><!-- /Profile Settings -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>