<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title-post">
                    <h1>Рекомендации при покупке автомобиля</h1>
                </div>

                <div class="gray-text">На что следует обращать внимание приобретая автомобиль</div>

            </div>
        </div>

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray">
                <div class="box-container">
                    <ul class="tabs three-tabs clearfix active-gray">
                        <li class="active"><a href="#" class="animate-custom">Все статьи</a></li>
                        <li><a href="#" class="animate-custom" >Обзоры Test-Drive</a></li>
                        <li><a href="#" class="animate-custom" >Рекомендации</a></li>
                    </ul>
                </div>
            </div>

            <div class="section section-sec">
                <div class="box-container">
                    <div class="title clearfix">
                        <h2 class="on-left">Как продать автомобиль с пробегом?</h2>
                        <div class="share-post on-right clearfix">
                            <a href="#" class="on-left gray-text">Поделиться статьёй</a>
                            <a class="fb social-icon-link on-left" href="#"><i class="fa fa-facebook animate-custom"></i></a>
                            <a class="gp social-icon-link on-left" href="#"><i class="fa fa-google-plus animate-custom"></i></a>
                            <a class="vk social-icon-link on-left" href="#"><i class="fa fa-vk animate-custom"></i></a>
                        </div>
                    </div>

                    <div class="body clearfix align-justify border-bottom">

                        <a href="content/posts/post-2.jpg" class="body-image colorbox animate-custom opacity-custom-hover show-on-left">
                            <img src="content/posts/post-2.jpg" alt="" />
                        </a>

                        <p><b>Для начала приведите свой автомобиль в надлежащий внешний вид – он должен выглядеть <strong>привлекательно, но не безупречно.</strong></b> Если есть небольшие повреждения, лучше их устранить, иначе покупатель потребует значительно снизить цену. Но и не переусердствуйте – перекрашивать автомобиль не стоит, потому что будущего владельца могут отпугнуть Самый простой, но не самый надежный способ продажи авто – рынок. Продажа машины на рынке рискует превратиться в долгий изнурительный процесс, отнимающий Ваши силы и время. Преимуществом этого способа является возможность продать свой автомобиль за максимально высокую цену. Не такой затратный и более эффективный способ выгодно расстаться со своим автомобилем – поместить объявление о продаже на специализированных сайтах в Интернете. Ваш успех будет зависеть от количества объявлений и конкурентоспособности предложения. Обратите особенное внимание на капитальный ремонт. Самый простой, но не самый надежный способ продажи авто – рынок. Продажа машины на рынке рискует превратиться в долгий изнурительный процесс, отнимающий Ваши силы и время. Преимуществом этого способа является возможность продать свой автомобиль за максимально высокую цену.<br/>Не такой затратный и более эффективный способ выгодно расстаться со своим автомобилем – поместить объявление о продаже на специализированных сайтах в Интернете. </p>

                        <a href="content/posts/post-1.jpg" class="body-image colorbox animate-custom opacity-custom-hover show-on-right">
                            <img src="content/posts/post-1.jpg" alt="" />
                        </a>

                        <p>Самый простой, но не самый надежный способ продажи авто – рынок. Продажа машины на рынке рискует превратиться в долгий изнурительный процесс, отнимающий Ваши силы и время. Преимуществом этого способа является возможность продать свой автомобиль за максимально высокую цену.<br/>Не такой затратный и более эффективный способ выгодно расстаться со своим автомобилем – поместить объявление о продаже на специализированных сайтах в Интернете. Ваш успех будет зависеть от количества объявлений и конкурентоспособности предложения. Обратите особенное внимание на форумы поклонников определенной марки или модели, так как среди их участников или гостей может быть лицо, заинтересованное в покупке.<br/>Иногда срабатывает объявление о продаже, помещенное на самой машине. Товар видят вживую, а это очень важно.<br/>Но все-таки самый простой способ продажи своего автомобиля – обращение в ГК FAVORIT MOTORS. Компания возьмет Вашу машину на комиссию и приложит все усилия, чтобы она была продана в предельно короткие сроки. Кроме того, ГК FAVORIT MOTORS предлагает услуги срочного выкупа и обмена Вашего автомобиля.</p>

                    </div>

                    <div class="likes-buttons on-center gray-text">
                        <div class="like-button right-margin"><a href="#" class="animate-custom"><i class="fa fa-thumbs-up animate-custom"></i>Статья была полезна</a><span>(7488 человек)</span></div>
                        <div class="dislike-button"><a href="#" class="animate-custom"><i class="fa fa-thumbs-down animate-custom"></i>Статья не понравилась</a><span>(1 упырь)</span></div>
                    </div>

                </div>
            </div>

            <div class="section">
                <div class="box-container">

                    <div class="post-items-block"><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                    --></div>

                    <div class="pagination">

                        <ul class="pagination-list">
                            <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                            <li><a href="#" class="animate-custom">3</a></li>
                            <li><a class="animate-custom active">4</a></li>
                            <li><a href="#" class="animate-custom">5</a></li>
                            <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                            <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                        </ul>

                        <div class="pagination-show-all"><a href="#" class="animate-custom">Архив статей</a></div>

                    </div>

                </div>
            </div>

            <!-- Seo Text -->
            <div class="section section-sec section-gray padding">
                <div class="box-container">

                    <div class="seo-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                    </div>

                </div>
            </div><!-- /Seo Text -->

        </div><!-- /Tabs Container -->

<?php include("footer.php"); ?>