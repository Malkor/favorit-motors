<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title-post">
                    <h1>Рекомендации при покупке автомобиля</h1>
                </div>

                <div class="gray-text">На что следует обращать внимание приобретая автомобиль</div>

            </div>
        </div>

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray">
                <div class="box-container">
                    <ul class="tabs three-tabs clearfix active-gray">
                        <li class="active"><a href="#" class="animate-custom">Все статьи</a></li>
                        <li><a href="#" class="animate-custom" >Обзоры Test-Drive</a></li>
                        <li><a href="#" class="animate-custom" >Рекомендации</a></li>
                    </ul>
                </div>
            </div>

            <div class="section section-sec">
                <div class="box-container">

                    <div class="post-items"><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                        --><div class="post-item">

                            <div class="post-item-image">
                                <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/posts/post-1.jpg); "></a>
                            </div>

                            <div class="post-item-title">
                                <a href="#" class="red-hover animate-custom">Заголовок</a>
                            </div>

                            <div class="post-item-desc align-justify">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem . . .</div>

                            <a href="#" class="post-item-info clearfix border-bottom animate-custom">

                                <span class="on-left">просмотров</span>

                                <span class="on-right">2041</span>

                            </a>

                        </div><!--

                    --></div>

                    <div>
                        <a href="#">Архив статей</a>
                    </div>

                </div>

            </div>

            <!-- Seo Text -->
            <div class="section section-sec section-gray padding">
                <div class="box-container">

                    <div class="seo-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                    </div>

                </div>
            </div><!-- /Seo Text -->

        </div><!-- /Tabs Container -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>