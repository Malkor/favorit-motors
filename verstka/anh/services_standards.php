<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Услуги</h1>
                    </div>

                    <div class="services-list-items"><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-user"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Личный кабинет – возможность on-line обращений к топ-менеджменту группы компаний</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-desktop"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Предоставление во временное пользование компьютера клиентам при посещении дилерского центра</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-coffee"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Бесплатные напитки<br/>Бесплатная деловая пресса</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-credit-card"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Клиентская программа накопительных скидок на покупку и обслуживание автомобилей</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-plug"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Предоставление зарядных устройств в клиентских зонах для всех типов мобильных устройств</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-bed"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Бесплатное массажное кресло в клиентской зоне</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-retweet"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>2% скидки при приеме любого автомобиля в Trade-in</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-trophy"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Гарантия лучшего страхового и кредитного предложения</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-taxi"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Заказ такси при посещении сервиса</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-thumbs-up"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Гарантированные специальные предложения, превосходящие предложения конкурентов</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-user-secret"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Предоставление одноразовой защитной одежды для клиентов, позволяющей участвовать в проведении прямой приемки</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-umbrella"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Предоставление зонтов клиентам автосалона и технического центра в дождливую погоду</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-shield"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Бесплатный автомат для чистки обуви с качественным защитным полиролем</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-gift"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Обязательный сувенир при покупке автомобиля</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-car"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Выдача автомобиля с использованием специального защитного чехла</span>
                            </div>
                        </div><!--

                        --><div class="services-list-item clearfix">
                            <div class="services-list-item-icon">
                                <a href="#" class="animate-custom opacity-custom-hover"><i class="fa fa-clock-o"></i></a>
                            </div>
                            <div class="services-list-item-text">
                                <span>Аренда автомобилей всех марок группы</span>
                            </div>
                        </div><!--

                    --></div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li class="active"><a href="#" class="animate-custom"><span>Услуги</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кредитование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Страхование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Выкуп и обмен Вашего автомобиля</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Тест–драйв</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Удалённое урегулирование убытков</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Гарантия</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Genius</span></a></li>
                        </ul>
                    </div>

                    <div class="accent-button">
                        <a href="#" class="animate-custom red-small-link">Кнопка с акцентом</a>
                    </div>

                </div>

            </div>
        </div>

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>