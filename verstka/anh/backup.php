<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <!-- Reviews -->
        <div id="profile-reviews" class="section section-sec">
            <div class="box-container">
                <div class="title-gray title-with-link">
                    <h2>Отзывы</h2>
                    <div class="title-link"><a class="animate-custom red-big-link" href="#">Написать Президенту компании</a></div>
                </div>

                <div class="reviews-items">
                    <div class="reviews-item border-bottom">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">Сегодня 13:54:46</div>
                                <div class="reviews-item-status positive">позитивный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                        <div class="reviews-item-vote clearfix">
                            <div class="on-right">
                                <span class="reviews-item-vote-label">Вам помог этот отзыв: </span>
                                <span class="reviews-item-vote-yes"><a class="animate-custom" href="#">Да</a></span>
                                <span class="reviews-item-vote-no"><a class="animate-custom" href="#">Нет</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="reviews-item border-bottom">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                <div class="reviews-item-status negative">негативный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                        <div class="reviews-item-vote clearfix">
                            <div class="on-right">
                                <span class="reviews-item-vote-label">Вам помог этот отзыв: </span>
                                <span class="reviews-item-vote-yes"><a class="animate-custom" href="#">Да</a></span>
                                <span class="reviews-item-vote-no"><a class="animate-custom" href="#">Нет</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bottom-part">
                    <a href="#">Все отзывы</a>
                </div>

            </div>
        </div><!-- /Reviews -->


    </section><!-- /Content -->

<?php include("footer.php"); ?>