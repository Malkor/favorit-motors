<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Страхование</h1>
                    </div>

                    <form>

                        <div class="five-step-form-section">
                            <div class="step-number">1</div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-what" id="page-car-insurance-what-1" /><label for="page-car-insurance-what-1">На выбранный автомобиль: Skoda Superb Elegance 3.6 Дизель 4WD AT6 (260 л.с.)</label>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-what" id="page-car-insurance-what-2" /><label for="page-car-insurance-what-2">Новый автомобиль, кубленный в салоне FAVORIT MOTORS</label>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-what" id="page-car-insurance-what-3" /><label for="page-car-insurance-what-3">На другой автомобиль</label>
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">2</div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-mark">Марка:</label>
                                    <select id="page-car-insurance-mark">
                                        <option>Выбрать</option>
                                        <option>Марка 1</option>
                                        <option>Марка 2</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-model">Модель:</label>
                                    <select id="page-car-insurance-model">
                                        <option>Выбрать</option>
                                        <option>Модель 1</option>
                                        <option>Модель 2</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-year">Год выпуска:</label>
                                    <input type="text" name="page-car-insurance-year" id="page-car-insurance-year" placeholder="Укажите год" />
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-mileage">Пробег:</label>
                                    <input type="text" name="page-car-insurance-mileage" id="page-car-insurance-mileage" placeholder="км" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-price">Стоимость:</label>
                                    <input type="text" name="page-car-insurance-price" id="page-car-insurance-price" placeholder="рублей" />
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">3</div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="checkbox" name="page-car-insurance-casko" id="page-car-insurance-casko" /><label for="page-car-insurance-casko">Полное КАСКО</label>
                                </div>
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-casko-type" id="page-car-insurance-casko-type-1" /><label for="page-car-insurance-casko-type-1">Только УГОН</label>
                                </div>
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-dosago" id="page-car-insurance-dosago" /><label for="page-car-insurance-dosago">ДОСАГО на сумму:</label>
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="checkbox" name="page-car-insurance-osago" id="page-car-insurance-osago" /><label for="page-car-insurance-osago">ОСАГО</label>
                                </div>
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-casko-type" id="page-car-insurance-casko-type-2" /><label for="page-car-insurance-casko-type-2">Только УЩЕРБ</label>
                                </div>
                                <div class="five-step-form-item">
                                    <input type="text" name="page-car-insurance-dosago-price" id="page-car-insurance-dosago-price" placeholder="Укажите сумму, руб." /><label for="page-car-insurance-dosago-price"></label>
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">4</div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-last-name-1">Ваша фамилия:</label>
                                    <input type="text" name="page-car-insurance-last-name-1" id="page-car-insurance-last-name-1" placeholder="Фамилия" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-first-name-1">Ваше имя:</label>
                                    <input type="text" name="page-car-insurance-first-name-1" id="page-car-insurance-first-name-1" placeholder="Имя" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-name-1">Ваше отчество:</label>
                                    <input type="text" name="page-car-insurance-name-1" id="page-car-insurance-name-1" placeholder="Отчество" />
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-phone-1">Ваш телефон:</label>
                                    <input type="text" name="page-car-insurance-phone-1" id="page-car-insurance-phone-1" class="phone-mask" placeholder="Телефон" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-email-1">Ваша электронная почта:</label>
                                    <input type="text" name="page-car-insurance-email-1" id="page-car-insurance-email-1" class="email-mask" placeholder="Email" />
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-old-1">Ваш возраст:</label>
                                    <input type="text" name="page-car-insurance-old-1" id="page-car-insurance-old-1" placeholder="Возраст" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-experience-1">Ваш водительсеий стаж:</label>
                                    <select id="page-car-insurance-experience-1">
                                        <option>Выбрать</option>
                                        <option>до 5 лет</option>
                                        <option>больше 5 лет</option>
                                    </select>
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-sex-1">Ваш пол:</label>
                                    <select id="page-car-insurance-sex-1">
                                        <option>Выбрать</option>
                                        <option>Мужской</option>
                                        <option>Женский</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-status-1">Семейное пол.:</label>
                                    <select id="page-car-insurance-status-1">
                                        <option>Выбрать</option>
                                        <option>Холост</option>
                                        <option>Женат</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-children-1">Дети:</label>
                                    <select id="page-car-insurance-children-1">
                                        <option>Выбрать</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-driver" id="page-car-insurance-driver-1" /><label for="page-car-insurance-driver-1">Рассчитать страховку только для Вас</label>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-driver" id="page-car-insurance-driver-2" /><label for="page-car-insurance-driver-2">Добавить водителя</label>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                Укажите данные другого водителя
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-last-name-2">Фамилия:</label>
                                    <input type="text" name="page-car-insurance-last-name-2" id="page-car-insurance-last-name-2" placeholder="Фамилия" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-first-name-2">Имя:</label>
                                    <input type="text" name="page-car-insurance-first-name-2" id="page-car-insurance-first-name-2" placeholder="Имя" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-name-2">Отчество:</label>
                                    <input type="text" name="page-car-insurance-name-2" id="page-car-insurance-name-2" placeholder="Отчество" />
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-phone-2">Телефон:</label>
                                    <input type="text" name="page-car-insurance-phone-2" id="page-car-insurance-phone-2" class="phone-mask" placeholder="Телефон" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-email-2">Электронная почта:</label>
                                    <input type="text" name="page-car-insurance-email-2" id="page-car-insurance-email-2" class="email-mask" placeholder="Email" />
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-old-2">Возраст:</label>
                                    <input type="text" name="page-car-insurance-old-2" id="page-car-insurance-old-2" placeholder="Возраст" />
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-experience-2">Водительсеий стаж:</label>
                                    <select id="page-car-insurance-experience-2">
                                        <option>Выбрать</option>
                                        <option>до 5 лет</option>
                                        <option>больше 5 лет</option>
                                    </select>
                                </div>
                            </div>

                            <div class="three-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-sex-2">Пол:</label>
                                    <select id="page-car-insurance-sex-2">
                                        <option>Выбрать</option>
                                        <option>Мужской</option>
                                        <option>Женский</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-status-2">Семейное пол.:</label>
                                    <select id="page-car-insurance-status-2">
                                        <option>Выбрать</option>
                                        <option>Холост</option>
                                        <option>Женат</option>
                                    </select>
                                </div>
                                <div class="five-step-form-item">
                                    <label for="page-car-insurance-children-2">Дети:</label>
                                    <select id="page-car-insurance-children-2">
                                        <option>Выбрать</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </div>

                            <div class="one-items-in-line clearfix">
                                <div class="five-step-form-item">
                                    <input type="radio" name="page-car-insurance-driver" id="page-car-insurance-driver-3" /><label for="page-car-insurance-driver-3">Добавить водителя</label>
                                </div>
                            </div>

                        </div>

                        <div class="five-step-form-section">
                            <div class="step-number">5</div>

                            <div class="car-insurance-upload">
                                <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ПАСПОРТА – все страницы </a></div>
                                <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ПТС – все страницы</a></div>
                                <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ВОДИТЕЛЬСКОГО УДОСТОВЕРЕНИЯ – с двух сторон</a></div>
                                <div class="car-insurance-upload-desc">Файлы можно загружать в форматах: JPEG, PNG, PDF.<br/>Скан желательно делать в разрешении: 2000х2000 рх.<br/>Размер одного файла не должен превышать: 2 mb</div>
                            </div>

                        </div>

                        <div class="five-step-form-section on-center">
                            <input type="submit" name="page-car-sell-submit" value="Отправить данные на обработку" class="animate-custom red-small-link" />
                        </div>

                        <div class="page-form-item on-center padding-top">
                            <input type="checkbox" name="page-car-insurance-check" id="page-car-insurance-check" /><label for="page-car-insurance-check">Отправляя данные, я даю своё согласие на использование указанной информации</label>
                        </div>


                    </form>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Услуги</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кредитование</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Страхование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Выкуп и обмен Вашего автомобиля</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Тест–драйв</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Удалённое урегулирование убытков</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Гарантия</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Genius</span></a></li>
                        </ul>
                    </div>

                    <div class="accent-button">
                        <a href="#" class="animate-custom red-small-link">Кнопка с акцентом</a>
                    </div>

                </div>

            </div>
        </div>


    </section><!-- /Content -->

<?php include("footer.php"); ?>