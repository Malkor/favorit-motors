<?php $user_bar = true; include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="profile-settings" class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>Настройки</h2>
                </div>
                <div class="user-settings">
                    <div class="photo">
                        <img src="images/no-avatar.gif" height="200">
                        <a href="#">изменить фото</a>
                    </div>
                    <div class="user-settings-info">
                        <div class="name">
                            <span>Николай Иванович Петров</span>
                            <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                        </div>
                        <div class="communicate">
                            <div class="phone">
                                <span class="user-settings-info-title">Телефон:</span>
                                <div class="communicate-existing">
                                    <span>+7 (495) 786-25-25</span>
                                    <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                                </div>
                                <div class="communicate-add">
                                    <a href="#"><span class="fa fa-plus"></span> добавить ещё</a>
                                </div>
                            </div>
                            <div class="email">
                                <span class="user-settings-info-title">e-mail:</span>
                                <div class="communicate-existing">
                                    <span>mail@mail.com</span>
                                    <a href="#" class="profile-settings-edit fa fa-pencil"></a>
                                </div>
                                <div class="communicate-add">
                                    <a href="#"><span class="fa fa-plus"></span> добавить ещё</a>
                                </div>
                            </div>
                            <div class="communicate-choose">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                        предпочтительный способ связи
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        предпочтительный способ связи
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>