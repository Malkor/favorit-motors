<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оригинальные запчасти");
?>
<?CModule::IncludeModule('iblock');?>
<?
$brands = array();
$res = CIBlockElement::GetList(array("sort"=>"asc"), array("IBLOCK_ID"=>10, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"), false, array(), array("ID", "NAME"));
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $brands[$arFields["ID"]] = $arFields["NAME"];
}
?>

<div class="filter">
	<form id="accessories-filter" class="clearfix">
		<div class="filter-item clearfix">
            <span for="brand">Выбирите марку:</span>
			<select id="brand" onchange="change_url('brand', this.value);">
				<option>Все марки</option>
                <?foreach ($brands as $id => $name):?>
                    <option value="<?=$id?>" <?if($_REQUEST["brand"] == $id):?>selected<?endif;?>><?=$name?></option>
                <?endforeach;?>
			</select>
		</div>
		<div class="filter-item clearfix">
            <span for="sorting">Сортировать по:</span>
			<select id="sorting" onchange="change_url('sort', this.value);">
				<option value="">Умолчанию</option>
				<option value="NAME" <?if($_REQUEST["sort"] == "NAME"):?>selected<?endif;?>>Названию</option>
				<option value="PRICE" <?if($_REQUEST["sort"] == "PRICE"):?>selected<?endif;?>>Цене (по убыванию)</option>
			</select>
		</div>

		<div class="filter-item clearfix">
			<div class="on-left">
                <input id="lets-search" type="submit" class="hidden"  value="">
			</div>
			<div class="on-right">
                <input id="search-text" type="text" class="large" name="q" placeholder="Поиск по каталогу запчастей" <?if($_REQUEST["q"]!=""):?>value="<?=$_REQUEST["q"];?>"<?endif;?>">
			</div>
		</div>
	</form>
</div>

<?
global $arrFilter;
$sect = array();
$name = "";

$sort="";?>
<?$sort_o = "";?>
<?if($_REQUEST["sort"] == "PRICE"):?>
    <?$sort = "property_COST";?>
    <?$sort_o = "desc";?>
<?else:?>
    <?$sort = "NAME";?>
    <?$sort_o = "asc";?>
<?endif;?>

<?
if($_REQUEST["SECTION_ID"])
    $sect = array("SECTION_ID" => $_REQUEST["SECTION_ID"]);

if($_REQUEST["brand"])
    $brand = array("PROPERTY_BRAND" => $_REQUEST["brand"]);

if($_REQUEST["q"])
    $name =  array("NAME"=>'%'.$_REQUEST['q'].'%');

$arrFilter = array("PROPERTY_TOP_VALUE" => "Нет", $sect, $brand, $name);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"original_parts_list", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "34",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => $sort,
		"SORT_ORDER1" => $sort_o,
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "IBLOCK_SECTION_ID",
		),
		"PROPERTY_CODE" => array(
			0 => "COST",
			1 => "TOP",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => $REQUEST["SECTION_ID"],
		"PARENT_SECTION_CODE" => $REQUEST["SECTION_CODE"],
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => "paginator_original_parts",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Оригинальные запчасти",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "form_footer",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_MODE" => "html",
    )
);?>

<script type="text/javascript">

    function change_url(name, value) {
        var variables = getUrlVar();
        var arr_res = "?";
        for(var var_name in variables){
            if (var_name != name) {
                arr_res += var_name+"="+variables[var_name]+"&";
            }
        }
        arr_res = arr_res+name+"="+value;
        window.location = "http://"+window.location.hostname.toString()+window.location.pathname.toString()+arr_res;
    }
    function getUrlVar(){
        var urlVar = window.location.search; // получаем параметры из урла
        var arrayVar = []; // массив для хранения переменных
        var valueAndKey = []; // массив для временного хранения значения и имени переменной
        var resultArray = []; // массив для хранения переменных
        arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
        if(arrayVar[0]=="") return false; // если нет переменных в урле
        for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
            valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
            resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
        }
        return resultArray; // возвращаем результат
    }
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>