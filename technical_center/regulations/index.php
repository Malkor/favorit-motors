<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регламент ТО");
?>

<div class="filter">
<form id="models-marks-filter" class="clearfix">

        <div class="filter-item clearfix">
            <label for="brand">Марка автомобиля:</label>
            <select id="brand">
                <option>Выбирите марку</option>
                <option>Марка 1</option>
                <option>Марка 2</option>
            </select>
        </div>

        <div class="filter-item clearfix">
            <label for="brand">Модель автомобиля:</label>
            <select id="brand">
                <option>Выбирите модель</option>
                <option>Модель 1</option>
                <option>Модель 2</option>
            </select>
        </div>

</form></div>

<div class="table-element">

    <div class="table-body">

        <table class="odd-even">
            <tbody>

                <tr class="border-bottom">
                    <th class="align-left">Пробег</th>
                    <th class="align-left">Двигатель</th>
                    <th class="align-left">Операции</th>
                    <th>Стоимость</th>
                    <th></th>
                </tr>
                <tr class="border-bottom">
                    <td class="align-left nowrap">20 000 км или 1 год</td>
                    <td class="align-left nowrap">1.6 (115 л.с.)</td>
                    <td class="align-left nowrap">
                        <ul>
                            <li>Замена масла в двигателе</li>
                            <li>Замена масляного фильтра</li>
                            <li>Замена топливного фильтра (бензиновые двигатели)</li>
                            <li>Замена фильтра дизтоплива (дизельный двигатель)</li>
                            <li>Замена воздушного фильтра</li>
                            <li>Замена свечей зажигания</li>
                            <li>Замена свечей зажигания на двигателях 1,6 л THP 200</li>
                            <li>Проверка плотности охлаждающей жидкости</li>
                            <li>Замена тормозной жидкости</li>
                            <li>Замена салонного фильтра</li>
                        </ul>
                    </td>
                    <td class="nowrap"><strong>20 000 p.</strong></td>
                    <td class="align-right"><a href="#" class="animate-custom red-small-link">Записаться</a></td>
                </tr>
                <tr class="border-bottom">
                    <td class="align-left nowrap">20 000 км или 1 год</td>
                    <td class="align-left nowrap">3.0 (258 л.с)</td>
                    <td class="align-left nowrap">
                        <ul>
                            <li>Замена масла в двигателе</li>
                            <li>Замена масляного фильтра</li>
                            <li>Замена топливного фильтра (бензиновые двигатели)</li>
                            <li>Замена фильтра дизтоплива (дизельный двигатель)</li>
                            <li>Замена воздушного фильтра</li>
                            <li>Замена свечей зажигания</li>
                            <li>Замена свечей зажигания на двигателях 1,6 л THP 200</li>
                            <li>Проверка плотности охлаждающей жидкости</li>
                            <li>Замена тормозной жидкости</li>
                            <li>Замена салонного фильтра</li>
                        </ul>
                    </td>
                    <td class="nowrap"><strong>40 000 p.</strong></td>
                    <td class="align-right"><a href="#" class="animate-custom red-small-link">Записаться</a></td>
                </tr>
                <tr class="border-bottom">
                    <td class="align-left nowrap">20 000 км или 1 год</td>
                    <td class="align-left nowrap">2.0 (158 л.с)</td>
                    <td class="align-left nowrap">
                        <ul>
                            <li>Замена масла в двигателе</li>
                            <li>Замена масляного фильтра</li>
                            <li>Замена топливного фильтра (бензиновые двигатели)</li>
                            <li>Замена фильтра дизтоплива (дизельный двигатель)</li>
                            <li>Замена воздушного фильтра</li>
                            <li>Замена свечей зажигания</li>
                            <li>Замена свечей зажигания на двигателях 1,6 л THP 200</li>
                            <li>Проверка плотности охлаждающей жидкости</li>
                            <li>Замена тормозной жидкости</li>
                            <li>Замена салонного фильтра</li>
                        </ul>
                    </td>
                    <td class="nowrap"><strong>30 000 p.</strong></td>
                    <td class="align-right"><a href="#" class="animate-custom red-small-link">Записаться</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>