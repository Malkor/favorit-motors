<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<?$iblock_id = 13;?>

<?
if (CModule::IncludeModule("iblock"))
$if_exist_option = CIBlockElement::GetList(array(), array("IBLOCK_CODE"=>$iblock_id, "NAME"=>$option["optionId"]), false, array("nTopCount"=>1), array("ID", "NAME"));
if($ar_res = $if_exist_option->GetNext())
		print_r();
	?>

<?//$vin = 0;?>

<!-- <pre><?print_r($res);?></pre> -->

<?
// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):

	// $res = CIBlockProperty::GetByID("VIN", false);
	// 	if($ar_res = $res->GetNext()){
	// 		?><pre><?print_r($ar_res);?></pre><?
	// }

// $soapURL = 'https://212.45.28.206/FavoritMotorsService/ExternalSite.asmx?WSDL';
// $method = "getCarActiveComplectStatic";
// $options = array('carBrandId' => "", 'carModelId' => "KIA VENGA");
// $res = soap($method, $options, $soapURL);
// header('Content-Type: application/xml; charset=utf-8');
// echo $res->Str;

$auto_catalog = simplexml_load_file("opel_original.xml");

// Перебираем все авто из выгрузки
foreach ($auto_catalog->carsOnHand->car as $car) {
	$vin = (string)$car->VIN;
	$carname = (string)$car->carname;
	$prop_arr = array();

	$if_exist_element = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "PROPERTY_VIN"=>$vin), false, array("nTopCount"=>1), array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*"));
	// Проверяем есть ли в каталоге авто по VIN
	if($ob = $if_exist_element->GetNextElement())  
   	{
   		// Если нашли авто с нужным VIN значит будем вносить изменения в уже существующую запись
      // $arFields = $ob->GetFields();
    	// print_r($arFields);
   	}
   	// Если не найдено авто с таким VIN добавляем новый элемент в каталог
   	else{
	   	foreach ($car as $key => $value){
	   		$prop_code = strtoupper($key);
				$res = CIBlockProperty::GetByID($prop_code, false);
				if ($ar_res = $res->GetNext()){
					if ($ar_res["PROPERTY_TYPE"] == "E"){
						$iblock_options_id;
						if($key == 'baseoptions'){
							$iblock_options_id = 14;
						}
						if($key == 'additionaloptions'){
							$iblock_options_id = 15;
						}
							$n = 0;
							foreach ($value->option as $option) {
								$option_name = (string)$option->optionId;
								$if_exist_option = CIBlockElement::GetList(array(), array("IBLOCK_CODE"=>strtolower($key), "NAME"=>$option_name), false, array("nTopCount"=>1), array("ID", "NAME"));
								if($ar_res = $if_exist_option->GetNext())
								{
									$prop_arr[$prop_code]["n".$n] = array("VALUE" => $ar_res["ID"]);
									$n++;
								}
								else{
									$option_props = "";
									foreach ($option as $pname => $pvalue) {
										$option_props[(string)strtoupper($pname)] = (string)$pvalue;
									}
									$el = new CIBlockElement;
									$arLoadProductArray = Array(
									  "MODIFIED_BY"    => $USER->GetID(), 	// элемент изменен текущим пользователем
									  "IBLOCK_SECTION_ID" => false,          		// элемент лежит в корне раздела
									  "IBLOCK_ID"		 	 => $iblock_options_id,
									  "PROPERTY_VALUES"=> $option_props,
									  "NAME"           => $option_name,
									  "ACTIVE"         => "Y"	         			// активен
									);
									if($PRODUCT_ID = $el->Add($arLoadProductArray))
									{
									 	echo "New property ID: ".$PRODUCT_ID;
									}
									else
									{
									  echo "Error: ".$el->LAST_ERROR;
							   	}
							  }
							}
						
					}

					if ($ar_res["PROPERTY_TYPE"] == "L") {
						$prop_id = "";
						// Находим нужное для нас свойство и ищем в нем текущее значение
						$property_enums = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$prop_code));
						while($enum_fields = $property_enums->GetNext())
						{
					  	if ($enum_fields["VALUE"] == $value) {
								$prop_id = $enum_fields["ID"];
								break;
					  	}
						}

						// Если значение не нашли, тогда добавляем новое значение
						if($prop_id == ""){
							$res = CIBlockProperty::GetByID($prop_code, false);
							if($ar_res = $res->GetNext()){
								$ibpenum = new CIBlockPropertyEnum;
								if($PropID = $ibpenum->Add(array('PROPERTY_ID' => $ar_res["ID"], 'VALUE' => $value))){
									$prop_arr[$prop_code] = array("VALUE" => $PropID);
								}
							}
						}
						else{
							$prop_arr[$prop_code] = $prop_id;
						}
					}
					if (($ar_res["PROPERTY_TYPE"] == "S") || ($ar_res["PROPERTY_TYPE"] == "N")) {
						if($ar_res["USER_TYPE"] == "directory")
							$value = strtolower($value);

						$prop_arr[$prop_code] = $value;
					}
				}
				else{

				}
			}

	   	$el = new CIBlockElement;

			$arLoadProductArray = Array(
			  "MODIFIED_BY"    => $USER->GetID(), 		// элемент изменен текущим пользователем
			  "IBLOCK_SECTION_ID" => false,          		// элемент лежит в корне раздела
			  "IBLOCK_ID"      => $iblock_id,
			  "PROPERTY_VALUES"=> $prop_arr,
			  "NAME"           => $carname,
			  "ACTIVE"         => "Y"	         		// активен
			  );

			if($PRODUCT_ID = $el->Add($arLoadProductArray))
			{
			 	echo "New ID: ".$PRODUCT_ID;
			}
			else
			{
			  echo "Error: ".$el->LAST_ERROR;
	   	}
   	}

	
}

// $res = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id), false, false, array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*"));
// while($ob = $res->GetNextElement())  
// {
// 	$arFields = $ob->GetFields();
// 	$arProperties = $ob->GetProperties();
// 	$arFields[$arFields["ID"]]["PROPERTIES"] = $arProperties;
// 	?><pre><?print_r($arFields)?></pre><?
// }

?>


<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>