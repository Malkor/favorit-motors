<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Import Departments");?>

<?$iblock_id = 27; // департаменты ?>

<?
// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):

    $departments = simplexml_load_file("departments.php");

// Перебираем все департаменты из выгрузки
    foreach ($departments->department as $department)
    {
        $markid = (string)$department->markid;
        $name = (string)$department->name.' ('.$markid.')';
        $id = (string)$department->id;
        $arSection = array();
        $ID = 0;
        $res = 0;

        $bs = new CIBlockSection;

        $arFields = Array(
            "ACTIVE" => "Y",
            "NAME" => $name,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblock_id,
            "UF_ID" => $id,
            "UF_NAME" => $name,
            "UF_MARKID" => $markid
        );

        $rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "UF_NAME"=>$name, "UF_ID"=>$id, "UF_MARKID"=>$markid));

        // Проверяем есть ли уже в инфоблоке департамент
        if ($arSection = $rsSections->Fetch())
        {
            // Если нашли, значит будем вносить изменения в уже существующую запись
            $res = $bs->Update($ID, $arFields);
            //print_r($arSection);
        }
        // Если не найден департамент, тогда добавляем новый
        else
        {
            $ID = $bs->Add($arFields);
            $res = ($ID > 0);
        }

        if($res > 0)
            echo $bs->LAST_ERROR;
    }

    ?>

<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>