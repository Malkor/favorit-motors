﻿<?xml version="1.0"?><carcomplects-catalog><creationdate-date>28.04.2015 16:09:34 GMT +3</creationdate-date><host>www.favorit-motors.ru</host><carComplects><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4KCMPD0A0B1</carcomplectid>
                            <carcomplectname>PEUGEOT 301</carcomplectname>
                            <carcomplectbaseprice>801 900,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>6,30</Fuel_consumption_city> <Fuel_consumption_track>4,60</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>980</Mass> <Equipment_code>1PM3A4KCMPD0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1467</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Aнтиблокировочная  система (ABS) + Система помощи при экстренном торможении (AFU)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Фронтальная подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Фронтальная подушка безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OP01</optionId>
                        <optionname>Ручное отключение подушки безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CF08</optionId>
                        <optionname>Трехточечные ремни безопасности с преднатяжителем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TG08</optionId>
                        <optionname>Предупреждающий сигнал о непристегнутом ремне водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JA11</optionId>
                        <optionname>1 ключ с ДУ с третьей кнопкой открытия багажника + 1 механический ключ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральный замок с кнопкой закрытия на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Металлическая защита картера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FB02</optionId>
                        <optionname>Задний противотуманный фонарь</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH01</optionId>
                        <optionname>Рулевое колесо с регулировкой по наклону</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>MO03</optionId>
                        <optionname>Кнопка открытия багажника на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OR03</optionId>
                        <optionname>Хромированные вставки в интерьере</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GM19</optionId>
                        <optionname>Панель приборов Active&#038;Allure</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02, LU02</optionId>
                        <optionname>Передние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE03</optionId>
                        <optionname>Задние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RJ04, RK04, ON02</optionId>
                        <optionname>Наружные зеркала заднего вида с электроприводом регулировок и подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WF13</optionId>
                        <optionname>Подогрев нижней части лобового стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IT01, IP01, IQ01, IU</optionId>
                        <optionname>Передние сиденья с ручными регулировками в 4-х направлениях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние подголовники, регулируемые по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Задние сиденья, складываемые в пропорции 1/3:2/3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RH12</optionId>
                        <optionname>Сиденье водителя, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>8HFX</optionId>
                        <optionname>Отделка сидений тканью Oline MIstral (темная)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>HY01</optionId>
                        <optionname>Салонный фильтр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PS02</optionId>
                        <optionname>Регулировка уровня наклона фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EJ03</optionId>
                        <optionname>Подсветка багажного отделения</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PY12</optionId>
                        <optionname>Солнцезащитные козырьки с зеркальцем со стороны пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Розетка 12В с прикуривателем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH39</optionId>
                        <optionname>15&#034; стальные диски, полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RZ89</optionId>
                        <optionname>Ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YI01</optionId>
                        <optionname>Наружные зеркала заднего вида, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампера, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQC</optionId>
                        <optionname>Аудиоподготовка с антенной на крыше</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>GV04</optionId>
                        <optionname>Пакет &#034;Внешний вид&#034;: Хром. окантовка окон дверей+Хром. вставка в пер. бампер+Противотуманные фары</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные фары</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE02</optionId>
                        <optionname>Кондиционер с электронным управлением</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQD</optionId>
                        <optionname>Аудиосистема с поддержкой CD + MP3, аудиовходом AUX и 2 динамиками</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH75</optionId>
                        <optionname>15&#034; литые диски, полноразмерное запасное колесо</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYFCGPD0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 064 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,50</Top100> <Fuel_consumption_city>6,00</Fuel_consumption_city> <Fuel_consumption_track>4,30</Fuel_consumption_track> <Fuel_consumption>4,90</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1045</Mass> <Equipment_code>1PI4SYFCGPD0A0B0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>169</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1582</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>NF01</optionId>
                        <optionname>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCALD0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 159 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>11,20</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1140</Mass> <Equipment_code>1PI4SYLCALD0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1666</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4D</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель (не доступно для двигателя 1.6 120 АКПП)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQU</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем, Bluetooth, USB, навигация и т.д.</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYMCA5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 174 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,50</Top100> <Fuel_consumption_city>7,70</Fuel_consumption_city> <Fuel_consumption_track>4,80</Fuel_consumption_track> <Fuel_consumption>5,90</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1080</Mass> <Equipment_code>1PI4SYMCA5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>196</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1644</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>ZH4D</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4F</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4E</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Противоугонная сигнализация с датчиками периметра и объема</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель (не доступно для двигателя 1.6 120 АКПП)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQU</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем, Bluetooth, USB, навигация и т.д.</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VP</carmodel>
                            <carcomplectid>2PG95QPBA632CG81</carcomplectid>
                            <carcomplectname>PEUGEOT EXPERT VP</carcomplectname>
                            <carcomplectbaseprice>1 612 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>минивэн</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>9</Seats> <Volume>1997</Volume> <Power>120</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>14,70</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>5135</Length> <Width>1895</Width> <Height>1942</Height> <Clearance>0</Clearance> <Trunk_size>553</Trunk_size> <Fuel_tank_size>80</Fuel_tank_size> <Mass>1919</Mass> <Equipment_code>2PG95QPBA632CG81</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>Expert Tepee</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>300/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2791</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>AN13</optionId>
                        <optionname>3-й ряд сидений (3 места)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Остекленные распашные задние двери</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD11</optionId>
                        <optionname>Застекленная откидная задняя дверь с электрообогревом и стеклоочистителем + темная тонировка стекол</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности для передних пассажиров</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK18</optionId>
                        <optionname>Подушка безопасности для передних пассажиров + подогрев передних сидений</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK23</optionId>
                        <optionname>Подушка безопасности пассажира + 1 сиденье для пассажира</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK40</optionId>
                        <optionname>Подушка безопасности пассажира, подогрев передних сидений + 1 сиденье для пассажира</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK26</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности (1 сиденье пассажира + водитель)</optionname>
                        <optionprice>20 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK27</optionId>
                        <optionname>Боковые подушки безопасности + подогрев сидений</optionname>
                        <optionprice>20 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK28</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, боковые шторки безопасности для 1-го,</optionname>
                        <optionprice>42 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK29</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, боковые шторки безопасности для 1-го,</optionname>
                        <optionprice>50 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Система помощи при парковке задним ходом</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK05</optionId>
                        <optionname>Автоматическое включение фар внешнего освещения + датчик дождя + противотуманные фары (стандарт при</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом , электрообогревом и электроприводом складывания</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное лобовое стекло</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AL30</optionId>
                        <optionname>2-й ряд сидений (3 места): (съемная складывающаяся 2-х местная банкетка + складывающееся откидное и</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Левая боковая остекленная сдвижная дверь</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Автоматический климат-контроль</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR02</optionId>
                        <optionname>Кондиционер для передних и задних пасажиров</optionname>
                        <optionprice>51 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампер и зеркала заднего вида, окрашенные в цвет кузова, + противотуманные фары +</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер для передних пасажиров</optionname>
                        <optionprice>40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK39</optionId>
                        <optionname>Подогрев сидений + 1 сиденье для пассажира</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLVZ</optionId>
                        <optionname>Аудиосистема RD45</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV0</optionId>
                        <optionname>Аудиосистема RD45</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV1</optionId>
                        <optionname>Навигационная система</optionname>
                        <optionprice>45 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; (серебрянный металлик GRIS ALUMINUM (ZRM0), серый металлик</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>3008</carfamily>
                            <carmodel>3008</carmodel>
                            <carcomplectid>1PTBSYHCUM32A0F0</carcomplectid>
                            <carcomplectname>PEUGEOT 3008</carcomplectname>
                            <carcomplectbaseprice>1 444 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,50</Top100> <Fuel_consumption_city>11,00</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,70</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4365</Length> <Width>1837</Width> <Height>1639</Height> <Clearance>0</Clearance> <Trunk_size>432</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1474</Mass> <Equipment_code>1PTBSYHCUM32A0F0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>3008 I Рecтайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>202</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2020</FullMass> <max_trunk_size>1241</max_trunk_size> <baseoptions><option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности водителя и пассажира, подушка пассажира деактивируемая</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система динамической стабилизации (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральная  блокировка  дверных  замков с помощью  пульта дистанционного  управления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IN03</optionId>
                        <optionname>Сиденье пассажира, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JB04</optionId>
                        <optionname>Передний центральный подлокотник с вентилируемым вещевым отделением объемом 13,5 л</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AU02</optionId>
                        <optionname>Подлокотник для 2-го ряда сидений, с доступом к багажному отделению из салона для облегчения перевоз</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние электрические стеклоподъемники  с защитой от защемления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ON02</optionId>
                        <optionname>Электрообогрев боковых зеркал заднего вида</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH09</optionId>
                        <optionname>Рулевое колесо с кожаной отделкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CN04</optionId>
                        <optionname>Рулевая колонка, регулируемая по вылету и наклону</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YQ01</optionId>
                        <optionname>Pack Visibilite: 
• система автоматического  включения стеклоочистителей и фар
• многослойное атерма</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RCM4</optionId>
                        <optionname>Аудиосистема RD4, включает проигрыватель компакт-дисков с поддержкой MP3, 6 динамиков</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UX17</optionId>
                        <optionname>Pack Look Aventure:
• защитные металлические накладки на бамперах 
• защитная пластмассовая накладка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>17&#034; стальные колесные диски с декоративными колпаками Atax</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша &#034;Cielo&#034;</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PER6</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH86</optionId>
                        <optionname>17&#034; легкосплавные колесные диски (не доступны к заказу в сочетании с системой &#034;Grip Control&#034;)</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZV02</optionId>
                        <optionname>Цветной проекционный &#034;head-up&#034; дисплей, с регулировкой угла наклона и памятью положения включая импу</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0PQ1</optionId>
                        <optionname>Pack Cuir: Кожаная обивка сидений,  коврики в салон (включая электрорегулировку сиденья водителя в 6</optionname>
                        <optionprice>59 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация с автономным питанием сирены и датчиками периметра и объема</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UD03</optionId>
                        <optionname>Солнцезащитные шторки для 2-го ряда сидений</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы (снег,</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контроль, ограничитель скорости движения автомобиля</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчик парковки задний</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Стекла второго ряда сидений и стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLRZ</optionId>
                        <optionname>АУДИОСИСТЕМА WIP SOUND</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSN</optionId>
                        <optionname>Навигационная система.....</optionname>
                        <optionprice>45 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>YO01</optionId>
                        <optionname>PACK VISIBILITE</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZD09</optionId>
                        <optionname>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZE13</optionId>
                        <optionname>Адаптивные би-ксеноновые поворотные фары, включая автоматический омыватель фар</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задний датчик парковки, камера заднего вида, включая навигационную систему RT6 с 7&#034; цветным ЖК-диспл</optionname>
                        <optionprice>49 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLWI</optionId>
                        <optionname>Аудиосистема WIP Sound, Bluetooth, разъем USB для подключения различных цифровых носителей, располож</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLWF</optionId>
                        <optionname>Навигационная система, включая CD/MP3 аудиосистему + AM/FM радио, 7&#034; цветной ЖК-дисплей, 6 динамиков</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM5/M6</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ISFX</optionId>
                        <optionname>Комбинированная отделка кожа/ткань</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4KBW5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 301</carcomplectname>
                            <carcomplectbaseprice>717 900,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>92</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>11,30</Top100> <Fuel_consumption_city>4,80</Fuel_consumption_city> <Fuel_consumption_track>3,70</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1165</Mass> <Equipment_code>1PM3A4KBW5D0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>230/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>180</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1549</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>ZH75</optionId>
                        <optionname>15&#034; литые диски, полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>8WFZ</optionId>
                        <optionname>Отделка сидений тканью Oza Lama (светлая) - ведет за собой установку боковых ПБ (NF01)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AB11</optionId>
                        <optionname>Противоугонная сигнализация + 2 ключа с ДУ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GV04</optionId>
                        <optionname>Пакет &#034;Внешний вид&#034;: Хром. окантовка окон дверей+Хром. вставка в пер. бампер+Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE02</optionId>
                        <optionname>Кондиционер с электронным управлением</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQD</optionId>
                        <optionname>Аудиосистема с поддержкой CD + MP3, аудиовходом AUX и 2 динамиками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AF01</optionId>
                        <optionname>Aнтиблокировочная  система (ABS) + Система помощи при экстренном торможении (AFU)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Фронтальная подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Фронтальная подушка безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OP01</optionId>
                        <optionname>Ручное отключение подушки безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CF08</optionId>
                        <optionname>Трехточечные ремни безопасности с преднатяжителем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TG08</optionId>
                        <optionname>Предупреждающий сигнал о непристегнутом ремне водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JA11</optionId>
                        <optionname>1 ключ с ДУ с третьей кнопкой открытия багажника + 1 механический ключ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральный замок с кнопкой закрытия на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Металлическая защита картера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FB02</optionId>
                        <optionname>Задний противотуманный фонарь</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH01</optionId>
                        <optionname>Рулевое колесо с регулировкой по наклону</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>MO03</optionId>
                        <optionname>Кнопка открытия багажника на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OR03</optionId>
                        <optionname>Хромированные вставки в интерьере</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GM19</optionId>
                        <optionname>Панель приборов Active&#038;Allure</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02, LU02</optionId>
                        <optionname>Передние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE03</optionId>
                        <optionname>Задние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RJ04, RK04, ON02</optionId>
                        <optionname>Наружные зеркала заднего вида с электроприводом регулировок и подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WF13</optionId>
                        <optionname>Подогрев нижней части лобового стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IT01, IP01, IQ01, IU</optionId>
                        <optionname>Передние сиденья с ручными регулировками в 4-х направлениях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние подголовники, регулируемые по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Задние сиденья, складываемые в пропорции 1/3:2/3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RH12</optionId>
                        <optionname>Сиденье водителя, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>8HFX</optionId>
                        <optionname>Отделка сидений тканью Oline MIstral (темная)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>HY01</optionId>
                        <optionname>Салонный фильтр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PS02</optionId>
                        <optionname>Регулировка уровня наклона фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EJ03</optionId>
                        <optionname>Подсветка багажного отделения</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PY12</optionId>
                        <optionname>Солнцезащитные козырьки с зеркальцем со стороны пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Розетка 12В с прикуривателем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH39</optionId>
                        <optionname>15&#034; стальные диски, полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RZ89</optionId>
                        <optionname>Ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YI01</optionId>
                        <optionname>Наружные зеркала заднего вида, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампера, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQC</optionId>
                        <optionname>Аудиоподготовка с антенной на крыше</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions></additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCA5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 124 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYLCA5D0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4D</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель (не доступно для двигателя 1.6 120 АКПП)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQU</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем, Bluetooth, USB, навигация и т.д.</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>3008</carfamily>
                            <carmodel>3008</carmodel>
                            <carcomplectid>1PTBSYHCA532A0F0</carcomplectid>
                            <carcomplectname>PEUGEOT 3008</carcomplectname>
                            <carcomplectbaseprice>1 307 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>11,80</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4365</Length> <Width>1837</Width> <Height>1639</Height> <Clearance>0</Clearance> <Trunk_size>432</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1474</Mass> <Equipment_code>1PTBSYHCA532A0F0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>3008 I Рecтайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2020</FullMass> <max_trunk_size>1241</max_trunk_size> <baseoptions><option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности водителя и пассажира, подушка пассажира деактивируемая</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система динамической стабилизации (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральная  блокировка  дверных  замков с помощью  пульта дистанционного  управления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IN03</optionId>
                        <optionname>Сиденье пассажира, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JB04</optionId>
                        <optionname>Передний центральный подлокотник с вентилируемым вещевым отделением объемом 13,5 л</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AU02</optionId>
                        <optionname>Подлокотник для 2-го ряда сидений, с доступом к багажному отделению из салона для облегчения перевоз</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние электрические стеклоподъемники  с защитой от защемления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ON02</optionId>
                        <optionname>Электрообогрев боковых зеркал заднего вида</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH09</optionId>
                        <optionname>Рулевое колесо с кожаной отделкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CN04</optionId>
                        <optionname>Рулевая колонка, регулируемая по вылету и наклону</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YQ01</optionId>
                        <optionname>Pack Visibilite: 
• система автоматического  включения стеклоочистителей и фар
• многослойное атерма</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RCM4</optionId>
                        <optionname>Аудиосистема RD4, включает проигрыватель компакт-дисков с поддержкой MP3, 6 динамиков</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UX17</optionId>
                        <optionname>Pack Look Aventure:
• защитные металлические накладки на бамперах 
• защитная пластмассовая накладка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>17&#034; стальные колесные диски с декоративными колпаками Atax</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>UF02</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы (снег,</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация с автономным питанием сирены и датчиками периметра и объема</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контроль, ограничитель скорости движения автомобиля</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZE13</optionId>
                        <optionname>Адаптивные би-ксеноновые поворотные фары, включая автоматический омыватель фар</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZV02</optionId>
                        <optionname>Цветной проекционный &#034;head-up&#034; дисплей, с регулировкой угла наклона и памятью положения включая импу</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UD03</optionId>
                        <optionname>Солнцезащитные шторки для 2-го ряда сидений</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZD09</optionId>
                        <optionname>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша &#034;Cielo&#034;</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчик парковки задний</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задний датчик парковки, камера заднего вида, включая навигационную систему RT6 с 7&#034; цветным ЖК-диспл</optionname>
                        <optionprice>49 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLWI</optionId>
                        <optionname>Аудиосистема WIP Sound, Bluetooth, разъем USB для подключения различных цифровых носителей, располож</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLWF</optionId>
                        <optionname>Навигационная система, включая CD/MP3 аудиосистему + AM/FM радио, 7&#034; цветной ЖК-дисплей, 6 динамиков</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Стекла второго ряда сидений и стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH86</optionId>
                        <optionname>17&#034; легкосплавные колесные диски (не доступны к заказу в сочетании с системой &#034;Grip Control&#034;)</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM5/M6</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ISFX</optionId>
                        <optionname>Комбинированная отделка кожа/ткань</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0PQ1</optionId>
                        <optionname>Pack Cuir: Кожаная обивка сидений,  коврики в салон (включая электрорегулировку сиденья водителя в 6</optionname>
                        <optionprice>49 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A024</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>935 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>2</Seats> <Volume>1560</Volume> <Power>75</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,60</Top100> <Fuel_consumption_city>6,70</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>5,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4380</Length> <Width>1810</Width> <Height>1844</Height> <Clearance>154</Clearance> <Trunk_size>3300</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1363</Mass> <Equipment_code>2PB9J0CBX532A024</Equipment_code> <equipment_name>VU</equipment_name> <Generation>Partner II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>185/1500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2215</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>OP9W</optionId>
                        <optionname>Тканевая обивка 9WFZ GRIS NARBONNAISCLAIR C&#038;T RESEAU G260+OMNI</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB07</optionId>
                        <optionname>Неостекленные распашные задние двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CZ11</optionId>
                        <optionname>Полезная нагрузка 850 кг</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL06</optionId>
                        <optionname>Стальные колеса 195/70 R15 с колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель и съемная пепельница</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Потолочная консоль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OPP0</optionId>
                        <optionname>Лакокрасочное покрытие кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX02</optionId>
                        <optionname>Пакет &#034;Повышенная проходимость&#034;: Защита картера + Увеличенный на 10 мм дорожный просвет засчёт испол</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX24</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLLO/WLRW</optionId>
                        <optionname>RCM4+DO01+USB</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова типа &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>898 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532A0C0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>189</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секвентальные электрические стеклоподъемники с функцией анти-зажим</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH09</optionId>
                        <optionname>Кожаный руль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLTV</optionId>
                        <optionname>Автомагнитола CD RD4  + MP3 + Управление аудиоситемой на рулевой  колонке  + Коммуникационная систем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZRM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;Серебристый металлик&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>LW02</optionId>
                        <optionname>Подогрев нижней части лобового стекла + обогреваемые форсунки омывателя</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль + воздуховоды для задних пассажиров</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>ЛАКОКРАСОЧНОЕ ПОКРЫТИЕ КУЗОВА МЕТАЛЛИК</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VU</carmodel>
                            <carcomplectid>2PG91CHBA632FC81</carcomplectid>
                            <carcomplectname>PEUGEOT EXPERT VU</carcomplectname>
                            <carcomplectbaseprice>1 259 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>1997</Volume> <Power>120</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>12,80</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,20</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4805</Length> <Width>1895</Width> <Height>1942</Height> <Clearance>0</Clearance> <Trunk_size>5000</Trunk_size> <Fuel_tank_size>80</Fuel_tank_size> <Mass>1710</Mass> <Equipment_code>2PG91CHBA632FC81</Equipment_code> <equipment_name>L1H1</equipment_name> <Generation>Expert</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>300/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2698</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>PJ0</optionId>
                        <optionname>Две сдвижные боковые двери</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Остекленные распашные задние двери со стеклоочистителем и электрообогревом</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампер, окрашенные в цвет кузова + противотуманные фары + зеркала заднего вида с э</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности пассажира (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK18</optionId>
                        <optionname>Подушка безопасности пассажира + подогрев передних сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK23</optionId>
                        <optionname>Подушка безопасности пассажира (1 сиденье для пассажира + водитель)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK40</optionId>
                        <optionname>Подушка безопасности пассажира + подогрев передних сидений (1 сиденье для пассажира + водитель)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK26</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности (1 сиденье пассажира + водитель)</optionname>
                        <optionprice>20 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK27</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, включая обогрев сидений (1 сиденье пас</optionname>
                        <optionprice>24 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX16</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX17</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль и ограничение максимальной скорости</optionname>
                        <optionprice>4 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Система помощи при парковке задним ходом</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация с автономным питанием сирены и датчиками периметра и объема</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK05</optionId>
                        <optionname>Автоматическое включение фар внешнего освещения + датчик дождя + противотуманные фары (стандарт при</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом регулировки, электрообогревом и электроприводом складывания</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное лобовое стекло</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ES03</optionId>
                        <optionname>Розетка на 12 В в грузовом отсеке</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB33</optionId>
                        <optionname>Покрытие пола грузового отделения пластиком</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB34</optionId>
                        <optionname>Покрытие пола+ защита стен грузового отделения пластиком</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB35</optionId>
                        <optionname>Покрытие пола грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB36</optionId>
                        <optionname>Покрытие пола багажного отсека + защита стен грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>11 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK39</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLVZ</optionId>
                        <optionname>Аудиосистема RD45,</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV0</optionId>
                        <optionname>Аудиосистема RD45,</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV1</optionId>
                        <optionname>Навигационная система</optionname>
                        <optionprice>40 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>QC01</optionId>
                        <optionname>Коммутационный блок прицепа, блок трансформации кузова (BTC)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; (серебрянный металлик GRIS ALUMINUM (ZRM0), серый металлик</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB33</optionId>
                        <optionname>Покрытие пола грузового отделения пластиком</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX16</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB34</optionId>
                        <optionname>Покрытие пола+ защита стен грузового отделения пластиком</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом регулировки, электрообогревом и электроприводом складывания</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности пассажира (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB35</optionId>
                        <optionname>Покрытие пола грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>11 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname></optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VU</carmodel>
                            <carcomplectid>2PG95DHBS532A081</carcomplectid>
                            <carcomplectname>PEUGEOT EXPERT VU</carcomplectname>
                            <carcomplectbaseprice>1 209 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>1560</Volume> <Power>90</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>16,70</Top100> <Fuel_consumption_city>7,90</Fuel_consumption_city> <Fuel_consumption_track>6,40</Fuel_consumption_track> <Fuel_consumption>6,90</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5135</Length> <Width>1895</Width> <Height>1942</Height> <Clearance>0</Clearance> <Trunk_size>6000</Trunk_size> <Fuel_tank_size>80</Fuel_tank_size> <Mass>1692</Mass> <Equipment_code>2PG95DHBS532A081</Equipment_code> <equipment_name>L2H1</equipment_name> <Generation>Expert</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>180/1500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>145</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2880</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB33</optionId>
                        <optionname>Покрытие пола грузового отделения пластиком</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX16</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname></optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB34</optionId>
                        <optionname>Покрытие пола+ защита стен грузового отделения пластиком</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом регулировки, электрообогревом и электроприводом складывания</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности пассажира (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB35</optionId>
                        <optionname>Покрытие пола грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>11 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PJ0</optionId>
                        <optionname>Две сдвижные боковые двери</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Остекленные распашные задние двери со стеклоочистителем и электрообогревом</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампер, окрашенные в цвет кузова + противотуманные фары + зеркала заднего вида с э</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности пассажира (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK18</optionId>
                        <optionname>Подушка безопасности пассажира + подогрев передних сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK23</optionId>
                        <optionname>Подушка безопасности пассажира (1 сиденье для пассажира + водитель)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK40</optionId>
                        <optionname>Подушка безопасности пассажира + подогрев передних сидений (1 сиденье для пассажира + водитель)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK26</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности (1 сиденье пассажира + водитель)</optionname>
                        <optionprice>20 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK27</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, включая обогрев сидений (1 сиденье пас</optionname>
                        <optionprice>24 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX16</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX17</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль и ограничение максимальной скорости</optionname>
                        <optionprice>4 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Система помощи при парковке задним ходом</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация с автономным питанием сирены и датчиками периметра и объема</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK05</optionId>
                        <optionname>Автоматическое включение фар внешнего освещения + датчик дождя + противотуманные фары (стандарт при</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом регулировки, электрообогревом и электроприводом складывания</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное лобовое стекло</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ES03</optionId>
                        <optionname>Розетка на 12 В в грузовом отсеке</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB33</optionId>
                        <optionname>Покрытие пола грузового отделения пластиком</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB34</optionId>
                        <optionname>Покрытие пола+ защита стен грузового отделения пластиком</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB35</optionId>
                        <optionname>Покрытие пола грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>11 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB36</optionId>
                        <optionname>Покрытие пола багажного отсека + защита стен грузового отделения фанерой (толщина 10 мм)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>11 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK39</optionId>
                        <optionname>Подогрев сидений (2 сиденья для пассажиров + водитель)</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLVZ</optionId>
                        <optionname>Аудиосистема RD45,</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV0</optionId>
                        <optionname>Аудиосистема RD45,</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV1</optionId>
                        <optionname>Навигационная система</optionname>
                        <optionprice>40 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>QC01</optionId>
                        <optionname>Коммутационный блок прицепа, блок трансформации кузова (BTC)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; (серебрянный металлик GRIS ALUMINUM (ZRM0), серый металлик</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>_Krossi_Zanotti_248840</optionname>
                        <optionprice>248 840,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC0</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>914 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC0</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секвентальные электрические стеклоподъемники с функцией анти-зажим</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LW02</optionId>
                        <optionname>Подогрев нижней части лобового стекла + обогреваемые форсунки омывателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль + воздуховоды для задних пассажиров</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLTV</optionId>
                        <optionname>Автомагнитола CD RD4  + MP3 + Управление аудиоситемой на рулевой  колонке  + Коммуникационная систем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZRM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;Серебристый металлик&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>Легкосплавные диски 17&#034; MELBOURNE 2</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OL08_1</optionId>
                        <optionname>Легкосплавные диски 17&#034; MELBOURNE 2</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 301</carcomplectname>
                            <carcomplectbaseprice>836 900,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Aнтиблокировочная  система (ABS) + Система помощи при экстренном торможении (AFU)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Фронтальная подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Фронтальная подушка безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OP01</optionId>
                        <optionname>Ручное отключение подушки безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CF08</optionId>
                        <optionname>Трехточечные ремни безопасности с преднатяжителем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TG08</optionId>
                        <optionname>Предупреждающий сигнал о непристегнутом ремне водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JA11</optionId>
                        <optionname>1 ключ с ДУ с третьей кнопкой открытия багажника + 1 механический ключ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральный замок с кнопкой закрытия на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Металлическая защита картера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FB02</optionId>
                        <optionname>Задний противотуманный фонарь</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH12</optionId>
                        <optionname>Рулевое колесо с регулировкой по наклону, с кожаной обивкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UH56</optionId>
                        <optionname>Ручка КПП с кожаной обивкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>MO03</optionId>
                        <optionname>Кнопка открытия багажника на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OR03</optionId>
                        <optionname>Хромированные вставки в интерьере</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA10</optionId>
                        <optionname>Рукоятки на передних дверях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JB04</optionId>
                        <optionname>Передний центральный подлокотник</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GM19</optionId>
                        <optionname>Панель приборов Active&#038;Allure</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02, LU02</optionId>
                        <optionname>Передние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE03</optionId>
                        <optionname>Задние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE02</optionId>
                        <optionname>Кондиционер с электронным управлением</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RJ04, RK04, ON02</optionId>
                        <optionname>Наружные зеркала заднего вида с электроприводом регулировок и подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WF13</optionId>
                        <optionname>Подогрев нижней части лобового стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IT01, IP01, IQ01, IU</optionId>
                        <optionname>Передние сиденья с ручными регулировками в 4-х направлениях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние подголовники, регулируемые по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Задние сиденья, складываемые в пропорции 1/3:2/3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RH12</optionId>
                        <optionname>Сиденье водителя, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>8WFX</optionId>
                        <optionname>Отделка сидений тканью Oza Mistral (темная)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>HY01</optionId>
                        <optionname>Салонный фильтр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PS02</optionId>
                        <optionname>Регулировка уровня наклона фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EJ03</optionId>
                        <optionname>Подсветка багажного отделения</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PY12</optionId>
                        <optionname>Солнцезащитные козырьки с зеркальцем со стороны пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Розетка 12В с прикуривателем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH1L</optionId>
                        <optionname>16&#034; литые диски, неполноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RZ89</optionId>
                        <optionname>Ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YI01</optionId>
                        <optionname>Наружные зеркала заднего вида, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампера, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQE</optionId>
                        <optionname>Аудиосистема с монохромным ЖК-дисплеем, поддержкой CD + MP3, управлением на рулевой колонке и 4 дина</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>Коммуникационная система Bluetooth</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YM04</optionId>
                        <optionname>Разъемы AUX и USB</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GV04</optionId>
                        <optionname>Пакет &#034;Внешний вид&#034;: Хром. окантовка окон дверей+Хром. вставка в пер. бампер+Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>MOMO</optionId>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3ERBT04A080</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AA05</optionId>
                        <optionname>Стеклоочиститель заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA03</optionId>
                        <optionname>Заднее сиденье, складывающееся в отношении 1/1</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AF01</optionId>
                        <optionname>Тормозная антиблокировочная система (ABS)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AJ01</optionId>
                        <optionname>Задние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CB09</optionId>
                        <optionname>Багажная полка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL01</optionId>
                        <optionname>Проводка для акустической системы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PK02</optionId>
                        <optionname>Резина Michelin</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WEFH</optionId>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>DD04</optionId>
                        <optionname>ЭЛЕКТОУСЕЛИТЕЛЬ  руля + рулевая колонка, регулируемая по высоте</optionname>
                        <optionprice>10 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WL45</optionId>
                        <optionname>Автомагнитола RCD3</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optionname>Кожаный руль+ рукоятка КПП отделанная кожей (для версии с мт)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3KRBT04A080</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Тормозная антиблокировочная система (ABS)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AJ01</optionId>
                        <optionname>Задние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL12</optionId>
                        <optionname>Заднее сиденье, складывающееся в отношении 50/50</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Централизованная блокировка дверных замков с помощью пульта дистанционного управления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DD04</optionId>
                        <optionname>ЭЛЕКТОУСЕЛИТЕЛЬ  руля + рулевая колонка, регулируемая по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02</optionId>
                        <optionname> Передние электрические стеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CS02</optionId>
                        <optionname>Тахометр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CB09</optionId>
                        <optionname>Багажная полка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL01</optionId>
                        <optionname>Проводка для акустической системы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL45</optionId>
                        <optionname>Автомагнитола RCD3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FE10</optionId>
                        <optionname>Светодиодные лампы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PK02</optionId>
                        <optionname>Резина Michelin</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WEFH</optionId>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UX01</optionId>
                        <optionname>Отделка хромом элементов бампера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH04+UH10</optionId>
                        <optionname>Кожаный руль (c подрулевыми лепестками) + рукоятка КПП отделанная кожей (для версии с 2-Tronic)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions></additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3KRBP04A080</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Тормозная антиблокировочная система (ABS)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AJ01</optionId>
                        <optionname>Задние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL12</optionId>
                        <optionname>Заднее сиденье, складывающееся в отношении 50/50</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Централизованная блокировка дверных замков с помощью пульта дистанционного управления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DD04</optionId>
                        <optionname>ЭЛЕКТОУСЕЛИТЕЛЬ  руля + рулевая колонка, регулируемая по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02</optionId>
                        <optionname> Передние электрические стеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CS02</optionId>
                        <optionname>Тахометр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CB09</optionId>
                        <optionname>Багажная полка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL01</optionId>
                        <optionname>Проводка для акустической системы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL45</optionId>
                        <optionname>Автомагнитола RCD3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FE10</optionId>
                        <optionname>Светодиодные лампы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PK02</optionId>
                        <optionname>Резина Michelin</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WEFH</optionId>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UX01</optionId>
                        <optionname>Отделка хромом элементов бампера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH04+UH10</optionId>
                        <optionname>Кожаный руль (c подрулевыми лепестками) + рукоятка КПП отделанная кожей (для версии с 2-Tronic)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions></additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VP</carmodel>
                            <carcomplectid>2PG95QEBA632CG82</carcomplectid>
                            <carcomplectname>PEUGEOT EXPERT VP</carcomplectname>
                            <carcomplectbaseprice>1 529 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>LV02</optionId>
                        <optionname>Набор для ремонта колеса</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера металлическая</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель. Съемная пепельница.</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ES03</optionId>
                        <optionname>Розетка 12В в передней и задней части салона</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Остекленные распашные задние двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AX01</optionId>
                        <optionname>Передний ряд сидений - 2 сиденья для пассажиров + водитель</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL29</optionId>
                        <optionname>2-й ряд сидений (3 места): съемная 2-х местная банкетка + откидное и съемное сиденье Easy Entry с по</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AN11</optionId>
                        <optionname>3-й ряд сидений (3 места): съемная 2-х местная банкетка + откидное  и съемное сиденье Easy Entry с п</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SE02</optionId>
                        <optionname>Усиленная подвеска</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL07</optionId>
                        <optionname>Стальные колеса 215/60 R16 с колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OPP0</optionId>
                        <optionname>Лакокрасочное покрытие кузова неметаллик (белый - Blanc Banquise WPP0, темно-синий - Bleu Imperial 4</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WK16</optionId>
                        <optionname>Подушка безопасности для передних пассажиров</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK18</optionId>
                        <optionname>Подушка безопасности для передних пассажиров + подогрев передних сидений</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK23</optionId>
                        <optionname>Подушка безопасности пассажира + 1 сиденье для пассажира</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK40</optionId>
                        <optionname>Подушка безопасности пассажира, подогрев передних сидений + 1 сиденье для пассажира</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK28</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, боковые шторки безопасности для 1-го,</optionname>
                        <optionprice>42 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK29</optionId>
                        <optionname>Подушка безопасности пассажира, боковые подушки безопасности, боковые шторки безопасности для 1-го,</optionname>
                        <optionprice>50 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optionname>Подогрев сидений</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WK39</optionId>
                        <optionname>Подогрев сидений + 1 сиденье для пассажира</optionname>
                        <optionprice>8 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Зеркала заднего вида с электроприводом , электрообогревом и электроприводом складывания</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Система помощи при парковке задним ходом</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное лобовое стекло</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB02</optionId>
                        <optionname>Застекленная откидная задняя дверь с электрообогревом и стеклоочистителем</optionname>
                        <optionprice>26 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD11</optionId>
                        <optionname>Застекленная откидная задняя дверь с электрообогревом и стеклоочистителем + темная тонировка стекол</optionname>
                        <optionprice>43 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VG02</optionId>
                        <optionname>Сдвижные стекла для 2-го ряда сидений</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AL30</optionId>
                        <optionname>2-й ряд сидений (3 места): (съемная складывающаяся 2-х местная банкетка + складывающееся откидное и</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Левая боковая остекленная сдвижная дверь</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK05</optionId>
                        <optionname>Автоматическое включение фар внешнего освещения + датчик дождя + противотуманные фары (стандарт при</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер для передних пасажиров</optionname>
                        <optionprice>40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR02</optionId>
                        <optionname>Кондиционер для передних и задних пасажиров</optionname>
                        <optionprice>73 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLVZ</optionId>
                        <optionname>Аудиосистема RD45</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV0</optionId>
                        <optionname>Аудиосистема RD45</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLV1</optionId>
                        <optionname>Навигационная система</optionname>
                        <optionprice>45 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампер и зеркала заднего вида, окрашенные в цвет кузова, + противотуманные фары +</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; (серебрянный металлик GRIS ALUMINUM (ZRM0), серый металлик</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4KCMPD0A0B4</carcomplectid>
                            <carcomplectname>PEUGEOT 301</carcomplectname>
                            <carcomplectbaseprice>801 900,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>6,30</Fuel_consumption_city> <Fuel_consumption_track>4,60</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>980</Mass> <Equipment_code>1PM3A4KCMPD0A0B4</Equipment_code> <equipment_name>Active</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1467</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Aнтиблокировочная  система (ABS) + Система помощи при экстренном торможении (AFU)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Фронтальная подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Фронтальная подушка безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OP01</optionId>
                        <optionname>Ручное отключение подушки безопасности переднего пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CF08</optionId>
                        <optionname>Трехточечные ремни безопасности с преднатяжителем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TG08</optionId>
                        <optionname>Предупреждающий сигнал о непристегнутом ремне водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JA11</optionId>
                        <optionname>1 ключ с ДУ с третьей кнопкой открытия багажника + 1 механический ключ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Центральный замок с кнопкой закрытия на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CO01</optionId>
                        <optionname>&#034;Детский замок&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Металлическая защита картера</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FB02</optionId>
                        <optionname>Задний противотуманный фонарь</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH01</optionId>
                        <optionname>Рулевое колесо с регулировкой по наклону</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>MO03</optionId>
                        <optionname>Кнопка открытия багажника на центральной консоли</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OR03</optionId>
                        <optionname>Хромированные вставки в интерьере</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GM19</optionId>
                        <optionname>Панель приборов Active&#038;Allure</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02, LU02</optionId>
                        <optionname>Передние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE03</optionId>
                        <optionname>Задние электростеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RJ04, RK04, ON02</optionId>
                        <optionname>Наружные зеркала заднего вида с электроприводом регулировок и подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WF13</optionId>
                        <optionname>Подогрев нижней части лобового стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IT01, IP01, IQ01, IU</optionId>
                        <optionname>Передние сиденья с ручными регулировками в 4-х направлениях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние подголовники, регулируемые по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Задние сиденья, складываемые в пропорции 1/3:2/3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RH12</optionId>
                        <optionname>Сиденье водителя, регулируемое по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>8HFX</optionId>
                        <optionname>Отделка сидений тканью Oline MIstral (темная)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>HY01</optionId>
                        <optionname>Салонный фильтр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PS02</optionId>
                        <optionname>Регулировка уровня наклона фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EJ03</optionId>
                        <optionname>Подсветка багажного отделения</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PY12</optionId>
                        <optionname>Солнцезащитные козырьки с зеркальцем со стороны пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Розетка 12В с прикуривателем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH39</optionId>
                        <optionname>15&#034; стальные диски, полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RZ89</optionId>
                        <optionname>Ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YI01</optionId>
                        <optionname>Наружные зеркала заднего вида, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PD22</optionId>
                        <optionname>Передний и задний бампера, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQC</optionId>
                        <optionname>Аудиоподготовка с антенной на крыше</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>MOMO</optionId>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IGDQ609FCC0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 689 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU93IGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>XY02</optionId>
                        <optionname>Догреватель непрограммируемый Webasto Thermo Top Z (5 кВт)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ES02</optionId>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>BF02</optionId>
                        <optionname>Аккумулятор повышенной емкости (110 Ач)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UG01</optionId>
                        <optionname>СИСТЕМА ПОМОЩИ ПРИ ЭКСТРЕННОМ ТОРМОЖЕНИИ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>ПОДУШКА БЕЗОПАСНОСТИ ВОДИТЕЛЯ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB07</optionId>
                        <optionname>Задние распашные на 180° цельнометаллические двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TD01</optionId>
                        <optionname>Гаечный ключ с трещоткой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PX02</optionId>
                        <optionname>Защитная решетка за водительским сиденьем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + Hill Descent Control</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя (несовместимо с оби</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR01</optionId>
                        <optionname>ПОДГОТОВКА ДЛЯ ЗАДНЕЙ КЛИМАТИЧЕСКОЙ УСТАНОВКИ (ВЕДЕТ ЗА СОБОЙ УСТАНОВКУ ПЕРЕДНЕГО КОНДИЦИОНЕРА)</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Боковые металлические двери со стеклами (недоступно для версий H3)</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BCBW532A026</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>975 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>2</Seats> <Volume>1560</Volume> <Power>90</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>13,40</Top100> <Fuel_consumption_city>6,70</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>5,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4628</Length> <Width>1810</Width> <Height>1842</Height> <Clearance>154</Clearance> <Trunk_size>3700</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1395</Mass> <Equipment_code>2PB95BCBW532A026</Equipment_code> <equipment_name>VU long</equipment_name> <Generation>Partner II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>215/1500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2195</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>OP9W</optionId>
                        <optionname>Тканевая обивка 9WFZ GRIS NARBONNAISCLAIR C&#038;T RESEAU G260+OMNI</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB07</optionId>
                        <optionname>Неостекленные распашные задние двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CZ11</optionId>
                        <optionname>Полезная нагрузка 850 кг</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL06</optionId>
                        <optionname>Стальные колеса 195/70 R15 с колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX02</optionId>
                        <optionname>Пакет &#034;Повышенная проходимость&#034;: Защита картера + Увеличенный на 10 мм дорожный просвет засчёт испол</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель и съемная пепельница</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Потолочная консоль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OPP0</optionId>
                        <optionname>Лакокрасочное покрытие кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK01</optionId>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>Интеллектуальная система распеределения крутящего момента+ESP</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLLO/ WLRW</optionId>
                        <optionname>RCM4 + DO01 Система Blue Tooth &#034;Free Hands Phone&#034; + USB BOX</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова типа &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYFCG5D0A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>919 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,50</Top100> <Fuel_consumption_city>6,00</Fuel_consumption_city> <Fuel_consumption_track>4,30</Fuel_consumption_track> <Fuel_consumption>4,90</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1045</Mass> <Equipment_code>1PI4SYFCG5D0A0C0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>169</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1582</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>9AFX</optionId>
                        <optionname>Отделка сидений темной тканью Corbi Mistral</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLPB</optionId>
                        <optionname>Аудиосистема CD/MP3, AM/FM радио, включая цифровой разъем AUX и 4 динамика</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4B</optionId>
                        <optionname>15&#034; стальные колесные диски с декоративными колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions></additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCA5D0A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 124 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYLCA5D0A0C0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>9BFX</optionId>
                        <optionname>Отделка сидений темной тканью Tokyo Mistral</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQK</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем высокого разрешения, системой беспроводн</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH54</optionId>
                        <optionname>16&#034; стальные колесные диски с декоративными колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4F</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4E</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>9BFQ</optionId>
                        <optionname>Отделка сидений коричневой тканью Tokyo Brundy</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель (не доступно для двигателя 1.6 120 АКПП)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQA</optionId>
                        <optionname>Мультимедийная аудиосистема , навигационной системой , Bluetooth, 2 разъемами USB,</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCALD0A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 159 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>11,20</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1140</Mass> <Equipment_code>1PI4SYLCALD0A0C0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1666</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>9BFX</optionId>
                        <optionname>Отделка сидений темной тканью Tokyo Mistral</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQK</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем высокого разрешения, системой беспроводн</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH54</optionId>
                        <optionname>16&#034; стальные колесные диски с декоративными колпаками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4F</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4E</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>9BFQ</optionId>
                        <optionname>Отделка сидений коричневой тканью Tokyo Brundy</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQA</optionId>
                        <optionname>Мультимедийная аудиосистема , навигационной системой , Bluetooth, 2 разъемами USB,</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYMCA5D0A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 2008</carcomplectname>
                            <carcomplectbaseprice>1 174 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYMCA5D0A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>9BFX</optionId>
                        <optionname>Отделка сидений темной тканью Tokyo Mistral</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQK</optionId>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем высокого разрешения, системой беспроводн</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4D</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4F</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4E</optionId>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>ZH4F/ZH4E</optionId>
                        <optionname>«Grip Control» , включая 17&#034; легкосплавные колесные диски</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Противоугонная сигнализация с датчиками периметра и объема</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>9BFQ</optionId>
                        <optionname>Отделка сидений коричневой тканью Tokyo Brundy</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P3U</optionId>
                        <optionname>Кожаная отделка сидений &#034;Cuir Claudia&#034;</optionname>
                        <optionprice>65 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель (не доступно для двигателя 1.6 120 АКПП)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQA</optionId>
                        <optionname>Мультимедийная аудиосистема , навигационной системой , Bluetooth, 2 разъемами USB,</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Трехслойное лакокрасочное покрытие кузова перламутр (N9M6 Blanc Nacre)</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5LCALD0A0D0</carcomplectid>
                            <carcomplectname>PEUGEOT 208 5 PORTES</carcomplectname>
                            <carcomplectbaseprice>990 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,70</Top100> <Fuel_consumption_city>9,80</Fuel_consumption_city> <Fuel_consumption_track>4,90</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1090</Mass> <Equipment_code>1PIAA5LCALD0A0D0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1625</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions><option>
                        <optionId>AF01</optionId>
                        <optionname>Aнтиблокировочная  система (ABS) + Система помощи при экстренном торможении (AFU)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Фронтальные подушки безопасности водителя и пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Передние боковые подушки безопасности</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CG18</optionId>
                        <optionname>Передние ремни безопасности с пиротехническими преднатяжителями</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RJ04</optionId>
                        <optionname>Наружные зеркала заднего вида с электроприводом регулировок и подогревом, секвентальный стеклоподъем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Второй ряд сидений, складываемый в отношении 40:60</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AJ01</optionId>
                        <optionname>Задние подголовники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CB09</optionId>
                        <optionname>Полка багажника</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLQK</optionId>
                        <optionname>Мультимедийный комплекс с 7&#034; сенсорным дисплеем высокого разрешения, функцией Bluetooth, USB, возмож</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>0P7B/7C</optionId>
                        <optionname>Тканевая обивка сидений темная Curve</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZH39</optionId>
                        <optionname>Стальные диски с декоративными колпаками Bore 15&#034; (только для двигателя 1.0л) и Chrome 15&#034; (для всех</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH02</optionId>
                        <optionname>Компактное рулевое колесо с кожаной обивкой и хромированной вставкой</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLOA</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLOB</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России + CD-проигрыватель</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH45</optionId>
                        <optionname>Легкосплавные диски 16&#034; Helium</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAC2</optionId>
                        <optionname>Пакет 2 Безопасность Active (Перед. противотуманные фары, ESP, зад. дат/парк, бок.под.и штор. безоп)</optionname>
                        <optionprice>36 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAC3</optionId>
                        <optionname>датчик света, датчик дождя, салонное зеркало заднего вида с автоматическим затемнением, климат-контр</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAC4</optionId>
                        <optionname>Пакет 4 Светодиодные дневные ходовые огни, световая линия, линзованные фары, тонированные задние и з</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5PKUMD0A0B1</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 395 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,50</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1255</Mass> <Equipment_code>1PT9A5PKUMD0A0B1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>211</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1780</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>RG04</optionId>
                        <optionname>Пакет Безопасность (адаптивный круиз-контроль, система предупреждения о столкновении, система тормож</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB08</optionId>
                        <optionname>Датчик слепой зоны + система автоматической парковки City Park</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB10</optionId>
                        <optionname>Камера заднего вида с парковочными линиями + датчик слепой зоны + система автоматической парковки Ci</optionname>
                        <optionprice>49 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UN01</optionId>
                        <optionname>Премиальная аудиосистема Hi-Fi DENON (6)</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>8DFR</optionId>
                        <optionname>Отделка сидений кожей + электрорегулировка сиденья водителя с памятью регулировок + функция массажа</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>1UFX</optionId>
                        <optionname>Комбинированная обивка сидений Alcantara + коврики</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>8DFX</optionId>
                        <optionname>Отделка сидений кожей + электрорегулировка сиденья водителя с памятью регулировок + функция массажа</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2R</optionId>
                        <optionname>Легкосплавные диски 17&#034; Rubis + докатка</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2S</optionId>
                        <optionname>Легкосплавные диски 17&#034; Rubis + Полноразмерное запасное колесо</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2N</optionId>
                        <optionname>Легкосплавные диски 16&#034; Topaze+полноразмерное колесо</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2P</optionId>
                        <optionname>Легкосплавные диски 16&#034; Topaze+ полноразмерноу колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KGDQ609A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 769 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>158</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KGDQ609A0C0</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + Hill Descent Control</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя (несовместимо с оби</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR01</optionId>
                        <optionname>ПОДГОТОВКА ДЛЯ ЗАДНЕЙ КЛИМАТИЧЕСКОЙ УСТАНОВКИ (ВЕДЕТ ЗА СОБОЙ УСТАНОВКУ ПЕРЕДНЕГО КОНДИЦИОНЕРА)</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE12</optionId>
                        <optionname>Светодиодные дневные ходовые огни + указатели поворота (только для L4)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Боковые металлические двери со стеклами (недоступно для версий H3)</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZ86</optionId>
                        <optionname>Легкосплавные диски 16&#039;&#039; (только для версии L4)</optionname>
                        <optionprice>31 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>508</carfamily>
                            <carmodel>508</carmodel>
                            <carcomplectid>1PW2A4PCLM32JGC0</carcomplectid>
                            <carcomplectname>PEUGEOT 508</carcomplectname>
                            <carcomplectbaseprice>1 734 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,20</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4792</Length> <Width>1853</Width> <Height>1456</Height> <Clearance>170</Clearance> <Trunk_size>545</Trunk_size> <Fuel_tank_size>72</Fuel_tank_size> <Mass>1485</Mass> <Equipment_code>1PW2A4PCLM32JGC0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>508 I Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>220</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2015</FullMass> <max_trunk_size>545</max_trunk_size> <baseoptions><option>
                        <optionId>WA53</optionId>
                        <optionname>Электрорегулировка сидений водителя и переднего пассажира в 6 направлениях, электрорегулировка поясн</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>3FFX</optionId>
                        <optionname>Обивка сидений кожа+ткань (черная) Обивка сидений кожа+ткань</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>AB08</optionId>
                        <optionname>Сигнализация с датчиками объема, периметра и наклона</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>Q1FD</optionId>
                        <optionname>Кожаная обивка сидений (светлая )</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>Q1FX</optionId>
                        <optionname>Кожаная обивка сидений (черная)</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PO01</optionId>
                        <optionname>Пакет 1 (RT05+TC07)</optionname>
                        <optionprice>40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB07</optionId>
                        <optionname>Кам. зад. вида + датчик &#034;слеп&#034; зоны+пер. датчики парк.+электропривод скл. зеркал</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PO03</optionId>
                        <optionname>Пакет 3 (0P4H+ZH88)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLXJ</optionId>
                        <optionname>Аудиосистема премиум класса Hi-Fi (500 Вт)</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH1C</optionId>
                        <optionname>18&#034; ЛЕГКОСПЛАВНЫЕ ДИСКИ С ШИНАМИ 235/45R18(STYLE07)</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;Металлик&#034;</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LKUMD0A0B1</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 299 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,50</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1255</Mass> <Equipment_code>1PT9A5LKUMD0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>211</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1780</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний  (4 датчика )</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Пакет Обзорность</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2H</optionId>
                        <optionname>Стальные диски с декоративными колпаками Corail 16&#039;&#039;+ полноразмерное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2K</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+ докатка</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4A</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+полноразмерное колесо</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Оконные (шторки) подушки безопасности</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2G</optionId>
                        <optionname>Стальные диски с декоративными колпаками Coral 16+докатка</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER Chassis Cabine</carmodel>
                            <carcomplectid>2PU96FGDQ609A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER Chassis Cabine</carcomplectname>
                            <carcomplectbaseprice>1 540 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>шасси</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5943</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1635</Mass> <Equipment_code>2PU96FGDQ609A0C0</Equipment_code> <equipment_name>ChCa 335 L3</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>3500</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>CTTM_Prom_SV_235</optionId>
                        <optionname>CTTM_Prom_SV_235_123100</optionname>
                        <optionprice>123 100,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя**</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев спереди</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RX01</optionId>
                        <optionname>Наружные зеркала заднего вида с боковым обзором на 2,20 м</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RX05</optionId>
                        <optionname>Наружные зеркала заднего вида с боковым обзором на 2,35 м</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>KY06</optionId>
                        <optionname>Проводка для подключения дополнительного навесного оборудования</optionname>
                        <optionprice>3 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE01</optionId>
                        <optionname>Брызговики (передние)</optionname>
                        <optionprice>3 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>24 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + система помощи при спуске по склону Hill Descent Control</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя + двухместная кабин</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>Индивидуальное пассажирское сиденье с подлокотником, регулировкой продольного положения и поясничной</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники (только для стандартной обивки 35FX</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE10</optionId>
                        <optionname>Светодиодные дневные ходовые огни (для всех версий кроме L4</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>CY10</optionId>
                        <optionname>Пульт для управления трансформированными автомобилями (3 кнопки)</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A + кондиционер</optionname>
                        <optionprice>71 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>se16</optionId>
                        <optionname>Усиленная задняя подвеска для нагрузок более 1000 кг (двухлистовые рессоры</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема FM Радио + USB + упр.аудиосистемой на руле (опция недоступна при заказе опции UB09</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>CTTM_Prom_89000</optionId>
                        <optionname>CTTM_Prom_89000</optionname>
                        <optionprice>89 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>CTTM_izoterm_140000</optionId>
                        <optionname>CTTM_izoterm_140000</optionname>
                        <optionprice>140 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B2</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 176 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B2</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>ZH2G</optionId>
                        <optionname>Стальные диски с декоративными колпаками Coral 16+докатка</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2K</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+ докатка</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4A</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+полноразмерное колесо</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2H</optionId>
                        <optionname>Стальные диски с декоративными колпаками Corail 16&#039;&#039;+ полноразмерное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Оконные (шторки) подушки безопасности</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний  (4 датчика )</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Пакет Обзорность</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AU02</optionId>
                        <optionname>Задний подлокотник + лючок для лыж</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LKUMD0A0B2</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 299 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,50</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1255</Mass> <Equipment_code>1PT9A5LKUMD0A0B2</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>211</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1780</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>NB08</optionId>
                        <optionname>Пакет Обзорность</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2K</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+ докатка</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4A</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+полноразмерное колесо</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2G</optionId>
                        <optionname>Стальные диски с декоративными колпаками Coral 16+докатка</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AU02</optionId>
                        <optionname>Задний подлокотник + лючок для лыж</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2H</optionId>
                        <optionname>Стальные диски с декоративными колпаками Corail 16&#039;&#039;+ полноразмерное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Оконные (шторки) подушки безопасности</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний  (4 датчика )</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>843 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLTI</optionId>
                        <optionname>Автомагнитола CD RD4 + MP3+ управоение аудиосистемой на рулевой колонке</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NBV532JUC1</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>923 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>112</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>11,00</Top100> <Fuel_consumption_city>6,20</Fuel_consumption_city> <Fuel_consumption_track>4,30</Fuel_consumption_track> <Fuel_consumption>5,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1400</Mass> <Equipment_code>1PTKA4NBV532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>254/1750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1810</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLTI</optionId>
                        <optionname>Автомагнитола CD RD4 + MP3+ управоение аудиосистемой на рулевой колонке</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C1</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>1 026 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLTI</optionId>
                        <optionname>Автомагнитола CD RD4 + MP3+ управоение аудиосистемой на рулевой колонке</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>Легкосплавные диски 17&#034; MELBOURNE 2</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>898 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>Интеллектуальная система распеределения крутящего момента+ESP</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLRW</optionId>
                        <optionname>Аудиосистема MP3 (4 динамика), Bluetooth и USB</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97PGDQ609ULC0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 748 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + Hill Descent Control</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR01</optionId>
                        <optionname>ПОДГОТОВКА ДЛЯ ЗАДНЕЙ КЛИМАТИЧЕСКОЙ УСТАНОВКИ (ВЕДЕТ ЗА СОБОЙ УСТАНОВКУ ПЕРЕДНЕГО КОНДИЦИОНЕРА)</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя (несовместимо с оби</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE12</optionId>
                        <optionname>Светодиодные дневные ходовые огни + указатели поворота (только для L4)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>71 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>24 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZ86</optionId>
                        <optionname>Легкосплавные диски 16&#039;&#039; (только для версии L4)</optionname>
                        <optionprice>31 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BC6S532A027</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>953 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLRW</optionId>
                        <optionname>Аудиосистема MP3 (4 динамика), Bluetooth и USB</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>Интеллектуальная система распеределения крутящего момента+ESP</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BCBW532A027</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>975 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>Интеллектуальная система распеределения крутящего момента+ESP</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLRW</optionId>
                        <optionname>Аудиосистема MP3 (4 динамика), Bluetooth и USB</optionname>
                        <optionprice>29 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>4008</carfamily>
                            <carmodel>4008</carmodel>
                            <carcomplectid>1PMJSUPRPH32A010</carcomplectid>
                            <carcomplectname>PEUGEOT 4008</carcomplectname>
                            <carcomplectbaseprice>1 589 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>150</Power> <VolumeReal>2,00</VolumeReal> <Transmission>вариатор</Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,80</Fuel_consumption_track> <Fuel_consumption>8,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4340</Length> <Width>1768</Width> <Height>1632</Height> <Clearance>0</Clearance> <Trunk_size>416</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1470</Mass> <Equipment_code>1PMJSUPRPH32A010</Equipment_code> <equipment_name>Active</equipment_name> <Generation>4008</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>197/4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1980</FullMass> <max_trunk_size>1540</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Стекла второго ряда сидений и стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE</optionId>
                        <optionname>Комплектация LE</optionname>
                        <optionprice>-40 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>БЕЗ UB01</optionId>
                        <optionname>ОТСУТСТВУЕТ UB01</optionname>
                        <optionprice>-10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZKZ1</optionId>
                        <optionname>Интергированные лампы сигнала поворотов в зеркала заднего вида + Электропривод складывания зеркал</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Сигнализация с датчиком периметра и объема</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Окраска кузова металлик</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подстветкой</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK09</optionId>
                        <optionname>Ксеноновые фары с автоматическим корректором</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZKZ</optionId>
                        <optionname>Пакет &#034;Look&#034;: 18&#034; легкосплавные колесные диски + Интегрированные лампы сигнала поворотов в зеркала з</optionname>
                        <optionprice>42 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3KRBT04A070</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>4</Seats> <Volume>998</Volume> <Power>68</Power> <VolumeReal>1,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,70</Top100> <Fuel_consumption_city>5,40</Fuel_consumption_city> <Fuel_consumption_track>4,00</Fuel_consumption_track> <Fuel_consumption>4,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3430</Length> <Width>1630</Width> <Height>1470</Height> <Clearance>145</Clearance> <Trunk_size>130</Trunk_size> <Fuel_tank_size>35</Fuel_tank_size> <Mass>790</Mass> <Equipment_code>1PB0A3KRBT04A070</Equipment_code> <equipment_name>Active</equipment_name> <Generation>107 Рестайлинг</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>93/3600</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>157</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1180</FullMass> <max_trunk_size>751</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>10 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLMZ</optionId>
                        <optionname>Автомагнитола RDE2+4 динамика+USB+MP3+Bluetooth</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Колесные диски из алюминиевого сплава, 14&#039;&#039;</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optionname>Кожаный руль+ рукоятка КПП отделанная кожей (для версии с мт)</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>KL05</optionId>
                        <optionname>Внутренняя отделка салона</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Заднее и задние боковые стекла с сильной тонировкой</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IHDQ609FC80</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 537 100,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93IHDQ609FC80</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>DO013</optionId>
                        <optionname>Tour L2_235</optionname>
                        <optionprice>235 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO015</optionId>
                        <optionname>ProfiT_135 000</optionname>
                        <optionprice>135 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя**</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека**</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>26 700,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>DX01</optionId>
                        <optionname>ASR-ПРОБУКСОВОЧНАЯ СИСТЕМА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE011</optionId>
                        <optionname>КОНДИЦИОНЕР + УСТАНОВКА</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO02</optionId>
                        <optionname>модификациифургона  &#034;Туристический&#034;</optionname>
                        <optionprice>240 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO32</optionId>
                        <optionname>Krossi_282 000</optionname>
                        <optionprice>282 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER Chassis Cabine</carmodel>
                            <carcomplectid>2PU96FHDQ609A680</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER Chassis Cabine</carcomplectname>
                            <carcomplectbaseprice>1 408 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>шасси</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,40</Fuel_consumption_city> <Fuel_consumption_track>8,80</Fuel_consumption_track> <Fuel_consumption>7,90</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5943</Length> <Width>2050</Width> <Height>2153</Height> <Clearance>200</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1655</Mass> <Equipment_code>2PU96FHDQ609A680</Equipment_code> <equipment_name>ChCa 335 L3</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>3500</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>DO016</optionId>
                        <optionname>Cttm_prom_S_106 500</optionname>
                        <optionprice>106 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO24</optionId>
                        <optionname>Cttm_prom_89 000</optionname>
                        <optionprice>89 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RX05</optionId>
                        <optionname>Наружные зеркала заднего вида с боковым обзором на 2,35 м</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>KY06</optionId>
                        <optionname>Проводка для подключения дополнительного навесного оборудования</optionname>
                        <optionprice>3 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO12</optionId>
                        <optionname>Холодильное оборудование</optionname>
                        <optionprice>120 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>Надстройка &#034;Промтоварный фургон&#034;</optionname>
                        <optionprice>89 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>YTIL</optionId>
                        <optionname>утилизационный сбор</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO02</optionId>
                        <optionname>Надстройка &#034;Бортовой тентовый фургон&#034;</optionname>
                        <optionprice>288 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO023</optionId>
                        <optionname>Cttm_prom_S_123 775</optionname>
                        <optionprice>123 775,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO024</optionId>
                        <optionname>Cttm_prom_S_106 275</optionname>
                        <optionprice>106 275,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO04</optionId>
                        <optionname>Cttm_izot_140 000</optionname>
                        <optionprice>140 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO03</optionId>
                        <optionname>надстройка Cttm_bort_96 000</optionname>
                        <optionprice>96 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO011</optionId>
                        <optionname>Надстройка &#034;Промтоварный фургон&#034; - не стандартный</optionname>
                        <optionprice>171 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>YTIL01</optionId>
                        <optionname>Утилизационный сбор</optionname>
                        <optionprice>141 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DX01</optionId>
                        <optionname>ASR - противобуксовочная система</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя**</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев спереди</optionname>
                        <optionprice>13 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RX01</optionId>
                        <optionname>Наружные зеркала заднего вида с боковым обзором на 2,20 м</optionname>
                        <optionprice>2 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE01</optionId>
                        <optionname>Брызговики (передние)</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>Отсутствие хронотахографа (для версий ChCa 440 L4)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE011</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>46 610,17</optionprice>
                        </option>
                        <option>
                        <optionId>DO13</optionId>
                        <optionname>Гидроборт Dhollandia 500 кг</optionname>
                        <optionprice>135 593,23</optionprice>
                        </option>
                        <option>
                        <optionId>DO14</optionId>
                        <optionname>Воздушний автономный отопитель 4 кВт</optionname>
                        <optionprice>55 762,72</optionprice>
                        </option>
                        <option>
                        <optionId>DO15</optionId>
                        <optionname>Будка изотермическая (19,5 куб.) 4,20 / 2,20 / 2,30</optionname>
                        <optionprice>264 067,80</optionprice>
                        </option>
                        <option>
                        <optionId>DO07</optionId>
                        <optionname>ПЕРЕОБОРУДОВАНИЕ ЦТТМ пром свес  96500</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO21</optionId>
                        <optionname>ПРОМТОВАРНЫЙ ФУРГОН С УДЛИНЕННЫМ СВЕСОМ</optionname>
                        <optionprice>106 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO012</optionId>
                        <optionname>Надстройка 1</optionname>
                        <optionprice>315 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO16</optionId>
                        <optionname>Cttm_bort_S_113 500</optionname>
                        <optionprice>113 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>CTTM_Prom_SV_235</optionId>
                        <optionname>CTTM_Prom_SV_235_123100</optionname>
                        <optionprice>123 100,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 595 800,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>MI01</optionId>
                        <optionname></optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO016</optionId>
                        <optionname>IMS_ProfiT_140 000</optionname>
                        <optionprice>140 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO030</optionId>
                        <optionname>КАТЕГОРИЯ C</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO03</optionId>
                        <optionname>profi</optionname>
                        <optionprice>186 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO05</optionId>
                        <optionname>Переоборудование IMS_ProfiT_137500</optionname>
                        <optionprice>140 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO26</optionId>
                        <optionname>IMS_ProfiT_186 000</optionname>
                        <optionprice>186 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MHDQ609U180</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 579 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>224</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2015</Mass> <Equipment_code>2PU97MHDQ609U180</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>DO016</optionId>
                        <optionname>IMS_ProfiT_140 000</optionname>
                        <optionprice>140 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>Надстройка &#034;Промтоварный фургон&#034;</optionname>
                        <optionprice>185 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>переоборудование &#034;Маршрутное такси&#034;</optionname>
                        <optionprice>185 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO02</optionId>
                        <optionname>модификациифургона  &#034;Туристический&#034;</optionname>
                        <optionprice>345 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB02</optionId>
                        <optionname>Камера заднего вида</optionname>
                        <optionprice>17 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO03</optionId>
                        <optionname>profi</optionname>
                        <optionprice>186 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>26 700,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO17</optionId>
                        <optionname>NN Mg_345</optionname>
                        <optionprice>345 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO24</optionId>
                        <optionname>NN_MT_184 000</optionname>
                        <optionprice>184 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO23</optionId>
                        <optionname>IMS_MT_182 500</optionname>
                        <optionprice>182 500,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97PHDQ609U180</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 599 400,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>1810</Width> <Height>2764</Height> <Clearance>184</Clearance> <Trunk_size>17000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2010</Mass> <Equipment_code>2PU97PHDQ609U180</Equipment_code> <equipment_name>FgTl 440 L4H3</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>DX01</optionId>
                        <optionname>ASR-ПРОБУКСОВОЧНАЯ СИСТЕМА</optionname>
                        <optionprice>8 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93KHDQ609A680</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 577 500,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>DX01</optionId>
                        <optionname>ASR-ПРОБУКСОВОЧНАЯ СИСТЕМА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU91DHDQ609A680</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 409 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>1810</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU91DHDQ609A680</Equipment_code> <equipment_name>FgTl 330 L1H1</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>DO11</optionId>
                        <optionname>переоборудование Tour L1_245</optionname>
                        <optionprice>245 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AT14</optionId>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC47</optionId>
                        <optionname>Пакет &#034;Аудио&#034; (CD RDS двойной тюнер, 4 дин.+ MP3 Плеер)</optionname>
                        <optionprice>20 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>15 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>4 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>27 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>49 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>67 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>2 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>6 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>9 900,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>15 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC11</optionId>
                        <optionname>Правая сдвижная дверь со стеклом</optionname>
                        <optionprice>11 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>17 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>26 700,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Диски из алюминевого сплава</optionname>
                        <optionprice>21 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97LGDQ609A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 819 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>224</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2060</Mass> <Equipment_code>2PU97LGDQ609A0C0</Equipment_code> <equipment_name>FgTl 435 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>PX02</optionId>
                        <optionname>Защитная решетка за водительским сиденьем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>XY02</optionId>
                        <optionname>Догреватель непрограммируемый Webasto Thermo Top Z (5 кВт)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ES02</optionId>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>BF02</optionId>
                        <optionname>Аккумулятор повышенной емкости (110 Ач)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UG01</optionId>
                        <optionname>СИСТЕМА ПОМОЩИ ПРИ ЭКСТРЕННОМ ТОРМОЖЕНИИ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>ПОДУШКА БЕЗОПАСНОСТИ ВОДИТЕЛЯ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB07</optionId>
                        <optionname>Задние распашные на 180° цельнометаллические двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TD01</optionId>
                        <optionname>Гаечный ключ с трещоткой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FE09</optionId>
                        <optionname>Указатели поворота на зеркалах заднего вида (только для версии L4)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя (несовместимо с оби</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname></optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE12</optionId>
                        <optionname>Светодиодные дневные ходовые огни + указатели поворота (только для L4)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Боковые металлические двери со стеклами (недоступно для версий H3)</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + Hill Descent Control</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname></optionname>
                        <optionprice>24 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZ86</optionId>
                        <optionname>Легкосплавные диски 16&#039;&#039; (только для версии L4)</optionname>
                        <optionprice>31 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5FEC5D0A0B1</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 109 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5FEC5D0A0B1</Equipment_code> <equipment_name>Access</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контроль + ограничитель скорости движения автомобиля</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сидения с подогревом (ведет за собой установку бок подушек безопасности)</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2H</optionId>
                        <optionname>Стальные диски с декоративными колпаками Corail 16&#039;&#039;+ полноразмерное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5PKUMD0A0B2</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 395 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,50</Top100> <Fuel_consumption_city>8,80</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1255</Mass> <Equipment_code>1PT9A5PKUMD0A0B2</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>211</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1780</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>ZH2R</optionId>
                        <optionname>Легкосплавные диски 17&#034; Rubis + докатка</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2S</optionId>
                        <optionname>Легкосплавные диски 17&#034; Rubis + Полноразмерное запасное колесо</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG17</optionId>
                        <optionname>Пакет Безопасность (адаптивный круиз-контроль, система предупреждения о столкновении, система тормож</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB07</optionId>
                        <optionname>Камера заднего вида с парковочными линиями</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2N</optionId>
                        <optionname>Легкосплавные диски 16&#034; Topaze+полноразмерное колесо</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2P</optionId>
                        <optionname>Легкосплавные диски 16&#034; Topaze+ полноразмерноу колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB08</optionId>
                        <optionname>Датчик слепой зоны + система автоматической парковки City Park</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UN01</optionId>
                        <optionname>Премиальная аудиосистема Hi-Fi DENON (6)</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>1UFX</optionId>
                        <optionname>Комбинированная обивка сидений Alcantara + коврики</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>8DFX</optionId>
                        <optionname>Отделка сидений кожей + электрорегулировка сиденья водителя с памятью регулировок + функция массажа</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>YE02</optionId>
                        <optionname>Алюминиевая отделка педалей, алюминиевые пороги, рычаг переключения передач с отделкой хромом (для М</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>914 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLTI</optionId>
                        <optionname>Автомагнитола CD RD4 + MP3+ управоение аудиосистемой на рулевой колонке</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MGDQ609ULC0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 728 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>222</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2105</Mass> <Equipment_code>2PU97MGDQ609ULC0</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>XY02</optionId>
                        <optionname>Догреватель непрограммируемый Webasto Thermo Top Z (5 кВт)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ES02</optionId>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>BF02</optionId>
                        <optionname>Аккумулятор повышенной емкости (110 Ач)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UG01</optionId>
                        <optionname>СИСТЕМА ПОМОЩИ ПРИ ЭКСТРЕННОМ ТОРМОЖЕНИИ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>ПОДУШКА БЕЗОПАСНОСТИ ВОДИТЕЛЯ</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB07</optionId>
                        <optionname>Задние распашные на 180° цельнометаллические двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>TD01</optionId>
                        <optionname>Гаечный ключ с трещоткой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FE09</optionId>
                        <optionname>Указатели поворота на зеркалах заднего вида (только для версии L4)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PX02</optionId>
                        <optionname>Защитная решетка за водительским сиденьем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NR01</optionId>
                        <optionname>ПОДГОТОВКА ДЛЯ ЗАДНЕЙ КЛИМАТИЧЕСКОЙ УСТАНОВКИ (ВЕДЕТ ЗА СОБОЙ УСТАНОВКУ ПЕРЕДНЕГО КОНДИЦИОНЕРА)</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK02</optionId>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE12</optionId>
                        <optionname>Светодиодные дневные ходовые огни + указатели поворота (только для L4)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Боковые металлические двери со стеклами (недоступно для версий H3)</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZ86</optionId>
                        <optionname>Легкосплавные диски 16&#039;&#039; (только для версии L4)</optionname>
                        <optionprice>31 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DO25</optionId>
                        <optionname>НН_МТ_185</optionname>
                        <optionprice>185 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU91DGDQ609FCC0</carcomplectid>
                            <carcomplectname>PEUGEOT BOXER FOURGON TOLE</carcomplectname>
                            <carcomplectbaseprice>1 659 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU91DGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L1H1</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>UF01</optionId>
                        <optionname>ESP+Hill Holder - система динамической стабилизации движения</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF04</optionId>
                        <optionname>GRIP CONTROL + Hill Descent Control</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности переднего пассажира</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые подушки безопасности и шторки безопасности + полка над сидением водителя (несовместимо с оби</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>60 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>70 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NR01</optionId>
                        <optionname>ПОДГОТОВКА ДЛЯ ЗАДНЕЙ КЛИМАТИЧЕСКОЙ УСТАНОВКИ (ВЕДЕТ ЗА СОБОЙ УСТАНОВКУ ПЕРЕДНЕГО КОНДИЦИОНЕРА)</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев сиденья водителя и пассажира; при заказе данного оборудования - кабина двухместная (несовме</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>XY01</optionId>
                        <optionname>ПРЕДПУСКОВОЙ ПОДОГРЕВ ДВИГАТЕЛЯ WEBASTO</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>2 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Полочка над сиденьем водителя</optionname>
                        <optionprice>4 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>TK01</optionId>
                        <optionname></optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM07</optionId>
                        <optionname>ИНДИВИДУАЛЬНОЕ ПАССАЖИРСКОЕ СИДЕНИЕ (КАБИНА ДВУХМЕСТНАЯ)</optionname>
                        <optionprice>2 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WM08</optionId>
                        <optionname>Индивидуальное пассажирское сиденье регулируемое по высоте (кабина двухместная)</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>18 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK07</optionId>
                        <optionname>Дополнительный обогрев спереди с выводом воздуховодов в заднюю часть салона</optionname>
                        <optionprice>21 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>DK08</optionId>
                        <optionname>Подготовка для установки обогрева сзади</optionname>
                        <optionprice>12 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>GI01</optionId>
                        <optionname>Увеличенные передние подголовники</optionname>
                        <optionprice>3 300,00</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE12</optionId>
                        <optionname>Светодиодные дневные ходовые огни + указатели поворота (только для L4)</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль, ограничитель скорости</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VF07</optionId>
                        <optionname>Атермальное ветровое стекло (+ датчик дождя)</optionname>
                        <optionprice>19 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>12 800,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX09</optionId>
                        <optionname>Перегородка цельнометаллическая со сдвижным стеклом</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC12</optionId>
                        <optionname>Вторая боковая металлическая дверь  (недоступно для L4H3)</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optionname>Боковые металлические двери со стеклами (недоступно для версий H3)</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VC02</optionId>
                        <optionname>Задние распашные двери со стеклами + обогрев стёкол</optionname>
                        <optionprice>16 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>14 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Генератор увеличенной мощности 180 A</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MI01</optionId>
                        <optionname>Колеса увеличенной размерности 225/70 R15</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC29</optionId>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC57</optionId>
                        <optionname>Аудиосистема CD/MP3/USB + управление аудиосистемой на руле</optionname>
                        <optionprice>19 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC58</optionId>
                        <optionname>Аудиосистема CD/MP3/USB/Bluetooth с 5 дюймовым сенсорным экраном</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB09</optionId>
                        <optionname>Задние датчики парковки + камера заднего вида</optionname>
                        <optionprice>55 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NO02</optionId>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC07</optionId>
                        <optionname>Хронотахограф</optionname>
                        <optionprice>35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HC08</optionId>
                        <optionname>ОТСУТСТВИЕ ХРОНОТАХОГРАФА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>DZ86</optionId>
                        <optionname>Легкосплавные диски 16&#039;&#039; (только для версии L4)</optionname>
                        <optionprice>31 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>JI01</optionId>
                        <optionname>Разьем для установки дополнительного оборудования, необходимого для трансформации автомобиля</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VU</carcomplectname>
                            <carcomplectbaseprice>935 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>WLRW</optionId>
                        <optionname>Аудиосистема MP3 (4 динамика), Bluetooth и USB</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>PC17</optionId>
                        <optionname>Правая боковая сдвижная дверь</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Две боковые сдвижные двери</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PB04</optionId>
                        <optionname>Двухстворчатая распашная задняя застекленная дверь</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>23 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>8 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX28</optionId>
                        <optionname>сплош. перег. в половину высоты съемная+решетка+окно для погрузки длинномерных грузов+боковые расшир</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX30</optionId>
                        <optionname>Сплош перегор в половину высоты+решетка+окно для погруз+боков расширит+улучш шумоизол</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PX34</optionId>
                        <optionname>Перегородка цельнометаллическая с окошком  + улучшенная шумоизоляция (отделка перегородки пластиком)</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контороль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF02</optionId>
                        <optionname>Интеллектуальная система распеределения крутящего момента+ESP</optionname>
                        <optionprice>30 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>33 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с подогревом</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLKX</optionId>
                        <optionname>RCM4 аудиосистема MP3 (4 динамика)</optionname>
                        <optionprice>13 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;Plus Light&#034;. Пульт дистанционного управления+ стеклопод водителя+стеклопод пассажира+зеркала</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>BQ01</optionId>
                        <optionname>Пакет &#034;Электрика&#034;</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3KRBP04A060</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AA01</optionId>
                        <optionname>Система креплений Isofix для устройств детской безопасности</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA04</optionId>
                        <optionname>Заднее сиденье, складывающееся в отношении 50/50</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA05</optionId>
                        <optionname>Стеклоочиститель заднего стекла</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA06</optionId>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA07</optionId>
                        <optionname>Защитные молдинги бамперов черного цвета</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA08</optionId>
                        <optionname>Зеркала заднего вида + дверные ручки, окрашенные в цвет кузова</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AA09</optionId>
                        <optionname>Боковые молдинги черного цвета</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AF01</optionId>
                        <optionname>Тормозная антиблокировочная система (ABS)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AJ01</optionId>
                        <optionname>Задние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AK01</optionId>
                        <optionname>Передние сиденья с подголовниками, регулируемыми по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CB09</optionId>
                        <optionname>Багажная полка</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DD04</optionId>
                        <optionname>ЭЛЕКТОУСЕЛИТЕЛЬ  руля + рулевая колонка, регулируемая по высоте</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LT02</optionId>
                        <optionname> Передние электрические стеклоподъемники</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NN01</optionId>
                        <optionname>Передняя подушка безопасности пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PK02</optionId>
                        <optionname>Резина Michelin</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>SH02</optionId>
                        <optionname>Подушка безопасности водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VB09</optionId>
                        <optionname>Централизованная блокировка дверных замков с помощью пульта дистанционного управления</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WL01</optionId>
                        <optionname>Проводка для акустической системы</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>VD10</optionId>
                        <optionname>Заднее и задние боковые стекла с сильной тонировкой</optionname>
                        <optionprice>2 400,00</optionprice>
                        </option>
                        <option>
                        <optionId>CS02</optionId>
                        <optionname>Тахометр</optionname>
                        <optionprice>1 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>10 600,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>OP3Z</optionId>
                        <optionname>Комбинированная отделка салона кожа &#034;Claudia&#034; / Алькантара + кожаный руль</optionname>
                        <optionprice>21 200,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Колесные диски из алюминиевого сплава, 14&#039;&#039;</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WL45</optionId>
                        <optionname>Автомагнитола RCD3</optionname>
                        <optionprice>7 400,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>4008</carfamily>
                            <carmodel>4008</carmodel>
                            <carcomplectid>1PMJSUMRPH32A010</carcomplectid>
                            <carcomplectname>PEUGEOT 4008</carcomplectname>
                            <carcomplectbaseprice>1 479 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>150</Power> <VolumeReal>2,00</VolumeReal> <Transmission>вариатор</Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,80</Fuel_consumption_track> <Fuel_consumption>8,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4340</Length> <Width>1768</Width> <Height>1632</Height> <Clearance>0</Clearance> <Trunk_size>416</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1470</Mass> <Equipment_code>1PMJSUMRPH32A010</Equipment_code> <equipment_name>Access</equipment_name> <Generation>4008</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>197/4200</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1980</FullMass> <max_trunk_size>1540</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>DZKX</optionId>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Окраска кузова металлик</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5FCE5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 208 5 PORTES</carcomplectname>
                            <carcomplectbaseprice>861 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>999</Volume> <Power>68</Power> <VolumeReal>1,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,00</Top100> <Fuel_consumption_city>5,20</Fuel_consumption_city> <Fuel_consumption_track>3,70</Fuel_consumption_track> <Fuel_consumption>4,30</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>975</Mass> <Equipment_code>1PIAA5FCE5D0A0B0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>95/3000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1484</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>WLPB</optionId>
                        <optionname>Автомагнитола RDE  + AUX +  Управление аудиоситемой на рулевой  колонке</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK1</optionId>
                        <optionname>Пакет 1 Базовый (RE01+RJ04)</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5LCG5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 208 5 PORTES</carcomplectname>
                            <carcomplectbaseprice>928 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>12,20</Top100> <Fuel_consumption_city>5,60</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>975</Mass> <Equipment_code>1PIAA5LCG5D0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>175</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1527</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>HU02</optionId>
                        <optionname>Складываемые боковые зеркала заднего вида (3-дверная версия)</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK2</optionId>
                        <optionname>Пакет 2 Безопасность Active (PR01+UB01+UF01+NF04)</optionname>
                        <optionprice>36 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK3</optionId>
                        <optionname>Пакет 3 Комфорт Active (WV33+RE07+RG10+JB01)</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK4</optionId>
                        <optionname>Пакет 4 Стиль Active (FE10+VD09)</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH02</optionId>
                        <optionname>Компактное рулевое колесо с кожаной обивкой и хромированной вставкой</optionname>
                        <optionprice>3 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQU</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России + второй разъем USB</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQX</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России + второй разъем USB + CD-</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH75</optionId>
                        <optionname>Легкосплавные диски 15&#034; Azote</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH89</optionId>
                        <optionname>Легкосплавные диски 16&#034; Helium</optionname>
                        <optionprice>17 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5MCG5D0A0B0</carcomplectid>
                            <carcomplectname>PEUGEOT 208 5 PORTES</carcomplectname>
                            <carcomplectbaseprice>988 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>12,20</Top100> <Fuel_consumption_city>5,60</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>975</Mass> <Equipment_code>1PIAA5MCG5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>175</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1527</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>0P3U</optionId>
                        <optionname>Отделка сидений кожей Claudia</optionname>
                        <optionprice>50 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Противоугонная сигнализация с датчиками периметра и объема</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>FE10</optionId>
                        <optionname>Светодиодные дневные ходовые огни, световая линия, линзованные фары</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подсветкой</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK5</optionId>
                        <optionname>Пакет 5 Стиль Allure (FE10+ZH89)</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK6</optionId>
                        <optionname>Пакет 6 Безопасность Allure (UF01+NF04)</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PAK7</optionId>
                        <optionname>Пакет 7 Комфорт Allure (UB01+RG10)</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQL</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>6 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQU</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России + второй разъем USB</optionname>
                        <optionprice>22 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQX</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России + второй разъем USB + CD-</optionname>
                        <optionprice>28 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>4008</carfamily>
                            <carmodel>4008</carmodel>
                            <carcomplectid>1PMJSURRPH32A010</carcomplectid>
                            <carcomplectname>PEUGEOT 4008</carcomplectname>
                            <carcomplectbaseprice>1 629 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>150</Power> <VolumeReal>2,00</VolumeReal> <Transmission>вариатор</Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,80</Fuel_consumption_track> <Fuel_consumption>8,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4340</Length> <Width>1768</Width> <Height>1632</Height> <Clearance>0</Clearance> <Trunk_size>416</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1470</Mass> <Equipment_code>1PMJSURRPH32A010</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>4008</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>197/4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1980</FullMass> <max_trunk_size>1540</max_trunk_size> <baseoptions><option>
                        <optionId>KG01</optionId>
                        <optionname>Набор адаптаций для России</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF09</optionId>
                        <optionname>Боковые шторки безопасности для первого и второго ряда сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>XA01</optionId>
                        <optionname>Коленная подушка безопасности для водителя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>CG35</optionId>
                        <optionname>Передние трехточечные ремни безопасности с двойными преднатяжителями, ограничителями усилия и регули</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GM81</optionId>
                        <optionname>Бортовой компьютер с цветным ЖК-дисплеем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RG01</optionId>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>IP02</optionId>
                        <optionname>Электропривод регулировки сиденья водителя и пассажира в 8 направлениях</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RL05</optionId>
                        <optionname>Внутреннее зеркало заднего вида с автозатемнением</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YQ01</optionId>
                        <optionname>Система автоматического включения фар (датчик света) + датчик дождя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YD01</optionId>
                        <optionname>Система бесключевого доступа в салон и запуска двигателя кнопкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EK09</optionId>
                        <optionname>Ксеноновые фары с автоматическим корректором</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Передние противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Стекла второго ряда сидений и стекло задней двери с сильной тонировкой</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ON02</optionId>
                        <optionname>Зеркала заднего вида с подогревом и электрорегулировками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PQ02</optionId>
                        <optionname>Релинги на крыше</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>EK08</optionId>
                        <optionname>Светодиодные огни дневного света (LED DRL)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DZKZ</optionId>
                        <optionname>Пакет &#034;Look&#034;: 18&#034; легкосплавные колесные диски + Интегрированные лампы сигнала поворотов в зеркала з</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS09</optionId>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RC48</optionId>
                        <optionname>Аудиосистема с поддержкой CD + MP3, 6 динамиков</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>DO01</optionId>
                        <optionname>Коммуникационная система Bluetooth</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>YM04</optionId>
                        <optionname>Разъемы AUX/USB для подключения внешних устройств</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>LE</optionId>
                        <optionname>Комплектация LE</optionname>
                        <optionprice>-35 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC55</optionId>
                        <optionname>Мультимедийная система с цветным дисплеем и камерой заднего вида</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Окраска кузова металлик</optionname>
                        <optionprice>16 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Сигнализация с датчиком периметра и объема</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RC40</optionId>
                        <optionname>Аудиосистема премиум класса  Hi-Fi Rockford Fosgate 710 Вт, 8 динамиков+ савбуфер</optionname>
                        <optionprice>32 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная стеклянная крыша со светодиодной подстветкой</optionname>
                        <optionprice>34 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0P9L</optionId>
                        <optionname>Кожаная обивка сидений</optionname>
                        <optionprice>85 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM0</optionId>
                        <optionname>Окраска кузова &#034;металлик&#034;</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 5 PORTES</carmodel>
                            <carcomplectid>1PB0A5KRBP04A070</carcomplectid>
                            <carcomplectname>PEUGEOT 107 5 PORTES</carcomplectname>
                            <carcomplectbaseprice>672 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>4</Seats> <Volume>998</Volume> <Power>68</Power> <VolumeReal>1,00</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,00</Top100> <Fuel_consumption_city>5,50</Fuel_consumption_city> <Fuel_consumption_track>4,10</Fuel_consumption_track> <Fuel_consumption>4,60</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3430</Length> <Width>1630</Width> <Height>1470</Height> <Clearance>145</Clearance> <Trunk_size>130</Trunk_size> <Fuel_tank_size>35</Fuel_tank_size> <Mass>835</Mass> <Equipment_code>1PB0A5KRBP04A070</Equipment_code> <equipment_name>Active</equipment_name> <Generation>107 Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>93/3600</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>157</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1190</FullMass> <max_trunk_size>751</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>RP02</optionId>
                        <optionname>Колесные диски из алюминиевого сплава, 14&#039;&#039;</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>KL05</optionId>
                        <optionname></optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Заднее и задние боковые стекла с сильной тонировкой</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLMZ</optionId>
                        <optionname>Автомагнитола RDE2 + 4 динамика+USB+MP3+BLUETOOTH</optionname>
                        <optionprice>12 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>10 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optionname>Кожаный руль+рукоятка КПП отделанная кожей (для версии с МТ)</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>2MOM</optionId>
                        <optionname>Двухтоновое покрытие металлик</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>107</carfamily>
                            <carmodel>107 3 PORTES</carmodel>
                            <carcomplectid>1PB0A3KRBP04A070</carcomplectid>
                            <carcomplectname>PEUGEOT 107 3 PORTES</carcomplectname>
                            <carcomplectbaseprice>0,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>4</Seats> <Volume>998</Volume> <Power>68</Power> <VolumeReal>1,00</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,00</Top100> <Fuel_consumption_city>5,50</Fuel_consumption_city> <Fuel_consumption_track>4,10</Fuel_consumption_track> <Fuel_consumption>4,60</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3430</Length> <Width>1630</Width> <Height>1470</Height> <Clearance>145</Clearance> <Trunk_size>130</Trunk_size> <Fuel_tank_size>35</Fuel_tank_size> <Mass>825</Mass> <Equipment_code>1PB0A3KRBP04A070</Equipment_code> <equipment_name>Active</equipment_name> <Generation>107 Рестайлинг</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>93/3600</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>157</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1180</FullMass> <max_trunk_size>751</max_trunk_size> <baseoptions><option>
                        <optionId>WL45</optionId>
                        <optionname>Автомагнитола RCD3</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLMZ</optionId>
                        <optionname>Автомагнитола RDE2+4 динамика+USB+MP3+Bluetooth</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Колесные диски из алюминиевого сплава, 14&#039;&#039;</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Заднее и задние боковые стекла с сильной тонировкой</optionname>
                        <optionprice>2 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>KL05</optionId>
                        <optionname>Внутренняя отделка салона</optionname>
                        <optionprice>1 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>10 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>17 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>7 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optionname>Кожаный руль+ рукоятка КПП отделанная кожей (для версии с мт)</optionname>
                        <optionprice>3 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>M0M2</optionId>
                        <optionname>двухтоновое покрытие металлик</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NEC5D0A0</carcomplectid>
                            <carcomplectname>PEUGEOT 301</carcomplectname>
                            <carcomplectbaseprice>926 900,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,40</Top100> <Fuel_consumption_city>8,70</Fuel_consumption_city> <Fuel_consumption_track>5,10</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1165</Mass> <Equipment_code>1PM3A4NEC5D0A0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1524</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>8WFZ</optionId>
                        <optionname>Отделка сидений тканью Oza Lama (светлая) - ведет за собой установку боковых ПБ (NF01)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB11</optionId>
                        <optionname>Противоугонная сигнализация + 2 ключа с ДУ</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C0</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>1 026 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optionname>Задние секвентальные электрические стеклоподъемники с функцией анти-зажим</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>LW02</optionId>
                        <optionname>Подогрев нижней части лобового стекла + обогреваемые форсунки омывателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Двухзонный климат-контроль + воздуховоды для задних пассажиров</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>VH09</optionId>
                        <optionname>Кожаный руль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLTV</optionId>
                        <optionname>Автомагнитола CD RD4  + MP3 + Управление аудиоситемой на рулевой  колонке  + Коммуникационная систем</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>ZRM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;Серебристый металлик&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>Легкосплавные диски 17&#034; MELBOURNE 2</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VP</carmodel>
                            <carcomplectid>1PB9AFSBFS32A026</carcomplectid>
                            <carcomplectname>PEUGEOT PARTNER VP</carcomplectname>
                            <carcomplectbaseprice>1 219 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>минивэн</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>7</Seats> <Volume>1560</Volume> <Power>90</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,50</Top100> <Fuel_consumption_city>5,00</Fuel_consumption_city> <Fuel_consumption_track>4,40</Fuel_consumption_track> <Fuel_consumption>4,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4380</Length> <Width>1810</Width> <Height>1801</Height> <Clearance>150</Clearance> <Trunk_size>2650</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1505</Mass> <Equipment_code>1PB9AFSBFS32A026</Equipment_code> <equipment_name>Outdoor</equipment_name> <Generation>Partner Tepee II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>230/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>165</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2070</FullMass> <max_trunk_size>3000</max_trunk_size> <baseoptions><option>
                        <optionId>NN01</optionId>
                        <optionname>Подушка безопасности пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NF02</optionId>
                        <optionname>Боковые подушки безопасности водителя и пассажира</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optionname>Система динамической стабилизации (ESP) (VP)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RS03</optionId>
                        <optionname>Запасное колесо</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FX02</optionId>
                        <optionname>Пакет &#034;Улучшенная проходимость&#034;: усиленная подвеска, увеличенный дорожный просвет (+7мм спереди / +</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WD36</optionId>
                        <optionname>Пакет &#034;plus light&#034;Стеклопод. водит. секвентальный+ стеклопод. пассаж. несеквентальный+ зерк с электр</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optionname>Прикуриватель. Съемная пепельница</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JO01</optionId>
                        <optionname>Потолочная консоль</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>AL04</optionId>
                        <optionname>Съемное сиденье второго ряда с подголовниками</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>JB03</optionId>
                        <optionname>Подлокотники водитель+ передний пассажир</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PC19</optionId>
                        <optionname>Боковые сдвижные двери</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PB02</optionId>
                        <optionname>Застекленная откидная задняя дверь Hayon</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>GB16</optionId>
                        <optionname>Ковролиновое покрытие пола в багажном отделении</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>FE10</optionId>
                        <optionname>Передние светодиодные фары дневного света (опция не доступна с окраской кузова T5M0)</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>PQ02</optionId>
                        <optionname>Продольные багажные дуги на крыше</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>WLKI</optionId>
                        <optionname>RCM4 ауиосистема MP3 (4 динамика)  аудиовход AUX</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>OL07</optionId>
                        <optionname>Размер колес 16&#034; (215/55 R16) стандартная комплектация для вариантов,оснащенных RP02</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>NF04</optionId>
                        <optionname>Боковые и оконные  подушки безопасности</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>RG03</optionId>
                        <optionname>Круиз контроль + ограничитель скорости</optionname>
                        <optionprice>4 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optionname>Наруж. зеркала зад. вида, складывающиеся с помощью электропривода</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Система помощи при парковке</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optionname>Cтекла второго ряда сидений и стекло задней двери с сильной тонировкой + заднее открываемое стекло</optionname>
                        <optionprice>9 500,00</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>AL05</optionId>
                        <optionname>Три независимых сиденья второго ряда, передний подлокотник</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Пакет &#034;City&#034;:Датчик дождя, датчик света</optionname>
                        <optionprice>11 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>EK01</optionId>
                        <optionname>ФАРЫ ДНЕВНОГО СВЕТА</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OU01</optionId>
                        <optionname>Многофункциональная крыша Zenith
• Продольные рейлинги на крыше Zenith</optionname>
                        <optionprice>75 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLLO/ WLRW</optionId>
                        <optionname>RCM4  Аудиосистема MP3
• DO01 Система Blue Tooth &#034;Free Hands Phone&#034; 
• Разъём USB</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLQI</optionId>
                        <optionname>Навигационная системавход AUX, Bluetooth - система беспроводного подключения мобильного телефона к а</optionname>
                        <optionprice>45 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>0MM0</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>15 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carcomplectname>PEUGEOT 308 5D PORTES</carcomplectname>
                            <carcomplectbaseprice>1 176 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionalavailableoptions><option>
                        <optionId>ZH2G</optionId>
                        <optionname>Стальные диски с декоративными колпаками Coral 16+докатка</optionname>
                        <optionprice>0,00</optionprice>
                        </option>
                        <option>
                        <optionId>AB08</optionId>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NB08</optionId>
                        <optionname>Пакет Обзорность</optionname>
                        <optionprice>9 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>VD10</optionId>
                        <optionname>Тонированные заднее и задние боковые стекла + хромированная окантовка боковых зеркал (Active)</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OK01</optionId>
                        <optionname>Панорамная   стеклянная   крыша   &#034;Cielo&#034;</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSY</optionId>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>25 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>WLSU</optionId>
                        <optionname>CD-проигрыватель</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2H</optionId>
                        <optionname>Стальные диски с декоративными колпаками Corail 16&#039;&#039;+ полноразмерное колесо</optionname>
                        <optionprice>5 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH2K</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+ докатка</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4A</optionId>
                        <optionname>Легкосплавные диски 16&#034; Quartz+полноразмерное колесо</optionname>
                        <optionprice>19 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>14 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optionname>Оконные (шторки) подушки безопасности</optionname>
                        <optionprice>7 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optionname>Парктроник задний  (4 датчика )</optionname>
                        <optionprice>10 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto><auto>
                            <carcomplect>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carcomplectname>PEUGEOT 408</carcomplectname>
                            <carcomplectbaseprice>978 000,00</carcomplectbaseprice>
                            </carcomplect>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions><option>
                        <optionId>PR01</optionId>
                        <optionname>Противотуманные   фары</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optionname>Кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optionname>Передние сиденья с регулируемым подогревом + кондиционер + охлаждаемый перчаточный ящик</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>0</optionprice>
                        </option>
                        </baseoptions><additionalavailableoptions><option>
                        <optionId>WLTI</optionId>
                        <optionname>Автомагнитола CD RD4 + MP3+ управоение аудиосистемой на рулевой колонке</optionname>
                        <optionprice>37 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OL08</optionId>
                        <optionname>Легкосплавные диски 17&#034; MELBOURNE 2</optionname>
                        <optionprice>20 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>OMMO</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>13 000,00</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>18 000,00</optionprice>
                        </option>
                        </additionalavailableoptions></auto></carComplects></carcomplects-catalog>