﻿<?xml version="1.0"?><auto-catalog><carsOnHand><auto>
                            <car>
                            <carid>00876673</carid>
                            <VIN>VF3DDHMY0EJ587386</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0L8</carcolorid>
                            <carcolorname>Серо-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00848083</carid>
                            <VIN>VF3CU5FJ0EY102326</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYMCA5D0A0C0</carcomplectid>
                            <carname>Peugeot 2008 Allure 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 186 000,00</price>
                            <optionprice>12 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYMCA5D0A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888702</carid>
                            <VIN>VF37HNFUCFJ564408</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>913 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878397</carid>
                            <VIN>Z8T4D5FEAFM000090</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C1</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 042 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875871</carid>
                            <VIN>VF3YEZMFC12715190</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MGDQ609ULC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 440 L4H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 913 000,00</price>
                            <optionprice>185 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>222</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2105</Mass> <Equipment_code>2PU97MGDQ609ULC0</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>DO25</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>НН_МТ_185</optionname>
                        <optionprice>НН_МТ_185</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00863984</carid>
                            <VIN>VF3LWNFPAES250205</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 190 000,00</price>
                            <optionprice>14 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876647</carid>
                            <VIN>VF3CCZMZ0DT138803</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5FCE5D0A0B0</carcomplectid>
                            <carname>Peugeot 208 Access 1 MT-5 68 (Л.С.)</carname>
                            <carcolorid>M0M7</carcolorid>
                            <carcolorname>Коричневый металлик Rouge Noir</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>897 000,00</price>
                            <optionprice>36 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>999</Volume> <Power>68</Power> <VolumeReal>1,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,00</Top100> <Fuel_consumption_city>5,20</Fuel_consumption_city> <Fuel_consumption_track>3,70</Fuel_consumption_track> <Fuel_consumption>4,30</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>975</Mass> <Equipment_code>1PIAA5FCE5D0A0B0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>95/3000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1484</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PAK1</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Пакет 1 Базовый (RE01+RJ04)</optionname>
                        <optionprice>Пакет 1 Базовый (RE01+RJ04)</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879535</carid>
                            <VIN>VF37H9HKCEJ708720</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A024</carcomplectid>
                            <carname>Peugeot Partner VU 1.6 дизель MT-5 75 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>983 500,00</price>
                            <optionprice>48 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>2</Seats> <Volume>1560</Volume> <Power>75</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,60</Top100> <Fuel_consumption_city>6,70</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>5,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4380</Length> <Width>1810</Width> <Height>1844</Height> <Clearance>154</Clearance> <Trunk_size>3300</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1363</Mass> <Equipment_code>2PB9J0CBX532A024</Equipment_code> <equipment_name>VU</equipment_name> <Generation>Partner II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>185/1500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2215</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00805159</carid>
                            <VIN>VF3YCZMFC12596858</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 617 800,00</price>
                            <optionprice>22 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878892</carid>
                            <VIN>Z8T4D5FEAFM000366</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0T4</carcolorid>
                            <carcolorname>Синий</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>989 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888710</carid>
                            <VIN>VF37H9HKCFJ566800</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875991</carid>
                            <VIN>Z8T4D5FEAEM013881</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C0</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 039 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876667</carid>
                            <VIN>VF3DDHMY0EJ598095</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0L8</carcolorid>
                            <carcolorname>Серо-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00873680</carid>
                            <VIN>VF30U5FS0ES080898</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>3008</carfamily>
                            <carmodel>3008</carmodel>
                            <carcomplectid>1PTBSYHCA532A0F0</carcomplectid>
                            <carname>Peugeot 3008 Active 1.6 MT-5 120 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 323 000,00</price>
                            <optionprice>16 000,00</optionprice>
                            <additionalequipmentprice>107 187,61</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>11,80</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4365</Length> <Width>1837</Width> <Height>1639</Height> <Clearance>0</Clearance> <Trunk_size>432</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1474</Mass> <Equipment_code>1PTBSYHCA532A0F0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>3008 I Рecтайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2020</FullMass> <max_trunk_size>1241</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>ZD09</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionname>
                        <optionprice>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874573</carid>
                            <VIN>Z8T4D5FEAEM018536</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C0</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 037 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00891262</carid>
                            <VIN>VF37RNFUCFJ581783</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BC6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 035 500,00</price>
                            <optionprice>82 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>AX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>3-ое сиденье Multi-Flex</optionname>
                        <optionprice>3-ое сиденье Multi-Flex</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876670</carid>
                            <VIN>VF3DDHMY0EJ589806</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0L8</carcolorid>
                            <carcolorname>Серо-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883599</carid>
                            <VIN>Z8T4D5FEAFM001128</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00871229</carid>
                            <VIN>VF3YCZMAU12703211</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER Chassis Cabine</carmodel>
                            <carcomplectid>2PU96FGDQ609A0C0</carcomplectid>
                            <carname>Peugeot Boxer ChCa 335 L3 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 549 500,00</price>
                            <optionprice>9 500,00</optionprice>
                            <additionalequipmentprice>233 133,84</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>шасси</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5943</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1635</Mass> <Equipment_code>2PU96FGDQ609A0C0</Equipment_code> <equipment_name>ChCa 335 L3</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>3500</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888712</carid>
                            <VIN>VF37H9HKCFJ564399</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888700</carid>
                            <VIN>VF37HNFUCFJ566804</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>913 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888706</carid>
                            <VIN>VF37H9HKCFJ566801</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>999 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876665</carid>
                            <VIN>VF3DDHMY0EJ581879</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0T6</carcolorid>
                            <carcolorname>Синий металлик Bleu Kyanos</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876663</carid>
                            <VIN>VF3DDHMY0EJ585550</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888704</carid>
                            <VIN>VF37HNFUCFJ566807</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>962 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00848089</carid>
                            <VIN>VF3YAZMFB12701918</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IGDQ609FCC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 771 300,00</price>
                            <optionprice>82 300,00</optionprice>
                            <additionalequipmentprice>1 654,03</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU93IGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878807</carid>
                            <VIN>Z8T4D5FEAFM000324</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>989 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878395</carid>
                            <VIN>Z8T4D5FEAFM000140</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C1</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 037 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875873</carid>
                            <VIN>VF3YEZMFC12715273</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MGDQ609ULC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 440 L4H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 913 000,00</price>
                            <optionprice>185 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>222</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2105</Mass> <Equipment_code>2PU97MGDQ609ULC0</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>DO25</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>НН_МТ_185</optionname>
                        <optionprice>НН_МТ_185</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876660</carid>
                            <VIN>VF3DDHMY0EJ581877</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00805160</carid>
                            <VIN>VF3YCZMFC12597627</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 617 800,00</price>
                            <optionprice>22 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876651</carid>
                            <VIN>VF3DD9HJCEJ562068</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4KBW5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Active 1.6 дизель MT-5 92 (Л.С.)</carname>
                            <carcolorid>P0XY</carcolorid>
                            <carcolorname>Черный лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>728 900,00</price>
                            <optionprice>11 000,00</optionprice>
                            <additionalequipmentprice>31 513,83</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>92</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>11,30</Top100> <Fuel_consumption_city>4,80</Fuel_consumption_city> <Fuel_consumption_track>3,70</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1165</Mass> <Equipment_code>1PM3A4KBW5D0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>230/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>180</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1549</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00873467</carid>
                            <VIN>VF3YAZMFB12571020</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IHDQ609FC80</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 607 600,00</price>
                            <optionprice>70 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93IHDQ609FC80</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00854528</carid>
                            <VIN>VF3YAZMFB12707822</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IGDQ609FCC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 701 800,00</price>
                            <optionprice>12 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU93IGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883139</carid>
                            <VIN>Z8T4D5FEAFM001228</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0T4</carcolorid>
                            <carcolorname>Синий</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876692</carid>
                            <VIN>VF3YCZMFC12478417</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>4 042,53</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882612</carid>
                            <VIN>Z8T4D5FS9FM001232</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876688</carid>
                            <VIN>VF3YCZMFC12579989</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 686 300,00</price>
                            <optionprice>90 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>TK02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>Защита до середины высоты грузового отсека</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00815456</carid>
                            <VIN>VF3YAZMFA12603295</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU91DHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L1H1 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 726 900,00</price>
                            <optionprice>317 900,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>1810</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU91DHDQ609A680</Equipment_code> <equipment_name>FgTl 330 L1H1</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>DO11</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>переоборудование Tour L1_245</optionname>
                        <optionprice>переоборудование Tour L1_245</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>Брызговики (передние и задние)</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879536</carid>
                            <VIN>VF37H9HKCEJ699181</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A024</carcomplectid>
                            <carname>Peugeot Partner VU 1.6 дизель MT-5 75 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>983 500,00</price>
                            <optionprice>48 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>3</Doors> <Seats>2</Seats> <Volume>1560</Volume> <Power>75</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,60</Top100> <Fuel_consumption_city>6,70</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>5,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4380</Length> <Width>1810</Width> <Height>1844</Height> <Clearance>154</Clearance> <Trunk_size>3300</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1363</Mass> <Equipment_code>2PB9J0CBX532A024</Equipment_code> <equipment_name>VU</equipment_name> <Generation>Partner II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>185/1500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2215</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00801209</carid>
                            <VIN>VF3YCZMFC12582306</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 667 300,00</price>
                            <optionprice>71 500,00</optionprice>
                            <additionalequipmentprice>1 947,23</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00877751</carid>
                            <VIN>VF3CC5FS9ET137181</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5LCALD0A0D0</carcomplectid>
                            <carname>Peugeot 208 Active 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M0G4</carcolorid>
                            <carcolorname>Серый металлик Blossom Grey</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 049 000,00</price>
                            <optionprice>59 000,00</optionprice>
                            <additionalequipmentprice>28 978,05</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,70</Top100> <Fuel_consumption_city>9,80</Fuel_consumption_city> <Fuel_consumption_track>4,90</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1090</Mass> <Equipment_code>1PIAA5LCALD0A0D0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1625</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>WLOA</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>Русифицированная навигационная система с предустановленными картами России</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionname>
                        <optionprice>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>Передние противотуманные   фары</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>Передние сиденья с 3-ступенчатым подогревом</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00850506</carid>
                            <VIN>VF3CU5FJ0EY101631</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYMCA5D0A0C0</carcomplectid>
                            <carname>Peugeot 2008 Allure 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 174 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYMCA5D0A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00891274</carid>
                            <VIN>VF37L9HECFJ579933</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BCBW532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>990 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882610</carid>
                            <VIN>Z8T4D5FS9FM001180</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>932 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888714</carid>
                            <VIN>VF37H9HKCFJ562441</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876681</carid>
                            <VIN>VF3DDHMY0EJ585553</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0T6</carcolorid>
                            <carcolorname>Синий металлик Bleu Kyanos</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00887409</carid>
                            <VIN>VF3XDRHKHFZ011760</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VP</carmodel>
                            <carcomplectid>2PG95QEBA632CG82</carcomplectid>
                            <carname>PEUGEOT EXPERT VP</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 597 000,00</price>
                            <optionprice>68 000,00</optionprice>
                            <additionalequipmentprice>22 009,55</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>RS03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер для передних пасажиров</optionname>
                        <optionprice>Кондиционер для передних пасажиров</optionprice>
                        </option>
                        <option>
                        <optionId>WK17</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев сидений</optionname>
                        <optionprice>Подогрев сидений</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом , электрообогревом и электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом , электрообогревом и электроприводом складывания</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888716</carid>
                            <VIN>VF37H9HKCFJ562439</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00877995</carid>
                            <VIN>Z8T4DNFP0FM000084</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>859 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876700</carid>
                            <VIN>VF3YEZMFC12718593</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MGDQ609ULC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 440 L4H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 913 000,00</price>
                            <optionprice>185 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>222</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2105</Mass> <Equipment_code>2PU97MGDQ609ULC0</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>DO25</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>НН_МТ_185</optionname>
                        <optionprice>НН_МТ_185</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879411</carid>
                            <VIN>Z8T4DNFP0FM000520</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>874 000,00</price>
                            <optionprice>33 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>RP02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 16&#034;</optionname>
                        <optionprice>Легкосплавные диски 16&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00861313</carid>
                            <VIN>VF3LWNFPAES244742</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>P09G</carcolorid>
                            <carcolorname>Серый  Gris Hurricane</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 176 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00882470</carid>
                            <VIN>Z8T4DNFP0FM001126</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>856 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876690</carid>
                            <VIN>VF3YCZMFC12568557</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 690 300,00</price>
                            <optionprice>94 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>RC29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиоподготовка (4 динамика)</optionname>
                        <optionprice>Аудиоподготовка (4 динамика)</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>NA03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionname>
                        <optionprice>Подогрев сиденья водителя (несовместимо с обивкой TJFT)</optionprice>
                        </option>
                        <option>
                        <optionId>DK01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Дополнительный обогрев передней части салона</optionname>
                        <optionprice>Дополнительный обогрев передней части салона</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00809465</carid>
                            <VIN>VF3BUAFZPEZ804929</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>4008</carfamily>
                            <carmodel>4008</carmodel>
                            <carcomplectid>1PMJSUPRPH32A010</carcomplectid>
                            <carname>Peugeot 4008 Active 2 4WD  вариатор 150 (Л.С.)</carname>
                            <carcolorid>YZM0</carcolorid>
                            <carcolorname>серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 595 000,00</price>
                            <optionprice>6 000,00</optionprice>
                            <additionalequipmentprice>10 788,62</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>150</Power> <VolumeReal>2,00</VolumeReal> <Transmission>вариатор</Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,80</Fuel_consumption_track> <Fuel_consumption>8,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4340</Length> <Width>1768</Width> <Height>1632</Height> <Clearance>0</Clearance> <Trunk_size>416</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1470</Mass> <Equipment_code>1PMJSUPRPH32A010</Equipment_code> <equipment_name>Active</equipment_name> <Generation>4008</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>197/4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1980</FullMass> <max_trunk_size>1540</max_trunk_size> <baseoptions><option>
                        <optionId>CG35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние трехточечные ремни безопасности с двойными преднатяжителями, ограничителями усилия и регули</optionname>
                        <optionprice>Передние трехточечные ремни безопасности с двойными преднатяжителями, ограничителями усилия и регули</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Окраска кузова металлик</optionname>
                        <optionprice>Окраска кузова металлик</optionprice>
                        </option>
                        <option>
                        <optionId>БЕЗ UB01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОТСУТСТВУЕТ UB01</optionname>
                        <optionprice>ОТСУТСТВУЕТ UB01</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883137</carid>
                            <VIN>Z8T4D5FS9FM001285</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>932 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00877533</carid>
                            <VIN>VF3DDHMY0EJ806902</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4KCMPD0A0B4</carcomplectid>
                            <carname>Peugeot 301 Active 1.2 RT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>818 900,00</price>
                            <optionprice>19 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>6,30</Fuel_consumption_city> <Fuel_consumption_track>4,60</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>980</Mass> <Equipment_code>1PM3A4KCMPD0A0B4</Equipment_code> <equipment_name>Active</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1467</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>RE02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер с электронным управлением</optionname>
                        <optionprice>Кондиционер с электронным управлением</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882619</carid>
                            <VIN>Z8T4DNFP0FM001136</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>861 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00890581</carid>
                            <VIN>VF3XDRHKHFZ013842</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VP</carmodel>
                            <carcomplectid>2PG95QPBA632CG81</carcomplectid>
                            <carname>Peugeot Expert Allure 2 дизель MT-6 120 (Л.С.)</carname>
                            <carcolorid>M0H8</carcolorid>
                            <carcolorname>Бежевый металлик</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 752 500,00</price>
                            <optionprice>108 500,00</optionprice>
                            <additionalequipmentprice>10 259,51</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>минивэн</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>9</Seats> <Volume>1997</Volume> <Power>120</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>14,70</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>5135</Length> <Width>1895</Width> <Height>1942</Height> <Clearance>0</Clearance> <Trunk_size>553</Trunk_size> <Fuel_tank_size>80</Fuel_tank_size> <Mass>1919</Mass> <Equipment_code>2PG95QPBA632CG81</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>Expert Tepee</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>300/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2791</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RS03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Левая боковая остекленная сдвижная дверь</optionname>
                        <optionprice>Левая боковая остекленная сдвижная дверь</optionprice>
                        </option>
                        <option>
                        <optionId>NR02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер для передних и задних пасажиров</optionname>
                        <optionprice>Кондиционер для передних и задних пасажиров</optionprice>
                        </option>
                        <option>
                        <optionId>WK27</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Боковые подушки безопасности + подогрев сидений</optionname>
                        <optionprice>Боковые подушки безопасности + подогрев сидений</optionprice>
                        </option>
                        <option>
                        <optionId>AN13</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>3-й ряд сидений (3 места)</optionname>
                        <optionprice>3-й ряд сидений (3 места)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00813726</carid>
                            <VIN>VF3CU5FJ0EY046805</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCA5D0A0B0</carcomplectid>
                            <carname>Peugeot 2008 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>G5M0</carcolorid>
                            <carcolorname>Бежевый металлик</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 148 000,00</price>
                            <optionprice>24 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYLCA5D0A0B0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>Двухзонный климат-контроль</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876698</carid>
                            <VIN>VF3YCZMFC12476692</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00861301</carid>
                            <VIN>VF3LWNFPAES249004</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M0KU</carcolorid>
                            <carcolorname>Синий металлик BLEU ENCRE</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 190 000,00</price>
                            <optionprice>14 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882464</carid>
                            <VIN>Z8T4D5FEAFM001179</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0T4</carcolorid>
                            <carcolorname>Синий</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882608</carid>
                            <VIN>Z8T4D5FS9FM001116</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M0G2</carcolorid>
                            <carcolorname>Бежевый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876683</carid>
                            <VIN>VF3DDHMY0EJ585551</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0L8</carcolorid>
                            <carcolorname>Серо-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878365</carid>
                            <VIN>VF3YCZMAU12713445</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER Chassis Cabine</carmodel>
                            <carcomplectid>2PU96FGDQ609A0C0</carcomplectid>
                            <carname>Peugeot Boxer ChCa 335 L3 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 689 500,00</price>
                            <optionprice>149 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>шасси</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>6,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5943</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1635</Mass> <Equipment_code>2PU96FGDQ609A0C0</Equipment_code> <equipment_name>ChCa 335 L3</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>3500</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>CTTM_izoterm_140000</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>CTTM_izoterm_140000</optionname>
                        <optionprice>CTTM_izoterm_140000</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882466</carid>
                            <VIN>Z8T4D5FEAFM000924</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879407</carid>
                            <VIN>Z8T4D5FEAFM000452</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>989 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879703</carid>
                            <VIN>Z8T4D5FEAFM000557</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0ZR</carcolorid>
                            <carcolorname>Серебристый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>978 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00843184</carid>
                            <VIN>VF3CU5FJ0EY098713</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYLCA5D0A0C0</carcomplectid>
                            <carname>Peugeot 2008 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M0F4</carcolorid>
                            <carcolorname>Серебристый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 145 000,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYLCA5D0A0C0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>LE05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883141</carid>
                            <VIN>Z8T4DNFP0FM001230</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>856 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876696</carid>
                            <VIN>VF3YCZMFC12476929</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876685</carid>
                            <VIN>VF3YCZMFC12602884</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P04P</carcolorid>
                            <carcolorname>Темно-синий лак Bleu Imperial</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 686 600,00</price>
                            <optionprice>90 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AT14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Антенна (радио), встроенная в зеркало заднего вида</optionname>
                        <optionprice>Антенна (радио), встроенная в зеркало заднего вида</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>NO02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Увеличенный объем топливного бака (125 литров)</optionname>
                        <optionprice>Увеличенный объем топливного бака (125 литров)</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>PE03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Брызговики (передние и задние)</optionname>
                        <optionprice>Брызговики (передние и задние)</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>DO030</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>КАТЕГОРИЯ C</optionname>
                        <optionprice>КАТЕГОРИЯ C</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888703</carid>
                            <VIN>VF37HNFUCFJ568602</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>962 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00861315</carid>
                            <VIN>VF3LWNFPAES244728</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M01S</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 200 000,00</price>
                            <optionprice>24 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парктроник задний  (4 датчика )</optionname>
                        <optionprice>Парктроник задний  (4 датчика )</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00864791</carid>
                            <VIN>VF3LWNFPAES249006</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5LEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Active 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M01S</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 190 000,00</price>
                            <optionprice>14 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5LEC5D0A0B1</Equipment_code> <equipment_name>Active</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или &#034;белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888709</carid>
                            <VIN>VF37H9HKCFJ562443</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>999 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00878396</carid>
                            <VIN>Z8T4D5FEAFM000121</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C1</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 037 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00803043</carid>
                            <VIN>VF3YCZMFC12573220</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 622 800,00</price>
                            <optionprice>27 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00873682</carid>
                            <VIN>VF30U5FS0ES082530</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>3008</carfamily>
                            <carmodel>3008</carmodel>
                            <carcomplectid>1PTBSYHCA532A0F0</carcomplectid>
                            <carname>Peugeot 3008 Active 1.6 MT-5 120 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 323 000,00</price>
                            <optionprice>16 000,00</optionprice>
                            <additionalequipmentprice>120 155,09</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>11,80</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4365</Length> <Width>1837</Width> <Height>1639</Height> <Clearance>0</Clearance> <Trunk_size>432</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1474</Mass> <Equipment_code>1PTBSYHCA532A0F0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>3008 I Рecтайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2020</FullMass> <max_trunk_size>1241</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>ZD09</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionname>
                        <optionprice>Pack Retrovision : 
• Наружные зеркала заднего вида с электроприводом складывания
• Самозатемняющеес</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876669</carid>
                            <VIN>VF3DDHMY0EJ589807</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0T6</carcolorid>
                            <carcolorname>Синий металлик Bleu Kyanos</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>49 671,34</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00854612</carid>
                            <VIN>VF3YAZMFA12707418</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU91DGDQ609FCC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L1H1 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 731 800,00</price>
                            <optionprice>72 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU91DGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L1H1</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00891272</carid>
                            <VIN>VF37L9HECFJ590602</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BCBW532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>990 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876666</carid>
                            <VIN>VF3DDHMY0EJ581878</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879534</carid>
                            <VIN>Z8T4D5FEAFM000466</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0G2</carcolorid>
                            <carcolorname>Бежевый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>989 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888711</carid>
                            <VIN>VF37H9HKCFJ562438</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876648</carid>
                            <VIN>VF3CCHMZ0DT132799</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5MCG5D0A0B0</carcomplectid>
                            <carname>Peugeot 208 Allure 1.2 MT-5 82 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 012 000,00</price>
                            <optionprice>24 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>12,20</Top100> <Fuel_consumption_city>5,60</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,50</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>975</Mass> <Equipment_code>1PIAA5MCG5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>175</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1527</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FE10</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Светодиодные дневные ходовые огни, световая линия, линзованные фары</optionname>
                        <optionprice>Светодиодные дневные ходовые огни, световая линия, линзованные фары</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>Задние датчики парковки</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00801555</carid>
                            <VIN>VF3YCZMFC12585173</VIN>
                            <VIN2>VF3YCZMFC12585173</VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 667 300,00</price>
                            <optionprice>71 500,00</optionprice>
                            <additionalequipmentprice>1 654,03</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876664</carid>
                            <VIN>VF3DDHMY0EJ583720</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888705</carid>
                            <VIN>VF37H9HKCFJ564400</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>999 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888701</carid>
                            <VIN>VF37HNFUCFJ570426</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0C6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>913 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888713</carid>
                            <VIN>VF37H9HKCFJ562442</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>999 500,00</price>
                            <optionprice>64 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionname>
                        <optionprice>Электрически складывающиеся зеркала заднего вида с подогревом + Пакет &#039;&#039;Plus&#039;&#039; (опция доступна для з</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00873466</carid>
                            <VIN>VF3YAZMFB12571218</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IHDQ609FC80</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 607 600,00</price>
                            <optionprice>70 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93IHDQ609FC80</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876671</carid>
                            <VIN>VF3DDHMY0EJ587387</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0T6</carcolorid>
                            <carcolorname>Синий металлик Bleu Kyanos</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00813212</carid>
                            <VIN>VF3BUAFZPEZ805199</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>4008</carfamily>
                            <carmodel>4008</carmodel>
                            <carcomplectid>1PMJSUPRPH32A010</carcomplectid>
                            <carname>Peugeot 4008 Active 2 4WD  вариатор 150 (Л.С.)</carname>
                            <carcolorid>N3M0</carcolorid>
                            <carcolorname>METAL.VERNIS B</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 595 000,00</price>
                            <optionprice>6 000,00</optionprice>
                            <additionalequipmentprice>12 463,57</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>150</Power> <VolumeReal>2,00</VolumeReal> <Transmission>вариатор</Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,80</Fuel_consumption_track> <Fuel_consumption>8,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4340</Length> <Width>1768</Width> <Height>1632</Height> <Clearance>0</Clearance> <Trunk_size>416</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1470</Mass> <Equipment_code>1PMJSUPRPH32A010</Equipment_code> <equipment_name>Active</equipment_name> <Generation>4008</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>197/4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1980</FullMass> <max_trunk_size>1540</max_trunk_size> <baseoptions><option>
                        <optionId>CG35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние трехточечные ремни безопасности с двойными преднатяжителями, ограничителями усилия и регули</optionname>
                        <optionprice>Передние трехточечные ремни безопасности с двойными преднатяжителями, ограничителями усилия и регули</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Окраска кузова металлик</optionname>
                        <optionprice>Окраска кузова металлик</optionprice>
                        </option>
                        <option>
                        <optionId>БЕЗ UB01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОТСУТСТВУЕТ UB01</optionname>
                        <optionprice>ОТСУТСТВУЕТ UB01</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00859754</carid>
                            <VIN>VF3LWNFPAES241312</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>308</carfamily>
                            <carmodel>308 5D PORTES</carmodel>
                            <carcomplectid>1PT9A5FEC5D0A0B1</carcomplectid>
                            <carname>Peugeot 308 Access 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>P09G</carcolorid>
                            <carcolorname>Серый  Gris Hurricane</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 116 000,00</price>
                            <optionprice>7 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,10</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,60</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4253</Length> <Width>1804</Width> <Height>1457</Height> <Clearance>152</Clearance> <Trunk_size>420</Trunk_size> <Fuel_tank_size>53</Fuel_tank_size> <Mass>1190</Mass> <Equipment_code>1PT9A5FEC5D0A0B1</Equipment_code> <equipment_name>Access</equipment_name> <Generation>308 II</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>193</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1720</FullMass> <max_trunk_size>1228</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>NF02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>Боковые подушки безопасности</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876662</carid>
                            <VIN>VF3DDHMY0EJ549948</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883597</carid>
                            <VIN>VF37J9HP8EJ771995</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VP</carmodel>
                            <carcomplectid>1PB9AFSBFS32A026</carcomplectid>
                            <carname>Peugeot Partner Outdoor 1.6 дизель RT-6 90 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 252 000,00</price>
                            <optionprice>33 000,00</optionprice>
                            <additionalequipmentprice>17 657,31</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>минивэн</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>7</Seats> <Volume>1560</Volume> <Power>90</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,50</Top100> <Fuel_consumption_city>5,00</Fuel_consumption_city> <Fuel_consumption_track>4,40</Fuel_consumption_track> <Fuel_consumption>4,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4380</Length> <Width>1810</Width> <Height>1801</Height> <Clearance>150</Clearance> <Trunk_size>2650</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1505</Mass> <Equipment_code>1PB9AFSBFS32A026</Equipment_code> <equipment_name>Outdoor</equipment_name> <Generation>Partner Tepee II Рecтайлинг</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>230/1750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>165</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2070</FullMass> <max_trunk_size>3000</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>0MM0</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев передних сидений</optionname>
                        <optionprice>Подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>VH04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>Отделка рулевого колеса кожей</optionprice>
                        </option>
                        <option>
                        <optionId>EK01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ФАРЫ ДНЕВНОГО СВЕТА</optionname>
                        <optionprice>ФАРЫ ДНЕВНОГО СВЕТА</optionprice>
                        </option>
                        <option>
                        <optionId>HU02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Наруж. зеркала зад. вида, складывающиеся с помощью электропривода</optionname>
                        <optionprice>Наруж. зеркала зад. вида, складывающиеся с помощью электропривода</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00891266</carid>
                            <VIN>VF37LNFUCFJ590652</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BC6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>968 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875691</carid>
                            <VIN>VF3CUHMZ0EY138752</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYFCG5D0A0C0</carcomplectid>
                            <carname>Peugeot 2008 Access 1.2 MT-5 82 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>8 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,50</Top100> <Fuel_consumption_city>6,00</Fuel_consumption_city> <Fuel_consumption_track>4,30</Fuel_consumption_track> <Fuel_consumption>4,90</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1045</Mass> <Equipment_code>1PI4SYFCG5D0A0C0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>169</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1582</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>NA01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>Передние сиденья с 3-ступенчатым подогревом</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00878362</carid>
                            <VIN>Z8T4D5FEAFM000172</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>989 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00798001</carid>
                            <VIN>VF3YCZMFC12567727</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 622 800,00</price>
                            <optionprice>27 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876697</carid>
                            <VIN>VF3YCZMFC12476839</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876687</carid>
                            <VIN>VF3YCZMFC12627921</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 686 300,00</price>
                            <optionprice>90 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>TK02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>Защита до середины высоты грузового отсека</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882463</carid>
                            <VIN>Z8T4D5FS9FM001175</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882607</carid>
                            <VIN>Z8T4D5FS9FM001225</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882467</carid>
                            <VIN>Z8T4DNFP0FM001166</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>856 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00859062</carid>
                            <VIN>VF38D5FEAEL037839</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>508</carfamily>
                            <carmodel>508</carmodel>
                            <carcomplectid>1PW2A4PCLM32JGC0</carcomplectid>
                            <carname>Peugeot 508 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 789 000,00</price>
                            <optionprice>55 000,00</optionprice>
                            <additionalequipmentprice>17 407,36</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,20</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4792</Length> <Width>1853</Width> <Height>1456</Height> <Clearance>170</Clearance> <Trunk_size>545</Trunk_size> <Fuel_tank_size>72</Fuel_tank_size> <Mass>1485</Mass> <Equipment_code>1PW2A4PCLM32JGC0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>508 I Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>220</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2015</FullMass> <max_trunk_size>545</max_trunk_size> <baseoptions><option>
                        <optionId>YD01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система бесключевого запуска двигателя</optionname>
                        <optionprice>Система бесключевого запуска двигателя</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>UB07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кам. зад. вида + датчик &#034;слеп&#034; зоны+пер. датчики парк.+электропривод скл. зеркал</optionname>
                        <optionprice>Кам. зад. вида + датчик &#034;слеп&#034; зоны+пер. датчики парк.+электропривод скл. зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00820307</carid>
                            <VIN>VF3YCZMAU12591259</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER Chassis Cabine</carmodel>
                            <carcomplectid>2PU96FHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer ChCa 335 L3 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 541 800,00</price>
                            <optionprice>133 800,00</optionprice>
                            <additionalequipmentprice>5 298,82</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>шасси</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,40</Fuel_consumption_city> <Fuel_consumption_track>8,80</Fuel_consumption_track> <Fuel_consumption>7,90</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5943</Length> <Width>2050</Width> <Height>2153</Height> <Clearance>200</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1655</Mass> <Equipment_code>2PU96FHDQ609A680</Equipment_code> <equipment_name>ChCa 335 L3</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>3500</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>RX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Наружные зеркала заднего вида с боковым обзором на 2,20 м</optionname>
                        <optionprice>Наружные зеркала заднего вида с боковым обзором на 2,20 м</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>DO16</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Cttm_bort_S_113 500</optionname>
                        <optionprice>Cttm_bort_S_113 500</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876693</carid>
                            <VIN>VF3YCZMFC12478265</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876682</carid>
                            <VIN>VF3DDHMY0EJ585552</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0L8</carcolorid>
                            <carcolorname>Серо-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00806202</carid>
                            <VIN>VF3YCZMFB12589871</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 649 000,00</price>
                            <optionprice>71 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879408</carid>
                            <VIN>Z8T4D5FEAFM000412</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0ZR</carcolorid>
                            <carcolorname>Серебристый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>978 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00878698</carid>
                            <VIN>Z8T4D5FEAFM000275</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M6N9</carcolorid>
                            <carcolorname>Белый перламутр</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>994 000,00</price>
                            <optionprice>18 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PERL</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;перламутр&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00873465</carid>
                            <VIN>VF3YAZMFB12570794</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IHDQ609FC80</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 607 600,00</price>
                            <optionprice>70 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93IHDQ609FC80</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879412</carid>
                            <VIN>Z8T4D5FEAFM000519</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4WCUM32A0C1</carcomplectid>
                            <carname>Peugeot 408 Allure 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0ZR</carcolorid>
                            <carcolorname>Серебристый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 026 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Да</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4WCUM32A0C1</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00882611</carid>
                            <VIN>Z8T4D5FS9FM001066</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M0G2</carcolorid>
                            <carcolorname>Бежевый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00891273</carid>
                            <VIN>VF37L9HECFJ576308</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BCBW532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>990 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882469</carid>
                            <VIN>Z8T4DNFP0FM001181</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09V</carcolorid>
                            <carcolorname>Черный</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>856 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882459</carid>
                            <VIN>Z8T4D5FS9FM001117</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M0G2</carcolorid>
                            <carcolorname>Бежевый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876679</carid>
                            <VIN>VF3DDHMY0EJ587385</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NCM5D0A0B0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.2 MT-5 72 (Л.С.)</carname>
                            <carcolorid>M0G6</carcolorid>
                            <carcolorname>Светло-коричневый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>847 900,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>72</Power> <VolumeReal>1,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>6,90</Fuel_consumption_city> <Fuel_consumption_track>4,20</Fuel_consumption_track> <Fuel_consumption>5,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1055</Mass> <Equipment_code>1PM3A4NCM5D0A0B0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>110/3000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>160</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1459</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions><option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00888715</carid>
                            <VIN>VF37H9HKCFJ562440</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB9J0CBX532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>950 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876684</carid>
                            <VIN>VF3YCZMFB12624624</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>M04F</carcolorid>
                            <carcolorname>Голубой металлик</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 700 600,00</price>
                            <optionprice>123 100,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Цвет &#034;металлик&#034;</optionname>
                        <optionprice>Цвет &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>LL02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Двери, открывающиеся на 270 градусов</optionname>
                        <optionprice>Двери, открывающиеся на 270 градусов</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>ND02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Омыватель фар</optionname>
                        <optionprice>Омыватель фар</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882465</carid>
                            <VIN>Z8T4D5FEAFM001080</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876699</carid>
                            <VIN>VF3YEZMFC12718394</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU97MGDQ609ULC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 440 L4H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 913 000,00</price>
                            <optionprice>185 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>6363</Length> <Width>2050</Width> <Height>2522</Height> <Clearance>222</Clearance> <Trunk_size>15000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>2105</Mass> <Equipment_code>2PU97MGDQ609ULC0</Equipment_code> <equipment_name>FgTl 440 L4H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>DO25</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>НН_МТ_185</optionname>
                        <optionprice>НН_МТ_185</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00891267</carid>
                            <VIN>VF37LNFUCFJ578142</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PARTNER</carfamily>
                            <carmodel>PARTNER VU</carmodel>
                            <carcomplectid>2PB95BC6S532A027</carcomplectid>
                            <carname>PEUGEOT PARTNER VU</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>968 500,00</price>
                            <optionprice>15 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>EK01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Фары дневного света</optionname>
                        <optionprice>Фары дневного света</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>LK07</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прорезиненное покрытие в кабине</optionname>
                        <optionprice>Прорезиненное покрытие в кабине</optionprice>
                        </option>
                        <option>
                        <optionId>PX26</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionname>
                        <optionprice>сплошная перегоротка в половину высоты съемная + решетка + окно для погрузки длинномерных грузов</optionprice>
                        </option>
                        <option>
                        <optionId>GB23</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покытие пола багажного отсека</optionname>
                        <optionprice>Покытие пола багажного отсека</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882461</carid>
                            <VIN>Z8T4D5FS9FM001176</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCNL32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>927 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,80</Top100> <Fuel_consumption_city>10,10</Fuel_consumption_city> <Fuel_consumption_track>5,60</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1398</Mass> <Equipment_code>1PTKA4NCNL32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>185</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00819298</carid>
                            <VIN>VF3DDNFP0EJ660717</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>301</carfamily>
                            <carmodel>301</carmodel>
                            <carcomplectid>1PM3A4NEC5D0A0</carcomplectid>
                            <carname>Peugeot 301 Allure 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>T6M0</carcolorid>
                            <carcolorname>BLEU KYANOS</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>957 900,00</price>
                            <optionprice>39 000,00</optionprice>
                            <additionalequipmentprice>42 559,65</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,40</Top100> <Fuel_consumption_city>8,70</Fuel_consumption_city> <Fuel_consumption_track>5,10</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4442</Length> <Width>1748</Width> <Height>1466</Height> <Clearance>142</Clearance> <Trunk_size>506</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1165</Mass> <Equipment_code>1PM3A4NEC5D0A0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>301</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1524</FullMass> <max_trunk_size>506</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Покрытие кузова &#034;металлик&#034; или лак</optionname>
                        <optionprice>Покрытие кузова &#034;металлик&#034; или лак</optionprice>
                        </option>
                        <option>
                        <optionId>AB11</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Противоугонная сигнализация + 2 ключа с ДУ</optionname>
                        <optionprice>Противоугонная сигнализация + 2 ключа с ДУ</optionprice>
                        </option>
                        <option>
                        <optionId>NF01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionname>
                        <optionprice>Боковые подушки+Крепления ISOFIX ((стандарт на  Active и Allure при заказе(8HFZ или 8WFZ))</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00877752</carid>
                            <VIN>VF3CC5FS9ET137180</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов-2 Пежо</cardepartmentid>
                            <cardepartmentname>Реутов PEUGEOT</cardepartmentname>
                            <caraddress>Московская область г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>208</carfamily>
                            <carmodel>208 5 PORTES</carmodel>
                            <carcomplectid>1PIAA5LCALD0A0D0</carcomplectid>
                            <carname>Peugeot 208 Active 1.6 AT-4 120 (Л.С.)</carname>
                            <carcolorid>M0G4</carcolorid>
                            <carcolorname>Серый металлик Blossom Grey</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 049 000,00</price>
                            <optionprice>59 000,00</optionprice>
                            <additionalequipmentprice>16 834,53</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>120</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>4</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,70</Top100> <Fuel_consumption_city>9,80</Fuel_consumption_city> <Fuel_consumption_track>4,90</Fuel_consumption_track> <Fuel_consumption>6,70</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>3962</Length> <Width>1739</Width> <Height>1460</Height> <Clearance>140</Clearance> <Trunk_size>311</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1090</Mass> <Equipment_code>1PIAA5LCALD0A0D0</Equipment_code> <equipment_name>Active</equipment_name> <Generation>208</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>160/4250</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1625</FullMass> <max_trunk_size>311</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>Передние противотуманные   фары</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>Передние сиденья с 3-ступенчатым подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionname>
                        <optionprice>Задние секвентальные электрические стеклоподъемники, складываемые боковые зеркала заднего вида, секв</optionprice>
                        </option>
                        <option>
                        <optionId>WLOA</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Русифицированная навигационная система с предустановленными картами России</optionname>
                        <optionprice>Русифицированная навигационная система с предустановленными картами России</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876689</carid>
                            <VIN>VF3YCZMFC12578337</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 686 300,00</price>
                            <optionprice>90 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>TK02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита до середины высоты грузового отсека</optionname>
                        <optionprice>Защита до середины высоты грузового отсека</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PR01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>AD01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00829992</carid>
                            <VIN>VF3CU5FJ0EY091643</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Варшавка</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Варшавка (ЮГ)</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYMCA5D0A0C0</carcomplectid>
                            <carname>Peugeot 2008 Allure 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 174 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,40</Top100> <Fuel_consumption_city>4,70</Fuel_consumption_city> <Fuel_consumption_track>3,60</Fuel_consumption_track> <Fuel_consumption>4,00</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1180</Mass> <Equipment_code>1PI4SYMCA5D0A0C0</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>270/1750</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>188</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1708</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions><option>
                        <optionId>NF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionname>
                        <optionprice>Передние боковые подушки безопасности (стандарт при заказе подогрева сидений)</optionprice>
                        </option>
                        <option>
                        <optionId>WLQK</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем высокого разрешения, системой беспроводн</optionname>
                        <optionprice>Мультимедийная аудиосистема с 7&#034; цветным сенсорным дисплеем высокого разрешения, системой беспроводн</optionprice>
                        </option>
                        <option>
                        <optionId>PR01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние противотуманные   фары</optionname>
                        <optionprice>Передние противотуманные   фары</optionprice>
                        </option>
                        <option>
                        <optionId>LE05</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionname>
                        <optionprice>Задние секв-ые электрические стеклоподъемники, электропривод складывания боковых зеркал заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>WV33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Пакет &#034;Обзорность&#034;</optionname>
                        <optionprice>Пакет &#034;Обзорность&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>RG10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль и ограничитель скорости</optionname>
                        <optionprice>Круиз-контроль и ограничитель скорости</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>Передние сиденья с 3-ступенчатым подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4E</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4D</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionprice>
                        </option>
                        <option>
                        <optionId>VD09</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Темная тонировка задних стекол</optionname>
                        <optionprice>Темная тонировка задних стекол</optionprice>
                        </option>
                        <option>
                        <optionId>RE07</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двухзонный климат-контроль</optionname>
                        <optionprice>Двухзонный климат-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4C</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные колесные диски</optionname>
                        <optionprice>16&#034; легкосплавные колесные диски</optionprice>
                        </option>
                        <option>
                        <optionId>9BFX</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка сидений темной тканью Tokyo Mistral</optionname>
                        <optionprice>Отделка сидений темной тканью Tokyo Mistral</optionprice>
                        </option>
                        <option>
                        <optionId>UF01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система курсовой устойчивости (ESP)</optionname>
                        <optionprice>Система курсовой устойчивости (ESP)</optionprice>
                        </option>
                        <option>
                        <optionId>ZH4F</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionname>
                        <optionprice>«Grip Control» - интеллектуальная система распределения крутящего момента с 5 режимами работы</optionprice>
                        </option>
                        <option>
                        <optionId>NF04</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые и оконные (шторки) подушки безопасности</optionname>
                        <optionprice>Боковые и оконные (шторки) подушки безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>UB01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние датчики парковки</optionname>
                        <optionprice>Задние датчики парковки</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00882613</carid>
                            <VIN>Z8T4D5FEAFM001120</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0ZR</carcolorid>
                            <carcolorname>Серебристый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>978 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00877532</carid>
                            <VIN>VF3CUHMZ0EY045364</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>2008</carfamily>
                            <carmodel>2008</carmodel>
                            <carcomplectid>1PI4SYFCGPD0A0B0</carcomplectid>
                            <carname>Peugeot 2008 Access 1.2 RT-5 82 (Л.С.)</carname>
                            <carcolorid>M0K4</carcolorid>
                            <carcolorname>Синий</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 084 000,00</price>
                            <optionprice>25 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1199</Volume> <Power>82</Power> <VolumeReal>1,20</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>13,50</Top100> <Fuel_consumption_city>6,00</Fuel_consumption_city> <Fuel_consumption_track>4,30</Fuel_consumption_track> <Fuel_consumption>4,90</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4159</Length> <Width>1739</Width> <Height>1556</Height> <Clearance>160</Clearance> <Trunk_size>350</Trunk_size> <Fuel_tank_size>50</Fuel_tank_size> <Mass>1045</Mass> <Equipment_code>1PI4SYFCGPD0A0B0</Equipment_code> <equipment_name>Access</equipment_name> <Generation>2008</Generation> <Start_year>2014</Start_year> <Fuel>бензин</Fuel> <Moment>118/2750</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>169</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1582</FullMass> <max_trunk_size>1194</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>NA01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Передние сиденья с 3-ступенчатым подогревом</optionname>
                        <optionprice>Передние сиденья с 3-ступенчатым подогревом</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883138</carid>
                            <VIN>Z8T4D5FEAFM001284</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00806201</carid>
                            <VIN>VF3YCZMFB12580748</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>WPP0</carcolorid>
                            <carcolorname>БЕЛЫЙ БАНКИЗ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 649 000,00</price>
                            <optionprice>71 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>SE08</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Усиленная задняя подвеска (двухлистовые рессоры)</optionname>
                        <optionprice>Усиленная задняя подвеска (двухлистовые рессоры)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00873463</carid>
                            <VIN>VF3YAZMFB12571044</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IHDQ609FC80</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 607 600,00</price>
                            <optionprice>70 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5413</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>0</Clearance> <Trunk_size>11500</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1925</Mass> <Equipment_code>2PU93IHDQ609FC80</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RE01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00854526</carid>
                            <VIN>VF3YAZMFB12707696</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IGDQ609FCC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 701 800,00</price>
                            <optionprice>12 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU93IGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00854529</carid>
                            <VIN>VF3YAZMFB12707171</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU93IGDQ609FCC0</carcomplectid>
                            <carname>Peugeot Boxer FgTl 330 L2H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0WP</carcolorid>
                            <carcolorname>Белый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 701 800,00</price>
                            <optionprice>12 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>3</Seats> <Volume>2198</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,20</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>4963</Length> <Width>2050</Width> <Height>2254</Height> <Clearance>176</Clearance> <Trunk_size>8000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1860</Mass> <Equipment_code>2PU93IGDQ609FCC0</Equipment_code> <equipment_name>FgTl 330 L2H2</equipment_name> <Generation>Boxer Рестайлинг</Generation> <Start_year>2014</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>150</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>ES02</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Розетка 12V</optionname>
                        <optionprice>Розетка 12V</optionprice>
                        </option>
                        <option>
                        <optionId>FX01</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882617</carid>
                            <VIN>Z8T4DNFP0FM001063</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NEC532JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 MT-5 115 (Л.С.)</carname>
                            <carcolorid>M09P</carcolorid>
                            <carcolorname>Темно-серый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>856 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1587</Volume> <Power>115</Power> <VolumeReal>1,60</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>10,90</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>5,80</Fuel_consumption_track> <Fuel_consumption>7,10</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1427</Mass> <Equipment_code>1PTKA4NEC532JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>150/4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>189</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1800</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00890580</carid>
                            <VIN>VF3XDRHKHFZ012965</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>PEUGEOT EXPERT</carfamily>
                            <carmodel>EXPERT VP</carmodel>
                            <carcomplectid>2PG95QPBA632CG81</carcomplectid>
                            <carname>Peugeot Expert Allure 2 дизель MT-6 120 (Л.С.)</carname>
                            <carcolorid>M0ZW</carcolorid>
                            <carcolorname>Серый металлик Gris Fer</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 752 500,00</price>
                            <optionprice>108 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>минивэн</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>9</Seats> <Volume>1997</Volume> <Power>120</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>14,70</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,50</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>5135</Length> <Width>1895</Width> <Height>1942</Height> <Clearance>0</Clearance> <Trunk_size>553</Trunk_size> <Fuel_tank_size>80</Fuel_tank_size> <Mass>1919</Mass> <Equipment_code>2PG95QPBA632CG81</Equipment_code> <equipment_name>Allure</equipment_name> <Generation>Expert Tepee</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>300/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2791</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>MOMO</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034;</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>RS03</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>PC13</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Левая боковая остекленная сдвижная дверь</optionname>
                        <optionprice>Левая боковая остекленная сдвижная дверь</optionprice>
                        </option>
                        <option>
                        <optionId>NR02</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кондиционер для передних и задних пасажиров</optionname>
                        <optionprice>Кондиционер для передних и задних пасажиров</optionprice>
                        </option>
                        <option>
                        <optionId>WK27</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Боковые подушки безопасности + подогрев сидений</optionname>
                        <optionprice>Боковые подушки безопасности + подогрев сидений</optionprice>
                        </option>
                        <option>
                        <optionId>AN13</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>3-й ряд сидений (3 места)</optionname>
                        <optionprice>3-й ряд сидений (3 места)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00876695</carid>
                            <VIN>VF3YCZMFC12477001</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>BOXER</carfamily>
                            <carmodel>BOXER FOURGON TOLE</carmodel>
                            <carcomplectid>2PU95KHDQ609A680</carcomplectid>
                            <carname>Peugeot Boxer FgTl 335 L3H2 2.2 дизель MT-6 130 (Л.С.)</carname>
                            <carcolorid>P0B1</carcolorid>
                            <carcolorname>Желтый лак</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 616 800,00</price>
                            <optionprice>21 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>фургон</Body> <Drive>передний</Drive> <Doors>2</Doors> <Seats>3</Seats> <Volume>2200</Volume> <Power>130</Power> <VolumeReal>2,20</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,70</Fuel_consumption_city> <Fuel_consumption_track>7,50</Fuel_consumption_track> <Fuel_consumption>8,70</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Нет</Air_con> <Length>5998</Length> <Width>1810</Width> <Height>2524</Height> <Clearance>180</Clearance> <Trunk_size>13000</Trunk_size> <Fuel_tank_size>90</Fuel_tank_size> <Mass>1975</Mass> <Equipment_code>2PU95KHDQ609A680</Equipment_code> <equipment_name>FgTl 335 L3H2</equipment_name> <Generation>Boxer</Generation> <Start_year>2006</Start_year> <Fuel>дизель</Fuel> <Moment>320/2000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>155</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>FX01</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Защита картера двигателя</optionname>
                        <optionprice>Защита картера двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>RK04</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод зеркал заднего вида, подогрев зеркал</optionname>
                        <optionprice>Электропривод зеркал заднего вида, подогрев зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>PX05</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Перегородка цельнометаллическая сплошная</optionname>
                        <optionprice>Перегородка цельнометаллическая сплошная</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883140</carid>
                            <VIN>Z8T4D5FEAFM001267</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Пежо</cardepartmentid>
                            <cardepartmentname>Варшавка PEUGEOT</cardepartmentname>
                            <caraddress>г. Москва, 1-й Дорожный пр-д, д. 4</caraddress>
                            <carbrand>PEUGEOT</carbrand>
                            <carfamily>408</carfamily>
                            <carmodel>408</carmodel>
                            <carcomplectid>1PTKA4NCUM32JUC1</carcomplectid>
                            <carname>Peugeot 408 Style 1.6 AT-6 150 (Л.С.)</carname>
                            <carcolorid>M0G2</carcolorid>
                            <carcolorname>Бежевый</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>991 000,00</price>
                            <optionprice>13 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1598</Volume> <Power>150</Power> <VolumeReal>1,60</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>9,60</Top100> <Fuel_consumption_city>11,60</Fuel_consumption_city> <Fuel_consumption_track>6,30</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4703</Length> <Width>1815</Width> <Height>1535</Height> <Clearance>178</Clearance> <Trunk_size>560</Trunk_size> <Fuel_tank_size>60</Fuel_tank_size> <Mass>1420</Mass> <Equipment_code>1PTKA4NCUM32JUC1</Equipment_code> <equipment_name>Style</equipment_name> <Generation>408</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>240/1400</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>207</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>1820</FullMass> <max_trunk_size>560</max_trunk_size> <baseoptions></baseoptions><additionaloptions><option>
                        <optionId>OMMO</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionname>
                        <optionprice>Лакокрасочное покрытие кузова &#034;металлик&#034; или цвет &#034;Белый лак&#034;</optionprice>
                        </option>
                        </additionaloptions></auto></carsOnHand></auto-catalog>