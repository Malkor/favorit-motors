<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("IMPORT CARS ORDER NEW!!!");
?>

<?
if (CModule::IncludeModule("iblock"))

$iblock_id = 17; //авто на заказ
$result_array = array(); //результирующий массив
$i = 0;

// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):
    $auto_catalog = simplexml_load_file("komplekt_kia.php");

    // Перебираем все авто из выгрузки
    foreach ($auto_catalog->carComplects->auto as $car){
        $prop_arr = array();
        $carbrand = (string)$car->carcomplect->carbrand;
        $carmodel = (string)$car->carcomplect->carmodel;

        foreach ($car as $key => $value) {
            if(strtolower((string)$key) == "carcomplect") {
                foreach ($car->carcomplect as $carcomplect) {
                    foreach ($carcomplect as $key => $value) {
                        if(!in_array((string)$value, $result_array[$carbrand][$carmodel][(string)$key]))
                            $result_array[$carbrand][$carmodel][(string)$key][] = (string)$value;
                    }
                }
            }
            else{
                if((string)$key == 'baseoptions' || (string)$key == 'additionalavailableoptions'){
                    if((string)$key == 'baseoptions'){
                        $iblock_options_id = 14;
                        $iblock_code = "baseoptions";
                    }
                    if((string)$key == 'additionalavailableoptions'){
                        $iblock_options_id = 15;
                        $iblock_code = "additionaloptions";
                    }
                    foreach ($value->option as $option){
                        foreach ($option as $option_name => $option_value){
                            $optionid = (string)$value->option->optionId;
                            if(!in_array((string)$optionid, $result_array[$carbrand][$carmodel][(string)$key]))
                                $result_array[$carbrand][$carmodel][(string)$key][$optionid][(string)$option_name] = (string)$option_value;
                        }
                    }
                }else{
                    if (!in_array((string)$value, $result_array[$carbrand][$carmodel][(string)$key]))
                        $result_array[$carbrand][$carmodel][(string)$key][] = (string)$value;
                }
            }
        }
    }

    foreach($result_array as $carbrand_name => $carbrand_value){
        foreach($carbrand_value as $carmodel_name => $carmodel_value){
            foreach($carmodel_value as $option_name => $option_value){
                $prop_code = strtoupper((string)$option_name);
                $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                if ($ar_res = $res->GetNext()){
                    // для свойств типа "список"
                    if(($ar_res["PROPERTY_TYPE"] == "L") && ($ar_res["MULTIPLE"] == "Y")){
                        $n = 0;
                        foreach($option_value as $key => $value){
                            $prop_id = "";
                            $property_enums = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$prop_code));
                            while($enum_fields = $property_enums->GetNext())
                            {
                                if ($enum_fields["VALUE"] == (string)$value) {
                                    $prop_id = $enum_fields["ID"];
                                    break;
                                }
                            }
                            if($prop_id == ""){
                                // если значение не нашли, тогда добавляем новое значение
                                $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                                if($ar_res = $res->GetNext()){
                                    $ibpenum = new CIBlockPropertyEnum;
                                    if($PropID = $ibpenum->Add(array("IBLOCK_ID"=>$iblock_id, 'PROPERTY_ID' => $ar_res["ID"], 'VALUE' => (string)$value))){
                                        $prop_arr[$prop_code]["n".$n] = array("VALUE" => $PropID);
                                    }
                                }
                            }
                            else{
                                $prop_arr[$prop_code]["n".$n] = array("VALUE" => (string)$prop_id);
                            }
                            $n++;
                        }
                    }
                    if((($ar_res["PROPERTY_TYPE"] == "S") || ($ar_res["PROPERTY_TYPE"] == "N")) && ($ar_res["MULTIPLE"] == "Y")){
                        $n = 0;
                        foreach ($option_value as $key => $value){
                            $prop_arr[$prop_code]["n".$n] = array("VALUE" => $value);
                            $n++;
                        }
                    }
                    if (($ar_res["PROPERTY_TYPE"] == "E") && ($ar_res["MULTIPLE"] == "Y")){
                        if((string)$option_name == 'baseoptions'){
                            $iblock_options_id = 14;
                            $iblock_code = "baseoptions";
                        }
                        if((string)$option_name == 'additionalavailableoptions'){
                            $iblock_options_id = 15;
                            $iblock_code = "additionaloptions";
                        }
                        $n = 0;
                        foreach ($option_value as $key => $value){
                            $optionid = (string)$value["optionId"];
                            $if_exist_option = CIBlockElement::GetList(array(), array("IBLOCK_CODE"=>$iblock_code, "NAME"=>$optionid), false, array("nTopCount"=>1), array("ID", "NAME"));
                            if($ar_res = $if_exist_option->GetNext())
                            {
                                $prop_arr[$prop_code]["n".$n] = array("VALUE" => $ar_res["ID"]);
                                $n++;
                            }
                            else{
                                $option_props = "";
                                foreach ($value as $pname => $pvalue) {
                                    $option_props[(string)strtoupper($pname)] = (string)$pvalue;
                                }
                                $el = new CIBlockElement;
                                $arLoadProductArray = Array(
                                    "MODIFIED_BY"    => $USER->GetID(),   // элемент изменен текущим пользователем
                                    "IBLOCK_SECTION_ID" => false,                 // элемент лежит в корне раздела
                                    "IBLOCK_ID"      => $iblock_options_id,
                                    "PROPERTY_VALUES"=> $option_props,
                                    "NAME"           => $optionid,
                                    "ACTIVE"         => "Y"                       // активен
                                );
                                if($PRODUCT_ID = $el->Add($arLoadProductArray))
                                {
                                    echo "New property ID: ".$PRODUCT_ID;
                                }
                                else
                                {
                                    echo "Error: ".$el->LAST_ERROR;
                                }
                            }
                        }
                    }
                }
            }
            $if_exist_element = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "PROPERTY_CARBRAND_VALUE"=>$carbrand_name, "PROPERTY_CARMODEL_VALUE"=>$carmodel_name), false, array("nTopCount"=>1), array("ID", "NAME", "IBLOCK_ID"));

            // Проверяем есть ли в каталоге авто
            if($ob = $if_exist_element->GetNextElement())
            {
                // Если нашли авто значит будем вносить изменения в уже существующую запись
                // $arFields = $ob->GetFields();
                // print_r($arFields);
            }
            else{
                // создаем новый элемент
                $el = new CIBlockElement;

                $correct_carmodel_name = preg_replace('/'.$carbrand.' /', '', $carmodel_name);
                $name = $carbrand_name." ".$correct_carmodel_name;

                $arLoadProductArray = Array(
                    "MODIFIED_BY"    => $USER->GetID(),       // элемент изменен текущим пользователем
                    "IBLOCK_SECTION_ID" => false,             // элемент лежит в корне раздела
                    "IBLOCK_ID"      => $iblock_id,
                    "PROPERTY_VALUES"=> $prop_arr,
                    "NAME"           => $name,
                    "ACTIVE"         => "Y"                   // активен
                );

                if($PRODUCT_ID = $el->Add($arLoadProductArray))
                {
                    echo "New ID: ".$PRODUCT_ID;
                }
                else
                {
                    echo "Error: ".$el->LAST_ERROR;
                }
            }
        }

//        if($i == 50)
//            break;
//
//        $i++;
    }

endif;
?>

<pre><?//print_r($result_array)?></pre>

<pre><?//print_r($prop_arr)?></pre>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>