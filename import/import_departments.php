<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Import Departments");?>

<?$iblock_id = 27; // департаменты ?>

<?
// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):

$departments = simplexml_load_file("departments.php");

// Перебираем все департаменты из выгрузки
foreach ($departments->department as $department) {
    $name = (string)$department->name;
    $id = (string)$department->id;
    $markid = (string)$department->markid;
    $prop_arr = array();

    $if_exist_element = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "PROPERTY_ID"=>$id, "PROPERTY_MARKID"=>$markid), false, array("nTopCount"=>1), array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*"));
    
    // Проверяем есть ли уже в инфоблоке департамент по id
    if($ob = $if_exist_element->GetNextElement())  
    {
        // Если нашли, значит будем вносить изменения в уже существующую запись
         $arFields = $ob->GetFields();
         print_r($arFields);
    }
    // Если не найден департамент, тогда добавляем новый элемент в инфоблок
    else
    {
        foreach ($department as $prop_code => $prop_value){
            $prop_arr[strtoupper($prop_code)] = (string)$prop_value;
        }
        
        $el = new CIBlockElement;

        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(),       // элемент изменен текущим пользователем
          "IBLOCK_SECTION_ID" => false,             // элемент лежит в корне раздела
          "IBLOCK_ID"      => $iblock_id,
          "PROPERTY_VALUES"=> $prop_arr,
          "NAME"           => $name,
          "ACTIVE"         => "Y"                   // активен
          );

        if($PRODUCT_ID = $el->Add($arLoadProductArray))
        {
            echo "New Department ID: ".$PRODUCT_ID;
        }
        else
        {
          echo "Error: ".$el->LAST_ERROR;
        }
    }  
}

?>

<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>