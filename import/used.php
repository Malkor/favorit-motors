<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">
	<NewDataSet xmlns="">
	<auto-catalog diffgr:id="auto-catalog1" msdata:rowOrder="0" msdata:hiddenauto-catalog_Id="0">
		<creation-date>11.05.2015 12:30:59 GMT +3</creation-date>
		<host>wwww.ldf.ru</host>
			<carsBY diffgr:id="carsBY1" msdata:rowOrder="0" msdata:hiddencarsBY_Id="0" msdata:hiddenauto-catalog_Id="0">
				<auto diffgr:id="auto1" msdata:rowOrder="0" msdata:hiddenauto_Id="0" msdata:hiddencarsBY_Id="0">
					<carId>00779337</carId>
					<VIN>JACUBS25DP7100766</VIN>
					<vehicletype>Легковой</vehicletype>
					<carstate>БУ</carstate>
					<state/>
					<caryear>1993</caryear>
					<ptsstate>original</ptsstate>
					<cardepartmentid>Коптево Skoda</cardepartmentid>
					<cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
					<caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
					<car-phone>+7 (495) 786-25-25</car-phone>
					<car-info/>
					<carbrand>Opel</carbrand>
					<carfamily/>
					<carmodel>Monterey A</carmodel>
					<carcolorname>синий</carcolorname>
					<carcolortype/>
					<price>150000</price>
					<additionalequipmentprice>0</additionalequipmentprice>
					<owners>1</owners>
					<body>Внедорожник</body>
					<drive>Полный</drive>
					<transmission>Механика</transmission>
					<transmission_speed/>
					<top100/>
					<speed/>
					<salontype/>
					<fuel>Бензин инжектор</fuel>
					<fuel_type/>
					<airbags/>
					<steering-wheel>Левый</steering-wheel>
					<power>177</power>
					<run-metric>КМ</run-metric>
					<run>200000</run>
					<displacement>3165</displacement>
					<custom-house-state>Растаможен</custom-house-state>
					<stock>в наличии</stock>
					<type>commercial</type>
					<valid-thru-date>18.05.2015</valid-thru-date>
					<vin>JACUBS25DP7100766</vin>
					<equipments diffgr:id="equipments1" msdata:rowOrder="0" msdata:hiddenequipments_Id="0" msdata:hiddenauto_Id="0">
						<equipment diffgr:id="equipment1" msdata:rowOrder="0" msdata:hiddenequipments_Id="0">ABS</equipment>
						<equipment diffgr:id="equipment2" msdata:rowOrder="1" msdata:hiddenequipments_Id="0">Электро-привод зеркал</equipment>
						<equipment diffgr:id="equipment3" msdata:rowOrder="2" msdata:hiddenequipments_Id="0">Легкосплавные диски</equipment>
						<equipment diffgr:id="equipment4" msdata:rowOrder="3" msdata:hiddenequipments_Id="0">Сигнализация</equipment>
						<equipment diffgr:id="equipment5" msdata:rowOrder="4" msdata:hiddenequipments_Id="0">Обогрев зеркал</equipment>
					</equipments>
<pic diffgr:id="pic1" msdata:rowOrder="0" msdata:hiddenpic_Id="0" msdata:hiddenauto_Id="0">
					<image diffgr:id="image1" msdata:rowOrder="0" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415825.jpg</image>
					<image diffgr:id="image2" msdata:rowOrder="1" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415816.jpg</image>
					<image diffgr:id="image3" msdata:rowOrder="2" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415828.jpg</image>
					<image diffgr:id="image4" msdata:rowOrder="3" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415852.jpg</image>
					<image diffgr:id="image5" msdata:rowOrder="4" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415813.jpg</image>
					<image diffgr:id="image6" msdata:rowOrder="5" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415822.jpg</image>
					<image diffgr:id="image7" msdata:rowOrder="6" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415832.jpg</image>
					<image diffgr:id="image8" msdata:rowOrder="7" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415842.jpg</image>
					<image diffgr:id="image9" msdata:rowOrder="8" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415838.jpg</image>
					<image diffgr:id="image10" msdata:rowOrder="9" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415819.jpg</image>
					<image diffgr:id="image11" msdata:rowOrder="10" msdata:hiddenauto_Id="0">http://www.ldf.ru/cars_img/51953829/1415846.jpg</image>
</pic>
				</auto>
			</carsBY>
		</auto-catalog>
	</NewDataSet>
</diffgr:diffgram>