<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Import Used");
?>

<?$iblock_id = 28; // БУ авто ?>

<?
// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):

$used = array(
  // 0 => "used_atc_13.php",
  // 1 => "used_atc_24.php",
    2 => "used_koptevo_1.php",
   3 => "used_koptevo_15.php",
   4 => "used_koptevo_19.php",
   5 => "used_koptevo_20.php",
  6 => "used_koptevo_21.php",
  7 => "used_koptevo_22.php",
  8 => "used_leninskiy_10.php",
  9 => "used_lubercy_4.php",
  10 => "used_reutov_5.php",
  11 => "used_reutov_12.php",
  12 => "used_reutov_14.php",
  13 => "used_semenovskaya_16.php",
  //14 => "used_semenovskaya_17.php",
   15 => "used_varshavka_3.php",
  16 => "used_varshavka_6.php",
  17 => "used_varshavka_8.php",
  18 => "used_varshavka_23.php"
);

foreach ($used as $num => $file) {

    $auto_catalog = simplexml_load_file($file);

    // Перебираем все авто из выгрузки
    foreach ($auto_catalog->NewDataSet->{'auto-catalog'}->carsBY->auto as $auto) {
        $vin = (string)$auto->vin;
        $carname = (string)$auto->carbrand."".(string)$auto->carmodel;
        $prop_arr = array();

        $if_exist_element = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "PROPERTY_VIN"=>$vin), false, array("nTopCount"=>1), array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*"));
        
        // Проверяем есть ли в каталоге авто по VIN
        if($ob = $if_exist_element->GetNextElement())  
        {
            // Если нашли авто с нужным VIN значит будем вносить изменения в уже существующую запись
            // $arFields = $ob->GetFields();

            // $i = 0;
            // foreach ($auto->equipments->equipment as $equipment){
            //   $prop_arr["EQUIPMENTS"]["n".$i] = array("VALUE" => (string)$equipment);
            //   $i++;
            // }

            // $el = new CIBlockElement;
            // $arLoadProductArray = Array(
            // "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
            // "IBLOCK_SECTION" => false,          // элемент лежит в корне раздела
            // "PROPERTY_VALUES"=> $prop_arr,
            // "NAME"           => $arFields["NAME"],
            // "ACTIVE"         => "Y",            // активен
            // );

            // //print_r($prop_arr);
            
            // if($res = $el->Update($arFields["ID"], $arLoadProductArray))
            // {
            //   echo "Updated Element ID: ".$arFields["ID"];
            // }
            // else
            // {
            //   echo "Error: ".$el->LAST_ERROR;
            // }
        }

        // Если не найдено авто с таким VIN добавляем новый элемент в каталог
        else{
            if($vin != "")
            {
              foreach ($auto as $key => $value){
                  $prop_code = strtoupper((string)$key);
                  $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                  if ($ar_res = $res->GetNext()){

                    if ($ar_res["PROPERTY_TYPE"] == "L") 
                    {
                        $prop_id = "";
                        //print_r("Находим нужное для нас свойство и ищем в нем текущее значение");
                        $property_enums = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$prop_code));
                        while($enum_fields = $property_enums->GetNext())
                        {
                            if ($enum_fields["VALUE"] == (string)$value) {
                                $prop_id = $enum_fields["ID"];
                                break;
                            }
                        }
                        
                        if($prop_id == ""){
                            //print_r("Если значение не нашли, тогда добавляем новое значение");
                            $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                            if($ar_res = $res->GetNext()){
                                $ibpenum = new CIBlockPropertyEnum;
                                if($PropID = $ibpenum->Add(array("IBLOCK_ID"=>$iblock_id, 'PROPERTY_ID' => $ar_res["ID"], 'VALUE' => (string)$value))){
                                    $prop_arr[$prop_code] = array("VALUE" => $PropID);
                                }
                            }
                        }
                        else{
                            $prop_arr[$prop_code] = $prop_id;
                        }
                      }
                      if (($ar_res["PROPERTY_TYPE"] == "S") || ($ar_res["PROPERTY_TYPE"] == "N")) {
                          if((string)$key == 'pic'){
                              $i = 0;
                              foreach ($value->image as $image){
                                  $prop_arr[$prop_code]["n".$i] = array("VALUE" => (string)$image);
                                  $i++;
                              }
                          }else{
                            if((string)$key == 'equipments'){
                               foreach ($value->equipment as $equipment){
                                  $prop_arr["EQUIPMENTS"]["n".$i] = array("VALUE" => (string)$equipment);
                                  $i++;
                                }
                            }
                            else{
                                $prop_arr[$prop_code] = (string)$value;
                            }
                          }
                      }
                    }
                }

                $el = new CIBlockElement;

                $arLoadProductArray = Array(
                  "MODIFIED_BY"    => $USER->GetID(),       // элемент изменен текущим пользователем
                  "IBLOCK_SECTION_ID" => false,             // элемент лежит в корне раздела
                  "IBLOCK_ID"      => $iblock_id,
                  "PROPERTY_VALUES"=> $prop_arr,
                  "NAME"           => $carname,
                  "ACTIVE"         => "Y"                   // активен
                  );

                if($PRODUCT_ID = $el->Add($arLoadProductArray))
                {
                    echo "New ID: ".$PRODUCT_ID;
                }
                else
                {
                  echo "Error: ".$el->LAST_ERROR;
                }

            }else{
                echo "No VIN in file ".$file;
            }
        }  
    }

}

?>


<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>