﻿<?xml version="1.0"?><auto-catalog><carsOnHand><auto>
                            <car>
                            <carid>00805370</carid>
                            <VIN>RUMA0A1SSE0023092</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 дизель MT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 102 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>7,50</Fuel_consumption_city> <Fuel_consumption_track>5,10</Fuel_consumption_track> <Fuel_consumption>6,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1666</Mass> <Equipment_code>C3R2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions><option>
                        <optionId>A66</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бортовой компьютер</optionname>
                        <optionprice>Бортовой компьютер</optionprice>
                        </option>
                        <option>
                        <optionId>A9</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>HSA (Система помощи при трогании на подъеме)</optionname>
                        <optionprice>HSA (Система помощи при трогании на подъеме)</optionprice>
                        </option>
                        <option>
                        <optionId>A69</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>Обогрев заднего стекла</optionprice>
                        </option>
                        <option>
                        <optionId>A71</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>A47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка поясничного упора сиденья водителя</optionname>
                        <optionprice>Регулировка поясничного упора сиденья водителя</optionprice>
                        </option>
                        <option>
                        <optionId>A6</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ESP (Электронная система стабилизации)</optionname>
                        <optionprice>ESP (Электронная система стабилизации)</optionprice>
                        </option>
                        <option>
                        <optionId>A7</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>BAS (Система экстренного торможения)</optionname>
                        <optionprice>BAS (Система экстренного торможения)</optionprice>
                        </option>
                        <option>
                        <optionId>A50</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionname>
                        <optionprice>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionprice>
                        </option>
                        <option>
                        <optionId>A1</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>6-ступенчатая механическая КП</optionname>
                        <optionprice>6-ступенчатая механическая КП</optionprice>
                        </option>
                        <option>
                        <optionId>A85</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Шторка багажного отделения</optionname>
                        <optionprice>Шторка багажного отделения</optionprice>
                        </option>
                        <option>
                        <optionId>A81</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Разъёмы AUX и USB</optionname>
                        <optionprice>Разъёмы AUX и USB</optionprice>
                        </option>
                        <option>
                        <optionId>A23</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка рулевой колонки по углу наклона</optionname>
                        <optionprice>Регулировка рулевой колонки по углу наклона</optionprice>
                        </option>
                        <option>
                        <optionId>A62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>A78</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиоподготовка (6 динамиков, антенна)</optionname>
                        <optionprice>Аудиоподготовка (6 динамиков, антенна)</optionprice>
                        </option>
                        <option>
                        <optionId>A40</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>Климат-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>A26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionname>
                        <optionprice>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionprice>
                        </option>
                        <option>
                        <optionId>A61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Пепельница</optionname>
                        <optionprice>Пепельница</optionprice>
                        </option>
                        <option>
                        <optionId>A30</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные диски, шины 215/65R16</optionname>
                        <optionprice>16&#034; легкосплавные диски, шины 215/65R16</optionprice>
                        </option>
                        <option>
                        <optionId>A64</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>A43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Центральный замок с дистанционным управлением</optionname>
                        <optionprice>Центральный замок с дистанционным управлением</optionprice>
                        </option>
                        <option>
                        <optionId>A10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушки безопасности водителя и переднего пассажира</optionname>
                        <optionprice>Подушки безопасности водителя и переднего пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>A33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>A24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Дверные ручки, окрашенные в цвет кузова</optionname>
                        <optionprice>Дверные ручки, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A59</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionname>
                        <optionprice>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>A25</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionname>
                        <optionprice>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionprice>
                        </option>
                        <option>
                        <optionId>A8</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ARP (Система предотвращения опрокидывания автомобиля)</optionname>
                        <optionprice>ARP (Система предотвращения опрокидывания автомобиля)</optionprice>
                        </option>
                        <option>
                        <optionId>A67</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>Датчики парковки сзади</optionprice>
                        </option>
                        <option>
                        <optionId>A35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        <option>
                        <optionId>A16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionname>
                        <optionprice>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionprice>
                        </option>
                        <option>
                        <optionId>A46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>A70</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев зоны стеклоочистителей</optionname>
                        <optionprice>Обогрев зоны стеклоочистителей</optionprice>
                        </option>
                        <option>
                        <optionId>A22</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Багажные рейлинги на крыше</optionname>
                        <optionprice>Багажные рейлинги на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>A49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья с регулировкой угла наклона спинки</optionname>
                        <optionprice>Задние сиденья с регулировкой угла наклона спинки</optionprice>
                        </option>
                        <option>
                        <optionId>A83</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод всех стеклоподъемников</optionname>
                        <optionprice>Электропривод всех стеклоподъемников</optionprice>
                        </option>
                        <option>
                        <optionId>A55</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Вставки в отделке салона &#034;под дерево&#034;</optionname>
                        <optionprice>Вставки в отделке салона &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>A74</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка стекол</optionname>
                        <optionprice>Легкая тонировка стекол</optionprice>
                        </option>
                        <option>
                        <optionId>A41</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A19</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>A60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Лампы освещения салона</optionname>
                        <optionprice>Лампы освещения салона</optionprice>
                        </option>
                        <option>
                        <optionId>A68</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>A86</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Сетка для крепления багажа</optionname>
                        <optionprice>Сетка для крепления багажа</optionprice>
                        </option>
                        <option>
                        <optionId>A11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности переднего пассажира с функцией деактивации</optionname>
                        <optionprice>Подушка безопасности переднего пассажира с функцией деактивации</optionprice>
                        </option>
                        <option>
                        <optionId>A14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Активные передние подголовники</optionname>
                        <optionprice>Активные передние подголовники</optionprice>
                        </option>
                        <option>
                        <optionId>A5</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionprice>
                        </option>
                        <option>
                        <optionId>A15</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>A20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>Охранная сигнализация</optionprice>
                        </option>
                        <option>
                        <optionId>A17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Три 3-точечных задних ремня безопасности</optionname>
                        <optionprice>Три 3-точечных задних ремня безопасности</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00788672</carid>
                            <VIN>RUMS0A16SE0011032</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G2A05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 AT-6 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>952 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>97 310,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code>K3G2A05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment></Moment> <climat_ctrl>Да</climat_ctrl> <Speed>0</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00871237</carid>
                            <VIN>RUMA0A18SE0028174</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875539</carid>
                            <VIN>RUMS0A16SF0018785</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875735</carid>
                            <VIN>RUMS0A16SE0017775</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875749</carid>
                            <VIN>RUMA0A18SF0032418</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G4M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 062 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,90</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875733</carid>
                            <VIN>RUMS0A16SE0017594</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875747</carid>
                            <VIN>RUMA0A1SSF0032400</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 дизель AT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 102 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>7,30</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1686</Mass> <Equipment_code>C3R2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875737</carid>
                            <VIN>RUMS0A16SE0017634</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875537</carid>
                            <VIN>RUMA0A18SF0031995</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00819780</carid>
                            <VIN>RUMA0A1SSE0027177</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 дизель MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 052 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>28 000,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>7,50</Fuel_consumption_city> <Fuel_consumption_track>5,10</Fuel_consumption_track> <Fuel_consumption>6,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1666</Mass> <Equipment_code>C3R2M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions><option>
                        <optionId>A15</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>A14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Активные передние подголовники</optionname>
                        <optionprice>Активные передние подголовники</optionprice>
                        </option>
                        <option>
                        <optionId>A49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья с регулировкой угла наклона спинки</optionname>
                        <optionprice>Задние сиденья с регулировкой угла наклона спинки</optionprice>
                        </option>
                        <option>
                        <optionId>A66</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бортовой компьютер</optionname>
                        <optionprice>Бортовой компьютер</optionprice>
                        </option>
                        <option>
                        <optionId>A1</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>6-ступенчатая механическая КП</optionname>
                        <optionprice>6-ступенчатая механическая КП</optionprice>
                        </option>
                        <option>
                        <optionId>A39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>A41</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        <option>
                        <optionId>A70</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев зоны стеклоочистителей</optionname>
                        <optionprice>Обогрев зоны стеклоочистителей</optionprice>
                        </option>
                        <option>
                        <optionId>A23</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A78</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиоподготовка (6 динамиков, антенна)</optionname>
                        <optionprice>Аудиоподготовка (6 динамиков, антенна)</optionprice>
                        </option>
                        <option>
                        <optionId>A46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>A55</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Вставки в отделке салона &#034;под дерево&#034;</optionname>
                        <optionprice>Вставки в отделке салона &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>A59</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionname>
                        <optionprice>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>A20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>Охранная сигнализация</optionprice>
                        </option>
                        <option>
                        <optionId>A64</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>A17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Три 3-точечных задних ремня безопасности</optionname>
                        <optionprice>Три 3-точечных задних ремня безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>A61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Пепельница</optionname>
                        <optionprice>Пепельница</optionprice>
                        </option>
                        <option>
                        <optionId>A69</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>Обогрев заднего стекла</optionprice>
                        </option>
                        <option>
                        <optionId>A30</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные диски, шины 215/65R16</optionname>
                        <optionprice>16&#034; легкосплавные диски, шины 215/65R16</optionprice>
                        </option>
                        <option>
                        <optionId>A26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionname>
                        <optionprice>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionprice>
                        </option>
                        <option>
                        <optionId>A18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Центральный замок с дистанционным управлением</optionname>
                        <optionprice>Центральный замок с дистанционным управлением</optionprice>
                        </option>
                        <option>
                        <optionId>A43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>A81</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Разъёмы AUX и USB</optionname>
                        <optionprice>Разъёмы AUX и USB</optionprice>
                        </option>
                        <option>
                        <optionId>A24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Дверные ручки, окрашенные в цвет кузова</optionname>
                        <optionprice>Дверные ручки, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>A10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушки безопасности водителя и переднего пассажира</optionname>
                        <optionprice>Подушки безопасности водителя и переднего пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>A5</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionprice>
                        </option>
                        <option>
                        <optionId>A50</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionname>
                        <optionprice>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionprice>
                        </option>
                        <option>
                        <optionId>A25</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionname>
                        <optionprice>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionprice>
                        </option>
                        <option>
                        <optionId>A60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Лампы освещения салона</optionname>
                        <optionprice>Лампы освещения салона</optionprice>
                        </option>
                        <option>
                        <optionId>A19</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>A37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка рулевой колонки по углу наклона</optionname>
                        <optionprice>Регулировка рулевой колонки по углу наклона</optionprice>
                        </option>
                        <option>
                        <optionId>A74</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка стекол</optionname>
                        <optionprice>Легкая тонировка стекол</optionprice>
                        </option>
                        <option>
                        <optionId>A16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionname>
                        <optionprice>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionprice>
                        </option>
                        <option>
                        <optionId>A71</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>A47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка поясничного упора сиденья водителя</optionname>
                        <optionprice>Регулировка поясничного упора сиденья водителя</optionprice>
                        </option>
                        <option>
                        <optionId>A68</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>A83</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод всех стеклоподъемников</optionname>
                        <optionprice>Электропривод всех стеклоподъемников</optionprice>
                        </option>
                        <option>
                        <optionId>A11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности переднего пассажира с функцией деактивации</optionname>
                        <optionprice>Подушка безопасности переднего пассажира с функцией деактивации</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00891022</carid>
                            <VIN>RUMA0A18SF0032181</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G4M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 049 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,90</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875712</carid>
                            <VIN>RUMA0A18SF0031666</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>992 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875552</carid>
                            <VIN>RUMS0A16SF0018545</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875542</carid>
                            <VIN>RUMA0A18SF0030865</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875718</carid>
                            <VIN>RUMS0A16SE0017718</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>25 599,63</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00863819</carid>
                            <VIN>RUMS0A16SF0018862</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G2M02</carcomplectid>
                            <carname>SsangYong Kyron Welcome 2.3 MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>962 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>задний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code>K3G2M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00871239</carid>
                            <VIN>RUMS0A16SE0016033</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00869960</carid>
                            <VIN>RUMA0A18SE0028611</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 029 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>103 400,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00856467</carid>
                            <VIN>RUMS0A16SF0018827</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G2M02</carcomplectid>
                            <carname>SsangYong Kyron Welcome 2.3 MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>962 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>задний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code>K3G2M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00871244</carid>
                            <VIN>RUMS0A16SF0019368</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G2M02</carcomplectid>
                            <carname>SsangYong Kyron Welcome 2.3 MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>962 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>задний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code>K3G2M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00813971</carid>
                            <VIN>RUMA0A18SE0026038</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>RAK</carcolorid>
                            <carcolorname>Красный</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>992 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>52 202,26</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions><option>
                        <optionId>A71</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>A74</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка стекол</optionname>
                        <optionprice>Легкая тонировка стекол</optionprice>
                        </option>
                        <option>
                        <optionId>A81</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Разъёмы AUX и USB</optionname>
                        <optionprice>Разъёмы AUX и USB</optionprice>
                        </option>
                        <option>
                        <optionId>A66</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бортовой компьютер</optionname>
                        <optionprice>Бортовой компьютер</optionprice>
                        </option>
                        <option>
                        <optionId>A19</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>A35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        <option>
                        <optionId>A68</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>A41</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Охранная сигнализация</optionname>
                        <optionprice>Охранная сигнализация</optionprice>
                        </option>
                        <option>
                        <optionId>A60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Лампы освещения салона</optionname>
                        <optionprice>Лампы освещения салона</optionprice>
                        </option>
                        <option>
                        <optionId>A14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Активные передние подголовники</optionname>
                        <optionprice>Активные передние подголовники</optionprice>
                        </option>
                        <option>
                        <optionId>A16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionname>
                        <optionprice>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира с преднатяжителя</optionprice>
                        </option>
                        <option>
                        <optionId>A1</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>6-ступенчатая механическая КП</optionname>
                        <optionprice>6-ступенчатая механическая КП</optionprice>
                        </option>
                        <option>
                        <optionId>A11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности переднего пассажира с функцией деактивации</optionname>
                        <optionprice>Подушка безопасности переднего пассажира с функцией деактивации</optionprice>
                        </option>
                        <option>
                        <optionId>A69</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев заднего стекла</optionname>
                        <optionprice>Обогрев заднего стекла</optionprice>
                        </option>
                        <option>
                        <optionId>A62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>A59</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionname>
                        <optionprice>Солнцезащитные козырьки с подсветкой интегрированных аксессуарных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>A70</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Обогрев зоны стеклоочистителей</optionname>
                        <optionprice>Обогрев зоны стеклоочистителей</optionprice>
                        </option>
                        <option>
                        <optionId>A24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Дверные ручки, окрашенные в цвет кузова</optionname>
                        <optionprice>Дверные ручки, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>A78</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиоподготовка (6 динамиков, антенна)</optionname>
                        <optionprice>Аудиоподготовка (6 динамиков, антенна)</optionprice>
                        </option>
                        <option>
                        <optionId>A55</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Вставки в отделке салона &#034;под дерево&#034;</optionname>
                        <optionprice>Вставки в отделке салона &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>A49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья с регулировкой угла наклона спинки</optionname>
                        <optionprice>Задние сиденья с регулировкой угла наклона спинки</optionprice>
                        </option>
                        <option>
                        <optionId>A15</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>A25</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionname>
                        <optionprice>Зеркала заднего вида, окрашенные в цвет кузова со светодиодными боковыми повторителями указателя пов</optionprice>
                        </option>
                        <option>
                        <optionId>A26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionname>
                        <optionprice>Зеркала заднего вида с подсветкой пространства при выходе/ входе</optionprice>
                        </option>
                        <option>
                        <optionId>A37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка рулевой колонки по углу наклона</optionname>
                        <optionprice>Регулировка рулевой колонки по углу наклона</optionprice>
                        </option>
                        <option>
                        <optionId>A47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка поясничного упора сиденья водителя</optionname>
                        <optionprice>Регулировка поясничного упора сиденья водителя</optionprice>
                        </option>
                        <option>
                        <optionId>A64</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>A10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушки безопасности водителя и переднего пассажира</optionname>
                        <optionprice>Подушки безопасности водителя и переднего пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>A83</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод всех стеклоподъемников</optionname>
                        <optionprice>Электропривод всех стеклоподъемников</optionprice>
                        </option>
                        <option>
                        <optionId>A46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>A5</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система) + EBD (Система распределения тормозных усилий)</optionprice>
                        </option>
                        <option>
                        <optionId>A17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Три 3-точечных задних ремня безопасности</optionname>
                        <optionprice>Три 3-точечных задних ремня безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>A43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>A18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Центральный замок с дистанционным управлением</optionname>
                        <optionprice>Центральный замок с дистанционным управлением</optionprice>
                        </option>
                        <option>
                        <optionId>A61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Пепельница</optionname>
                        <optionprice>Пепельница</optionprice>
                        </option>
                        <option>
                        <optionId>A30</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные диски, шины 215/65R16</optionname>
                        <optionprice>16&#034; легкосплавные диски, шины 215/65R16</optionprice>
                        </option>
                        <option>
                        <optionId>A33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>A23</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>A50</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionname>
                        <optionprice>Задние сиденья, складывающиеся по частям в соотношении 60/40</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875723</carid>
                            <VIN>RUMA0A18SF0031792</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875716</carid>
                            <VIN>RUMS0A16SE0017609</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00856465</carid>
                            <VIN>RUMS0A16SF0018828</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G2M02</carcomplectid>
                            <carname>SsangYong Kyron Welcome 2.3 MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>962 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>задний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code>K3G2M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875761</carid>
                            <VIN>RUMS0A16SF0018550</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>478 462,38</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875554</carid>
                            <VIN>RUMS0A16SF0018508</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00870002</carid>
                            <VIN>RUMA0A1SSE0029590</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R4M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 дизель 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 122 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>7,90</Fuel_consumption_city> <Fuel_consumption_track>5,50</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1747</Mass> <Equipment_code>C3R4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2260</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875546</carid>
                            <VIN>RUMA0A1SSF0032397</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 дизель AT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 089 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>7,30</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1686</Mass> <Equipment_code>C3R2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875544</carid>
                            <VIN>RUMA0A1SSF0032359</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 дизель AT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 102 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,40</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>7,30</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1686</Mass> <Equipment_code>C3R2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00762546</carid>
                            <VIN>KPTG0B1TSDP326786</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>REXTON</carfamily>
                            <carmodel>REXTON</carmodel>
                            <carcomplectid>R20YA01</carcomplectid>
                            <carname>SSANG YONG REXTON</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 642 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>R2</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionname>
                        <optionprice>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionprice>
                        </option>
                        <option>
                        <optionId>R9</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система)</optionprice>
                        </option>
                        <option>
                        <optionId>R11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности водителя и пассажира</optionname>
                        <optionprice>Подушка безопасности водителя и пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>R12</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>Боковые подушки безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>R13</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>R14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Энергопоглощающая рулевая колонка</optionname>
                        <optionprice>Энергопоглощающая рулевая колонка</optionprice>
                        </option>
                        <option>
                        <optionId>R65</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Сетка для крепления багажа</optionname>
                        <optionprice>Сетка для крепления багажа</optionprice>
                        </option>
                        <option>
                        <optionId>R66</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защитная  сетка между салоном и багажным отделением</optionname>
                        <optionprice>Защитная  сетка между салоном и багажным отделением</optionprice>
                        </option>
                        <option>
                        <optionId>R48</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод складывания наружных зеркал</optionname>
                        <optionprice>Электропривод складывания наружных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>R49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Повторители указателей поворота в наружных зеркалах</optionname>
                        <optionprice>Повторители указателей поворота в наружных зеркалах</optionprice>
                        </option>
                        <option>
                        <optionId>R60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кнопки переключения передач на руле</optionname>
                        <optionprice>Кнопки переключения передач на руле</optionprice>
                        </option>
                        <option>
                        <optionId>R61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionname>
                        <optionprice>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionprice>
                        </option>
                        <option>
                        <optionId>R62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Многофункциональная потолочная консоль</optionname>
                        <optionprice>Многофункциональная потолочная консоль</optionprice>
                        </option>
                        <option>
                        <optionId>R64</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Шторка багажного отделения</optionname>
                        <optionprice>Шторка багажного отделения</optionprice>
                        </option>
                        <option>
                        <optionId>R39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>Отделка рулевого колеса кожей</optionprice>
                        </option>
                        <option>
                        <optionId>R42</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Высококонтрастная панель приборов</optionname>
                        <optionprice>Высококонтрастная панель приборов</optionprice>
                        </option>
                        <option>
                        <optionId>R43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Коврики салонные напольные впереди с сзади</optionname>
                        <optionprice>Коврики салонные напольные впереди с сзади</optionprice>
                        </option>
                        <option>
                        <optionId>R45</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>R46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>R47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>R27</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>Климат-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>R30</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R32</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>R36</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подогрев задних сидений</optionname>
                        <optionprice>Подогрев задних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R38</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Декоративные вставки в салоне &#034;под металл&#034;</optionname>
                        <optionprice>Декоративные вставки в салоне &#034;под металл&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Багажные рейлинги на крыше</optionname>
                        <optionprice>Багажные рейлинги на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>R17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>R18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>R22</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>R23</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные диски, шины 235/75R16</optionname>
                        <optionprice>16&#034; легкосплавные диски, шины 235/75R16</optionprice>
                        </option>
                        <option>
                        <optionId>R26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875736</carid>
                            <VIN>RUMS0A16SE0017802</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00869945</carid>
                            <VIN>RUMA0A18SE0026476</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M02</carcomplectid>
                            <carname>SsangYong Actyon Welcome 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>962 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>102 490,37</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00869964</carid>
                            <VIN>RUMA0A18SE0028483</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875538</carid>
                            <VIN>RUMA0A18SF0030697</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 092 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>8 484,59</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875734</carid>
                            <VIN>RUMS0A16SE0017637</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875748</carid>
                            <VIN>RUMA0A18SF0032411</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G4M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 062 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,90</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875717</carid>
                            <VIN>RUMA0A18SF0032341</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 029 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00869959</carid>
                            <VIN>RUMA0A18SE0028440</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 029 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875543</carid>
                            <VIN>RUMA0A18SF0030885</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875768</carid>
                            <VIN>RUMA0A18SF0031731</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>992 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00786594</carid>
                            <VIN>KPTG0B1TSEP331935</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Семеновская SY</cardepartmentid>
                            <cardepartmentname>ЗАКРЫТ Семеновская SSANGYONG</cardepartmentname>
                            <caraddress>г. Москва, ул. Б. Семеновская, д. 42</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>REXTON</carfamily>
                            <carmodel>REXTON</carmodel>
                            <carcomplectid>R20YM01</carcomplectid>
                            <carname>SsangYong Rexton Original 2 дизель MT-6 155 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 592 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>задний</Drive> <Doors>5</Doors> <Seats>7</Seats> <Volume>1998</Volume> <Power>155</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>13,40</Top100> <Fuel_consumption_city>8,90</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,20</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4755</Length> <Width>1900</Width> <Height>1840</Height> <Clearance>206</Clearance> <Trunk_size>678</Trunk_size> <Fuel_tank_size>78</Fuel_tank_size> <Mass>1901</Mass> <Equipment_code>R20YM01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Rexton III</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>360/2800?–?1500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>173</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2760</FullMass> <max_trunk_size>678</max_trunk_size> <baseoptions><option>
                        <optionId>R1</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>6-ступенчатая механическая КП</optionname>
                        <optionprice>6-ступенчатая механическая КП</optionprice>
                        </option>
                        <option>
                        <optionId>R2</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionname>
                        <optionprice>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionprice>
                        </option>
                        <option>
                        <optionId>R3</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система подключаемого полного привода (Part Time)</optionname>
                        <optionprice>Система подключаемого полного привода (Part Time)</optionprice>
                        </option>
                        <option>
                        <optionId>R4</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система автоматически подключаемого полного привода (TOD)</optionname>
                        <optionprice>Система автоматически подключаемого полного привода (TOD)</optionprice>
                        </option>
                        <option>
                        <optionId>R5</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система постоянного полного привода (AWD)</optionname>
                        <optionprice>Система постоянного полного привода (AWD)</optionprice>
                        </option>
                        <option>
                        <optionId>R6</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Многорычажная задняя подвеска</optionname>
                        <optionprice>Многорычажная задняя подвеска</optionprice>
                        </option>
                        <option>
                        <optionId>R64</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Шторка багажного отделения</optionname>
                        <optionprice>Шторка багажного отделения</optionprice>
                        </option>
                        <option>
                        <optionId>R65</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Сетка для крепления багажа</optionname>
                        <optionprice>Сетка для крепления багажа</optionprice>
                        </option>
                        <option>
                        <optionId>R66</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защитная  сетка между салоном и багажным отделением</optionname>
                        <optionprice>Защитная  сетка между салоном и багажным отделением</optionprice>
                        </option>
                        <option>
                        <optionId>R58</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на руле</optionname>
                        <optionprice>Управление аудиосистемой на руле</optionprice>
                        </option>
                        <option>
                        <optionId>R59</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Bluetooth</optionname>
                        <optionprice>Bluetooth</optionprice>
                        </option>
                        <option>
                        <optionId>R60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кнопки переключения передач на руле</optionname>
                        <optionprice>Кнопки переключения передач на руле</optionprice>
                        </option>
                        <option>
                        <optionId>R61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionname>
                        <optionprice>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionprice>
                        </option>
                        <option>
                        <optionId>R62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Многофункциональная потолочная консоль</optionname>
                        <optionprice>Многофункциональная потолочная консоль</optionprice>
                        </option>
                        <option>
                        <optionId>R63</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Люк с электроприводом</optionname>
                        <optionprice>Люк с электроприводом</optionprice>
                        </option>
                        <option>
                        <optionId>R52</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>Датчики парковки сзади</optionprice>
                        </option>
                        <option>
                        <optionId>R53</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчики парковки спереди</optionname>
                        <optionprice>Датчики парковки спереди</optionprice>
                        </option>
                        <option>
                        <optionId>R54</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка стекол передних дверей</optionname>
                        <optionprice>Легкая тонировка стекол передних дверей</optionprice>
                        </option>
                        <option>
                        <optionId>R55</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка и дополнительная звукоизоляция лобового стекла</optionname>
                        <optionprice>Легкая тонировка и дополнительная звукоизоляция лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>R56</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Глубокая тонировка стекол задних дверей и стекла пятой двери</optionname>
                        <optionprice>Глубокая тонировка стекол задних дверей и стекла пятой двери</optionprice>
                        </option>
                        <option>
                        <optionId>R57</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиосистема формата 2DIN с CD/MP3 -проигрывателем и USB</optionname>
                        <optionprice>Аудиосистема формата 2DIN с CD/MP3 -проигрывателем и USB</optionprice>
                        </option>
                        <option>
                        <optionId>R46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>R47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>R48</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод складывания наружных зеркал</optionname>
                        <optionprice>Электропривод складывания наружных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>R49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Повторители указателей поворота в наружных зеркалах</optionname>
                        <optionprice>Повторители указателей поворота в наружных зеркалах</optionprice>
                        </option>
                        <option>
                        <optionId>R50</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Самозатемняющееся зеркало заднего вида</optionname>
                        <optionprice>Самозатемняющееся зеркало заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>R51</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>R40</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей со вставками &#034;под дерево&#034;</optionname>
                        <optionprice>Отделка рулевого колеса кожей со вставками &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R41</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Декоративные вставки в салоне &#034;под дерево&#034;</optionname>
                        <optionprice>Декоративные вставки в салоне &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R42</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Высококонтрастная панель приборов</optionname>
                        <optionprice>Высококонтрастная панель приборов</optionprice>
                        </option>
                        <option>
                        <optionId>R43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Коврики салонные напольные впереди с сзади</optionname>
                        <optionprice>Коврики салонные напольные впереди с сзади</optionprice>
                        </option>
                        <option>
                        <optionId>R44</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Автоматическая система контроля света</optionname>
                        <optionprice>Автоматическая система контроля света</optionprice>
                        </option>
                        <option>
                        <optionId>R45</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>R34</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод пассажирского сиденья</optionname>
                        <optionprice>Электропривод пассажирского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>R35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Память настроек положения сиденья водителя и зеркал заднего вида</optionname>
                        <optionprice>Память настроек положения сиденья водителя и зеркал заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>R36</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подогрев задних сидений</optionname>
                        <optionprice>Подогрев задних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R38</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Декоративные вставки в салоне &#034;под металл&#034;</optionname>
                        <optionprice>Декоративные вставки в салоне &#034;под металл&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>Отделка рулевого колеса кожей</optionprice>
                        </option>
                        <option>
                        <optionId>R28</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер для 3-го ряда сидений с регулировкой скорости воздуха (регулировка температуры общая с к</optionname>
                        <optionprice>Кондиционер для 3-го ряда сидений с регулировкой скорости воздуха (регулировка температуры общая с к</optionprice>
                        </option>
                        <option>
                        <optionId>R29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>R30</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R31</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кожаная обивка сидений</optionname>
                        <optionprice>Кожаная обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R32</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>R33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья с функцией &#034;Easy Access&#034;</optionname>
                        <optionprice>Электропривод водительского сиденья с функцией &#034;Easy Access&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R21</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Спойлер с дополнительным светодиодным стоп-сигналом</optionname>
                        <optionprice>Спойлер с дополнительным светодиодным стоп-сигналом</optionprice>
                        </option>
                        <option>
                        <optionId>R22</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>R23</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; легкосплавные диски, шины 235/75R16</optionname>
                        <optionprice>16&#034; легкосплавные диски, шины 235/75R16</optionprice>
                        </option>
                        <option>
                        <optionId>R24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>18&#034; легкосплавные диски (Diamond Cutting), шины 255/60R18</optionname>
                        <optionprice>18&#034; легкосплавные диски (Diamond Cutting), шины 255/60R18</optionprice>
                        </option>
                        <option>
                        <optionId>R26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        <option>
                        <optionId>R27</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>Климат-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>R14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Энергопоглощающая рулевая колонка</optionname>
                        <optionprice>Энергопоглощающая рулевая колонка</optionprice>
                        </option>
                        <option>
                        <optionId>R16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Багажные рейлинги на крыше</optionname>
                        <optionprice>Багажные рейлинги на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>R17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>R18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>R19</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подножки</optionname>
                        <optionprice>Боковые подножки</optionprice>
                        </option>
                        <option>
                        <optionId>R20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Открывающееся стекло задней двери</optionname>
                        <optionprice>Открывающееся стекло задней двери</optionprice>
                        </option>
                        <option>
                        <optionId>R7</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-й ряд сидений</optionname>
                        <optionprice>3-й ряд сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R9</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система)</optionprice>
                        </option>
                        <option>
                        <optionId>R10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ESP &#038; ARP(Электронная система стабилизации и Система предотвращения опрокидывания автомобиля)</optionname>
                        <optionprice>ESP &#038; ARP(Электронная система стабилизации и Система предотвращения опрокидывания автомобиля)</optionprice>
                        </option>
                        <option>
                        <optionId>R11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности водителя и пассажира</optionname>
                        <optionprice>Подушка безопасности водителя и пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>R12</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>Боковые подушки безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>R13</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875551</carid>
                            <VIN>RUMS0A16SF0018777</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875722</carid>
                            <VIN>RUMA0A18SF0030874</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>ACQ</carcolorid>
                            <carcolorname>Карбоново-серый</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 042 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875541</carid>
                            <VIN>RUMA0A18SE0029372</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>992 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>6 000,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,20</Fuel_consumption_track> <Fuel_consumption>7,50</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G2M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875770</carid>
                            <VIN>RUMA0A18SF0032287</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 029 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00805471</carid>
                            <VIN>KPACA1ETSEP200476</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON SPORTS</carfamily>
                            <carmodel>ACTYON SPORTS</carmodel>
                            <carcomplectid>Q3D4M02</carcomplectid>
                            <carname>SsangYong Actyon Sports Welcome 2 дизель 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>ACM</carcolorid>
                            <carcolorname>СЕРЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 289 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>34 204,54</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>пикап</Body> <Drive>полный</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>15,00</Top100> <Fuel_consumption_city>9,70</Fuel_consumption_city> <Fuel_consumption_track>6,00</Fuel_consumption_track> <Fuel_consumption>7,40</Fuel_consumption> <Airbags>1</Airbags> <Air_con>Да</Air_con> <Length>4990</Length> <Width>1910</Width> <Height>1790</Height> <Clearance>188</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1982</Mass> <Equipment_code>Q3D4M02</Equipment_code> <equipment_name>Welcome</equipment_name> <Generation>Actyon Sports II</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>360/1500?–?2800</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>164</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>2740</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>AS53</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Защитное покрытие грузового отсека</optionname>
                        <optionprice>Защитное покрытие грузового отсека</optionprice>
                        </option>
                        <option>
                        <optionId>AS2</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>6-ступенчатая механическая КП</optionname>
                        <optionprice>6-ступенчатая механическая КП</optionprice>
                        </option>
                        <option>
                        <optionId>AS14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>Ручки дверей, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>AS15</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>AS42</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>AS4</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система подключаемого полного привода (Part Time)</optionname>
                        <optionprice>Система подключаемого полного привода (Part Time)</optionprice>
                        </option>
                        <option>
                        <optionId>AS35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Коврики салонные напольные впереди с сзади</optionname>
                        <optionprice>Коврики салонные напольные впереди с сзади</optionprice>
                        </option>
                        <option>
                        <optionId>AS21</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Полноразмерное запасное колесо</optionname>
                        <optionprice>Полноразмерное запасное колесо</optionprice>
                        </option>
                        <option>
                        <optionId>AS8</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности водителя</optionname>
                        <optionprice>Подушка безопасности водителя</optionprice>
                        </option>
                        <option>
                        <optionId>AS43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>AS24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бортовой компьютер</optionname>
                        <optionprice>Бортовой компьютер</optionprice>
                        </option>
                        <option>
                        <optionId>AS22</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер</optionname>
                        <optionprice>Кондиционер</optionprice>
                        </option>
                        <option>
                        <optionId>AS48</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиоподготовка (4 динамика, антенна)</optionname>
                        <optionprice>Аудиоподготовка (4 динамика, антенна)</optionprice>
                        </option>
                        <option>
                        <optionId>AS27</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>AS29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>AS38</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Центральный замок с дистанционным управлением</optionname>
                        <optionprice>Центральный замок с дистанционным управлением</optionprice>
                        </option>
                        <option>
                        <optionId>AS10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Энергопоглощающая рулевая колонка</optionname>
                        <optionprice>Энергопоглощающая рулевая колонка</optionprice>
                        </option>
                        <option>
                        <optionId>AS39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Сигнализация</optionname>
                        <optionprice>Сигнализация</optionprice>
                        </option>
                        <option>
                        <optionId>AS11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира</optionname>
                        <optionprice>Регулируемые по высоте 3-точечные ремни безопасности водителя и переднего пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>AS16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>16&#034; диски, шины 225/75 R16</optionname>
                        <optionprice>16&#034; диски, шины 225/75 R16</optionprice>
                        </option>
                        <option>
                        <optionId>AS25</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Тканевая обивка сидений</optionname>
                        <optionprice>Тканевая обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>AS13</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида, окрашенные в цвет кузова</optionname>
                        <optionprice>Зеркала заднего вида, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>AS37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Пепельница</optionname>
                        <optionprice>Пепельница</optionprice>
                        </option>
                        <option>
                        <optionId>AS36</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Прикуриватель</optionname>
                        <optionprice>Прикуриватель</optionprice>
                        </option>
                        <option>
                        <optionId>AS20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля</optionname>
                        <optionprice>Гидроусилитель руля</optionprice>
                        </option>
                        <option>
                        <optionId>AS40</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>AS52</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрические стеклоподъемники всех дверей</optionname>
                        <optionprice>Электрические стеклоподъемники всех дверей</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875715</carid>
                            <VIN>RUMS0A16SE0017582</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M01</carcomplectid>
                            <carname>SsangYong Kyron Original 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>ABS</carcolorid>
                            <carcolorname>СИНЕ-СЕРЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 022 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875545</carid>
                            <VIN>RUMA0A18SF0032422</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G4M01</carcomplectid>
                            <carname>SsangYong Actyon Original 2 4WD  MT-6 149 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 062 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,90</Fuel_consumption_track> <Fuel_consumption>8,20</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Да</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1612</Mass> <Equipment_code>C3G4M01</Equipment_code> <equipment_name>Original</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875772</carid>
                            <VIN>RUMA0A18SF0032405</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 079 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00870018</carid>
                            <VIN>RUMA0A1SSE0028825</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3R2M06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 дизель MT-6 149 (Л.С.)</carname>
                            <carcolorid>WAA</carcolorid>
                            <carcolorname>БЕЛЫЙ</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 089 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1998</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>7,50</Fuel_consumption_city> <Fuel_consumption_track>5,10</Fuel_consumption_track> <Fuel_consumption>6,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1666</Mass> <Equipment_code>C3R2M06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>360/2000?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>174</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2180</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00762926</carid>
                            <VIN>KPTG0B1TSDP326709</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2013</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>REXTON</carfamily>
                            <carmodel>REXTON</carmodel>
                            <carcomplectid>R20YT48</carcomplectid>
                            <carname>SsangYong Rexton Luxury Family 2 дизель 4WD  AT-5 155 (Л.С.)</carname>
                            <carcolorid>SAI</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>2 092 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>151 045,33</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>7</Seats> <Volume>1998</Volume> <Power>155</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>14,00</Top100> <Fuel_consumption_city>9,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>7,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4755</Length> <Width>1900</Width> <Height>1840</Height> <Clearance>206</Clearance> <Trunk_size>678</Trunk_size> <Fuel_tank_size>78</Fuel_tank_size> <Mass>1985</Mass> <Equipment_code>R20YT48</Equipment_code> <equipment_name>Luxury Family</equipment_name> <Generation>Rexton III</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>360/2800?–?1500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>175</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2760</FullMass> <max_trunk_size>678</max_trunk_size> <baseoptions><option>
                        <optionId>R2</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionname>
                        <optionprice>5-ступенчатая автоматическая КП T-Tronic с возможностью ручного переключения передач</optionprice>
                        </option>
                        <option>
                        <optionId>R3</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система подключаемого полного привода (Part Time)</optionname>
                        <optionprice>Система подключаемого полного привода (Part Time)</optionprice>
                        </option>
                        <option>
                        <optionId>R7</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-й ряд сидений</optionname>
                        <optionprice>3-й ряд сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R9</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ABS (Антиблокировочная тормозная система)</optionname>
                        <optionprice>ABS (Антиблокировочная тормозная система)</optionprice>
                        </option>
                        <option>
                        <optionId>R10</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ESP &#038; ARP(Электронная система стабилизации и Система предотвращения опрокидывания автомобиля)</optionname>
                        <optionprice>ESP &#038; ARP(Электронная система стабилизации и Система предотвращения опрокидывания автомобиля)</optionprice>
                        </option>
                        <option>
                        <optionId>R11</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подушка безопасности водителя и пассажира</optionname>
                        <optionprice>Подушка безопасности водителя и пассажира</optionprice>
                        </option>
                        <option>
                        <optionId>R60</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кнопки переключения передач на руле</optionname>
                        <optionprice>Кнопки переключения передач на руле</optionprice>
                        </option>
                        <option>
                        <optionId>R61</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionname>
                        <optionprice>Электрические стеклоподъемники всех дверей с авторежимом двери водителя</optionprice>
                        </option>
                        <option>
                        <optionId>R62</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Многофункциональная потолочная консоль</optionname>
                        <optionprice>Многофункциональная потолочная консоль</optionprice>
                        </option>
                        <option>
                        <optionId>R63</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Люк с электроприводом</optionname>
                        <optionprice>Люк с электроприводом</optionprice>
                        </option>
                        <option>
                        <optionId>R54</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка стекол передних дверей</optionname>
                        <optionprice>Легкая тонировка стекол передних дверей</optionprice>
                        </option>
                        <option>
                        <optionId>R55</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкая тонировка и дополнительная звукоизоляция лобового стекла</optionname>
                        <optionprice>Легкая тонировка и дополнительная звукоизоляция лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>R56</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Глубокая тонировка стекол задних дверей и стекла пятой двери</optionname>
                        <optionprice>Глубокая тонировка стекол задних дверей и стекла пятой двери</optionprice>
                        </option>
                        <option>
                        <optionId>R57</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудиосистема формата 2DIN с CD/MP3 -проигрывателем и USB</optionname>
                        <optionprice>Аудиосистема формата 2DIN с CD/MP3 -проигрывателем и USB</optionprice>
                        </option>
                        <option>
                        <optionId>R58</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на руле</optionname>
                        <optionprice>Управление аудиосистемой на руле</optionprice>
                        </option>
                        <option>
                        <optionId>R59</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Bluetooth</optionname>
                        <optionprice>Bluetooth</optionprice>
                        </option>
                        <option>
                        <optionId>R48</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод складывания наружных зеркал</optionname>
                        <optionprice>Электропривод складывания наружных зеркал</optionprice>
                        </option>
                        <option>
                        <optionId>R49</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Повторители указателей поворота в наружных зеркалах</optionname>
                        <optionprice>Повторители указателей поворота в наружных зеркалах</optionprice>
                        </option>
                        <option>
                        <optionId>R50</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Самозатемняющееся зеркало заднего вида</optionname>
                        <optionprice>Самозатемняющееся зеркало заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>R51</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>R52</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчики парковки сзади</optionname>
                        <optionprice>Датчики парковки сзади</optionprice>
                        </option>
                        <option>
                        <optionId>R53</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Датчики парковки спереди</optionname>
                        <optionprice>Датчики парковки спереди</optionprice>
                        </option>
                        <option>
                        <optionId>R42</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Высококонтрастная панель приборов</optionname>
                        <optionprice>Высококонтрастная панель приборов</optionprice>
                        </option>
                        <option>
                        <optionId>R43</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Коврики салонные напольные впереди с сзади</optionname>
                        <optionprice>Коврики салонные напольные впереди с сзади</optionprice>
                        </option>
                        <option>
                        <optionId>R44</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Автоматическая система контроля света</optionname>
                        <optionprice>Автоматическая система контроля света</optionprice>
                        </option>
                        <option>
                        <optionId>R45</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Корректор фар</optionname>
                        <optionprice>Корректор фар</optionprice>
                        </option>
                        <option>
                        <optionId>R46</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Иммобилайзер</optionname>
                        <optionprice>Иммобилайзер</optionprice>
                        </option>
                        <option>
                        <optionId>R47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Наружные зеркала с электроприводом регулировок и подогревом</optionname>
                        <optionprice>Наружные зеркала с электроприводом регулировок и подогревом</optionprice>
                        </option>
                        <option>
                        <optionId>R34</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод пассажирского сиденья</optionname>
                        <optionprice>Электропривод пассажирского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>R35</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Память настроек положения сиденья водителя и зеркал заднего вида</optionname>
                        <optionprice>Память настроек положения сиденья водителя и зеркал заднего вида</optionprice>
                        </option>
                        <option>
                        <optionId>R36</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулируемый подогрев передних сидений</optionname>
                        <optionprice>Регулируемый подогрев передних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R37</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Подогрев задних сидений</optionname>
                        <optionprice>Подогрев задних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R40</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей со вставками &#034;под дерево&#034;</optionname>
                        <optionprice>Отделка рулевого колеса кожей со вставками &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R41</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Декоративные вставки в салоне &#034;под дерево&#034;</optionname>
                        <optionprice>Декоративные вставки в салоне &#034;под дерево&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R27</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Климат-контроль</optionname>
                        <optionprice>Климат-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>R28</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кондиционер для 3-го ряда сидений с регулировкой скорости воздуха (регулировка температуры общая с к</optionname>
                        <optionprice>Кондиционер для 3-го ряда сидений с регулировкой скорости воздуха (регулировка температуры общая с к</optionprice>
                        </option>
                        <option>
                        <optionId>R29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз-контроль</optionname>
                        <optionprice>Круиз-контроль</optionprice>
                        </option>
                        <option>
                        <optionId>R31</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Кожаная обивка сидений</optionname>
                        <optionprice>Кожаная обивка сидений</optionprice>
                        </option>
                        <option>
                        <optionId>R32</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Регулировка сиденья водителя по высоте</optionname>
                        <optionprice>Регулировка сиденья водителя по высоте</optionprice>
                        </option>
                        <option>
                        <optionId>R33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья с функцией &#034;Easy Access&#034;</optionname>
                        <optionprice>Электропривод водительского сиденья с функцией &#034;Easy Access&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>R19</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подножки</optionname>
                        <optionprice>Боковые подножки</optionprice>
                        </option>
                        <option>
                        <optionId>R20</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Открывающееся стекло задней двери</optionname>
                        <optionprice>Открывающееся стекло задней двери</optionprice>
                        </option>
                        <option>
                        <optionId>R21</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Спойлер с дополнительным светодиодным стоп-сигналом</optionname>
                        <optionprice>Спойлер с дополнительным светодиодным стоп-сигналом</optionprice>
                        </option>
                        <option>
                        <optionId>R22</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Противотуманные фары</optionname>
                        <optionprice>Противотуманные фары</optionprice>
                        </option>
                        <option>
                        <optionId>R24</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>18&#034; легкосплавные диски (Diamond Cutting), шины 255/60R18</optionname>
                        <optionprice>18&#034; легкосплавные диски (Diamond Cutting), шины 255/60R18</optionprice>
                        </option>
                        <option>
                        <optionId>R26</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Гидроусилитель руля с переменным усилием</optionname>
                        <optionprice>Гидроусилитель руля с переменным усилием</optionprice>
                        </option>
                        <option>
                        <optionId>R12</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Боковые подушки безопасности</optionname>
                        <optionprice>Боковые подушки безопасности</optionprice>
                        </option>
                        <option>
                        <optionId>R13</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Крепление для детских кресел ISOFIX</optionname>
                        <optionprice>Крепление для детских кресел ISOFIX</optionprice>
                        </option>
                        <option>
                        <optionId>R14</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Энергопоглощающая рулевая колонка</optionname>
                        <optionprice>Энергопоглощающая рулевая колонка</optionprice>
                        </option>
                        <option>
                        <optionId>R16</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Багажные рейлинги на крыше</optionname>
                        <optionprice>Багажные рейлинги на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>R17</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Бамперы, окрашенные в цвет кузова</optionname>
                        <optionprice>Бамперы, окрашенные в цвет кузова</optionprice>
                        </option>
                        <option>
                        <optionId>R18</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionname>
                        <optionprice>Зеркала заднего вида и ручки дверей, окрашенные в цвет кузова</optionprice>
                        </option>
                        </baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875555</carid>
                            <VIN>RUMS0A16SF0018813</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Реутов</cardepartmentid>
                            <cardepartmentname>Реутов CITROEN SSANGYONG</cardepartmentname>
                            <caraddress>г. Реутов, 2 км МКАД</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>KYRON</carfamily>
                            <carmodel>KYRON</carmodel>
                            <carcomplectid>K3G4M05</carcomplectid>
                            <carname>SsangYong Kyron Comfort 2.3 4WD  MT-5 150 (Л.С.)</carname>
                            <carcolorid>SAF</carcolorid>
                            <carcolorname>СЕРЕБРИСТЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 082 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>внедорожник</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2295</Volume> <Power>150</Power> <VolumeReal>2,30</VolumeReal> <Transmission>механика</Transmission> <Transmission_speeds>5</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>14,20</Top100> <Fuel_consumption_city>14,90</Fuel_consumption_city> <Fuel_consumption_track>8,70</Fuel_consumption_track> <Fuel_consumption>11,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4660</Length> <Width>1880</Width> <Height>1755</Height> <Clearance>199</Clearance> <Trunk_size>625</Trunk_size> <Fuel_tank_size>75</Fuel_tank_size> <Mass>1862</Mass> <Equipment_code>K3G4M05</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Kyron I Рестайлинг</Generation> <Start_year>2007</Start_year> <Fuel>бензин</Fuel> <Moment>214/3500?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>167</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2530</FullMass> <max_trunk_size>625</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto><auto>
                            <car>
                            <carid>00875713</carid>
                            <VIN>RUMA0A18SF0032124</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2014</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Пежо</cardepartmentid>
                            <cardepartmentname>Коптево PEUGEOT SSANGYONG CITROEN</cardepartmentname>
                            <caraddress>125239, г. Москва, Коптевская ул., д. 69А, стр. 5</caraddress>
                            <carbrand>SSANG YONG</carbrand>
                            <carfamily>ACTYON</carfamily>
                            <carmodel>ACTYON 210</carmodel>
                            <carcomplectid>C3G2A06</carcomplectid>
                            <carname>SsangYong Actyon Comfort 2 AT-6 149 (Л.С.)</carname>
                            <carcolorid>LAK</carcolorid>
                            <carcolorname>ЧЕРНЫЙ МЕТАЛЛИК</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 092 990,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2000</Volume> <Power>149</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>10,30</Fuel_consumption_city> <Fuel_consumption_track>6,60</Fuel_consumption_track> <Fuel_consumption>8,00</Fuel_consumption> <Airbags>2</Airbags> <Air_con>Нет</Air_con> <Length>4410</Length> <Width>1830</Width> <Height>1675</Height> <Clearance>180</Clearance> <Trunk_size>486</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1620</Mass> <Equipment_code>C3G2A06</Equipment_code> <equipment_name>Comfort</equipment_name> <Generation>Actyon II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>197/3500?–?4500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>163</Speed> <abs>Да</abs> <Engine></Engine> <FullMass>2100</FullMass> <max_trunk_size>486</max_trunk_size> <baseoptions></baseoptions><additionaloptions></additionaloptions></auto></carsOnHand></auto-catalog>