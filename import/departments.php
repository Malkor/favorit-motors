﻿<?xml version="1.0" encoding="utf-8" ?>
<departments>
	<department>
		<id>13</id>
		<name>АТЦ Москва</name>
		<markid>SKODA</markid>
	</department>
	<department>
		<id>24</id>
		<name>АТЦ Москва</name>
		<markid>CITROEN</markid>
	</department>
	<department>
		<id>16</id>
		<name>Восток (Б. Семеновская ул.)</name>
		<markid>KIA</markid>
	</department>
	<department>
		<id>17</id>
		<name>Восток (Б. Семеновская ул.)</name>
		<markid>CHEVROLET</markid>
	</department>
	<department>
		<id>17</id>
		<name>Восток (Б. Семеновская ул.)</name>
		<markid>OPEL</markid>
	</department>
	<department>
		<id>5</id>
		<name>МКАД (г. Реутов)</name>
		<markid>CITROEN</markid>
	</department>
	<department>
		<id>14</id>
		<name>МКАД (г. Реутов)</name>
		<markid>PEUGEOT</markid>
	</department>
	<department>
		<id>5</id>
		<name>МКАД (г. Реутов)</name>
		<markid>SSANGYONG</markid>
	</department>
	<department>
		<id>12</id>
		<name>МКАД (г. Реутов)</name>
		<markid>KIA</markid>
	</department>
	<department><id>19</id><name>Север (Коптевская ул.)</name><markid>OPEL</markid></department><department><id>21</id><name>Север (Коптевская ул.)</name><markid>FORD</markid></department><department><id>22</id><name>Север (Коптевская ул.)</name><markid>VOLVO</markid></department><department><id>15</id><name>Север (Коптевская ул.)</name><markid>SSANGYONG</markid></department><department><id>15</id><name>Север (Коптевская ул.)</name><markid>SKODA</markid></department><department><id>1</id><name>Север (Коптевская ул.)</name><markid>KIA</markid></department><department><id>20</id><name>Север (Коптевская ул.)</name><markid>PEUGEOT</markid></department><department><id>19</id><name>Север (Коптевская ул.)</name><markid>CHEVROLET</markid></department><department><id>20</id><name>Север (Коптевская ул.)</name><markid>CITROEN</markid></department><department><id>23</id><name>Юг (1-й Дорожный пр-д)</name><markid>KIA</markid></department><department><id>6</id><name>Юг (1-й Дорожный пр-д)</name><markid>SKODA</markid></department><department><id>3</id><name>Юг (1-й Дорожный пр-д)</name><markid>SSANGYONG</markid></department><department><id>8</id><name>Юг (1-й Дорожный пр-д)</name><markid>PEUGEOT</markid></department><department><id>6</id><name>Юг (1-й Дорожный пр-д)</name><markid>SEAT</markid></department><department><id>4</id><name>Юго-Восток (г. Люберцы)</name><markid>KIA</markid></department><department><id>10</id><name>Юго-Запад (Ленинский пр-т)</name><markid>KIA</markid></department>
</departments>