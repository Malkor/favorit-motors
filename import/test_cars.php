<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Test Cars");

header('Content-type: text/html; charset=utf-8');

?>

<div style="width:900px; margin:0 auto;">
<br /><br />
<form action="test_cars.php" method="get">
  <input type="radio" name="catalog_type" value="stock" <?if($_REQUEST["catalog_type"] == "stock"):?>checked<?endif;?>>В наличии</input><br />
    <!--<input type="radio" name="catalog_type" value="used">Б/У</input><br /> -->
  <input type="radio" name="catalog_type" value="order" <?if($_REQUEST["catalog_type"] == "order"):?>checked<?endif;?>>На заказ</input><br />
  <br /><br />
  <select name="carbrand">
    <option value="kia" <?if($_REQUEST["carbrand"] == "kia"):?>selected<?endif;?>>KIA</option>
    <option value="opel" <?if($_REQUEST["carbrand"] == "opel"):?>selected<?endif;?>>OPEL</option>
    <option value="ford" <?if($_REQUEST["carbrand"] == "ford"):?>selected<?endif;?>>FORD</option>
    <option value="skoda" <?if($_REQUEST["carbrand"] == "skoda"):?>selected<?endif;?>>SKODA</option>
    <option value="volvo" <?if($_REQUEST["carbrand"] == "volvo"):?>selected<?endif;?>>VOLVO</option>
    <option value="peugeot" <?if($_REQUEST["carbrand"] == "peugeot"):?>selected<?endif;?>>PEUGEOT</option>
    <option value="citroen" <?if($_REQUEST["carbrand"] == "citroen"):?>selected<?endif;?>>CITROEN</option>
    <option value="chevrolet" <?if($_REQUEST["carbrand"] == "chevrolet"):?>selected<?endif;?>>CHEVROLET</option>
    <option value="ssangyong" <?if($_REQUEST["carbrand"] == "ssangyong"):?>selected<?endif;?>>SSANGYONG</option>
    <option value="seat" <?if($_REQUEST["carbrand"] == "seat"):?>selected<?endif;?>>SEAT</option>
  </select>
    <br /><br />
  <button type="submit" value="TEST">TEST</button>
</form>

<?
if($_REQUEST["carbrand"] != ""):
    $carbrand = "";

    if($_REQUEST["catalog_type"] == "order"):
        if($_REQUEST["carbrand"] == "kia")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_kia.php";
        
        if($_REQUEST["carbrand"] == "opel")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_opel.php";

        if($_REQUEST["carbrand"] == "ford")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_ford.php";

        if($_REQUEST["carbrand"] == "skoda")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_skoda.php";

        if($_REQUEST["carbrand"] == "volvo")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_volvo.php";

        if($_REQUEST["carbrand"] == "peugeot")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_peugeot.php";
        
        if($_REQUEST["carbrand"] == "citroen")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_citroen.php";

        if($_REQUEST["carbrand"] == "chevrolet")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_chevrolet.php";
        
        if($_REQUEST["carbrand"] == "ssangyong")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_ssangyong.php";
        
        if($_REQUEST["carbrand"] == "seat")
            $carbrand = "http://www.favorit-motors.ru/srv/komplekt_seat.php";
    endif;

    if($_REQUEST["catalog_type"] == "stock"):
        if($_REQUEST["carbrand"] == "kia")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_kia.php";
        
        if($_REQUEST["carbrand"] == "opel")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_opel.php";

        if($_REQUEST["carbrand"] == "ford")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_ford.php";

        if($_REQUEST["carbrand"] == "skoda")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_skoda.php";

        if($_REQUEST["carbrand"] == "volvo")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_volvo.php";

        if($_REQUEST["carbrand"] == "peugeot")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_peugeot.php";
        
        if($_REQUEST["carbrand"] == "citroen")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_citroen.php";

        if($_REQUEST["carbrand"] == "chevrolet")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_chevrolet.php";
        
        if($_REQUEST["carbrand"] == "ssangyong")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_ssangyong.php";
        
        if($_REQUEST["carbrand"] == "seat")
            $carbrand = "http://www.favorit-motors.ru/srv/instock_seat.php";
    endif;
    ?>

    <?
    $xml = file_get_contents($carbrand);
    $xml = str_replace('windows-1251', 'UTF-8', (string)$xml);
    ?>

    <pre style="display:none;" rel="1"><?=$xml;?></pre>

    <?
    $auto_catalog = simplexml_load_string($xml);
    print_r("-----------------EMPTY TAGS-----------------");
    ?>

    <br />

    <table>

    <?
    if($_REQUEST["catalog_type"] == "order"):
        // Перебираем все авто под заказ
        foreach ($auto_catalog->carComplects->auto as $car) {

            foreach ($car as $key => $value){
                   
                    if((string)$value == ""){?>
                        <tr><td><?print_r((string)$car->carcomplect->carcomplectid);?></td><td><?print_r((string)$key);?></td></tr>
                    <?}

                    if($key == 'carcomplect'){?>
                        <?foreach ($value as $key=>$value) {?>
                            <?if(((string)$value == "") && ((string)$key != "VIN2")){?>
                                <tr><td><?print_r((string)$car->carcomplect->carcomplectid);?></td><td><?print_r((string)$key);?></td></tr>
                            <?}
                        }
                    }

                    if($key == 'baseoptions' || $key == 'additionaloptions'){
                        foreach ($value->option as $option) {
                            foreach ($option as $key=>$value) {     
                                if((string)$value == ""){?>
                                    <tr><td><?print_r((string)$car->carcomplect->carcomplectid);?></td><td><?print_r((string)$key);?></td></tr>
                                <?}
                            }
                        }
                    }
            }
        }
    endif;
    ?>

    <?
    if($_REQUEST["catalog_type"] == "stock"):
        // Перебираем все авто в наличии
        foreach ($auto_catalog->carsOnHand->auto as $auto) {
            foreach ($auto as $key => $value){
                
                    if((string)$value == ""){?>
                        <tr><td><?print_r((string)$auto->car->VIN);?></td><td><?print_r((string)$key);?></td></tr>
                    <?}

                    if($key == 'car'){
                        foreach ($value as $key=>$value) {
                            if(((string)$value == "") && ((string)$key != "VIN2")){?>
                               <tr><td><?print_r((string)$auto->car->VIN);?></td><td><?print_r((string)$key);?></td></tr>
                    <?      }
                        }
                    }

                    if($key == 'baseoptions' || $key == 'additionaloptions'){
                        foreach ($value->option as $option) {
                            foreach ($option as $key=>$value) {     
                                if((string)$value == ""){?>
                                    <tr><td><?print_r((string)$auto->car->VIN);?></td><td><?print_r((string)$key);?></td></tr>
                    <?
                                }
                            }
                        }
                    }
            }
        }
    endif;
    ?>

    <table>

    <?print_r("---------------END EMPTY TAGS---------------");?>

<?endif;?>

</div>

<style>
    tr, td {border: 1px solid #111;}
    td {width:200px;}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>