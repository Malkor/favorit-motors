<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

<?$iblock_id = 13; //авто в наличии?>
<?//$iblock_id = 17; авто на заказ?>

<?
// Проверяем установлен ли модуль "Информационные блоки" и если да то подключим его
if (CModule::IncludeModule("iblock")):

$auto_catalog = simplexml_load_file("instock_citroen.php");

// Перебираем все авто из выгрузки
foreach ($auto_catalog->carsOnHand->auto as $auto) {
    $vin = (string)$auto->car->VIN;
    $carname = (string)$auto->car->carname;
    $prop_arr = array();
    
    //print_r($auto);
    
    $if_exist_element = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "PROPERTY_VIN"=>$vin), false, array("nTopCount"=>1), array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*"));
    
    // Проверяем есть ли в каталоге авто по VIN
    if($ob = $if_exist_element->GetNextElement())  
    {
        $arFields = $ob->GetFields();
        print_r($arFields);
        // Если нашли авто с нужным VIN значит будем вносить изменения в уже существующую запись
        // $arFields = $ob->GetFields();
        // print_r($arFields);
    }
    // Если не найдено авто с таким VIN добавляем новый элемент в каталог
    else{
        foreach ($auto as $key => $value)
        {            
            if(strtolower((string)$key) == "car")
            {
                foreach ($auto->car as $car)
                {
                    foreach ($car as $key => $value) 
                    {                                        
                        $prop_code = strtoupper((string)$key);

                        $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                        if ($ar_res = $res->GetNext()){
                            if ($ar_res["PROPERTY_TYPE"] == "E"){
                                if((string)$key == 'baseoptions'){
                                    $iblock_options_id = 14;
                                    $iblock_code = "baseoptions";
                                }
                                if((string)$key == 'additionaloptions'){
                                    $iblock_options_id = 15;
                                    $iblock_code = "additionaloptions";
                                }
                                $n = 0;
                                foreach ($value->option as $option){
                                    $option_name = (string)$option->optionId;
                                    $if_exist_option = CIBlockElement::GetList(array(), array("IBLOCK_CODE"=>$iblock_code, "NAME"=>$option_name), false, array("nTopCount"=>1), array("ID", "NAME"));
                                    if($ar_res = $if_exist_option->GetNext())
                                    {
                                        $prop_arr[$prop_code]["n".$n] = array("VALUE" => $ar_res["ID"]);
                                        $n++;
                                    }
                                    else{
                                        $option_props = "";
                                        foreach ($option as $pname => $pvalue) {
                                            $option_props[(string)strtoupper($pname)] = (string)$pvalue;
                                        }
                                        $el = new CIBlockElement;
                                        $arLoadProductArray = Array(
                                          "MODIFIED_BY"    => $USER->GetID(),   // элемент изменен текущим пользователем
                                          "IBLOCK_SECTION_ID" => false,                 // элемент лежит в корне раздела
                                          "IBLOCK_ID"      => $iblock_options_id,
                                          "PROPERTY_VALUES"=> $option_props,
                                          "NAME"           => $option_name,
                                          "ACTIVE"         => "Y"                       // активен
                                        );
                                        if($PRODUCT_ID = $el->Add($arLoadProductArray))
                                        {
                                            echo "New property ID: ".$PRODUCT_ID;
                                        }
                                        else
                                        {
                                          echo "Error: ".$el->LAST_ERROR;
                                        }
                                    }
                                }
                            }

                            if ($ar_res["PROPERTY_TYPE"] == "L") {
                                $prop_id = "";
                                //print_r("Находим нужное для нас свойство и ищем в нем текущее значение");
                                $property_enums = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$prop_code));
                                while($enum_fields = $property_enums->GetNext())
                                {
                                    if ($enum_fields["VALUE"] == (string)$value) {
                                        $prop_id = $enum_fields["ID"];
                                        break;
                                    }
                                }
                                
                                if($prop_id == ""){
                                    //print_r("Если значение не нашли, тогда добавляем новое значение");
                                    $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                                    if($ar_res = $res->GetNext()){
                                        $ibpenum = new CIBlockPropertyEnum;
                                        if($PropID = $ibpenum->Add(array("IBLOCK_ID"=>$iblock_id, 'PROPERTY_ID' => $ar_res["ID"], 'VALUE' => (string)$value))){
                                            $prop_arr[$prop_code] = array("VALUE" => $PropID);
                                        }
                                    }
                                }
                                else{
                                    $prop_arr[$prop_code] = $prop_id;
                                }
                            }
                            if (($ar_res["PROPERTY_TYPE"] == "S") || ($ar_res["PROPERTY_TYPE"] == "N")) {
                                if($ar_res["USER_TYPE"] == "directory")
                                    $value = strtolower((string)$value);

                                $prop_arr[$prop_code] = (string)$value;
                            }
                        }
                    }
                }
            }
            else{
                $prop_code = strtoupper((string)$key);
                $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                if ($ar_res = $res->GetNext())
                {
                    if ($ar_res["PROPERTY_TYPE"] == "E"){
                        if((string)$key == 'baseoptions'){
                            $iblock_options_id = 14;
                            $iblock_code = "baseoptions";
                        }
                        if((string)$key == 'additionaloptions'){
                            $iblock_options_id = 15;
                            $iblock_code = "additionaloptions";
                        }
                        $n = 0;
                        foreach ($value->option as $option){
                            $option_name = (string)$option->optionId;
                            $if_exist_option = CIBlockElement::GetList(array(), array("IBLOCK_CODE"=>$iblock_code, "NAME"=>$option_name), false, array("nTopCount"=>1), array("ID", "NAME"));
                            if($ar_res = $if_exist_option->GetNext())
                            {
                                $prop_arr[$prop_code]["n".$n] = array("VALUE" => $ar_res["ID"]);
                                $n++;
                            }
                            else{
                                $option_props = "";
                                foreach ($option as $pname => $pvalue) {
                                    $option_props[(string)strtoupper($pname)] = (string)$pvalue;
                                }
                                $el = new CIBlockElement;
                                $arLoadProductArray = Array(
                                  "MODIFIED_BY"    => $USER->GetID(),   // элемент изменен текущим пользователем
                                  "IBLOCK_SECTION_ID" => false,                 // элемент лежит в корне раздела
                                  "IBLOCK_ID"      => $iblock_options_id,
                                  "PROPERTY_VALUES"=> $option_props,
                                  "NAME"           => $option_name,
                                  "ACTIVE"         => "Y"                       // активен
                                );
                                if($PRODUCT_ID = $el->Add($arLoadProductArray))
                                {
                                    echo "New property ID: ".$PRODUCT_ID;
                                }
                                else
                                {
                                  echo "Error: ".$el->LAST_ERROR;
                                }
                            }
                        }
                    }

                    if ($ar_res["PROPERTY_TYPE"] == "L") {
                        $prop_id = "";
                        //print_r("Находим нужное для нас свойство и ищем в нем текущее значение");
                        $property_enums = CIBlockPropertyEnum::GetList(array(), array("IBLOCK_ID"=>$iblock_id, "CODE"=>$prop_code));
                        while($enum_fields = $property_enums->GetNext())
                        {
                            if ($enum_fields["VALUE"] == (string)$value) {
                                $prop_id = $enum_fields["ID"];
                                break;
                            }
                        }
                        
                        if($prop_id == ""){
                            //print_r("Если значение не нашли, тогда добавляем новое значение");
                            $res = CIBlockProperty::GetByID($prop_code, $iblock_id, false);
                            if($ar_res = $res->GetNext()){
                                $ibpenum = new CIBlockPropertyEnum;
                                if($PropID = $ibpenum->Add(array("IBLOCK_ID"=>$iblock_id, 'PROPERTY_ID' => $ar_res["ID"], 'VALUE' => (string)$value))){
                                    $prop_arr[$prop_code] = array("VALUE" => $PropID);
                                }
                            }
                        }
                        else{
                            $prop_arr[$prop_code] = $prop_id;
                        }
                    }
                    if (($ar_res["PROPERTY_TYPE"] == "S") || ($ar_res["PROPERTY_TYPE"] == "N")) {
                        if($ar_res["USER_TYPE"] == "directory")
                            $value = strtolower((string)$value);

                        $prop_arr[$prop_code] = (string)$value;
                    }
                }
            }
        }

        $el = new CIBlockElement;

        $arLoadProductArray = Array(
          "MODIFIED_BY"    => $USER->GetID(),       // элемент изменен текущим пользователем
          "IBLOCK_SECTION_ID" => false,             // элемент лежит в корне раздела
          "IBLOCK_ID"      => $iblock_id,
          "PROPERTY_VALUES"=> $prop_arr,
          "NAME"           => $carname,
          "ACTIVE"         => "Y"                   // активен
          );

        if($PRODUCT_ID = $el->Add($arLoadProductArray))
        {
            echo "New ID: ".$PRODUCT_ID;
        }
        else
        {
          echo "Error: ".$el->LAST_ERROR;
        }
    }  
}
?>


<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>