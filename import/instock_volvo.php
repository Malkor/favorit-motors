﻿<?xml version="1.0"?><auto-catalog><carsOnHand><auto>
                            <car>
                            <carid>00874993</carid>
                            <VIN>YV1DZ8156F2721355</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 415 930,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>51 043,94</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883212</carid>
                            <VIN>YV1DZ8156F2752975</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>019</carcolorid>
                            <carcolorname>Черный , (Black Stone)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 345 230,00</price>
                            <optionprice>96 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>019</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Stone</optionname>
                        <optionprice>Black Stone</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885260</carid>
                            <VIN>YV1DZ8156F2760048</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>019</carcolorid>
                            <carcolorname>Черный , (Black Stone)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 372 030,00</price>
                            <optionprice>123 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>019</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Stone</optionname>
                        <optionprice>Black Stone</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883510</carid>
                            <VIN>YV1DZ8256F2754910</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-82-13(15)</carcomplectid>
                            <carname>VOLVO XC60</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 717 530,00</price>
                            <optionprice>183 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875052</carid>
                            <VIN>YV1DZ8156F2717601</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 647 530,00</price>
                            <optionprice>289 100,00</optionprice>
                            <additionalequipmentprice>25 280,48</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G31T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кожа AGNES Бежевого цвета в бежев. интер., бежев. потолок</optionname>
                        <optionprice>Кожа AGNES Бежевого цвета в бежев. интер., бежев. потолок</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>463</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Рулевое колесо с деревянными вставками</optionname>
                        <optionprice>Рулевое колесо с деревянными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>602</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев форсунок омывателя лобового стекла</optionname>
                        <optionprice>Подогрев форсунок омывателя лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874844</carid>
                            <VIN>YV1DZ8156F2721043</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 698 330,00</price>
                            <optionprice>339 900,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G30M</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон AGNES Кожа Спортивная</optionname>
                        <optionprice>Салон AGNES Кожа Спортивная</optionprice>
                        </option>
                        <option>
                        <optionId>MC337</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОХРАНА (SECURITY)</optionname>
                        <optionprice>ОХРАНА (SECURITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885641</carid>
                            <VIN>YV1DZ8156F2759061</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 553 730,00</price>
                            <optionprice>195 300,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884290</carid>
                            <VIN>YV1DZ8156F2725184</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 659 830,00</price>
                            <optionprice>301 400,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>179</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Тонировка стекол задних дверей и багажного отделения</optionname>
                        <optionprice>Тонировка стекол задних дверей и багажного отделения</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>583</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Технология - без ключа зажигания</optionname>
                        <optionprice>Технология - без ключа зажигания</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>752</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев задних сидений</optionname>
                        <optionprice>Подогрев задних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885550</carid>
                            <VIN>YV1AS40LDF1192556</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S80</carfamily>
                            <carmodel>S80</carmodel>
                            <carcomplectid>124-40-13(15)</carcomplectid>
                            <carname>VOLVO S80</carname>
                            <carcolorid>467</carcolorid>
                            <carcolorname>Синий перламутр , (Magic Blue Metallic)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 502 260,00</price>
                            <optionprice>444 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод пассажирского сиденья</optionname>
                        <optionprice>Электропривод пассажирского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>897</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; PANDORA</optionname>
                        <optionprice>Легкосплавные диски 17&#034; PANDORA</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>583</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Технология - без ключа зажигания</optionname>
                        <optionprice>Технология - без ключа зажигания</optionprice>
                        </option>
                        <option>
                        <optionId>602</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев форсунок омывателя лобового стекла</optionname>
                        <optionprice>Подогрев форсунок омывателя лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>887</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; MAGNI</optionname>
                        <optionprice>Легкосплавные диски 18&#034; MAGNI</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        <option>
                        <optionId>800060</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Административная опция Inscription</optionname>
                        <optionprice>Административная опция Inscription</optionprice>
                        </option>
                        <option>
                        <optionId>800064</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Рулевое колесо, отделанное темным деревом, Inscription</optionname>
                        <optionprice>Рулевое колесо, отделанное темным деревом, Inscription</optionprice>
                        </option>
                        <option>
                        <optionId>800071</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка темным деревом, Inscription</optionname>
                        <optionprice>Отделка темным деревом, Inscription</optionprice>
                        </option>
                        <option>
                        <optionId>467</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Magic Blue metallic</optionname>
                        <optionprice>Magic Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>2G1T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Inscription Мягкая кожа перфорированная</optionname>
                        <optionprice>Inscription Мягкая кожа перфорированная</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>10</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод пассажирского сиденья</optionname>
                        <optionprice>Электропривод пассажирского сиденья</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00872078</carid>
                            <VIN>YV1FS42H0F2360898</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S60</carfamily>
                            <carmodel>S60</carmodel>
                            <carcomplectid>134-42-12(15)</carcomplectid>
                            <carname>Volvo S60 Momentum 2 AT-6 180 (Л.С.)</carname>
                            <carcolorid>487</carcolorid>
                            <carcolorname>Темно-коричневый металлик, (Inscription Ember Black)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 885 560,00</price>
                            <optionprice>88 400,00</optionprice>
                            <additionalequipmentprice>53 029,18</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1984</Volume> <Power>180</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,70</Top100> <Fuel_consumption_city>12,10</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>8,30</Fuel_consumption> <Airbags>6</Airbags> <Air_con>Нет</Air_con> <Length>4635</Length> <Width>1865</Width> <Height>1484</Height> <Clearance>136</Clearance> <Trunk_size>380</Trunk_size> <Fuel_tank_size>67</Fuel_tank_size> <Mass>1566</Mass> <Equipment_code>134-42-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>S60 II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>300/2700?–?4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>220</Speed> <abs>Да</abs> <Engine>T4</Engine> <FullMass>2070</FullMass> <max_trunk_size>380</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>983</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 16&#034; HERA</optionname>
                        <optionprice>Легкосплавные диски 16&#034; HERA</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>487</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ember Black metallic</optionname>
                        <optionprice>Ember Black metallic</optionprice>
                        </option>
                        <option>
                        <optionId>3601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон DYNAMIC Текстиль</optionname>
                        <optionprice>Салон DYNAMIC Текстиль</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874868</carid>
                            <VIN>YV1DZ8156F2721286</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 734 330,00</price>
                            <optionprice>375 900,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G361</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон AGNES Кожа Спортивная</optionname>
                        <optionprice>Салон AGNES Кожа Спортивная</optionprice>
                        </option>
                        <option>
                        <optionId>MC337</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОХРАНА (SECURITY)</optionname>
                        <optionprice>ОХРАНА (SECURITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>399</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Система &#034;Volvo On Call&#034;</optionname>
                        <optionprice>Система &#034;Volvo On Call&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        <option>
                        <optionId>800044</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; MERAC черные</optionname>
                        <optionprice>Легкосплавные диски 18&#034; MERAC черные</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883210</carid>
                            <VIN>YV1DZ8156F2750319</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 345 230,00</price>
                            <optionprice>96 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885123</carid>
                            <VIN>YV1BZ8156F1230986</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15)</carcomplectid>
                            <carname>VOLVO XC70 I</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 500 450,00</price>
                            <optionprice>179 200,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа</optionname>
                        <optionprice>Салон кожа</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875064</carid>
                            <VIN>YV1FS40LDF2357776</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S60</carfamily>
                            <carmodel>S60</carmodel>
                            <carcomplectid>134-40-12(15)</carcomplectid>
                            <carname>Volvo S60 Momentum 2 AT-8 245 (Л.С.)</carname>
                            <carcolorid>487</carcolorid>
                            <carcolorname>Темно-коричневый металлик, (Inscription Ember Black)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 057 160,00</price>
                            <optionprice>136 400,00</optionprice>
                            <additionalequipmentprice>24 822,14</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1969</Volume> <Power>245</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>8</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>6,30</Top100> <Fuel_consumption_city>8,30</Fuel_consumption_city> <Fuel_consumption_track>4,70</Fuel_consumption_track> <Fuel_consumption>6,00</Fuel_consumption> <Airbags>6</Airbags> <Air_con>Нет</Air_con> <Length>4635</Length> <Width>1865</Width> <Height>1484</Height> <Clearance>136</Clearance> <Trunk_size>380</Trunk_size> <Fuel_tank_size>67</Fuel_tank_size> <Mass>1664</Mass> <Equipment_code>134-40-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>S60 II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>350/1500?–?4800</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>230</Speed> <abs>Да</abs> <Engine>T5 Drive-E</Engine> <FullMass>2170</FullMass> <max_trunk_size>380</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>904</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; SADIA</optionname>
                        <optionprice>Легкосплавные диски 17&#034; SADIA</optionprice>
                        </option>
                        <option>
                        <optionId>983</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 16&#034; HERA</optionname>
                        <optionprice>Легкосплавные диски 16&#034; HERA</optionprice>
                        </option>
                        <option>
                        <optionId>781</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрический подогреватель воздуха (быстрый прогрев салона)</optionname>
                        <optionprice>Электрический подогреватель воздуха (быстрый прогрев салона)</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>487</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ember Black metallic</optionname>
                        <optionprice>Ember Black metallic</optionprice>
                        </option>
                        <option>
                        <optionId>3601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон DYNAMIC Текстиль</optionname>
                        <optionprice>Салон DYNAMIC Текстиль</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00880955</carid>
                            <VIN>YV1FS40LDF2363940</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S60</carfamily>
                            <carmodel>S60</carmodel>
                            <carcomplectid>134-40-12(15)</carcomplectid>
                            <carname>Volvo S60 Momentum 2 AT-8 245 (Л.С.)</carname>
                            <carcolorid>619</carcolorid>
                            <carcolorname>Ярко-синий , (Rebel Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 049 760,00</price>
                            <optionprice>129 000,00</optionprice>
                            <additionalequipmentprice>14 777,20</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1969</Volume> <Power>245</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>8</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>6,30</Top100> <Fuel_consumption_city>8,30</Fuel_consumption_city> <Fuel_consumption_track>4,70</Fuel_consumption_track> <Fuel_consumption>6,00</Fuel_consumption> <Airbags>6</Airbags> <Air_con>Нет</Air_con> <Length>4635</Length> <Width>1865</Width> <Height>1484</Height> <Clearance>136</Clearance> <Trunk_size>380</Trunk_size> <Fuel_tank_size>67</Fuel_tank_size> <Mass>1664</Mass> <Equipment_code>134-40-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>S60 II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>350/1500?–?4800</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>230</Speed> <abs>Да</abs> <Engine>T5 Drive-E</Engine> <FullMass>2170</FullMass> <max_trunk_size>380</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>781</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрический подогреватель воздуха (быстрый прогрев салона)</optionname>
                        <optionprice>Электрический подогреватель воздуха (быстрый прогрев салона)</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>904</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; SADIA</optionname>
                        <optionprice>Легкосплавные диски 17&#034; SADIA</optionprice>
                        </option>
                        <option>
                        <optionId>983</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 16&#034; HERA</optionname>
                        <optionprice>Легкосплавные диски 16&#034; HERA</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>619</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rebel Blue</optionname>
                        <optionprice>Rebel Blue</optionprice>
                        </option>
                        <option>
                        <optionId>3601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон DYNAMIC Текстиль</optionname>
                        <optionprice>Салон DYNAMIC Текстиль</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>793</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; TITANIA</optionname>
                        <optionprice>Легкосплавные диски 18&#034; TITANIA</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875216</carid>
                            <VIN>YV1BZ8156F1227789</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15) SE</carcomplectid>
                            <carname>Volvo XC70 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 348 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>63 934,32</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-13(15) SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>F701</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа</optionname>
                        <optionprice>Салон кожа</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886333</carid>
                            <VIN>YV1DZ8156F2760335</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>713</carcolorid>
                            <carcolorname>Power Blue</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 542 030,00</price>
                            <optionprice>183 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>713</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Power Blue</optionname>
                        <optionprice>Power Blue</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884117</carid>
                            <VIN>YV1DZ8156F2752309</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 324 930,00</price>
                            <optionprice>76 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>65</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Омыватели фар</optionname>
                        <optionprice>Омыватели фар</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875072</carid>
                            <VIN>YV1MZ845BF2072994</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>V40 CrossCountry</carfamily>
                            <carmodel>V40 CrossCountry</carmodel>
                            <carcomplectid>526-84-12(15)</carcomplectid>
                            <carname>Volvo V40 Cross Country Momentum 1.6 дизель RT-6 114 (Л.С.)</carname>
                            <carcolorid>708</carcolorid>
                            <carcolorname>Светло-бронзовый металлик , (Raw Copper)</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 677 950,00</price>
                            <optionprice>156 700,00</optionprice>
                            <additionalequipmentprice>12 953,51</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>114</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>12,10</Top100> <Fuel_consumption_city>4,50</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4370</Length> <Width>1783</Width> <Height>1458</Height> <Clearance>145</Clearance> <Trunk_size>496</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1412</Mass> <Equipment_code>526-84-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>V40 Cross Country</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>270/1750?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D2</Engine> <FullMass>1920</FullMass> <max_trunk_size>615</max_trunk_size> <baseoptions><option>
                        <optionId>57</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Ручная регулировка высоты пассажирского сидения</optionname>
                        <optionprice>Ручная регулировка высоты пассажирского сидения</optionprice>
                        </option>
                        <option>
                        <optionId>867</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски LARENTA R17</optionname>
                        <optionprice>Легкосплавные диски LARENTA R17</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionname>
                        <optionprice>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>708</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Raw Copper metallic</optionname>
                        <optionprice>Raw Copper metallic</optionprice>
                        </option>
                        <option>
                        <optionId>S500</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Jeans/T-Tec</optionname>
                        <optionprice>DYNAMIC Jeans/T-Tec</optionprice>
                        </option>
                        <option>
                        <optionId>MC344</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00877730</carid>
                            <VIN>YV1MZ845BF2076454</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>V40 CrossCountry</carfamily>
                            <carmodel>V40 CrossCountry</carmodel>
                            <carcomplectid>526-84-12(15)</carcomplectid>
                            <carname>Volvo V40 Cross Country Momentum 1.6 дизель RT-6 114 (Л.С.)</carname>
                            <carcolorid>708</carcolorid>
                            <carcolorname>Светло-бронзовый металлик , (Raw Copper)</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>1 574 750,00</price>
                            <optionprice>53 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>114</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>12,10</Top100> <Fuel_consumption_city>4,50</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4370</Length> <Width>1783</Width> <Height>1458</Height> <Clearance>145</Clearance> <Trunk_size>496</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1412</Mass> <Equipment_code>526-84-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>V40 Cross Country</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>270/1750?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D2</Engine> <FullMass>1920</FullMass> <max_trunk_size>615</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>57</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Ручная регулировка высоты пассажирского сидения</optionname>
                        <optionprice>Ручная регулировка высоты пассажирского сидения</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionname>
                        <optionprice>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>867</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски LARENTA R17</optionname>
                        <optionprice>Легкосплавные диски LARENTA R17</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>S500</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Jeans/T-Tec</optionname>
                        <optionprice>DYNAMIC Jeans/T-Tec</optionprice>
                        </option>
                        <option>
                        <optionId>836</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски LARENTA R17 серые</optionname>
                        <optionprice>Легкосплавные диски LARENTA R17 серые</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>708</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Raw Copper metallic</optionname>
                        <optionprice>Raw Copper metallic</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875082</carid>
                            <VIN>YV1FS42H0F1360112</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S60</carfamily>
                            <carmodel>S60</carmodel>
                            <carcomplectid>134-42-13(15)SE</carcomplectid>
                            <carname>Volvo S60 Summum 2 AT-6 180 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 907 160,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>60 423,46</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1984</Volume> <Power>180</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,70</Top100> <Fuel_consumption_city>12,10</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>8,30</Fuel_consumption> <Airbags>6</Airbags> <Air_con>Нет</Air_con> <Length>4635</Length> <Width>1865</Width> <Height>1484</Height> <Clearance>136</Clearance> <Trunk_size>380</Trunk_size> <Fuel_tank_size>67</Fuel_tank_size> <Mass>1566</Mass> <Equipment_code>134-42-13(15)SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>S60 II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>300/2700?–?4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>220</Speed> <abs>Да</abs> <Engine>T4</Engine> <FullMass>2070</FullMass> <max_trunk_size>380</max_trunk_size> <baseoptions><option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>371T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа ARIANNE</optionname>
                        <optionprice>Салон кожа ARIANNE</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874961</carid>
                            <VIN>YV1DZ8156F2721525</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 583 830,00</price>
                            <optionprice>225 400,00</optionprice>
                            <additionalequipmentprice>13 255,48</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885570</carid>
                            <VIN>YV1DZ8156F2759614</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 425 630,00</price>
                            <optionprice>177 200,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885144</carid>
                            <VIN>YV1DZ8156F2755515</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 415 930,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875077</carid>
                            <VIN>YV1MZ4250F2073149</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>V40 CrossCountry</carfamily>
                            <carmodel>V40 CrossCountry</carmodel>
                            <carcomplectid>526-42-12(15)</carcomplectid>
                            <carname>Volvo V40 Cross Country Momentum 2 AT-6 180 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 810 550,00</price>
                            <optionprice>89 300,00</optionprice>
                            <additionalequipmentprice>12 953,51</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1984</Volume> <Power>180</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,70</Top100> <Fuel_consumption_city>10,50</Fuel_consumption_city> <Fuel_consumption_track>6,00</Fuel_consumption_track> <Fuel_consumption>7,60</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4370</Length> <Width>1783</Width> <Height>1458</Height> <Clearance>145</Clearance> <Trunk_size>496</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1503</Mass> <Equipment_code>526-42-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>V40 Cross Country</Generation> <Start_year>2012</Start_year> <Fuel>бензин</Fuel> <Moment>300/2700?–?4000</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>210</Speed> <abs>Да</abs> <Engine>T4</Engine> <FullMass>2010</FullMass> <max_trunk_size>615</max_trunk_size> <baseoptions><option>
                        <optionId>57</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Ручная регулировка высоты пассажирского сидения</optionname>
                        <optionprice>Ручная регулировка высоты пассажирского сидения</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionname>
                        <optionprice>3-хспиц. рулевое колесо (Кожа) с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>867</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски LARENTA R17</optionname>
                        <optionprice>Легкосплавные диски LARENTA R17</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>S500</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Jeans/T-Tec</optionname>
                        <optionprice>DYNAMIC Jeans/T-Tec</optionprice>
                        </option>
                        <option>
                        <optionId>MC344</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>836</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски LARENTA R17 серые</optionname>
                        <optionprice>Легкосплавные диски LARENTA R17 серые</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев ветрового стекла</optionname>
                        <optionprice>Подогрев ветрового стекла</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886174</carid>
                            <VIN>YV1DZ8156F2760371</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 542 730,00</price>
                            <optionprice>184 300,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885238</carid>
                            <VIN>YV1DZ8156F2760256</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 553 730,00</price>
                            <optionprice>195 300,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G70B</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885812</carid>
                            <VIN>YV1DZ8156F2760167</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 515 230,00</price>
                            <optionprice>156 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884125</carid>
                            <VIN>YV1DZ8156F2753005</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 389 130,00</price>
                            <optionprice>140 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875069</carid>
                            <VIN>YV1MZ845BF2072902</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>V40 CrossCountry</carfamily>
                            <carmodel>V40 CrossCountry</carmodel>
                            <carcomplectid>526-84-11(15)</carcomplectid>
                            <carname>Volvo V40 Cross Country Kinetic 1.6 дизель RT-6 114 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 520 450,00</price>
                            <optionprice>59 200,00</optionprice>
                            <additionalequipmentprice>35 507,30</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>114</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>12,10</Top100> <Fuel_consumption_city>4,50</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4370</Length> <Width>1783</Width> <Height>1458</Height> <Clearance>145</Clearance> <Trunk_size>496</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1412</Mass> <Equipment_code>526-84-11(15)</Equipment_code> <equipment_name>Kinetic</equipment_name> <Generation>V40 Cross Country</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>270/1750?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D2</Engine> <FullMass>1920</FullMass> <max_trunk_size>615</max_trunk_size> <baseoptions><option>
                        <optionId>29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>Отделка рулевого колеса кожей</optionprice>
                        </option>
                        <option>
                        <optionId>274</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски GEMINUS R16</optionname>
                        <optionprice>Легкосплавные диски GEMINUS R16</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>S500</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Jeans/T-Tec</optionname>
                        <optionprice>DYNAMIC Jeans/T-Tec</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев ветрового стекла</optionname>
                        <optionprice>Подогрев ветрового стекла</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00880967</carid>
                            <VIN>YV1BZ8156F1229276</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-12(15)</carcomplectid>
                            <carname>Volvo XC70 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>019</carcolorid>
                            <carcolorname>Черный , (Black Stone)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 243 850,00</price>
                            <optionprice>32 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>584</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Графическая панель приборов 8&#034; без хромированной окантовки</optionname>
                        <optionprice>Графическая панель приборов 8&#034; без хромированной окантовки</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>019</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Stone</optionname>
                        <optionprice>Black Stone</optionprice>
                        </option>
                        <option>
                        <optionId>F501</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>SUNDBY Текстиль/Винил</optionname>
                        <optionprice>SUNDBY Текстиль/Винил</optionprice>
                        </option>
                        <option>
                        <optionId>65</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Омыватели фар</optionname>
                        <optionprice>Омыватели фар</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00880983</carid>
                            <VIN>YV1DZ3150F2749122</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-31-12(15)</carcomplectid>
                            <carname>VOLVO XC60</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 182 530,00</price>
                            <optionprice>86 200,00</optionprice>
                            <additionalequipmentprice>40 089,63</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>308</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Без эмблемы объема двигателя</optionname>
                        <optionprice>Без эмблемы объема двигателя</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885237</carid>
                            <VIN>YV1DZ8156F2761363</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-81(15)</carcomplectid>
                            <carname>Volvo XC60 Ocean Race 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>706</carcolorid>
                            <carcolorname>Ocean Blue II</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 467 130,00</price>
                            <optionprice>118 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-81(15)</Equipment_code> <equipment_name>Ocean Race</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>729</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше - серебристые</optionname>
                        <optionprice>Рейлинги продольные на крыше - серебристые</optionprice>
                        </option>
                        <option>
                        <optionId>998996</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Административная опция VOR</optionname>
                        <optionprice>Административная опция VOR</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>706</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ocean Blue II</optionname>
                        <optionprice>Ocean Blue II</optionprice>
                        </option>
                        <option>
                        <optionId>GV6Z</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кожа Ocean Race Edition</optionname>
                        <optionprice>Кожа Ocean Race Edition</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00881977</carid>
                            <VIN>YV1DZAHH6F2742675</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-AH-12(15)</carcomplectid>
                            <carname>VOLVO XC60</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 516 580,00</price>
                            <optionprice>180 400,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>781</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электрический подогреватель воздуха (быстрый прогрев салона)</optionname>
                        <optionprice>Электрический подогреватель воздуха (быстрый прогрев салона)</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00879394</carid>
                            <VIN>YV1MZ845BF2074932</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>V40 CrossCountry</carfamily>
                            <carmodel>V40 CrossCountry</carmodel>
                            <carcomplectid>526-84-11(15)</carcomplectid>
                            <carname>Volvo V40 Cross Country Kinetic 1.6 дизель RT-6 114 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 506 050,00</price>
                            <optionprice>44 800,00</optionprice>
                            <additionalequipmentprice>50 349,11</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>хэтчбек</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1560</Volume> <Power>114</Power> <VolumeReal>1,60</VolumeReal> <Transmission>робот</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>12,10</Top100> <Fuel_consumption_city>4,50</Fuel_consumption_city> <Fuel_consumption_track>3,90</Fuel_consumption_track> <Fuel_consumption>4,10</Fuel_consumption> <Airbags>5</Airbags> <Air_con>Нет</Air_con> <Length>4370</Length> <Width>1783</Width> <Height>1458</Height> <Clearance>145</Clearance> <Trunk_size>496</Trunk_size> <Fuel_tank_size>57</Fuel_tank_size> <Mass>1412</Mass> <Equipment_code>526-84-11(15)</Equipment_code> <equipment_name>Kinetic</equipment_name> <Generation>V40 Cross Country</Generation> <Start_year>2012</Start_year> <Fuel>дизель</Fuel> <Moment>270/1750?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D2</Engine> <FullMass>1920</FullMass> <max_trunk_size>615</max_trunk_size> <baseoptions><option>
                        <optionId>29</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка рулевого колеса кожей</optionname>
                        <optionprice>Отделка рулевого колеса кожей</optionprice>
                        </option>
                        <option>
                        <optionId>274</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски GEMINUS R16</optionname>
                        <optionprice>Легкосплавные диски GEMINUS R16</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>S500</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Jeans/T-Tec</optionname>
                        <optionprice>DYNAMIC Jeans/T-Tec</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>298</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Функция складывания спинки сиденья пассажира</optionname>
                        <optionprice>Функция складывания спинки сиденья пассажира</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885600</carid>
                            <VIN>YV1DZ8156F2760258</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 523 430,00</price>
                            <optionprice>165 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875063</carid>
                            <VIN>YV1FS42H0F2358308</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S60</carfamily>
                            <carmodel>S60</carmodel>
                            <carcomplectid>134-42-12(15)</carcomplectid>
                            <carname>Volvo S60 Momentum 2 AT-6 180 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>1 933 560,00</price>
                            <optionprice>136 400,00</optionprice>
                            <additionalequipmentprice>24 822,14</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>седан</Body> <Drive>передний</Drive> <Doors>4</Doors> <Seats>5</Seats> <Volume>1984</Volume> <Power>180</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>8,70</Top100> <Fuel_consumption_city>12,10</Fuel_consumption_city> <Fuel_consumption_track>6,10</Fuel_consumption_track> <Fuel_consumption>8,30</Fuel_consumption> <Airbags>6</Airbags> <Air_con>Нет</Air_con> <Length>4635</Length> <Width>1865</Width> <Height>1484</Height> <Clearance>136</Clearance> <Trunk_size>380</Trunk_size> <Fuel_tank_size>67</Fuel_tank_size> <Mass>1566</Mass> <Equipment_code>134-42-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>S60 II Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>300/2700?–?4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>220</Speed> <abs>Да</abs> <Engine>T4</Engine> <FullMass>2070</FullMass> <max_trunk_size>380</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>983</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 16&#034; HERA</optionname>
                        <optionprice>Легкосплавные диски 16&#034; HERA</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>3601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон DYNAMIC Текстиль</optionname>
                        <optionprice>Салон DYNAMIC Текстиль</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885121</carid>
                            <VIN>YV1BZ8156F1230989</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-12(15)</carcomplectid>
                            <carname>Volvo XC70 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 378 750,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>584</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Графическая панель приборов 8&#034; без хромированной окантовки</optionname>
                        <optionprice>Графическая панель приборов 8&#034; без хромированной окантовки</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze metallic</optionname>
                        <optionprice>Twilight Bronze metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F501</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>SUNDBY Текстиль/Винил</optionname>
                        <optionprice>SUNDBY Текстиль/Винил</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875211</carid>
                            <VIN>YV1BZ8156F1226322</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15) SE</carcomplectid>
                            <carname>Volvo XC70 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 348 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>102 379,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-13(15) SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>MC343</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>F701</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа</optionname>
                        <optionprice>Салон кожа</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00880985</carid>
                            <VIN>YV1DZ8156F2743191</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 281 030,00</price>
                            <optionprice>32 600,00</optionprice>
                            <additionalequipmentprice>15 327,49</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>65</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Омыватели фар</optionname>
                        <optionprice>Омыватели фар</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874994</carid>
                            <VIN>YV1DZ8156F2721510</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 415 930,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883215</carid>
                            <VIN>YV1DZ8256F2753764</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-82-12(15)</carcomplectid>
                            <carname>VOLVO XC60</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 593 630,00</price>
                            <optionprice>169 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884288</carid>
                            <VIN>YV1BZ8156F1227312</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-12(15)</carcomplectid>
                            <carname>Volvo XC70 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>484</carcolorid>
                            <carcolorname>Бежевый металлик , (Seashell metallic)</carcolorname>
                            <carcolortype>Металлик</carcolortype>
                            <price>2 378 750,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>26 686,79</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>584</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Графическая панель приборов 8&#034; без хромированной окантовки</optionname>
                        <optionprice>Графическая панель приборов 8&#034; без хромированной окантовки</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>484</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Seashell metallic</optionname>
                        <optionprice>Seashell metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F501</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>SUNDBY Текстиль/Винил</optionname>
                        <optionprice>SUNDBY Текстиль/Винил</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875209</carid>
                            <VIN>YV1BZ8156F1226490</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15) SE</carcomplectid>
                            <carname>Volvo XC70 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 348 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>75 676,99</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-13(15) SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>MC340</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze metallic</optionname>
                        <optionprice>Twilight Bronze metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F70M</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Красный/коричневый в черном интерьере, серый потолок</optionname>
                        <optionprice>Красный/коричневый в черном интерьере, серый потолок</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886323</carid>
                            <VIN>YV1DZ8156F2760221</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 515 230,00</price>
                            <optionprice>156 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G70B</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874996</carid>
                            <VIN>YV1DZ8156F2721443</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 415 930,00</price>
                            <optionprice>167 500,00</optionprice>
                            <additionalequipmentprice>51 043,94</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874870</carid>
                            <VIN>YV1DZ8156F2721174</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 620 030,00</price>
                            <optionprice>261 600,00</optionprice>
                            <additionalequipmentprice>15 817,53</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G301</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон AGNES Кожа Спортивная</optionname>
                        <optionprice>Салон AGNES Кожа Спортивная</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875206</carid>
                            <VIN>YV1DZAHH6F2733275</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-AH-13(15)SE</carcomplectid>
                            <carname>Volvo XC60 Summum 2.5 4WD  AT-6 249 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 502 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>107 737,60</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2497</Volume> <Power>249</Power> <VolumeReal>2,50</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>7,10</Top100> <Fuel_consumption_city>11,90</Fuel_consumption_city> <Fuel_consumption_track>7,20</Fuel_consumption_track> <Fuel_consumption>9,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1660</Mass> <Equipment_code>156-AH-13(15)SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>360/1800?–?4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>210</Speed> <abs>Да</abs> <Engine>T5 AWD</Engine> <FullMass>2150</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885252</carid>
                            <VIN>YV1DZ8156F2760302</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 567 330,00</price>
                            <optionprice>208 900,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883211</carid>
                            <VIN>YV1DZ8156F2755628</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 345 230,00</price>
                            <optionprice>96 800,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00882037</carid>
                            <VIN>YV1BZ73CDF1223632</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-73-12(15)</carcomplectid>
                            <carname>Volvo XC70 Momentum 2 дизель AT-8 181 (Л.С.)</carname>
                            <carcolorid>477</carcolorid>
                            <carcolorname>Темно-серебристый металлик, (Inscription Electric Silver)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 253 250,00</price>
                            <optionprice>129 500,00</optionprice>
                            <additionalequipmentprice>56 224,86</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>передний</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>1969</Volume> <Power>181</Power> <VolumeReal>2,00</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>8</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>8,80</Top100> <Fuel_consumption_city>5,60</Fuel_consumption_city> <Fuel_consumption_track>4,50</Fuel_consumption_track> <Fuel_consumption>4,90</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1757</Mass> <Equipment_code>136-73-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>400/1750?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>210</Speed> <abs>Да</abs> <Engine>D4 Drive-E</Engine> <FullMass>2320</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>584</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Графическая панель приборов 8&#034; без хромированной окантовки</optionname>
                        <optionprice>Графическая панель приборов 8&#034; без хромированной окантовки</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC336</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОХРАНА (SECURITY)</optionname>
                        <optionprice>ОХРАНА (SECURITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>477</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Electric Silver metallic</optionname>
                        <optionprice>Electric Silver metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F501</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>SUNDBY Текстиль/Винил</optionname>
                        <optionprice>SUNDBY Текстиль/Винил</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874842</carid>
                            <VIN>YV1DZ8156F2720968</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 706 430,00</price>
                            <optionprice>348 000,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G30M</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон AGNES Кожа Спортивная</optionname>
                        <optionprice>Салон AGNES Кожа Спортивная</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>MC334</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ДЕТСКИЙ (FAMILY)</optionname>
                        <optionprice>ДЕТСКИЙ (FAMILY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        <option>
                        <optionId>399</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Система &#034;Volvo On Call&#034;</optionname>
                        <optionprice>Система &#034;Volvo On Call&#034;</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886332</carid>
                            <VIN>YV1DZ8156F2759915</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>713</carcolorid>
                            <carcolorname>Power Blue</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 569 530,00</price>
                            <optionprice>211 100,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>713</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Power Blue</optionname>
                        <optionprice>Power Blue</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885549</carid>
                            <VIN>YV1AS40LDF1192460</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>S80</carfamily>
                            <carmodel>S80</carmodel>
                            <carcomplectid>124-40-13(15)</carcomplectid>
                            <carname>VOLVO S80</carname>
                            <carcolorid>487</carcolorid>
                            <carcolorname>Темно-коричневый металлик, (Inscription Ember Black)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 327 360,00</price>
                            <optionprice>269 100,00</optionprice>
                            <additionalequipmentprice>15 694,82</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод пассажирского сиденья</optionname>
                        <optionprice>Электропривод пассажирского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>897</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; PANDORA</optionname>
                        <optionprice>Легкосплавные диски 17&#034; PANDORA</optionprice>
                        </option>
                        <option>
                        <optionId>2701</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа Comfort</optionname>
                        <optionprice>Салон кожа Comfort</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>487</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ember Black metallic</optionname>
                        <optionprice>Ember Black metallic</optionprice>
                        </option>
                        <option>
                        <optionId>MC327</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЧИСТЫЙ ВОЗДУХ (CLEAN AIR)</optionname>
                        <optionprice>ЧИСТЫЙ ВОЗДУХ (CLEAN AIR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>179</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Тонировка стекол задних дверей и багажного отделения</optionname>
                        <optionprice>Тонировка стекол задних дверей и багажного отделения</optionprice>
                        </option>
                        <option>
                        <optionId>255</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Навигационная система RTI</optionname>
                        <optionprice>Навигационная система RTI</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>752</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев задних сидений</optionname>
                        <optionprice>Подогрев задних сидений</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875058</carid>
                            <VIN>YV1DZAHH6F2720473</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-AH-13(15)SE</carcomplectid>
                            <carname>Volvo XC60 Summum 2.5 4WD  AT-6 249 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 502 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>61 393,30</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2497</Volume> <Power>249</Power> <VolumeReal>2,50</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>95</fuel_type> <Top100>7,10</Top100> <Fuel_consumption_city>11,90</Fuel_consumption_city> <Fuel_consumption_track>7,20</Fuel_consumption_track> <Fuel_consumption>9,60</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1660</Mass> <Equipment_code>156-AH-13(15)SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>бензин</Fuel> <Moment>360/1800?–?4200</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>210</Speed> <abs>Да</abs> <Engine>T5 AWD</Engine> <FullMass>2150</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885255</carid>
                            <VIN>YV1DZ8156F2760294</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 528 830,00</price>
                            <optionprice>170 400,00</optionprice>
                            <additionalequipmentprice>41 205,87</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874963</carid>
                            <VIN>YV1DZ8156F2721466</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 372 030,00</price>
                            <optionprice>123 600,00</optionprice>
                            <additionalequipmentprice>51 043,94</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885158</carid>
                            <VIN>YV1DZ8256F2755029</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-82-12(15)</carcomplectid>
                            <carname>VOLVO XC60</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 614 330,00</price>
                            <optionprice>190 400,00</optionprice>
                            <additionalequipmentprice>15 327,49</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body></Body> <Drive></Drive> <Doors>0</Doors> <Seats>0</Seats> <Volume>0</Volume> <Power>0</Power> <VolumeReal>0,00</VolumeReal> <Transmission></Transmission> <Transmission_speeds>0</Transmission_speeds> <fuel_type></fuel_type> <Top100>0,00</Top100> <Fuel_consumption_city>0,00</Fuel_consumption_city> <Fuel_consumption_track>0,00</Fuel_consumption_track> <Fuel_consumption>0,00</Fuel_consumption> <Airbags>0</Airbags> <Air_con>Нет</Air_con> <Length>0</Length> <Width>0</Width> <Height>0</Height> <Clearance>0</Clearance> <Trunk_size>0</Trunk_size> <Fuel_tank_size>0</Fuel_tank_size> <Mass>0</Mass> <Equipment_code></Equipment_code> <equipment_name></equipment_name> <Generation></Generation> <Start_year>0</Start_year> <Fuel></Fuel> <Moment></Moment> <climat_ctrl>Нет</climat_ctrl> <Speed>0</Speed> <abs>Нет</abs> <Engine></Engine> <FullMass>0</FullMass> <max_trunk_size>0</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884119</carid>
                            <VIN>YV1DZ8156F2754921</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>498</carcolorid>
                            <carcolorname>Темно-синий металлик, (Caspian Blue)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 389 130,00</price>
                            <optionprice>140 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>498</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Caspian Blue metallic</optionname>
                        <optionprice>Caspian Blue metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885257</carid>
                            <VIN>YV1DZ8156F2760059</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>614</carcolorid>
                            <carcolorname>Белый , (Ice White)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 372 030,00</price>
                            <optionprice>123 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>614</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ice White</optionname>
                        <optionprice>Ice White</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885147</carid>
                            <VIN>YV1DZ8156F2755254</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 389 130,00</price>
                            <optionprice>140 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00875011</carid>
                            <VIN>YV1DZ8156F2721584</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-81(15)</carcomplectid>
                            <carname>Volvo XC60 Ocean Race 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>706</carcolorid>
                            <carcolorname>Ocean Blue II</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 663 830,00</price>
                            <optionprice>315 400,00</optionprice>
                            <additionalequipmentprice>69 349,36</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-81(15)</Equipment_code> <equipment_name>Ocean Race</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>729</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше - серебристые</optionname>
                        <optionprice>Рейлинги продольные на крыше - серебристые</optionprice>
                        </option>
                        <option>
                        <optionId>998996</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Административная опция VOR</optionname>
                        <optionprice>Административная опция VOR</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>GV1Z</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Кожа Ocean Race Edition</optionname>
                        <optionprice>Кожа Ocean Race Edition</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>706</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Ocean Blue II</optionname>
                        <optionprice>Ocean Blue II</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>691</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный радар, передний и задний</optionname>
                        <optionprice>Парковочный радар, передний и задний</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886330</carid>
                            <VIN>YV1DZ8156F2760481</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 583 730,00</price>
                            <optionprice>225 300,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G30M</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон AGNES Кожа Спортивная</optionname>
                        <optionprice>Салон AGNES Кожа Спортивная</optionprice>
                        </option>
                        <option>
                        <optionId>MC341</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY) (MC341) (Для салонов G3XX, вариантов R-Design, VOR)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>840</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 18&#034; ZEPHYRUS</optionname>
                        <optionprice>Легкосплавные диски 18&#034; ZEPHYRUS</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885810</carid>
                            <VIN>YV1DZ8156F2759664</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 542 030,00</price>
                            <optionprice>183 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G70B</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886347</carid>
                            <VIN>YV1BZ8156F1227136</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15) SE</carcomplectid>
                            <carname>Volvo XC70 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 348 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>70 970,20</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-13(15) SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>F701</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа</optionname>
                        <optionprice>Салон кожа</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze metallic</optionname>
                        <optionprice>Twilight Bronze metallic</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885119</carid>
                            <VIN>YV1BZ8156F1230673</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-12(15)</carcomplectid>
                            <carname>Volvo XC70 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 351 950,00</price>
                            <optionprice>140 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>235</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Управление аудиосистемой на рулевом колесе</optionname>
                        <optionprice>Управление аудиосистемой на рулевом колесе</optionprice>
                        </option>
                        <option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>584</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Графическая панель приборов 8&#034; без хромированной окантовки</optionname>
                        <optionprice>Графическая панель приборов 8&#034; без хромированной окантовки</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionname>
                        <optionprice>Отделка салона &#039;CROSS AVENUE ALUMINIUM&#039;</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>800120</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка MILLED ALUMINUM</optionname>
                        <optionprice>Отделка MILLED ALUMINUM</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>F501</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>SUNDBY Текстиль/Винил</optionname>
                        <optionprice>SUNDBY Текстиль/Винил</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886325</carid>
                            <VIN>YV1DZ8156F2759844</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>700</carcolorid>
                            <carcolorname>Темно-бронзовый металлик, (Twilight bronze)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 542 030,00</price>
                            <optionprice>183 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>700</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Twilight Bronze</optionname>
                        <optionprice>Twilight Bronze</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00874105</carid>
                            <VIN>YV1DZ8156F2743943</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>019</carcolorid>
                            <carcolorname>Черный , (Black Stone)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 525 630,00</price>
                            <optionprice>167 200,00</optionprice>
                            <additionalequipmentprice>15 327,49</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>935</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Аудио Sensus Connect HIGH PERFORMANCE</optionname>
                        <optionprice>Аудио Sensus Connect HIGH PERFORMANCE</optionprice>
                        </option>
                        <option>
                        <optionId>019</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Stone</optionname>
                        <optionprice>Black Stone</optionprice>
                        </option>
                        <option>
                        <optionId>G701</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00886350</carid>
                            <VIN>YV1BZ8156F1222371</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево VOLVO</cardepartmentid>
                            <cardepartmentname>Коптево VOLVO</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC70</carfamily>
                            <carmodel>XC70 I</carmodel>
                            <carcomplectid>136-81-13(15) SE</carcomplectid>
                            <carname>Volvo XC70 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>477</carcolorid>
                            <carcolorname>Темно-серебристый металлик, (Inscription Electric Silver)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 348 000,00</price>
                            <optionprice>0,00</optionprice>
                            <additionalequipmentprice>52 934,32</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>универсал</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,80</Top100> <Fuel_consumption_city>8,60</Fuel_consumption_city> <Fuel_consumption_track>5,20</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4838</Length> <Width>1870</Width> <Height>1604</Height> <Clearance>210</Clearance> <Trunk_size>944</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1802</Mass> <Equipment_code>136-81-13(15) SE</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC70 III Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>195</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2400</FullMass> <max_trunk_size>1580</max_trunk_size> <baseoptions><option>
                        <optionId>437</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка салона под шлифованный алюминий</optionname>
                        <optionprice>Отделка салона под шлифованный алюминий</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        <option>
                        <optionId>F701</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Салон кожа</optionname>
                        <optionprice>Салон кожа</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>477</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Electric Silver metallic</optionname>
                        <optionprice>Electric Silver metallic</optionprice>
                        </option>
                        <option>
                        <optionId>MC342</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00883512</carid>
                            <VIN>YV1DZ8156F2756541</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>702</carcolorid>
                            <carcolorname>Красный металлик , (Flamenco Red)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 389 130,00</price>
                            <optionprice>140 700,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>702</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Flamenco Red metallic</optionname>
                        <optionprice>Flamenco Red metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00884127</carid>
                            <VIN>YV1DZ8156F2750309</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>true</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-12(15)</carcomplectid>
                            <carname>Volvo XC60 Momentum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>452</carcolorid>
                            <carcolorname>Черный металлик , (Black Sapphire)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 324 930,00</price>
                            <optionprice>76 500,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-12(15)</Equipment_code> <equipment_name>Momentum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>499</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Двойная выхлопная труба, интегрированная</optionname>
                        <optionprice>Двойная выхлопная труба, интегрированная</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>65</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Омыватели фар</optionname>
                        <optionprice>Омыватели фар</optionprice>
                        </option>
                        <option>
                        <optionId>452</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Black Sapphire metallic</optionname>
                        <optionprice>Black Sapphire metallic</optionprice>
                        </option>
                        <option>
                        <optionId>G601</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>DYNAMIC Текстиль/Trico Tech</optionname>
                        <optionprice>DYNAMIC Текстиль/Trico Tech</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>370</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Датчик дождя</optionname>
                        <optionprice>Датчик дождя</optionprice>
                        </option>
                        <option>
                        <optionId>871</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подогрев лобового стекла</optionname>
                        <optionprice>Подогрев лобового стекла</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto><auto>
                            <car>
                            <carid>00885239</carid>
                            <VIN>YV1DZ8156F2760434</VIN>
                            <VIN2></VIN2>
                            <vehicletype>Легковой</vehicletype>
                            <carstatus>На складе</carstatus>
                            <carstate>Новый</carstate>
                            <caryear>2015</caryear>
                            <ptsonhand>false</ptsonhand>
                            <cardepartmentid>Коптево Skoda</cardepartmentid>
                            <cardepartmentname>Коптево SKODA SEAT</cardepartmentname>
                            <caraddress>г. Москва, ул. Коптевская, д. 71</caraddress>
                            <carbrand>VOLVO</carbrand>
                            <carfamily>XC60</carfamily>
                            <carmodel>XC60</carmodel>
                            <carcomplectid>156-81-13(15)</carcomplectid>
                            <carname>Volvo XC60 Summum 2.4 дизель 4WD  AT-6 181 (Л.С.)</carname>
                            <carcolorid>712</carcolorid>
                            <carcolorname>Багровый , (RICH JAVA)</carcolorname>
                            <carcolortype>Не металлик</carcolortype>
                            <price>2 542 030,00</price>
                            <optionprice>183 600,00</optionprice>
                            <additionalequipmentprice>0,00</additionalequipmentprice>
                            <colorprice>0,00</colorprice>
                            </car>
                            <Body>кроссовер</Body> <Drive>полный</Drive> <Doors>5</Doors> <Seats>5</Seats> <Volume>2400</Volume> <Power>181</Power> <VolumeReal>2,40</VolumeReal> <Transmission>автомат</Transmission> <Transmission_speeds>6</Transmission_speeds> <fuel_type>ДТ</fuel_type> <Top100>10,20</Top100> <Fuel_consumption_city>8,50</Fuel_consumption_city> <Fuel_consumption_track>5,30</Fuel_consumption_track> <Fuel_consumption>6,40</Fuel_consumption> <Airbags>4</Airbags> <Air_con>Нет</Air_con> <Length>4644</Length> <Width>1891</Width> <Height>1713</Height> <Clearance>230</Clearance> <Trunk_size>490</Trunk_size> <Fuel_tank_size>70</Fuel_tank_size> <Mass>1886</Mass> <Equipment_code>156-81-13(15)</Equipment_code> <equipment_name>Summum</equipment_name> <Generation>XC60 I Рестайлинг</Generation> <Start_year>2013</Start_year> <Fuel>дизель</Fuel> <Moment>420/1500?–?2500</Moment> <climat_ctrl>Да</climat_ctrl> <Speed>190</Speed> <abs>Да</abs> <Engine>D4 AWD</Engine> <FullMass>2505</FullMass> <max_trunk_size>1450</max_trunk_size> <baseoptions><option>
                        <optionId>832</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Аудио High Performance</optionname>
                        <optionprice>Аудио High Performance</optionprice>
                        </option>
                        <option>
                        <optionId>850</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Отделка Dark Aluminium</optionname>
                        <optionprice>Отделка Dark Aluminium</optionprice>
                        </option>
                        <option>
                        <optionId>861</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionname>
                        <optionprice>3-хспицевое рулевое колесо, отделанное кожей с декоративными вставками</optionprice>
                        </option>
                        <option>
                        <optionId>33</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Круиз контроль</optionname>
                        <optionprice>Круиз контроль</optionprice>
                        </option>
                        <option>
                        <optionId>39</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Рейлинги продольные на крыше</optionname>
                        <optionprice>Рейлинги продольные на крыше</optionprice>
                        </option>
                        <option>
                        <optionId>47</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод водительского сиденья</optionname>
                        <optionprice>Электропривод водительского сиденья</optionprice>
                        </option>
                        <option>
                        <optionId>167</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Зеркала заднего вида с электроприводом складывания</optionname>
                        <optionprice>Зеркала заднего вида с электроприводом складывания</optionprice>
                        </option>
                        <option>
                        <optionId>529</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Задний парковочный радар</optionname>
                        <optionprice>Задний парковочный радар</optionprice>
                        </option>
                        <option>
                        <optionId>629</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Электропривод регулировки поясничной поддержки</optionname>
                        <optionprice>Электропривод регулировки поясничной поддержки</optionprice>
                        </option>
                        <option>
                        <optionId>684</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Металлическая накладка в багажном отделении</optionname>
                        <optionprice>Металлическая накладка в багажном отделении</optionprice>
                        </option>
                        <option>
                        <optionId>609</optionId>
                        <optiontype>Базовая опция</optiontype>
                        <optionname>Система контроля давления воздуха в шинах</optionname>
                        <optionprice>Система контроля давления воздуха в шинах</optionprice>
                        </option>
                        </baseoptions><additionaloptions><option>
                        <optionId>712</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Rich Java</optionname>
                        <optionprice>Rich Java</optionprice>
                        </option>
                        <option>
                        <optionId>G71T</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Салон кожа AGNES</optionname>
                        <optionprice>Салон кожа AGNES</optionprice>
                        </option>
                        <option>
                        <optionId>MC340</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ПРАКТИЧНОСТЬ (VERSATILITY)</optionname>
                        <optionprice>ПРАКТИЧНОСТЬ (VERSATILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC343</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ОБЗОР (VISIBILITY)</optionname>
                        <optionprice>ОБЗОР (VISIBILITY)</optionprice>
                        </option>
                        <option>
                        <optionId>MC347</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>ЗИМНИЙ (WINTER)</optionname>
                        <optionprice>ЗИМНИЙ (WINTER)</optionprice>
                        </option>
                        <option>
                        <optionId>273</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Парковочный подогреватель c таймером, топливный</optionname>
                        <optionprice>Парковочный подогреватель c таймером, топливный</optionprice>
                        </option>
                        <option>
                        <optionId>315</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Отделка салона натуральным светлым деревом</optionname>
                        <optionprice>Отделка салона натуральным светлым деревом</optionprice>
                        </option>
                        <option>
                        <optionId>820</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Подготовка под установку видеокамеры для облегчения парковки</optionname>
                        <optionprice>Подготовка под установку видеокамеры для облегчения парковки</optionprice>
                        </option>
                        <option>
                        <optionId>854</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Чехол для автомобиля транспортировочный</optionname>
                        <optionprice>Чехол для автомобиля транспортировочный</optionprice>
                        </option>
                        <option>
                        <optionId>885</optionId>
                        <optiontype>Дополнительная опция</optiontype>
                        <optionname>Легкосплавные диски 17&#034; VALDER</optionname>
                        <optionprice>Легкосплавные диски 17&#034; VALDER</optionprice>
                        </option>
                        </additionaloptions></auto></carsOnHand></auto-catalog>