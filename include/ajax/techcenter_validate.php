<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
require 'soap3.php';
global $USER;
    $user_id = $USER->GetID();
    
    $autoIds = array();
    $GET_USER_FIELDS = CUser::GetByID($user_id);                //get user fields!
    while($USER_FIELDS = $GET_USER_FIELDS -> fetch()){
            $autoIds[] =  $USER_FIELDS['UF_USER_AUTO'];;        
    }
    
    $brandValues    = array();
    $modelValues    = array();
    $runValues      = array();
    $bodyValues     = array();
    $transValues    = array();   
    $driveValues    = array(); 
    $powerValues    = array();
    $stateValues    = array();
    $ownerValues    = array();
    
    $depName        = array();
    
$resDepartaments = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 27), false, array("ID", "NAME"));
    while($departaments = $resDepartaments ->fetch()){
        $depName[] = $departaments['NAME'];
    }


    //get connect to WSDL
    $soapURL = 'https://212.45.28.206/TradeWebServices/TradeWebService.asmx?WSDL';
    $method = 'createOrUpdateContactStatic';
    $options = array(
        "WebContactId" => "", // всегда пусто
        "Name" => "LDF",
        "phone" => "", //пусто, если неизвестно
        "cellularPhone" => "786 25 25",
        "webContactType" => 1, // всегда 1
        "RequestTypeId" => "", // пусто, если неизвестно
        "webFormId" => "", // Тип формы
        "vin" => '', // всегда пусто
        "departmentId" => "", // всегда пусто
        "salesDepartmentId" => "", // всегда пусто
        "emplId" => "", // всегда пусто
        "carBrandId" => $_REQUEST['auto_mark'],
        "carModelId" => $_REQUEST['auto_model'], // пусто, если неизвестно
        "source" => $_SERVER ['HTTP_REFERER'],
        "location" => '',
        "cite" => $_SERVER['SERVER_NAME'],
        "comment" => '',
    );
        
        $options['Name']                = $_REQUEST['add-service-name'];
        $options['phone']               = $_REQUEST['add-service-phone'];
        $options['cellularPhone']       = $_REQUEST['add-service-phone'];
        //$options['vin']                 = $_REQUEST['add-service-vin'];
        $options['RequestTypeId']       = "Сервисный центр (ТЕСТ)";
        $options['webFormId']           = 'entry_form';
        $options['carBrandId']          = $_REQUEST['auto_mark'];
        $options['carModelId']          = $_REQUEST['auto_model']; 
        $options['comment']             = 'Гос. номер - '.$_REQUEST['add-service-number']. '. Страна - ' .$_REQUEST['country']. '. Сервисный центр - ' .$_REQUEST['center']. '. Вид работы - ' . $_REQUEST['kind_work'];
  
    try {
        $res = soap_ex($method, $options, $soapURL);
    } catch (exception $e) {
        pre($e);
    }
    
$err = array(); 
if($_REQUEST['add-service-name'] == "")
    $err['name'] = 'add-service-name';
    
if($_REQUEST['add-service-phone'] == "")
    $err['phone'] = 'add-service-phone';


$loader = $res->Str;
$err['numb'] = $loader;

echo json_encode($err);

if (!count($err)){
    $text  = "Имя:                      {$_REQUEST['add-service-name']} \n";
    $text .="№                          {$res->Str}\n";
    $text .= "Номер телефона:           {$_REQUEST['add-service-phone']}\n";
    $text .= "Марка автомобиля:         {$_REQUEST['auto_mark']}\n";
    $text .= "Модель автомобиля:        {$_REQUEST['auto_model']}\n";
    $text .= "Год автомобиля:           {$_REQUEST['auto_year']}\n";
    $text .= "Модификация автомобиля:   {$_REQUEST['auto_mod']}\n";
    $text .= "Пробег автомобиля:        {$_REQUEST['add-service-dist']}\n";
    $text .= "Гос. Номер:               {$_REQUEST['add-service-number']}\n";
    $text .= "Регион:                   {$_REQUEST['add-service-region']}\n";
    $text .= "Страна:                   {$_REQUEST['country']}\n";
    $text .= "Вид работы:               {$_REQUEST['kind_work']}\n";
    $text .= "ТО:                       {$_REQUEST['service-to']}\n";
    $text .= "Описание авто/проблем:    {$_REQUEST['auto_description']}\n";
    $text .= "Время:                    {$_REQUEST['add-service-time']}\n";
    $text .= "Дата:                     {$_REQUEST['add-service-date']}\n";
    $text .= "Адрес сервис-центра:      {$_REQUEST['center']}\n";

    $arEventFields = array(
        "TIME"  => date('H:i', time()),
        "DATE"  => date('d.m.Y'),
        "TEXT"   => $text,
        );
    CEvent::SendImmediate("FM_ENTRY_TECHCENTER", 's1', $arEventFields); //more quickly send message

} 
?>