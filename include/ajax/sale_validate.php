<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
require 'soap3.php';
global $USER;
    $user_id = $USER->GetID();
    
    $autoIds = array();
    $GET_USER_FIELDS = CUser::GetByID($user_id);                //get user fields!
    while($USER_FIELDS = $GET_USER_FIELDS -> fetch()){
            $autoIds[] =  $USER_FIELDS['UF_USER_AUTO'];;        
    }
    
    $brandValues    = array();
    $modelValues    = array();
    $runValues      = array();
    $bodyValues     = array();
    $transValues    = array();   
    $driveValues    = array(); 
    $powerValues    = array();
    $stateValues    = array();
    $ownerValues    = array();
    
    $depName        = array();
    
$resDepartaments = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 27), false, array("ID", "NAME"));
    while($departaments = $resDepartaments ->fetch()){
        $depName[] = $departaments['NAME'];
    }


    //get connect to WSDL
    $soapURL = 'https://212.45.28.206/TradeWebServices/TradeWebService.asmx?WSDL';
    $method = 'createOrUpdateContactStatic';
    $options = array(
        "WebContactId" => "", // всегда пусто
        "Name" => "LDF",
        "phone" => "", //пусто, если неизвестно
        "cellularPhone" => "786 25 25",
        "webContactType" => 1, // всегда 1
        "RequestTypeId" => "", // пусто, если неизвестно
        "webFormId" => "", // Тип формы
        "vin" => '', // всегда пусто
        "departmentId" => "", // всегда пусто
        "salesDepartmentId" => "", // всегда пусто
        "emplId" => "", // всегда пусто
        "carBrandId" => $_REQUEST['auto_mark'],
        "carModelId" => $_REQUEST['auto_model'], // пусто, если неизвестно
        "source" => $_SERVER ['HTTP_REFERER'],
        "location" => '',
        "cite" => $_SERVER['SERVER_NAME'],
        "comment" => '',
    );
        
        $options['Name']                = $_REQUEST['car-sell-name'];
        $options['phone']               = $_REQUEST['car-sell-phone'];
        $options['cellularPhone']       = $_REQUEST['car-sell-phone'];
        $options['vin']                 = $_REQUEST['car-sell-vin'];
        $options['RequestTypeId']       = "ТрейдИн (ТЕСТ)";
        $options['webFormId']           = 'sale_form';
        $options['carBrandId']          = $_REQUEST['auto_mark'];
        $options['carModelId']          = $_REQUEST['auto_model']; 
        $options['comment']             = 'Кол-во собственников - ' . $_REQUEST['auto_owners'] . '. Состояние авто - ' . $_REQUEST['auto_condition'] . '. Автосалон - ' .$_REQUEST['center'];
  
    try {
        $res = soap_ex($method, $options, $soapURL);
    } catch (exception $e) {
        pre($e);
    }
    
$err = array(); 
if($_REQUEST['car-sell-name'] == "")
    $err['name'] = 'car-sell-name';
    
if($_REQUEST['car-sell-phone'] == "")
    $err['phone'] = 'car-sell-phone';
if($_REQUEST['car-sell-vin'] == ""){
    $err['vin'] = 'car-sell-vin';
}
//
$loader = $res->Str;
$err['numb'] = $loader;
    
    
    

    if (!count($err)){
        $text  = "Номер заявки:             {$loader}\n";
        $text .= "Имя:                      {$_REQUEST['car-sell-name']}\n";
        $text .= "Номер телефона:           {$_REQUEST['car-sell-phone']}\n";
        $text .= "Адрес сервис-центра:      {$_REQUEST['center']}\n";
        $text .= "Марка автомобиля:         {$_REQUEST['auto_mark']}\n";
        $text .= "Модель автомобиля:        {$_REQUEST['auto_model']}\n";
        $text .= "Год автомобиля:           {$_REQUEST['auto_year']}\n";
        $text .= "Модификация автомобиля:   {$_REQUEST['auto_mod']}\n";
        $text .= "Пробег автомобиля:        {$_REQUEST['car-sell-mileage']}\n";
        $text .= "Двигатель(л/с):           {$_REQUEST['auto_mod']}\n";
        $text .= "Объем двигателя:          {$_REQUEST['car-sell-volume']}\n";
        $text .= "Состояние автомобиля:     {$_REQUEST['auto_condition']}\n";
        $text .= "Количество владельцев:    {$_REQUEST['auto_owners']}\n";
        $text .= "Коробка передач:          {$_REQUEST['auto_trans']}\n";
        $text .= "Привод автомобиля:        {$_REQUEST['auto_drive']}\n";
        $text .= "VIN номер автомобиля:     {$_REQUEST['car-sell-vin']}\n";

        $arEventFields = array(
            "TIME"  => date('H:i', time()),
            "DATE"  => date('d.m.Y'),
            "TEXT"   => $text,
            );
    
        CEvent::SendImmediate("FM_REQUEST_SALE", 's1', $arEventFields); //more quickly send message
    } 
    echo json_encode($err);
    //echo json_encode($mass);

?>