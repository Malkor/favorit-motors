<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
require 'soap3.php';
$DEFAULT_AUTO = array();

global $USER;
$uploaddir = './for_uploads/';

    $user_id = $USER->GetID();
    
    $autoIds = array();
    $GET_USER_FIELDS = CUser::GetByID($user_id);                //get user fields!
    while($USER_FIELDS = $GET_USER_FIELDS -> fetch()){
            $autoIds[] =  $USER_FIELDS['UF_USER_AUTO'];       
    }
    $arSelect = array("NAME", "ID", "PROPERTY_CARBRAND", "PROPERTY_CARMODEL", "PROPERTY_CARYEAR", "PROPERTY_RUN", "PROPERTY_DEFAULT", "PROPERTY_POWER", "PROPERTY_FUEL", "PROPERTY_VIN", "PROPERTY_CARID");
    $arStockAutoFields = CIBlockElement::GetList(array(), array("ID" => $autoIds[0], "PROPERTY_DEFAULT_VALUE" => 'true'), false, false, $arSelect);
        $arStockAuto = $arStockAutoFields -> fetch();
    //let get list of new auto
    $arAutoMark         = array();
    $arAutoModel        = array();
    $arAutoYear         = array();
 
    $arNewAutoFelds = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 13), false, false, $arSelect);
        while($auto = $arNewAutoFelds -> fetch()){
            $arAutoMark[]       = $auto['PROPERTY_CARBRAND_VALUE'];
            $arAutoModel[]      = $auto['PROPERTY_CARMODEL_VALUE'];
            $arAutoYear[]       = $auto['PROPERTY_CARYEAR_VALUE'];
        }   
        $arMarkUnique     = array_unique($arAutoMark); 
        $arModelUnique    = array_unique($arAutoModel); 
        $arYearUnique     = array_unique($arAutoYear);

$rsUser = CUser::GetByID($user_id);
$arUser = $rsUser->Fetch();

          

    $soapURL = 'https://212.45.28.206/TradeWebServices/TradeWebService.asmx?WSDL';
    $data = explode('::', $_POST['data']);
    $method = 'createOrUpdateContactStatic';
    $options = array(
        "WebContactId" => "", // всегда пусто
        "Name" => "LDF",
        "phone" => "", //пусто, если неизвестно
        "cellularPhone" => "786 25 25",
        "webContactType" => 1, // всегда 1
        "RequestTypeId" => "", // пусто, если неизвестно
        "webFormId" => "", // Тип формы
        "vin" => "", // всегда пусто
        "departmentId" => "", // всегда пусто
        "salesDepartmentId" => "", // всегда пусто
        "emplId" => "", // всегда пусто
        "carBrandId" => $_REQUEST['auto_mark'],
        "carModelId" =>$_REQUEST['auto_model'], // пусто, если неизвестно
        "source" => $_SERVER ['HTTP_REFERER'],
        "location" => '',
        "cite" => $_SERVER['SERVER_NAME'],
        "comment" => "",
    );
    
    
        $options['Name']                = $_POST['car-insurance-last-name-1'] . ' ' . $_POST['car-insurance-first-name-1'] . ' ' . $_POST['car-insurance-name-1'];
        $options['phone']               = $_POST['car-insurance-phone-1'];
        $options['cellularPhone']       = $_POST['car-insurance-phone-1'];
        $options['RequestTypeId']       = "Страховка (ТЕСТ)";
        $options['webFormId']           = 'insurance_form';
        $options['carBrandId']          = '';
        $options['carModelId']          = '';  
        $options['comment']             = 'Год авто -' .$_POST['auto_year']. '. Пробег -' .$_POST['car-insurance-mileage']. '. Цена -' .$_POST['car-insurance-price']. '. Название банка -' .$_POST['car-insurance-bank']. '. Название компании -' .$_POST['car-insurance-company']. '. Пол -' .$_POST['sex']. 'Кол-во детей -' .$_POST['children']. '. Опыт вождения -' .$_POST['exp']. '. КАСКО -' .$_POST['car-insurance-casko']. 'Тип КАСКО -' .$_POST['car-insurance-casko-type']. '. Адрес почты: ' .$_POST['car-insurance-email-1'];
        
       
    try {
        $res = soap_ex($method, $options, $soapURL);
        //pre(soap_ex($method, $options, $soapURL));
    } catch (exception $e) {
        pre($e);
    }


$err = array(); 
if($_REQUEST['auto_mark'] == "")
    $err['auto_mark'] = 'auto_mark';
    
/*if($_REQUEST['car-sell-phonecar-insurance-phone-1'] == "")
    $err['phone'] = 'car-insurance-phone-1';
*/

$loader = $res->Str;
$err['numb'] = $loader;
            
echo json_encode($err);   
    if (!count($err)){           
            $text  = "На какой автомобиль ориентируется пользователь:                       {$_REQUEST['car-insurance-what']} \n";
            $text .= "Марка:                                                                {$_REQUEST['auto_mark']}\n";
            $text .= "Модель:                                                               {$_REQUEST['auto_model']}\n";
            $text .= "Год:                                                                  {$_REQUEST['auto_year']}\n";
            $text .= "Пробег:                                                               {$_REQUEST['auto_year']}\n";
            $text .= "Стоимость:                                                            {$_REQUEST['car-insurance-price']}\n";
            $text .= "Вид и особенности страхования:                                        {$_REQUEST['car-insurance-casko']} с типом Страхования - {$_REQUEST['car-insurance-casko-type']} \n";
                if(!empty($_REQUEST['car-insurance-dosago'])){
                    $text .= "Сумма для ДОСАГО:                                                 {$_REQUEST['car-insurance-dosago-price']}\n";    
                }
            $text .= "Данные об оплате автомобиля:                                          {$_REQUEST['car-insurance-car-price']}\n";
            $text .= "Название банка:                                                       {$_REQUEST['car-insurance-bank']}\n";
            $text .= "Название компании:                                                    {$_REQUEST['car-insurance-company']}\n";
                if ($USER->IsAuthorized()){
                    $text .= "Фамилия:                                                          {$arUser['LAST_NAME']}\n";
                    $text .= "Имя:                                                              {$arUser['NAME']}\n";
                    $text .= "Отчество:                                                         {$arUser['SECOND_NAME']}\n";
                    $text .= "Телефон:                                                          {$arUser['PERSONAL_MOBILE']}\n";
                    $text .= "E-Mail:                                                           {$arUser['EMAIL']}\n";
                    $text .= "Пол:                                                              {$_REQUEST['sex']}\n";
                    $text .= "Опыт:                                                             {$_REQUEST['exp']}\n";
                    $text .= "Статус:                                                           {$_REQUEST['status']}\n";
                    $text .= "Количество детей:                                                 {$_REQUEST['children']}\n";
                } else {
                    $text .= "Фамилия:                                                          {$_REQUEST['car-insurance-last-name-1']}\n";
                    $text .= "Имя:                                                              {$_REQUEST['car-insurance-first-name-1']}\n";
                    $text .= "Отчество:                                                         {$_REQUEST['car-insurance-name-1']}\n";
                    $text .= "Телефон:                                                          {$_REQUEST['car-insurance-phone-1']}\n";
                    $text .= "E-Mail:                                                           {$_REQUEST['car-insurance-email-1']}\n";
                    $text .= "Пол:                                                              {$_REQUEST['sex']}\n";
                    $text .= "Опыт:                                                             {$_REQUEST['exp']}\n";
                    $text .= "Статус:                                                           {$_REQUEST['status']}\n";
                    $text .= "Количество детей:                                                 {$_REQUEST['children']}\n";
                }
        
            $arEventFields = array(
                "TIME"  => date('H:i', time()),
                "DATE"  => date('d.m.Y'),
                "TEXT"   => $text,
                );
        
            CEvent::SendImmediate("FM_INSURANCE", 's1', $arEventFields); //more quickly send message
            
        }

 ?>      
