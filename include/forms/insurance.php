<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
//require 'soap3.php';
global $USER;
    $user_id = $USER->GetID();
    
    $autoIds = array();
    $GET_USER_FIELDS = CUser::GetByID($user_id);                //get user fields!
    while($USER_FIELDS = $GET_USER_FIELDS -> fetch()){
            $autoIds[] =  $USER_FIELDS['UF_USER_AUTO'];;        
    }
    
    $brandValues    = array();
    $modelValues    = array();
    $runValues      = array();
    $bodyValues     = array();
    $transValues    = array();   
    $driveValues    = array(); 
    $powerValues    = array();
    $stateValues    = array();
    $ownerValues    = array();
    $yearValues     = array();
    $depName        = array();
    
$resDepartaments = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 27), false, array("ID", "NAME"));
    while($departaments = $resDepartaments ->fetch()){
        $depName[] = $departaments['NAME'];
    }
    
    $arSelect = array(  "NAME",
                        "ID", 
                        "PROPERTY_CARBRAND",
                        "PROPERTY_CARMODEL",
                        "PROPERTY_CARSTATE",
                        "PROPERTY_TRANSMISSION",
                        "PROPERTY_RUN",
                        "PROPERTY_BODY",
                        "PROPERTY_CARSTATE",
                        "PROPERTY_POWER",
                        "PROPERTY_VIN",
                        "PROPERTY_DRIVE",
                        "PROPERTY_CARYEAR",
                        "PROPERTY_OWNERS"
                    );
                    
        $arAutoFields = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 28), false, false, $arSelect);
        //shit coding
            while($resSaleAuto = $arAutoFields -> Fetch()){
                
                $brandValues[]  = $resSaleAuto['PROPERTY_CARBRAND_VALUE']; 
                $modelValues[]  = $resSaleAuto['PROPERTY_CARMODEL_VALUE'];  
                $bodyValues[]   = $resSaleAuto['PROPERTY_BODY_VALUE'];
                $driveValues[]  = $resSaleAuto['PROPERTY_DRIVE_VALUE'];
                $transValues[]  = $resSaleAuto['PROPERTY_TRANSMISSION_VALUE'];
                $powerValues[]  = $resSaleAuto['PROPERTY_POWER_VALUE'];
                $stateValues[]  = $resSaleAuto['PROPERTY_CARSTATE_VALUE'];
                $ownerValues[]  = $resSaleAuto['PROPERTY_OWNERS_VALUE'];
                $yearValues[]  = $resSaleAuto['PROPERTY_CARYEAR_VALUE'];
            }
        
        $brand      = array_unique($brandValues);
        $body       = array_unique($bodyValues);
        $drive      = array_unique($driveValues);
        $trans      = array_unique($transValues);
        $power      = array_unique($powerValues);
        $state      = array_unique($stateValues);
        $owners     = array_unique($ownerValues);
        $year       = array_unique($yearValues);

/**
If You see tv, dv etc it's second letter is Value
*/

            $select_mark = '';
            foreach($brand as $bn){
                $select_mark .='<option value="' . $bn . '">' . $bn . '</option>';
            }
            $select_models = '';
            foreach($modelValues as $mn){
                $select_models .='<option value="' . $mn . '">' . $mn . '</option>';
            }
            $select_body = '';
            foreach($body as $bv){
                //pre($bv);
                $select_body .='<option value="' . $bv . '">' . $bv . '</option>';
            }
            $select_drive = '';
            foreach($drive as $dv){
                //pre($bv);
                $select_drive .='<option value="' . $dv . '">' . $dv . '</option>';
            }
            $select_trans = '';
            foreach($trans as $tv){
                //pre($bv);
                $select_trans .='<option value="' . $tv . '">' . $tv . '</option>';
            }
            $select_power = '';
            foreach($power as $pv){
                //pre($bv);
                $select_power .='<option value="' . $pv . '">' . $pv . '</option>';
            }
            $select_state = '';
            foreach($state as $sv){
                //pre($bv);
                $select_state .='<option value="' . $sv . '">' . $sv . '</option>';
            }
            $select_owners = '';
            foreach($owners as $ov){
                //pre($bv);
                $select_owners .='<option value="' . $ov . '">' . $ov . '</option>';
            }
            $select_years = '';
            foreach($year as $yv){
                //pre($bv);
                $select_years .='<option value="' . $yv . '">' . $yv . '</option>';
            }
?>
<style>
.none{display: none;}
</style>



                    <form method="post" id="insurance" class="popup-form action-popup" enctype="multipart/form-data">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Заявка на расчёт Страхования</h2>
                            </div>
                            <div class="action-line">
                                <ul class="action-line-list clearfix" data-block="1">
                                    <li class="animate-custom action-line-1 active">1</li>
                                    <li class="animate-custom action-line-2">2</li>
                                    <li class="animate-custom action-line-3">3</li>
                                    <li class="animate-custom action-line-4">4</li>
                                    <li class="animate-custom action-line-5">5</li>
                                </ul>
                            </div>
                            <div class="popup-action-1 popup-action active">
                                <div class="title">
                                    <h2 style="margin: 0 -10px 0 0;">На какой автомобиль Вы хотите рассчитать страховку?</h2>
                                </div>
                                <div class="form-items">
                                <?if(!empty($arStockAuto['PROPERTY_DEFAULT_VALUE'])):?>
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" value="<?=$arStockAuto['NAME']?>" id="car-insurance-what-1" /><label for="car-insurance-what-1">На выбранный автомобиль: <?=$arStockAuto['NAME']?> <?=$arStockAuto['PROPERTY_FUEL_VALUE']?> (<?=$arStockAuto['PROPERTY_POWER_VALUE']?> л.с.)</label>
                                    </div>
                                <?endif;?>
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" value="Новый автомобиль, купленный в салоне FAVORIT MOTORS" id="car-insurance-what-2" /><label for="car-insurance-what-2">Новый автомобиль, купленный в салоне FAVORIT MOTORS</label>
                                    </div>
                                    <div class="form-item clearfix">
                                        <input type="radio" name="car-insurance-what" value="На другой автомобиль" id="car-insurance-what-3" /><label for="car-insurance-what-3">На другой автомобиль</label>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-2 popup-action">
                                <div class="title">
                                    <h2>Данные о вашем автомобиле</h2>
                                </div>
                                    <?
                                    if(!empty($arStockAuto['PROPERTY_DEFAULT_VALUE'])):
                                    ?>
                                        <div class="car-sell-line clearfix">
                                            <div class="form-item">
                                                <label for="car-insurance-mark">Марка:</label>
                                                <select name="auto_mark" id="car-insurance-mark">
                                                    <option value="<?=$arStockAuto['PROPERTY_CARBRAND_VALUE']?>"><?=$arStockAuto['PROPERTY_CARBRAND_VALUE']?></option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="car-insurance-model">Модель:</label>
                                                <select name="auto_model" id="car-insurance-model">
                                                    <option value="<?=$arStockAuto['PROPERTY_CARMODEL_VALUE']?>"><?=$arStockAuto['PROPERTY_CARMODEL_VALUE']?></option>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="car-insurance-year">Год:</label>
                                                <select name="auto_year" id="car-insurance-year">
                                                    <option value="2000">2000</option>
                                                    <option value="2001">2001</option>
                                                    <option value="2002">2002</option>
                                                    <option value="2003">2003</option>
                                                    <option value="2004">2004</option>
                                                    <option value="2005">2005</option>
                                                    <option value="2006">2006</option>
                                                    <option value="2007">2007</option>
                                                    <option value="2008">2008</option>
                                                    <option value="2009">2009</option>
                                                    <option value="2010">2010</option>
                                                    <option value="2011">2011</option>
                                                    <option value="2012">2012</option>
                                                    <option value="2013">2013</option>
                                                    <option value="2014">2014</option>
                                                </select>
                                            </div>
                                        </div>
                                    <?else:?>
                                        <div class="car-sell-line clearfix">
                                            <div class="form-item">
                                                <label for="car-insurance-mark">Марка:</label>
                                                <select name="auto_mark" id="car-insurance-mark" >
                                                    <option>Выбрать</option>
                                                    <?=$select_mark;?>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="car-insurance-model">Модель:</label>
                                                <select name="auto_model" id="car-insurance-model">
                                                    <option>Выбрать</option>
                                                    <?=$select_models;?>
                                                </select>
                                            </div>
                                            <div class="form-item">
                                                <label for="car-insurance-year">Год:</label>
                                                <select name="auto_year" id="car-insurance-year">
                                                    <option>Год</option>
                                                    <?=$select_years?>
                                                </select>
                                            </div>
                                        </div>
                                    <?endif;?>
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="car-insurance-mileage">Пробег:</label>
                                            <input type="text" value="<?=$_POST['mileage']?>" name="car-insurance-mileage" id="car-insurance-mileage" placeholder="км" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="car-insurance-price">Стоимость:</label>
                                            <input type="text" value="<?=$_POST['price']?>" name="car-insurance-price" id="car-insurance-price" placeholder="рублей" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-3 popup-action">
                                <div class="title">
                                    <h2>Вид и особенности страхования</h2>
                                </div>
                                <div class="three-form-items clearfix">
                                    <div class="form-item on-left clearfix">
                                        <input type="checkbox" value="Полное КАСКО" name="car-insurance-casko" id="car-insurance-casko" /><label for="car-insurance-casko">Полное КАСКО</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" value="Только УГОН" name="car-insurance-casko-type" id="car-insurance-casko-type-1" /><label for="car-insurance-casko-type-1">Только УГОН</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" value="Только УЩЕРБ" name="car-insurance-casko-type" id="car-insurance-casko-type-2" /><label for="car-insurance-casko-type-2">Только УЩЕРБ</label>
                                    </div>
                                </div>
                                <div class="three-form-items clearfix">
                                    <div class="form-item on-left clearfix">
                                        <input type="checkbox" value="ОСАГО" name="car-insurance-osago" id="car-insurance-osago" /><label for="car-insurance-osago">ОСАГО</label>
                                    </div>
                                    <div class="form-item on-left clearfix">
                                        <input type="radio" value="ДОСАГО на сумму" name="car-insurance-dosago" id="car-insurance-dosago" /><label for="car-insurance-dosago">ДОСАГО на сумму:</label>
                                    </div>
                                    <div class="form-item on-left">
                                        <input type="text" value="<?=$_POST['dosago_price']?>" name="car-insurance-dosago-price" id="car-insurance-dosago-price" placeholder="Укажите сумму, руб." /><label for="car-insurance-dosago-price"></label>
                                    </div>
                                </div>
                                <div class="form-items clearfix">
                                    <div class="on-left">
                                        <div class="form-item clearfix">
                                            <input type="radio" value="Автомобиль оплачен полностью" name="car-insurance-car-price" id="car-insurance-car-price-1" /><label for="car-insurance-car-price-1">Автомобиль оплачен полностью</label>
                                        </div>
                                        <div class="form-item clearfix">
                                            <input type="radio" value="Автомобиль куплен в кредит" name="car-insurance-car-price" id="car-insurance-car-price-2" /><label for="car-insurance-car-price-2">Автомобиль куплен в кредит</label>
                                        </div>
                                        <div class="form-item clearfix">
                                            <input type="radio" value="Автомобиль взят в лизинг" name="car-insurance-car-price" id="car-insurance-car-price-3" /><label for="car-insurance-car-price-3">Автомобиль взят в лизинг</label>
                                        </div>
                                    </div>
                                    <div class="on-right">
                                        <div class="form-item">
                                            <input type="text" value="<?=$_POST['bank_name']?>" name="car-insurance-bank" id="car-insurance-bank" placeholder="Название банка" /><label for="car-insurance-bank"></label>
                                        </div>
                                        <div class="form-item">
                                            <input type="text" value="<?=$_POST['company_name']?>" name="car-insurance-company" id="car-insurance-company" placeholder="Название компании" /><label for="car-insurance-company"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-4 popup-action">
                                <div class="title">
                                    <h2>Данные о владельце</h2>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="car-insurance-last-name-1">Фамилия:</label>
                                        <input value="<?=$_POST['last_name']?>" type="text" name="car-insurance-last-name-1" id="car-insurance-last-name-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-first-name-1">Имя:</label>
                                        <input value="<?=$_POST['name']?>" type="text" name="car-insurance-first-name-1" id="car-insurance-first-name-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-name-1">Отчество:</label>
                                        <input value="<?=$_POST['second_name']?>" type="text" name="car-insurance-name-1" id="car-insurance-name-1" />
                                    </div>
                                </div>
                                <div class="clearfix three-form-items">
                                    <div class="form-item on-left">
                                        <label for="car-insurance-old-1">Возраст:</label>
                                        <input  value="<?=$_POST['age']?>" type="text" name="car-insurance-old-1" id="car-insurance-old-1" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-phone-1">Телефон:</label>
                                        <input value="<?=$_POST['phone']?>" type="text" name="car-insurance-phone-1" id="car-insurance-phone-1" class="phone-mask" />
                                    </div>
                                    <div class="form-item on-left">
                                        <label for="car-insurance-email-1">Email:</label>
                                        <input value="<?=$_POST['mail']?>" type="text" name="car-insurance-email-1" id="car-insurance-email-1" class="email-mask" />
                                    </div>
                                </div>
                                <div class="clearfix car-insurance-line">
                                    <div class="form-item">
                                        <label for="car-insurance-sex">Ваш пол:</label>
                                        <select name="sex" id="car-insurance-sex">
                                            <option>Выбрать</option>
                                            <option value="Мужской">Мужской</option>
                                            <option value="Женский">Женский</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-experience">Стаж вождения:</label>
                                        <select name="exp" id="car-insurance-experience">
                                            <option>Выбрать</option>
                                            <option value="до 5 лет">до 5 лет</option>
                                            <option value="больше 5 лет">больше 5 лет</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-status">Семейное пол.:</label>
                                        <select name="status" id="car-insurance-status">
                                            <option></option>Выбрать</option>
                                            <option value="Холост">Холост</option>
                                            <option value="Женат">Женат</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="car-insurance-children">Дети:</label>
                                        <select name="children" id="car-insurance-children">
                                            <option>Выбрать</option>
                                            <option value="Нет детей">Нет детей</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="больше 3">больше 3</option>
                                        </select>
                                    </div>
                                </div>
                                <!--div class="add-driver-line"><i class="fa fa-plus"></i><a href="#">Добавить водителя</a></div-->
                            </div>
                            <div class="popup-action-5 popup-action">
                                <div class="title">
                                    <h2>Загрузите сканы документов</h2>
                                </div>
                                <div class="car-insurance-upload">
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i><input type="file" name="userfile[]" /> Загрузить скан ПАСПОРТА – все страницы </a></div>
                                    
                                    
                                    
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ПТС – все страницы</a></div>
                                    <div class="car-insurance-upload-link"><a href="#" class="animate-custom"><i class="fa fa-upload"></i> Загрузить скан ВОДИТЕЛЬСКОГО УДОСТОВЕРЕНИЯ – с двух сторон</a></div>
                                    <div class="car-insurance-upload-desc">Файлы можно загружать в форматах: JPEG, PNG, PDF.<br/>Скан желательно делать в разрешении: 2000х2000 рх.<br/>Размер одного файла не должен превышать: 2 mb</div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom gray-small-link grayer prev">Назад</a></div>
                                <div class="on-right">
                                    <a id="next" class="animate-custom red-small-link next active">Далее</a>
                                    <input type="submit" onclick="CheckInsForm(); return false;"  name="car-insurance-submit" id="car-insurance-submit" class="animate-custom red-small-link" value="Отправить данные" />
                                </div>
                                <div class="block"></div>
                            </div>
                        </div>
                    </form>
                <form class="popup-form none" id="thanks">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Спасибо!</h2>
                            </div>
                            <div class="form-item">
                                <p>Ваша заявка <span class="red-text">№ </span> <span id="numb_order"></span><br/> принята!</p>
                                <p class="text-body">Чтобы получить больше скидок и экономить время, зарегистрируйтесь в личном кабитене клиента  FAVORIT-MOTORS</p>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                            <?if(!$USER->IsAuthorized()):?>
                                <div class="on-left"><a class="animate-custom red-small-link active" href="/personal/register.php">Регистрация</a></div>
                            <?endif;?>
                                <div class="on-right"><a class="animate-custom grayer gray-small-link close-popup active">Закрыть</a></div>
                            </div>
                        </div>
                </form>
                    
<?//pre($_REQUEST);?>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    function CheckInsForm(){  
       $.ajax({
            type: "POST",                                       
            url: "/include/ajax/insurance_validate.php",     
            data: $('#insurance').serialize(),                    
            dataType:"json", 
            success: function(data){
                err = false; 
                if(!err) {
                    console.log(data);
                    $('#numb_order').html(data['numb']);
                    $('#thanks').removeClass('none');
                    //$('#insurance').addClass('none');
               } 
             }
          }); 
    }
</script>   
