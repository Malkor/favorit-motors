<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 

$filter = Array(
       "ACTIVE" => "Y",
       "LOGIN" => $_REQUEST['user-email'],
   );
   $arSel = array(
      "ID", "LOGIN", "PERSONAL_MOBILE", "PASSWORD"
   );
   $rsUsers = CUser::GetList(($by="name"), ($order="asc"), $filter, array("FIELDS"=>$arSel)); // выбираем пользователей
   $rsUsers->NavStart(5); // разбиваем постранично по 5 записей
   $rsUsers->bShowAll = false;
    $arUsersLogin = array();   
    $arUsersPhone = array();
    $arUsersID    =  array();
   while($arr = $rsUsers->GetNext()) :
      //pre($arr);
      $arUsersLogin[]   = $arr['LOGIN'];
      $arUsersPhone[]   = $arr['PERSONAL_MOBILE'];
      $arUsersID[]      = $arr['ID'];
   endwhile;
   
$RANDPASS = generateRandomString();

    if(!empty($_POST['user-email']) or !empty($_POST['user-phone'])){
        
        $user = new CUser;
        $fields = Array(
          "EMAIL"             => $_REQUEST['user-email'],
          "LOGIN"             => $_REQUEST['user-email'],
          "PERSONAL_MOBILE"   => $arUsersPhone,
          "LID"               => "s1",
          "ACTIVE"            => "Y",
          "GROUP_ID"          => array(3,5),
          "PASSWORD"          => $RANDPASS,
          "CONFIRM_PASSWORD"  => $RANDPASS,
        );
    $user->Update($ID, $fields);  
        $text  = "Ваш логин:                    {$_POST['[user-email']} \n";
        $text .= "Ваш новый пароль:             {$RANDPASS}\n";
        
        $arEventFields = array(
            "TIME"  => date('H:i', time()),
            "DATE"  => date('d.m.Y'),
            "TEXT"   => $text,
            );     
        CEvent::SendImmediate("FM_FORGOT_PASS", 's1', $arEventFields); //more quickly send message  
    } /*else {
        echo "Такого пользователя не существует!";
    }*/
//pre($_REQUEST);
?>





<section id="content">
        <div class="login-block">
            <div class="box-container">

                <form method="post" id="login" class="on-center">
                    <div class="title on-center">
                        <h1>Восстановление пароля</h1>
                    </div>
                    <div class="form-item">
                        <label for="user-email" class="align-right">Ваш e-mail</label>
                        <input type="text" value="<?=$_POST['user-email']?>" name="user-email" id="user-email" placeholder="email@site.com" class="email-mask" required />
                        <div class="login-link align-left">
                            <a href="/personal/register.php" class="animate-custom red-text">Зарегистрироваться</a>
                        </div>
                    </div>
                    <div class="form-item with-or gray-text">или</div>
                    <div class="form-item">
                        <label for="user-phone" class="align-right">Телефон</label>
                        <input type="text" <?=$_POST['user-phone']?> name="user-phone" id="user-phone" class="phone-mask" />
                        <div></div>
                    </div>
                    <div class="form-item">
                        <input type="submit" class="animate-custom red-big-link" value="Восстановить" />
                    </div>
                    <div class="back-button">
                        <a href="/personal/login.php"><i class="fa fa-angle-left"></i>Вход</a>
                    </div>
                </form>

            </div>
        </div>

    </section>