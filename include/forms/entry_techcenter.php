<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");
//require 'soap3.php';
global $USER;
    $user_id = $USER->GetID();
   
    $autoIds = array();
    $GET_USER_FIELDS = CUser::GetByID($user_id);                //get user fields!
    while($USER_FIELDS = $GET_USER_FIELDS -> fetch()){
            $autoIds[] =  $USER_FIELDS['UF_USER_AUTO'];;        
    }
   
    $brandValues    = array();
    $modelValues    = array();
    $runValues      = array();
    $bodyValues     = array();
    $transValues    = array();   
    $driveValues    = array(); 
    $powerValues    = array();
    $stateValues    = array();
    $ownerValues    = array();
    
    $depName        = array();
    
$resDepartaments = CIBlockSection::GetList(array(), array("IBLOCK_ID" => 27), false, array("ID", "NAME"));
    while($departaments = $resDepartaments ->fetch()){
        $depName[] = $departaments['NAME'];
    }
    
    $arSelect = array(  "NAME",
                        "ID", 
                        "PROPERTY_CARBRAND",
                        "PROPERTY_CARMODEL",
                        "PROPERTY_CARSTATE",
                        "PROPERTY_TRANSMISSION",
                        "PROPERTY_RUN",
                        "PROPERTY_BODY",
                        "PROPERTY_CARSTATE",
                        "PROPERTY_POWER",
                        "PROPERTY_VIN",
                        "PROPERTY_DRIVE",
                        "PROPERTY_CARYEAR",
                        "PROPERTY_OWNERS"
                    );
                    
        $arAutoFields = CIBlockElement::GetList(array(), array( "IBLOCK_ID" => 28, 
                                                                "ID" => $autoIds[0], 
                                                                "PROPERTY_CARBRAND_VALUE" => $_REQUEST['auto_mark'], 
                                                                "PROPERTY_CARMODEL_VALUE" => $_REQUEST['auto_model'], 
                                                                "PROPERTY_BODY_VALUE" => $_REQUEST['auto_mod']), 
                                                                false, false, $arSelect);
        //shit coding
            while($resSaleAuto = $arAutoFields -> Fetch()){
                $brandValues[]  = $resSaleAuto['PROPERTY_CARBRAND_VALUE']; 
                $modelValues[]  = $resSaleAuto['PROPERTY_CARMODEL_VALUE'];  
                $bodyValues[]   = $resSaleAuto['PROPERTY_BODY_VALUE'];
                $driveValues[]  = $resSaleAuto['PROPERTY_DRIVE_VALUE'];
                $transValues[]  = $resSaleAuto['PROPERTY_TRANSMISSION_VALUE'];
                $powerValues[]  = $resSaleAuto['PROPERTY_POWER_VALUE'];
                $stateValues[]  = $resSaleAuto['PROPERTY_CARSTATE_VALUE'];
                $ownerValues[]  = $resSaleAuto['PROPERTY_OWNERS_VALUE'];
            }
       
        $brand      = array_unique($brandValues);
        $body       = array_unique($bodyValues);
        $drive      = array_unique($driveValues);
        $trans      = array_unique($transValues);
        $power      = array_unique($powerValues);
        $state      = array_unique($stateValues);
        $owners     = array_unique($ownerValues);

            $select_mark = '';
            foreach($brand as $bn){
                $select_mark .='<option value="' . $bn . '">' . $bn . '</option>';
            }
            $select_models = '';
            foreach($modelValues as $mn){
                $select_models .='<option value="' . $mn . '">' . $mn . '</option>';
            }
            $select_body = '';
            foreach($body as $bv){
                $select_body .='<option value="' . $bv . '">' . $bv . '</option>';
            }
            $select_drive = '';
            foreach($drive as $dv){
                $select_drive .='<option value="' . $dv . '">' . $dv . '</option>';
            }
            $select_trans = '';
            foreach($trans as $tv){
                $select_trans .='<option value="' . $tv . '">' . $tv . '</option>';
            }
            $select_power = '';
            foreach($power as $pv){
                $select_power .='<option value="' . $pv . '">' . $pv . '</option>';
            }
            $select_state = '';
            foreach($state as $sv){
                $select_state .='<option value="' . $sv . '">' . $sv . '</option>';
            }
            $select_owners = '';
            foreach($owners as $ov){
                $select_owners .='<option value="' . $ov . '">' . $ov . '</option>';
            }
            
            

?>
<style>
.none{display: none;}
</style>


                    <form name="entry_tech" id="entry_tech" method="post" class="popup-form action-popup">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Запись в сервисный центр</h2>
                            </div>
                            <div class="action-line">
                                <ul class="action-line-list clearfix" data-block="1">
                                    <li class="animate-custom action-line-1 active">1</li>
                                    <li class="animate-custom action-line-2">2</li>
                                    <li class="animate-custom action-line-3">3</li>
                                    <li class="animate-custom action-line-4">4</li>
                                    <li class="animate-custom action-line-5">5</li>
                                </ul>
                            </div>
                            <div class="popup-action-1 popup-action active">
                                <div class="title">
                                    <h2>Запишитесь самостоятельно - получите скидку!</h2>
                                </div>
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="add-service-name">Ваше имя:</label>
                                            <input type="text" value="<?=$_POST['NAME'];?>" name="add-service-name" id="add-service-name" />
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="add-service-phone">Ваш телефон:</label>
                                            <input type="text" value="<?=$_POST['PHONE'];?>" name="add-service-phone" id="add-service-phone" class="phone-mask" />
                                        </div>
                                    </div>
                                </div>
                                <div class="agreement-checkbox-container"><a href="#">Запишите меня по телефону</a></div>
                            </div>
                            <div class="popup-action-2 popup-action">
                                <div class="clearfix car-line-custom-widths">
                                    <div class="form-item">
                                        <label for="add-service-mark">Марка:</label>
                                        <select  name="auto_mark" id="add-service-mark">
                                            <option value="0">Выбрать</option>
                                            <?=$select_mark;?>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-model">Модель:</label>
                                        <select  name="auto_model" id="add-service-model">
                                            <option value="0">Выбрать</option>
                                            <?=$select_models;?>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-year">Год:</label>
                                        <select name="auto_year" id="add-service-year">
                                            <option>Год</option>
                                            <option value="2000">2000</option>
                                            <option value="2001">2001</option>
                                            <option value="2002">2002</option>
                                            <option value="2003">2003</option>
                                            <option value="2004">2004</option>
                                            <option value="2005">2005</option>
                                            <option value="2006">2006</option>
                                            <option value="2007">2007</option>
                                            <option value="2008">2008</option>
                                            <option value="2009">2009</option>
                                            <option value="2010">2010</option>
                                            <option value="2011">2011</option>
                                            <option value="2012">2012</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                        </select>
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-mod">Модификация:</label>
                                        <select name="auto_mod" id="add-service-mod">
                                            <option>Выбрать</option>
                                            <?=$select_body;?>
                                        </select>
                                    </div>
                                </div>
                                <!--div class="test-drive-image">
                                    <div class="test-drive-image-container"><img src="/content/models/popup-car.png" alt="model" /></div>
                                </div-->
                            </div>
                            <div class="popup-action-3 popup-action">
                                <div class="clearfix car-line-custom-widths-2">
                                    <div class="form-item">
                                        <label for="add-service-dist">Пробег:</label>
                                        <input type="text" value="<?=$_POST['AUTO_RUN']?>" name="add-service-dist" id="add-service-dist" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-number">Гос.Номер:</label>
                                        <input type="text" value="<?=$_POST['AUTO_NUMBER']?>" name="add-service-number" id="add-service-number" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-region">Регион:</label>
                                        <input type="text" value="<?=$_POST['AUTO_REGION']?>" name="add-service-region" id="add-service-region" />
                                    </div>
                                    <div class="form-item">
                                        <label for="add-service-region-sel"> </label>
                                        <select name="country" id="add-service-region-sel">
                                            <option value="RUS">RUS</option>
                                            <option value="BEL">BEL</option>
                                            <option value="EUR">EUR</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-4 popup-action">
                                <div class="clearfix form-items">
                                    <div class="form-item on-left">
                                        <label for="add-service-work">Вид работ:</label>
                                        <select name="kind_work" id="add-service-work">
                                            <option value="0">Выбрать</option>
                                            <?=$toN;?>
                                        </select>
                                    </div>
                                    <div class="form-item on-right align-left">
                                        <label for="add-service-to">Номер ТО:</label>
                                        <select name="service-to" id="add-service-to">
                                            <option>Выбрать</option>
                                            <option value="ТО 1">ТО 1</option>
                                            <option value="ТО 2">ТО 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-item full-text">
                                    <label for="add-service-desc">Описание:</label>
                                    <textarea name="auto_description" value="<?=$_POST['AUTO_DESC']?>" id="add-service-desc" placeholder="Опишите проблемы, если таковые имеются"></textarea>
                                </div>
                                <div class="agreement-checkbox-container"><strong>ТО - 30</strong>. Ориентировочная стоимость <strong>11 000</strong> р.</div>
                                <div class="agreement-checkbox-container"><a href="#" class="show-hidden-desc">Подробное описание работ</a></div>
                                <div class="add-service-to-desc">
                                    <p><strong>ТО - 30</strong>. Ориентировочная стоимость <strong>11 000</strong> р.</p>
                                    <div class="agreement-checkbox-container">
                                        <p>Контроль уровня жидкости: системы охлаждения двигателя; тормозной и привода сцепления. Визуальный осмотр: Лакокрасочного покрытия; Наружных световых приборов и индикаторов; Вакуумные трубки и шланги; Валы приводов колёс, ШРУС; Герметичность системы выпуска; Приводные ремни; Радиаторы охлаждения и кондиционера; Топливные, шланги и соединения; Трубки, шланги и соединения тормозной системы; Шланги вентиляции.</p>
                                    </div>
                                    <div class="add-service-to-desc-buttons clearfix">
                                        <div class="on-left clearfix">
                                            <div class="on-left"><a href="#" class="animate-custom red-hover"><i class="fa fa-file-pdf-o red-text"></i>Скачать PDF</a></div>
                                            <div class="on-left red-hover"><a href="#" class="animate-custom red-hover"><i class="fa fa-print red-text"></i>Распечатать</a></div>
                                        </div>
                                        <div class="on-right on-center">
                                            <a href="#" class="hide-hidden-desc gray-small-link grayer animate-custom">Закрыть</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="popup-action-5 popup-action">
                                <div class="clearfix form-items">
                                    <div class="on-left">
                                        <div class="form-item">
                                            <label for="add-service-date">Дата:</label>
                                            <input type="text" name="add-service-date" id="add-service-date" class="datepicker" />
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="on-right align-left">
                                        <div class="form-item">
                                            <label for="add-service-location">Ближайший сервисный центр:</label>
                                            <select name="center" id="add-service-location" class="reload-map">
                                                <option>Выбрать центр</option>
                                                <option value="ул. Коптевская, д. 69 А">ул. Коптевская, д. 69 А</option>
                                                <option value="Выхино, МКАД, 2 км">Выхино, МКАД, 2 км</option>
                                                <option value="ул. Б. Семеновская, д. 42">ул. Б. Семеновская, д. 42</option>
                                                <option value="м. Войковская, ул. Коптевская, д. 79-81" selected>м. Войковская, ул. Коптевская, д. 79-81</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="date-area clearfix">
                                    <div class="on-left">

                                    </div>
                                    <div class="on-right">
                                        <div class="map-container">
                                            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=kP7S117fmGML_wbYmCEsYO5sqoTHXPuW&width=240&height=120"></script>
                                        </div>
                                        <div class="address-container">м. Войковская, ул. Коптевская, д. 79-81</div>
                                    </div>
                                </div>
                                <div class="time-line clearfix">
                                    <div class="time-line-label">Время:</div>
                                    <div class="time-line-buttons clearfix">
                                        <div class="time-line-button animate-custom" data-block="morning">Утро 7:00 - 12:00</div>
                                        <div class="time-line-button animate-custom" data-block="daytime">День 12:00 - 17:00</div>
                                        <div class="time-line-button animate-custom" data-block="evening">Вечер 17:00 - 22:00</div>
                                    </div>
                                    <div class="time-line-schema time-line-schema-morning" data-block="утро">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">7:00</li>
                                            <li class="animate-custom">7:30</li>
                                            <li class="animate-custom">8:00</li>
                                            <li class="animate-custom">8:30</li>
                                            <li class="animate-custom">9:00</li>
                                            <li class="animate-custom">9:30</li>
                                            <li class="animate-custom">10:00</li>
                                            <li class="animate-custom">10:30</li>
                                            <li class="animate-custom">11:00</li>
                                            <li class="animate-custom">11:30</li>
                                        </ul>
                                    </div>
                                    <div class="time-line-schema time-line-schema-daytime" data-block="день">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">12:00</li>
                                            <li class="animate-custom">12:30</li>
                                            <li class="animate-custom">13:00</li>
                                            <li class="animate-custom">13:30</li>
                                            <li class="animate-custom">14:00</li>
                                            <li class="animate-custom">14:30</li>
                                            <li class="animate-custom">15:00</li>
                                            <li class="animate-custom">15:30</li>
                                            <li class="animate-custom">16:00</li>
                                            <li class="animate-custom">16:30</li>
                                        </ul>
                                    </div>
                                    <div class="time-line-schema time-line-schema-evening" data-block="вечер">
                                        <ul class="time-line-list clearfix">
                                            <li class="animate-custom">17:00</li>
                                            <li class="animate-custom">17:30</li>
                                            <li class="animate-custom">18:00</li>
                                            <li class="animate-custom">18:30</li>
                                            <li class="animate-custom">19:00</li>
                                            <li class="animate-custom">19:30</li>
                                            <li class="animate-custom">20:00</li>
                                            <li class="animate-custom">20:30</li>
                                            <li class="animate-custom">21:00</li>
                                            <li class="animate-custom">21:30</li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="add-service-time" id="add-service-time" />
                                    <div class="time-line-text"><span>Вы выбрали:</span><span class="time-line-text-daytime">день</span><span class="time-line-text-time">14:30</span><i class="fa fa-times animate-custom opacity-custom-hover"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom gray-small-link grayer prev">Назад</a></div>
                                <div id="but_spec" class="on-right">
                                    <a class="animate-custom red-small-link next active">Далее</a>
                                    <input type="submit" name="add-service-submit" onclick="Checker(); return false;" class="animate-custom red-small-link" value="Записаться" />
                                </div>
                                <div class="block"></div>
                            </div>
                        </div>
                    </form>
                   
                <form class="popup-form none" id="thanks">
                        <div class="popup-section">
                            <div class="title">
                                <h2>Спасибо!</h2>
                            </div>
                            <div class="form-item">
                                <p>Ваша заявка <span class="red-text">№ </span> <span id="numb_order"></span><br/> принята!</p>
                                <p class="text-body">Чтобы получить больше скидок и экономить время, зарегистрируйтесь в личном кабитене клиента  FAVORIT-MOTORS</p>
                            </div>
                        </div>
                        <div class="popup-section section-sec on-center">
                            <div class="navigation-buttons clearfix">
                                <div class="on-left"><a class="animate-custom red-small-link active" href="#">Регистрация</a></div>
                                <div class="on-right"><a class="animate-custom grayer gray-small-link close-popup active">Закрыть</a></div>
                            </div>
                        </div>
                </form>                       
                    
                    
                    
                    
                    
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    function Checker(){  
       $.ajax({
            type: "POST",                                       
            url: "/include/ajax/techcenter_validate.php",     
            data: $('#entry_tech').serialize(),                    
            dataType:"json", 
            success: function(data){
                err = false; 
                if(!err) {
                    $('#numb_order').html(data['numb']);
                    console.log(data);
                    $('#thanks').removeClass('none');
                    $('#entry_tech ').addClass('none');
               } 
             }
          }); 
    }
</script>                          
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    