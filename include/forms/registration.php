<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("iblock");

/**
create new USER - registration and send letter to mail
use random password for registration
*/

$RANDPASS = generateRandomString();

$user = new CUSER;

$arFields = Array(
  "EMAIL"             => $_REQUEST['user-email'],
  "LOGIN"             => $_REQUEST['user-email'],
  "LID"               => "s1",
  "PERSONAL_MOBILE"    => $_REQUEST['user-phone'],
  "ACTIVE"            => "Y",
  "GROUP_ID"          => array(3,5),
  "PASSWORD"          => $RANDPASS,
  "CONFIRM_PASSWORD"  => $RANDPASS,
  "UF_AUTO_VIN"       => $_REQUEST['user-vin']
);


$ID = $user->Add($arFields);
if (intval($ID) > 0){
    $text  = "Адрес электронного ящика:     {$_REQUEST['[user-email']} \n";
    $text .= "Номер телефона:               {$_REQUEST['user-phone']}\n";
    $text .= "VIN-код:                      {$_REQUEST['user-vin']}\n";
    $text .= "Временный пароль:             {$RANDPASS}\n";
    
    
    $arEventFields = array(
        "TIME"  => date('H:i', time()),
        "DATE"  => date('d.m.Y'),
        "TEXT"   => $text,
        );

    CEvent::SendImmediate("FM_REGISTRATION_EVENT", 's1', $arEventFields); //more quickly send message
}
?>




    <section id="content">
        <div class="login-block">
            <div class="box-container">
                <form method="post" id="login" class="on-center">
                    <div class="title on-center">
                        <h1>Регистрация</h1>
                    </div>
                    <div class="form-item">
                        <label for="user-email" class="align-right">Ваш e-mail</label>
                        <input type="text" value="<?=$_POST['mail'];?>" name="user-email" id="user-email" placeholder="email@site.com" class="email-mask" required=""/>
                        <div></div>
                    </div>
                    <div class="form-item with-or gray-text"></div>
                    <div class="form-item">
                        <label for="user-phone" class="align-right">Телефон</label>
                        <input type="text" <?=$_POST['phone'];?> name="user-phone" id="user-phone" class="phone-mask" />
                        <div></div>
                    </div>
                    <div class="form-item with-or gray-text"></div>
                    <div class="form-item">
                        <label for="user-vin" class="align-right">VIN-код</label>
                        <input type="text" <?=$_POST['vin'];?> name="user-vin" id="user-vin" placeholder="VIN-код" required="" />
                        <div></div>
                    </div>
                    <div class="form-item">
                        <input type="submit" class="animate-custom red-big-link" onclick="CheckContactForm(); return false;" value="Зарегистрироваться" />
                    </div>
                    <div class="back-button">
                        <a href="/personal/login.php"><i class="fa fa-angle-left"></i>Вход</a>
                    </div>
                    <div class="description-text">Проблемы с регистрацией?   8 (495) 786 25 25   Ежедневно: пн-пт 09:00 – 20:00</div>
                </form>
            </div>
        </div>

    </section>