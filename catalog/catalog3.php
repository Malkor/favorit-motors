<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Авто за заказ");
$APPLICATION->SetTitle("Автомобили в наличии");
?>

<div class="top-bar">
    <div class="top-bar-container">
        <div class="logo"></div>
        <div class="menu-wrapper">
            <ul id="jMenu">
                <li><a class="active">Автомобили</a>
                    <ul>
                        <li><a>Авто в наличии</a></li>
                        <li><a>Подержанные авто</a></li>
                        <li><a>На заказ</a></li>
                        <li><a>Коммерческий транспорт</a>
                            <ul>
                                <li><a>Citroen</a></li>
                                <li><a>Peugeot</a></li>
                                <li><a>Volvo</a></li>
                                <li><a>Kia</a></li>
                            </ul>
                        </li>
                        <li><a>Наши марки</a>
                            <ul>
                                <li><a>Citroen</a></li>
                                <li><a>Peugeot</a></li>
                                <li><a>Volvo</a></li>
                                <li><a>Kia</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a>Технический центр</a></li>
                <li><a>Услуги</a></li>
                <li><a>Контакты</a></li>

            </ul>
        </div>
        <div class="promotions-block">
            <div class="content">Акции</div>
        </div>
        <div class="info-block">Справочная<br>08:00 – 22:00</div>
        <div class="contact-block">
            <div class="content">(495) 786 25 25</div>
        </div>
    </div>
</div>

<section class="section section--grey section--catalog-top clearfix">
    <div class="wrapper wrapper--sector">
        <h1>ГК FAVORIT MOTORS</h1>
        <p>ГК FAVORIT MOTORS на сегодняшний день является официальным дилером самых известных европейских, американских и корейских автомобильных концернов. К их числу относятся: KIA, Ford, Skoda, Volvo, Peugeot, Chevrolet, Citroen, Opel, SsangYong, Seat.</p>
        <p>Мы предлагаем своим клиентам самый полный перечень услуг по сервисному обслуживанию данных марок автомобилей. К тому же мы оказываем помощь при автокредитовании, а также при страховании Вашего транспортного средства. Каждый месяц наша компания реализует около 1000 авто, а более 5500 машин проходят у нас техническое обслуживание.</p>
        <div class="cat_main_nav clearfix">
            <div class="column--small-right">
                <div class="watched-late">
                    Недавно просмотренные: [ <span class="red-text">7</span> ]
                    <div class="watched-late__pop">
                        <table class="watched-late__listtable">
                            <tr>
                                <td class="watched-late__listtable__ltd">KIA Rio</td>
                                <td class="watched-late__listtable__rtd">698 000 р.</td>
                            </tr>
                            <tr>
                                <td class="watched-late__listtable__ltd">KIA Rio</td>
                                <td class="watched-late__listtable__rtd">698 000 р.</td>
                            </tr>
                            <tr>
                                <td class="watched-late__listtable__ltd">KIA Rio</td>
                                <td class="watched-late__listtable__rtd">698 000 р.</td>
                            </tr>
                        </table>
                        <a class="watched-late__show-all-link" href="#">Показать все</a>
                    </div>
                </div>
            </div>
            <div class="column--wide-left">
                <nav class="cat_nav"><!--
                    --><a href="catalog1.php" class="cat_nav__link">Автомобили в наличии</a><!--
                    --><a href="catalog2.php" class="cat_nav__link">Подержанные автомобили</a><!--
                    --><a href="catalog3.php" class="cat_nav__link cat_nav__link--last cat_nav__link--active">Авто на заказ</a><!--
             --></nav>
            </div>
        </div>
    </div>
</section>

<section class="section section--shadow section--catalog-logos clearfix">
    <div class="wrapper wrapper--sector">
        <div class="column--wide-left">
            <div class="logos">
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/chevrolet.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/citroen.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/skoda.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/seat.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/ford.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/volvo.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/kia.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/peugeot.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/ssangyong.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <img class="logo-item__img" src="images/logos/opel.png" alt=""/>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--sedan logo-item__type--active">Седан</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--hatchback">Хэтчбэк</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--universal">Универсал</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--crossover">Кроссовер</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--vnedoroz">Внедорожник</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--kupe">Купе</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--cabriolet">Кабриолет</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--pickup">Пикап</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--minivan">Минивэн</div>
                </div>
                <div class="logo-item">
                    <div class="valigner"></div>
                    <div class="logo-item__type logo-item__type--commerce">Ком. транс.</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--shadow section--catalog-cats-previews clearfix">
    <div class="wrapper wrapper--sector">
        <div class="column--small-right">
            <div class="sidebar sidebar--new">
                <div class="big-filter">
                    <div class="widget">
                        <div class="widget__title widget__title--clickable">Марки</div>
                        <div class="widget__allcontent">
                            <div class="widget__content">

                                <div class="marks-container">

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--citroen"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark1" id="mark1" name="mark[]" />
                                            <label for="mark1"></label>
                                        </div>
                                        <span data-input="mark1" class="big-filter__checkbox-label--widget">Citroen</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--chevrolet"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark2" id="mark2" name="mark[]" />
                                            <label for="mark2"></label>
                                        </div>
                                        <span data-input="mark2" class="big-filter__checkbox-label--widget">Chevrolet</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--ford"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark3" id="mark3" name="mark[]" />
                                            <label for="mark3"></label>
                                        </div>
                                        <span data-input="mark3" class="big-filter__checkbox-label--widget">Ford</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--kia"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark4" id="mark4" name="mark[]" />
                                            <label for="mark4"></label>
                                        </div>
                                        <span data-input="mark4" class="big-filter__checkbox-label--widget">Kia</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--opel"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark5" id="mark5" name="mark[]" />
                                            <label for="mark5"></label>
                                        </div>
                                        <span data-input="mark5" class="big-filter__checkbox-label--widget">Opel</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--peugeot"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark6" id="mark6" name="mark[]" />
                                            <label for="mark6"></label>
                                        </div>
                                        <span data-input="mark6" class="big-filter__checkbox-label--widget">Peugeot</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--seat"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark7" id="mark7" name="mark[]" />
                                            <label for="mark7"></label>
                                        </div>
                                        <span data-input="mark7" class="big-filter__checkbox-label--widget">Seat</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--ssangyong"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark8" id="mark8" name="mark[]" />
                                            <label for="mark8"></label>
                                        </div>
                                        <span data-input="mark8" class="big-filter__checkbox-label--widget">SsangYong</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--skoda"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark9" id="mark9" name="mark[]" />
                                            <label for="mark9"></label>
                                        </div>
                                        <span data-input="mark9" class="big-filter__checkbox-label--widget">Skoda</span>
                                    </div>

                                    <div class="marks__item">
                                        <div class="marks__logo marks__logo--volvo"></div>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mark10" id="mark10" name="mark[]" />
                                            <label for="mark10"></label>
                                        </div>
                                        <span data-input="mark10" class="big-filter__checkbox-label--widget">Volvo</span>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div><!-- /widget -->
                    <div class="widget widget--active">
                        <div class="widget__title widget__title--clickable">Основные параметры</div>
                        <div class="widget__allcontent">

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">
                                    Цена:
                                    <div class="big-filter__price">
                                        <span id="price-first">0</span>.000
                                        –
                                        <span id="price-second">0</span>.000 р.
                                    </div>
                                </div>
                                <div class="big-fiter-price__inputs">
                                    <div class="big-fiter-price__input big-fiter-price__input--right">
                                        <input type="text" id="price-val-second" readonly>
                                    </div>
                                    <div class="big-fiter-price__input big-fiter-price__input--left">
                                        <input type="text" id="price-val-first" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-price__range">
                                    <div id="price-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Коробка передач:</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="k1" id="korobka1" name="korobka[]" />
                                            <label for="korobka1"></label>
                                        </div>
                                        <span data-input="korobka1" class="big-filter__checkbox-label--widget">Автоматическая</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="k2" id="korobka2" name="korobka[]" />
                                            <label for="korobka2"></label>
                                        </div>
                                        <span data-input="korobka2" class="big-filter__checkbox-label--widget">Механическая</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Двигатель:</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="engine1" id="engine1" name="engine[]" />
                                            <label for="engine1"></label>
                                        </div>
                                        <span data-input="engine1" class="big-filter__checkbox-label--widget">Бензин</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="engine2" id="engine2" name="engine[]" />
                                            <label for="engine2"></label>
                                        </div>
                                        <span data-input="engine2" class="big-filter__checkbox-label--widget">Дизель</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Привод:</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="p1" id="privod1" name="privod[]" />
                                            <label for="privod1"></label>
                                        </div>
                                        <span data-input="privod1" class="big-filter__checkbox-label--widget">Передний</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="p2" id="privod2" name="privod[]" />
                                            <label for="privod2"></label>
                                        </div>
                                        <span data-input="privod2" class="big-filter__checkbox-label--widget">Задний</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="p3" id="privod3" name="privod[]" />
                                            <label for="privod3"></label>
                                        </div>
                                        <span data-input="privod3" class="big-filter__checkbox-label--widget">Полный</span>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div><!-- /widget -->
                    <div class="widget widget--active">
                        <div class="widget__title widget__title--clickable">Двигатель и мощность</div>
                        <div class="widget__allcontent">

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Объем (л):</div>
                                <select class="select--styled-ui">
                                    <option></option>
                                    <option>1.6</option>
                                    <option>1.8</option>
                                    <option>2.0</option>
                                </select>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Разгон от 0 до 100 км/ч
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Мощность (л.с.)
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Максимальная скорость (км/ч)
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                        </div>
                    </div><!-- /widget -->
                    <div class="widget widget--active">
                        <div class="widget__title widget__title--clickable">Детали</div>
                        <div class="widget__allcontent">

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Кузов</div>
                                <select class="select--styled-ui">
                                    <option></option>
                                    <option>308</option>
                                    <option>3008</option>
                                    <option>408</option>
                                </select>
                                <div class="remove-select">х <span>Удалить</span></div>
                                <div class="add-more-select" data-quantity="1">+ <span>добавить больше</span></div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Назнчение</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="purpose1" id="purpose1" name="purpose[]" />
                                            <label for="purpose1"></label>
                                        </div>
                                        <span data-input="purpose1" class="big-filter__checkbox-label--widget">Легковой</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="purpose2" id="purpose2" name="purpose[]" />
                                            <label for="purpose2"></label>
                                        </div>
                                        <span data-input="purpose2" class="big-filter__checkbox-label--widget">Коммерческий</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="purpose3" id="purpose3" name="purpose[]" />
                                            <label for="purpose3"></label>
                                        </div>
                                        <span data-input="purpose3" class="big-filter__checkbox-label--widget">Такси</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Клиренс (см)
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Объем багажника (см3)
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Материал салона</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mater1" id="mater1" name="mater[]" />
                                            <label for="mater1"></label>
                                        </div>
                                        <span data-input="mater1" class="big-filter__checkbox-label--widget">Кожа</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mater2" id="mater2" name="mater[]" />
                                            <label for="mater2"></label>
                                        </div>
                                        <span data-input="mater2" class="big-filter__checkbox-label--widget">Текстиль</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="mater3" id="mater3" name="mater[]" />
                                            <label for="mater3"></label>
                                        </div>
                                        <span data-input="mater3" class="big-filter__checkbox-label--widget">Алькантра</span>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div><!-- /widget -->
                    <div class="widget widget--active">
                        <div class="widget__title widget__title--clickable">Расход и безопасность</div>
                        <div class="widget__allcontent">

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Потребляемое топливо</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="gaz1" id="gaz1" name="gaz[]" />
                                            <label for="gaz1"></label>
                                        </div>
                                        <span data-input="gaz1" class="big-filter__checkbox-label--widget">АИ-92</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="gaz2" id="gaz2" name="gaz[]" />
                                            <label for="gaz2"></label>
                                        </div>
                                        <span data-input="gaz2" class="big-filter__checkbox-label--widget">АИ-95</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="gaz3" id="gaz3" name="gaz[]" />
                                            <label for="gaz3"></label>
                                        </div>
                                        <span data-input="gaz3" class="big-filter__checkbox-label--widget">АИ-98</span>
                                    </li>
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="gaz4" id="gaz4" name="gaz[]" />
                                            <label for="gaz4"></label>
                                        </div>
                                        <span data-input="gaz4" class="big-filter__checkbox-label--widget">Дизель</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-fiter-slider__inputs">
                                    <div class="big-filter__label big-filter__label--widget">
                                        Минимальный расход (л/100км)
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--right">
                                        <input type="text" class="input-slider-r" value="1000" readonly>
                                    </div>
                                    <div class="big-fiter-slider__input big-fiter-slider__input--left">
                                        <input type="text" class="input-slider-l" value="1" readonly>
                                    </div>
                                </div>
                                <div class="big-fiter-slider__range">
                                    <div class="slide-range"></div>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Активная безопасность</div>
                                <ul class="big-filter__checkboxes">
                                    <li>
                                        <div class="styledCheckbox">
                                            <input type="checkbox" value="abs" id="abs" name="abs" />
                                            <label for="abs"></label>
                                        </div>
                                        <span data-input="abs" class="big-filter__checkbox-label--widget">ABS</span>
                                    </li>
                                </ul>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Рейтинг EuroNCAP</div>
                                <div class="stars">
                                    <input name="star1" type="radio" class="star"/>
                                    <input name="star1" type="radio" class="star"/>
                                    <input name="star1" type="radio" class="star"/>
                                    <input name="star1" type="radio" class="star"/>
                                    <input name="star1" type="radio" class="star"/>
                                </div>
                            </div>

                            <div class="widget__content widget__content--grey">
                                <div class="big-filter__label big-filter__label--widget">Подушки безопасности</div>
                                <select class="select--styled-ui">
                                    <option></option>
                                    <option>не менее 1</option>
                                    <option>не менее 2</option>
                                    <option>не менее 3</option>
                                </select>
                            </div>

                        </div>
                    </div><!-- /widget -->
                </div><!-- /big-filter -->
                <div class="widget widget--active">
                    <div class="widget__content">
                        <div class="big-filter-controls">
                            <!--div class="big-filter-controls__show-result">Показать: 3 моделей</div-->
                            <div class="big-filter-controls__link">Сбросить фильтр</div>
                            <!--div class="big-filter-controls__present"><span class="underlined">Авто в наличии:</span> [<span class="red-text">43</span>]</div-->
                        </div>
                    </div>
                </div><!-- /widget -->
                <div class="widget widget--active">
                    <div class="widget__title">Категории</div>
                    <div class="widget__content widget__content--grey">
                        <ul class="category-list">
                            <li>
                                <a class="opener">Кроссоверы до 600 т.р.</a>
                                <ul>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a class="opener">Кроссоверы до 600 т.р.</a>
                                        <ul>
                                            <li><a href="#">Текст текст</a></li>
                                            <li><a href="#">Текст текст</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="opener">Семейные</a>
                                <ul>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a href="#">Текст текст</a></li>
                                </ul>
                            </li>
                            <li><a class="opener">Для девушек</a>
                                <ul>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a href="#">Текст текст</a></li>
                                    <li><a href="#">Текст текст</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Экономичные</a></li>
                            <li><a href="#">Для поездок на дачу</a></li>
                        </ul>
                    </div>
                </div><!-- /widget -->
            </div><!-- /sidebar -->
        </div>
        <div class="column--wide-left">
            <div class="catalog_excerpt">
                <h3 class="catalog_excerpt__title red-text">Предложение дня</h3>
                <div class="catalog-items catalog-items--excerpt clearfix">

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                </div><!-- /catalog-items -->
                <a class="catalog_excerpt__look-all" href="#">смотреть все: 76</a>
            </div>

            <div class="catalog_excerpt">
                <h3 class="catalog_excerpt__title">Автомобили в наличии</h3>
                <div class="catalog-items catalog-items--excerpt clearfix">

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                </div><!-- /catalog-items -->
                <a class="catalog_excerpt__look-all" href="#">смотреть все: 88</a>
            </div>

            <div class="catalog_excerpt">
                <h3 class="catalog_excerpt__title">Подержанные автомобили</h3>
                <div class="catalog-items catalog-items--excerpt clearfix">

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                    <div class="catalog-items__item catalog-items__item--excerpt">
                        <div class="catalog-item catalog-item--excerpt clearfix">
                            <div class="catalog-item__img catalog-item__img--excerpt">
                                <div class="valigner"></div>
                                <img src="images/car.jpg" alt=""/>
                            </div><!-- /catalog-item__img -->
                            <div class="catalog-item__text">
                                <h3 class="catalog-item__title catalog-item__title--excerpt">KIA Otima Седан</h3>
                                <div class="catalog-item__description catalog-item__description--excerpt">
                                    <p>2014, Luxe 2.0 AT</p>
                                    <p>2 л, бензин (150 л.с.) АКПП</p>
                                </div>
                                <div class="catalog-item__price catalog-item__price--excerpt">
                                    1 019 900 р.
                                    <div class="catalog-item__old-price">1 320 000 р.</div>
                                </div>
                                <a class="catalog-item__more-link catalog-item__more-link--excerpt" href="#">Подробнее</a>
                            </div>
                        </div><!-- /catalog-item -->
                    </div><!-- /catalog-items__item -->

                </div><!-- /catalog-items -->
                <a class="catalog_excerpt__look-all" href="catalog1.php">смотреть все: 420</a>
            </div>
        </div>
</section>

<section class="section section--grey section--shadow section--catalog-text clearfix">
    <div class="wrapper wrapper--sector">
        <h2>Заголовок</h2>
        <p>Еще с начала 1994 года ГК FAVORIT MOTORS развивалась со стремительной силой и заняла лидирующую позицию среди других дилерских сетей Москвы. На данный момент ГК FAVORIT MOTORS является лидером российского автомобильного рынка.</p>
        <p>Наши специалисты гарантируют не только самое высокое качество обслуживания, но и доступные цены на новые автомобили. У нас имеется независимая клиентская служба, которая является дополнительной гарантией нашего честного и внимательного отношения к клиентам.</p>
<p>ГК FAVORIT MOTORS уже более 20 лет находится на российском рынке, мы предлагаем нашим клиентам множество различных услуг, среди которых:</p>
<ul>
<li>•	продажа новых автомобилей;</li>
<li>•	продажа автомобилей с пробегом;</li>
<li>•	оформление различных кредитных программ;</li>
<li>•	лизинг;</li>
<li>•	выкуп автомобилей;</li>
<li>•	снятие с учета;</li>
<li>•	автосервис.</li>
</ul> <br />
<p>Кроме продажи новых легковых автомобилей, наш дилерский центр также осуществляет реализацию автомобилей с пробегом.</p>
<p>Также, предоставляем покупателю каталог с полным списком имеющихся в наличии моделей. Кроме того, клиент может ознакомиться со всеми возможными комплектациями и разнообразными цветовыми решениями представленных транспортных средств.</p>
<h2>Покупка нового автомобиля</h2>
<p>FAVORIT MOTORS предлагает новые автомобили 2014-2015 года. Перед тем как купить новое авто в Москве, мы консультируем каждого клиента, чтобы сделать его приобретение успешным и безопасным.</p>
<p>Если Вы решили приобрести себе автомобиль в ГК FAVORIT MOTORS, но не располагаете суммой денег для оплаты полной стоимости, то можете воспользоваться рядом кредитных программ, которые предложат наши менеджеры.</p>
<p>Соблюдая вышеприведенные правила, Вы сможете безопасно и быстро купить автомобиль в Москве.</p>
<p>Доступные цены, сервисное обслуживание и гарантированное качество – главные составляющие нашей деятельности.<br />
Цены указаны на автомобили 2014-2015 г.в. с учетом всех возможных акций.<br />
Подробности у консультантов отделов продаж и по телефону +7 (495) 786 25 25.</p>
    </div>
</section>

<div class="footer-menu-container">
    <div class="footer-menu">
        <div class="cabinet"><a href="#">Личный кабинет</a></div>
        <div class="call"><a href="#">Обратный звонок</a></div>
        <div class="search"><span>Поиск по сайту:</span></div>
        <input type="text" />
        <div class="consultant"></div>
    </div>
</div>

<div class="footer"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>