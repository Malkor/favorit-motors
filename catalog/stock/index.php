<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Stock");?>

<?$arrFilter = array();?>
<?$arFields = array();?>

<?CModule::IncludeModule('iblock');?>
<?if($_REQUEST["tag"] != ""):?>
    <?
        $arSelect = Array("ID", "IBLOCK_ID", "CODE", "PREVIEW_TEXT", "DETAIL_TEXT");
        $arFilter = Array("IBLOCK_ID"=>16, "CODE"=>$_REQUEST["tag"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arrFilter = array("PROPERTY_TAGS"=>$arFields["ID"]);
        }
    ?> 
<?endif?>

<section class="section section--grey section--catalog-top clearfix">
    <div class="wrapper wrapper--sector">
        
        <?if($_REQUEST["tag"] == ""):?>
          <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "header_text",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => ""
            )
          );?>
        <?else:?>
            <?=$arFields["PREVIEW_TEXT"]?>
        <?endif;?>
        <div class="cat_main_nav clearfix">
            <div class="column--small-right">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.viewed.products", 
                    "catalog.viewed.products", 
                    array(
                        "COMPONENT_TEMPLATE" => "vertical",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "13",
                        "SHOW_FROM_SECTION" => "N",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "Y",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_NAME" => "Y",
                        "SHOW_IMAGE" => "Y",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "PAGE_ELEMENT_COUNT" => "5",
                        "LINE_ELEMENT_COUNT" => "3",
                        "TEMPLATE_THEME" => "blue",
                        "DETAIL_URL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "SHOW_OLD_PRICE" => "N",
                        "PRICE_CODE" => array(
                        ),
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "SHOW_PRODUCTS_13" => "Y",
                        "SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
                        "SECTION_CODE" => "",
                        "SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
                        "SECTION_ELEMENT_CODE" => "",
                        "DEPTH" => "2",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PROPERTY_CODE_13" => array(
                            0 => "PRICE"
                        ),
                        "CART_PROPERTIES_13" => array(
                            0 => ""
                        ),
                        "ADDITIONAL_PICT_PROP_13" => "",
                        "LABEL_PROP_13" => "-"
                    ),
                    false
                );?>
            </div>
            <div class="column--wide-left">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_catalog_inner", Array(
            "COMPONENT_TEMPLATE" => ".default",
                "ROOT_MENU_TYPE" => "catalog",  // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",   // Тип кеширования
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                "MAX_LEVEL" => "1", // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "catalog", // Тип меню для остальных уровней
                "USE_EXT" => "Y",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N", // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
            false
        );?>
            </div>
        </div>
    </div>
</section>

<section class="section section--shadow section--catalog-filter clearfix">
    <div class="wrapper wrapper--sector">
        <div class="column--wide-left">
            <h2><?=$APPLICATION->ShowHead();?></h2>
            <div id="filter-indicator" class="filter-indicator">
                 Фильтр:
                <div id="filter-indicator__items" class="filter-indicator__items">
                </div>
                 <!-- /filter-indicator__items -->
            </div>
             <!-- /filter-indicator -->
            <div class="num-by-page">
                 Показывать на странице по:
                <div class="num-by-page__nums" id="num-by-page__nums">
                    <span class="num-by-page__num <?if((($_REQUEST['num_c'] == "") && ($_REQUEST['SHOWALL_1'] != 1)) || ($_REQUEST['num_c'] == 10)):?>num-by-page__num--active<?endif;?>" data-num="10" onclick="change_nums(this);">10</span>
                    <span class="num-by-page__num <?if(($_REQUEST['num_c'] == 20) && ($_REQUEST['SHOWALL_1'] != 1)):?>num-by-page__num--active<?endif;?>" data-num="20" onclick="change_nums(this);">20</span>
                    <span class="num-by-page__num <?if(($_REQUEST['num_c'] == 30) && ($_REQUEST['SHOWALL_1'] != 1)):?>num-by-page__num--active<?endif;?>" data-num="30" onclick="change_nums(this);">30</span>
                    <span class="num-by-page__num <?if(($_REQUEST['num_c'] == 5) || ($_REQUEST['SHOWALL_1'] == 1)):?>num-by-page__num--active<?endif;?>" data-num="5" onclick="change_nums(this);">Все (5)</span>
                </div>
            </div>
             <!-- /num-by-page -->
            <div class="sort-by">
                 Сортировать по:
                <div class="sort-by__selecter" id="sort-by__select">
                    <select class="sort-by__select" onchange="change_url(this.value);">
                        <option value="NAME" data-order="asc" <?if($_REQUEST["sort_f"] == "NAME"):?>selected<?endif;?>>Умолчанию</option>
                        <option value="PRICE" data-order="desc" <?if($_REQUEST["sort_f"] == "PRICE"):?>selected<?endif;?>>Цене (по убыванию)</option>
                    </select>
                </div>
            </div>
             <!-- /num-by-page -->
        </div>
    </div>
</section> 

<section class="section section--grey section--acatalog-list clearfix">
    <div class="wrapper wrapper--sector">
        <div class="column--small-right">
            <div class="sidebar sidebar--bu">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.smart.filter",
                    "catalog_smart_filter_used",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "IBLOCK_TYPE" => "",
                        "IBLOCK_ID" => "13",
                        "SECTION_ID" => $_REQUEST["SECTION_ID"],
                        "SECTION_CODE" => "",
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "FILTER_VIEW_MODE" => "vertical",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "SAVE_IN_SESSION" => "N",
                        "INSTANT_RELOAD" => "N",
                        "PRICE_CODE" => array(),
                        "XML_EXPORT" => "N",
                        "SECTION_TITLE" => "-",
                        "SECTION_DESCRIPTION" => "-",
                        "POPUP_POSITION" => "left"
                    )
                );?>
        <div class="widget widget--active">
                <div class="widget__title">Категории</div>
                <div class="widget__content widget__content--grey">
                    <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list", 
                    "tags_catalog_section_list", 
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "IBLOCK_TYPE" => "references",
                        "IBLOCK_ID" => "16",
                        "SECTION_ID" => $_REQUEST["SECTION_ID"],
                        "SECTION_CODE" => "",
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => "2",
                        "SECTION_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "VIEW_MODE" => "LIST",
                        "SHOW_PARENT_NAME" => "Y",
                        "SECTION_URL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    $component
                );?>
                </div>
            </div><!-- /widget -->
            </div>
        </div>
    </div>
</section>

<?$sort_f="";?>
<?$sort_o = "asc";?>
<?if($_REQUEST["sort_f"] == "PRICE"):?>
    <?$sort_f = "property_PRICE";?>
    <?$sort_o = "desc";?>
<?else:?>
    <?$sort_f = "NAME";?>
<?endif;?>

<?$num_c="";?>
<?if($_REQUEST["num_c"] != ""):?>
    <?$num_c = (int)$_REQUEST["num_c"];?>
<?else:?>
    <?$num_c = 10;?>
<?endif;?>



<section class="section section--shadow section--catalog-list clearfix">
    <div class="wrapper wrapper--sector">
        <div class="column--wide-left">
            <div class="catalog-items">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", 
                    "catalog_section_order", 
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "13",
                        "SECTION_ID" => $_REQUEST["SECTION_ID"],
                        "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "ELEMENT_SORT_FIELD" => $sort_f,
                        "ELEMENT_SORT_ORDER" => $sort_o,
                        "ELEMENT_SORT_FIELD2" => "",
                        "ELEMENT_SORT_ORDER2" => "",
                        "FILTER_NAME" => "arrFilter",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "PAGE_ELEMENT_COUNT" => $num_c,
                        "LINE_ELEMENT_COUNT" => "3",
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "OFFERS_LIMIT" => "5",
                        "TEMPLATE_THEME" => "blue",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_CLOSE_POPUP" => "N",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "SET_TITLE" => "Y",
                        "SET_BROWSER_TITLE" => "Y",
                        "BROWSER_TITLE" => "-",
                        "SET_META_KEYWORDS" => "Y",
                        "META_KEYWORDS" => "-",
                        "SET_META_DESCRIPTION" => "Y",
                        "META_DESCRIPTION" => "-",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "SET_STATUS_404" => "N",
                        "CACHE_FILTER" => "N",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRICE_CODE" => array(
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => array(
                        ),
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "DISPLAY_COMPARE" => "N",
                        "PAGER_TEMPLATE" => "paginator",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "MESS_BTN_COMPARE" => "Сравнить",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "ADD_PICT_PROP" => "-",
                        "LABEL_PROP" => "-"
                    ),
                    false
                );?>
            </div><!-- /catalog-items -->
        </div>
    </div>
</section>

<section class="section section--grey section--shadow section--catalog-text clearfix">
    <div class="wrapper wrapper--sector">
        <?if($_REQUEST["tag"] == ""):?>
          <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "footer_text",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => ""
            )
          );?>
        <?else:?>
            <?=$arFields["DETAIL_TEXT"]?>
        <?endif;?>
    </div>
</section>

<script type="text/javascript">

    function change_nums(num) {
        if(num.className != 'num-by-page__num--active'){
            document.getElementsByClassName('num-by-page__num--active')[0].classList.remove('num-by-page__num--active');
            num.classList.add('num-by-page__num--active');
            var num_c = num.getAttribute('data-num');

            var variables = getUrlVar();
            var arr_res = "?";
            for(var var_name in variables){
            if ((var_name != 'num_c') && (var_name != 'SHOWALL_1')) {
                arr_res = arr_res+var_name+"="+variables[var_name]+"&";
            }
        }
        arr_res = arr_res+"num_c="+num_c;
        window.location = "http://"+window.location.hostname.toString()+window.location.pathname.toString()+arr_res;
        }
    }
 
    function change_url(sort_f) {
        //var params = window.location.search.toString();
        var variables = getUrlVar();
        var arr_res = "?";
        for(var var_name in variables){
            if ((var_name != 'sort_f') && (var_name != 'sort_q')) {
                arr_res = arr_res+var_name+"="+variables[var_name]+"&";
            }
        }
        arr_res = arr_res+"sort_f="+sort_f;
        window.location = "http://"+window.location.hostname.toString()+window.location.pathname.toString()+arr_res;
    }
    function getUrlVar(){
        var urlVar = window.location.search; // получаем параметры из урла
        var arrayVar = []; // массив для хранения переменных
        var valueAndKey = []; // массив для временного хранения значения и имени переменной
        var resultArray = []; // массив для хранения переменных
        arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
        if(arrayVar[0]=="") return false; // если нет переменных в урле
        for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
            valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
            resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
        }
        return resultArray; // возвращаем результат
    }
</script>


<style>    
.wrapper--sector {
    width: 1168px;
    margin: 0 auto;
    position: relative;
}

.big-filter-controls__link{
    background:none;
    border:0;
    outline:0 !important;
}

/****************************
    Добавлять смело можно отсюда
******************************/

.section--grey {
    background-color: #f7f7f7;
}

.section--shadow {
    box-shadow: inset 0 10px 20px #efefef;
}

.column--wide-left {
    width: 884px;
    float: left;
}

.column--small-right {
    width: 253px;
    float: right;
}

.red-text {
    color: #c61700;
}

.valigner {
    width: 0;
    height: 100%;
    display: inline-block;
    vertical-align: middle;
}

.padding_forty {
    padding: 0 20px;
}

.underlined {
    text-decoration: underline;
}

.select--styled-ui {
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    height: 31px;
    padding: 3px 9px 0;
    margin-top: 15px;
    border: none;
    outline: none;
}

.styledCheckbox {
    display: inline-block;
    width: 14px;
    height: 14px;
    background: #fff;
    margin: 0 10px 0 0;
    position: relative;
    border: 1px solid #F4D1CC;
}

.styledCheckbox input {
    visibility: hidden;
    width: 9px;
    height: 9px;
}

.styledCheckbox label {
    cursor: pointer;
    position: absolute;
    width: 14px;
    height: 14px;
    left: 0;
    top: 0;
    background: transparent;
}

.styledCheckbox label:after {
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    filter: alpha(opacity=0);
    opacity: 0;
    content: '';
    position: absolute;
    width: 7px;
    height: 4px;
    background: rgba(0, 0, 0, 0);
    top: 2px;
    left: 2px;
    border: 3px solid #6e6f6e;
    border-top: none;
    border-right: none;

    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    transform: rotate(-45deg);
}

.styledCheckbox label:hover::after {
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
    filter: alpha(opacity=30);
    opacity: 0.3;
}

.styledCheckbox input[type=checkbox]:checked + label:after {
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
    filter: alpha(opacity=100);
    opacity: 1;
}

/****************************
    Секция с текстом
******************************/
.section--catalog-top {
    padding-top: 40px;
    border-bottom: 1px solid #fff;
    position: relative;
}

.cat_main_nav {
    margin-top: 32px;
}

.cat_nav {}
.cat_nav__link {
    display: inline-block;
    width: 280px;
    height: 34px;
    margin-right: 11px;
    padding-top: 5px;
    vertical-align: baseline;
    background-color: #fff;
    color: #898989;
    font-size: 18px;
    line-height: 34px;
    text-align: center;
    text-decoration: none;
}
.cat_nav__link--active {
    background-color: #c61700;
    color: #fff;
}
.cat_nav__link--last {
    margin-right: 0;
}

.watched-late {
    padding-top: 5px;
    text-align: right;
    font-size: 18px;
    line-height: 34px;
    position: relative;
}

.watched-late:hover .watched-late__pop {
    display: block;
}

.watched-late__pop {
    display: none;
    position: absolute;
    left: -268px;
    top: 0;
    width: 245px;
    padding: 10px 10px 0;
    border: 1px solid #efc2c5;
    background: #f2f2f2;
    font-size: 15px;
    text-align: left;
    z-index: 3;
}

.watched-late__pop:after {
    content: url("../images/pop_ar.png");
    position: absolute;
    top: 7px;
    right: -9px;
}

.watched-late__listtable {
    width: 100%;
    margin-bottom: 10px;
    line-height: 1.3;
}

.watched-late__listtable__ltd {
    width: 155px;
}

.watched-late__listtable__ltd a{
    text-decoration:none;
}

.watched-late__listtable__ltd a:hover{
    text-decoration:underline;
}

.watched-late__listtable__rtd {
    text-align: right;
}

.watched-late__show-all-link {
    color: #858484;
}

/****************************
   Секция фильтра по центру
******************************/
.section--catalog-filter {
    padding: 40px 0 30px;
    position: relative;
}

.filter-indicator {
    margin-bottom: 27px;
}
.filter-indicator__items {
    display: inline-block;
    margin-left: 16px;
}
.filter-indicator__item {
    display: inline-block;
    margin-right: 16px;
}
.filter-indicator__red-cross {
    display: inline-block;
    width: 13px;
    height: 13px;
    margin-right: 8px;
    background-image: url('../images/red-cross.png');
    vertical-align: baseline;
    margin-left:10px;
}

.num-by-page {
    float: right;
    width: 402px;
    line-height: 34px;
    text-align: right;
}
.num-by-page__nums {
    display: inline-block;
    text-align: right;
}
.num-by-page__num {
    padding-left: 13px;
    color: #313131;
}
.num-by-page__num--active {
    color: #c61700;
}

.sort-by {}
.sort-by__selecter {
    display: inline-block;
    width: 210px;
    margin-left: 30px;
}
.sort-by__selecter .ui-selectmenu-button {
    display: inline-block;
    padding: 0 9px 0;
    vertical-align: middle;
    border: 1px solid #f4d1cc;
    margin-top: 0;
}
.sort-by__selecter .ui-selectmenu-button .ui-icon-triangle-1-s {
    display: block;
    width: 9px;
    height: 5px;
    position: absolute;
    top: 12px;
    right: 9px;
    background-image: url('../images/arr-down--grey.png');
    background-repeat: no-repeat;
    background-position: center center;
}
.sort-by__select {
    border: 1px solid #f4d1cc;
}

/****************************
    Секция со списком б/у
******************************/
.section--acatalog-list {
    padding: 19px 0 0;
    position: relative;
}

.section--acatalog-list .catalog-items__item {
    border-top: 1px solid #f1aeb2;
    border-bottom: 1px solid #f1aeb2;
}

.section--catalog-list {
    padding: 20px 0 110px;
    position: relative;
}

.catalog-items__item {
    padding: 9px 0;
    margin-bottom: 19px;
    border-top: 1px solid #d5d5d5;
    border-bottom: 1px solid #d5d5d5;
}

.catalog-item__img {
    float: left;
    position: relative;
    width: 220px;
    height: 122px;
    margin-right: 20px;
    text-align: center;
    overflow: hidden;
    border: 1px solid #d5d5d5;
}
.catalog-item__img img {
    display: inline-block;
    vertical-align: middle;
}
.catalog-item__amark {
    width: 24px;
    height: 22px;
    position: absolute;
    top: 0;
    right: 10px;
    background-image: url('../images/a.jpg');
}

.catalog-item__text {
    position: relative;
}

.catalog-item__text p {
    margin-bottom: 12px;
}

.catalog-item__text--bu {
    float: left;
    width: 642px;
}

.catalog-item__title,
.catalog-item__subtitle {
    margin: 0;
    font-size: 18px;
    font-weight: bold;
}
.catalog-item__title {
    margin: 10px 0 0;
}
.catalog-item__subtitle--bu {
    margin-bottom: 25px;
}

.catalog-item__description {
    width: 340px;
    font-size: 14px;
    color: #303030;
}

.catalog-item__price {
    position: absolute;
    top: 31px;
    right: 0;
    font-size: 18px;
    font-weight: bold;
    text-align: right;
}

.catalog-item__action {
    position: absolute;
    top: 16px;
    right: 0;
    font-size: 14px;
    font-weight: bold;
    color: #c61700;
    text-align: right;
}

.catalog-item__old-price {
    color: #787878;
    font-weight: normal;
    font-size: 14px;
    text-decoration: line-through;
}

.catalog-item__more-link {
    display: block;
    position: absolute;
    bottom: 0;
    right: 0;
    width: 107px;
    padding: 9px 0 7px;
    background-color: #ececed;
    text-align: center;
    text-decoration: none;
}

.catalog-item__left-descr,
.catalog-item__right-descr {
    width: 48%;
    display: inline-block;
}

.paginator {
    position: relative;
    margin-top: 40px;
    text-align: center;
}

.paginator__link {
    display: inline-block;
    margin: 0 13px;
    font-size: 18px;
    text-decoration: none;
    vertical-align: middle;
}

.paginator__link--current {
    font-weight: bold;
    color: #c61700;
}

.paginator__arr {
    width: 13px;
    height: 22px;
    margin: 0 25px;
    display: inline-block;
    vertical-align: middle;
}
.paginator__arr--left {
    background-image: url('../images/arr-left.png');
}
.paginator__arr--right {
    background-image: url('../images/arr-right.png');
}

.show-all-link {
    position: absolute;
    top: 0;
    right: 0;
    font-size: 18px;
}

/****************************
    Сайдбар
******************************/
.sidebar {
    background-color: #fff;
    position: absolute;
    width: 253px;
    z-index: 1;
}

.sidebar--bu {
    top: -199px;
}

.sidebar--new {
    top: -685px;
}

.widget {

}
.widget.widget--active {
    border-bottom: none;
}
.widget--active .widget__content,
.ui-accordion-content-active {
    display: block;
    border-bottom: 1px solid #ffffff;
}

.widget__allcontent {
    background-color: #F2F2F2;
}

.widget__title {
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    height: 51px;
    padding: 3px 30px 0 19px;
    background-color: #000;
    color: #fff;
    font-size: 18px;
    line-height: 51px;
    border-bottom: 1px solid #ffffff;
    outline: none;
}

.widget__content {
    padding: 20px;
}

.widget__content--grey {
    background-color: #f2f2f2;
}

.widget__content--model-mark {
    border-bottom: 1px solid #ffffff;
}

.widget__title--clickable {
    background-image: url('../images/arr-down.png');
    background-position: right 25px;
    background-repeat: no-repeat;
    cursor: pointer;
}

.category-list {
    padding: 7px 0 0;
    margin: 0;
}
.category-list li {
    padding-left: 18px;
    margin-bottom: 13px;
    list-style: none;
    font-size: 16px;
    line-height: 1.2;
    background:none !important;
}

.category-list li ul {
    margin-top: 13px;
    //display: none;
}

.category-list li a.active ul{
    display: block !important;
}

.category-list a {
    text-decoration: none;
    cursor: pointer;
}
.category-list a.open {
    position: relative;
    text-decoration: underline;
}
.category-list a.open:before {
    content: url('../images/li-arr-pink.png');
    position: absolute;
    left: -17px;
    top: -2px;
}

.category-list a.open.active:before {
    transform: rotate(90deg);
}



.big-filter {margin-top:6px;}

.big-filter__label--widget {
    position: relative;
    display: block;
    margin-bottom: 15px;
}

.big-filter__checkbox--widget {
    width: 14px;
    height: 14px;
    border: 1px solid #F4D1CC;
}

.big-filter__label--model {
    margin-top: 20px;
}

.big-filter__checkboxes li {
    margin-bottom: 15px;
}

.big-filter__checkboxes li:last-of-type {
    margin-bottom: 0;
}

.big-filter-controls {
    text-align: center;
}

.big-filter-controls__show-result {
    display: inline-block;
    width: 168px;
    height: 31px;
    margin-bottom: 20px;
    line-height: 31px;
    background-color: #c51700;
    color: #fff;
    cursor: pointer;
}

.big-filter-controls__link {
    margin-bottom: 20px;
    text-decoration: underline;
    cursor: pointer;
}

.big-filter__price {
    position: absolute;
    top: 0;
    right: 0;
}

.big-fiter-price__inputs,
.big-fiter-slider__input {
    margin-bottom: 25px;
}

.big-fiter-price__input,
.big-fiter-slider__input {
    width: 104px;
}

.big-fiter-price__input input,
.big-fiter-slider__input input {
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    height: 31px;
    padding: 0 10px;
    background-color: #fff;
    border: none;
}

.big-fiter-price__input--right,
.big-fiter-slider__input--right {
    float: right;
}

.big-fiter-price__range,
.big-fiter-slider__range {
    margin-bottom: 25px;
}

.marks__item {
    margin-bottom: 10px;
}

.marks__logo {
    display: inline-block;
    vertical-align: middle;
    width: 44px;
    height: 30px;
    margin-right: 15px;
    background-image: url('../images/marks_logos.png');
    background-repeat: no-repeat;
    background-position: center top;
}

.marks__logo--chevrolet {
    background-position-y: 7px;
}
.marks__logo--citroen {
    background-position-y: -35px;
}
.marks__logo--ford {
    background-position-y: -120px;
}
.marks__logo--kia {
    background-position-y: -161px;
}
.marks__logo--opel {
    background-position-y: -203px;
}
.marks__logo--peugeot {
    background-position-y: -75px;
}
.marks__logo--seat {
    background-position-y: -243px;
}
.marks__logo--ssangyong {
    background-position-y: -330px;
}
.marks__logo--skoda {
    background-position-y: -287px;
}
.marks__logo--volvo {
    background-position-y: bottom;
}

.mark__subselect {
    margin: 5px 20px 20px;
    display: none;
}

.mark__subselect .ui-selectmenu-button,
.mark__subselect select {
    border: 1px solid #CCCCCC;
}

.stars {
    margin-bottom: 25px;
}

.add-more-select {
    margin-top: 20px;
    text-align: right;
    cursor: pointer;
}

.add-more-mark-model,
.remove-link,
.remove-select {
    text-align: right;
    cursor: pointer;
}

.remove-link {
    margin-top: 15px;
}

.remove-select {
    display: none;
    margin-top: 15px;
}

.add-more-select span,
.add-more-mark-model span,
.remove-link span,
.remove-select span {
    text-decoration: underline;
}

/****************************
    Секция с внизу текстом
******************************/
.section--catalog-text {
    min-height: 475px;
    padding-top: 40px;
    position: relative;
}

/****************************
    Секция со списком логотипов
******************************/
.section--catalog-logos {
    padding: 10px 0 30px;
    position: relative;
}

.logos {
    text-align: center;
}

.logo-item {
    display: inline-block;
    width: 160px;
    height: 150px;
}

.logo-item__img {
    display: inline-block;
    vertical-align: middle;
}

.logo-item__type {
    display: inline-block;
    vertical-align: middle;
    min-width: 71px;
    padding-top: 80px;
    font-size: 16px;
    background-position: center top;
    background-repeat: no-repeat;
}

.logo-item__type:hover {
    cursor: pointer;
    color: #c61700;
}

.logo-item__type--sedan {
    background-image: url('../images/types/sedan.png');
}

.logo-item__type--hatchback {
    background-image: url('../images/types/hatchback.png');
}

.logo-item__type--universal {
    background-image: url('../images/types/universal.png');
}

.logo-item__type--crossover {
    background-image: url('../images/types/crossover.png');
}

.logo-item__type--vnedoroz {
    background-image: url('../images/types/vnedoroz.png');
}

.logo-item__type--kupe {
    background-image: url('../images/types/kupe.png');
}

.logo-item__type--cabriolet {
    background-image: url('../images/types/cabriolet.png');
}

.logo-item__type--pickup {
    background-image: url('../images/types/pickup.png');
}

.logo-item__type--minivan {
    background-image: url('../images/types/minivan.png');
}

.logo-item__type--commerce {
    background-image: url('../images/types/commerce.png');
}

.logo-item__type--sedan:hover {
    background-image: url('../images/types/sedan_active.png');
}

.logo-item__type--hatchback:hover {
    background-image: url('../images/types/hatchback_active.png');
}

.logo-item__type--universal:hover {
    background-image: url('../images/types/universal_active.png');
}

.logo-item__type--crossover:hover {
    background-image: url('../images/types/crossover_active.png');
}

.logo-item__type--vnedoroz:hover {
    background-image: url('../images/types/vnedoroz_active.png');
}

.logo-item__type--kupe:hover {
    background-image: url('../images/types/kupe_active.png');
}

.logo-item__type--cabriolet:hover {
    background-image: url('../images/types/cabriolet_active.png');
}

.logo-item__type--pickup:hover {
    background-image: url('../images/types/pickup_active.png');
}

.logo-item__type--minivan:hover {
    background-image: url('../images/types/minivan_active.png');
}

.logo-item__type--commerce:hover {
    background-image: url('../images/types/commerce_active.png');
}

/****************************
    Секция с выдержками из категорий
******************************/
.section--catalog-cats-previews {
    padding: 45px 0 50px;
}

.catalog_excerpt {
    margin-bottom: 30px;
}

.catalog_excerpt__title {
    font-size: 24px;
    margin-bottom: 10px;
}

.catalog-items--excerpt {
    padding-top: 10px;
    border-top: 1px solid #D5D5D5;
}

.catalog-items__item--excerpt {
    width: 215px;
    float: left;
    border: none;
}

.catalog-items__item--excerpt:nth-child(2) {
    margin: 0 119px;
}

.catalog-item--excerpt {
    position: relative;
}

.catalog-item__img--excerpt {
    width: 100%;
    height: 85px;
    float: none;
    border: none;
}

.catalog-item__img--excerpt img {
    max-width: 100%;
}

.catalog-item__title--excerpt {
    margin-bottom: 15px;
    font-size: 17px;
    font-weight: normal;
}

.catalog-item__description--excerpt {
    margin-bottom: 20px;
    color: #787878;
}

.catalog-item__description--excerpt p {
    margin-bottom: 0;
}

.catalog-item__price--excerpt {
    position: relative;
    top: auto;
    right: auto;
    text-align: left;
}

.catalog-item__more-link--excerpt {
    bottom: 1px;
}

.catalog_excerpt__look-all {
    display: block;
    text-align: right;
    font-size: 14px;
    color: #787878;
}

/****************************
    Каталог бренда
******************************/
.sidebar--brand {
    margin-top: -244px;
    position: relative;
}

.section--catalog-brand-logos {
    padding: 30px 0;
}

.h2logo {
    display: inline-block;
    width: 110px;
    height: 90px;
    text-align: left;
}

.catalog-brand__h2 {
    display: inline-block;
    vertical-align: middle;
}

.catalog-items--brand {
    padding: 35px 0 0;
    position: relative;
}

.catalog-items__item--brand {
    width: 224px;
    height: 236px;
    float: left;
    border: none;
    position: relative;
}

.catalog-items__item--brand:nth-child(3n - 1) {
    margin: 0 106px;
}

.catalog-item--brand {
    width: 215px;
    padding: 3px;
    border: 1px solid #ffffff;
    margin-bottom: 60px;
}

.catalog-items__item--brand:hover .catalog-item--brand {
    border-color: #F4D1CC;
    position: absolute;
    top: 9px;
    left: 0;
    z-index: 3;
}

.catalog-item__img--brand {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    width: 100%;
    height: 120px;
    float: none;
    border: 1px solid #f5f5f5;
}

.catalog-item__img--brand img {
    max-width: 98%;
}

.catalog-item__title--brand {
    margin-bottom: 8px;
    padding: 0 20px;
    font-size: 17px;
    font-weight: normal;
}

.catalog-item__description--brand {
    display: none;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 215px;
    padding: 20px 20px 8px;
    color: #2c2c2c;
    font-size: 14px;
    background-color: #ececed;
    border-top: 1px solid #ffffff;
    z-index: 3;
}

.catalog-items__item--brand:hover .catalog-item__description--brand {
    display: block;
}

.catalog-item__price--brand {
    position: relative;
    top: auto;
    right: auto;
    padding: 0 20px;
    text-align: left;
}

.catalog-item__price--brand span {
    display: inline-block;
    vertical-align: middle;
    font-size: 14px;
    font-weight: normal;
    color: #787878;
}

.catalog-item__more-link--brand {
    position: static;
    bottom: auto;
    right: auto;
    width: auto;
    margin-top: 15px;
}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>