<?if(isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == 'true'){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	if(CModule::IncludeModule("iblock"))
	{ 
		if($_REQUEST["ID"] != "")
		{
			$id = $_REQUEST["ID"];
			$arResult = array();
			$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_BASEOPTIONS", "PROPERTY_ADDITIONALOPTIONS");
			$arFilter = Array("IBLOCK_ID"=>13, "ID"=>IntVal($id), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ob = $res->GetNextElement()){ 
				$arFields = $ob->GetFields();  
				$arResult = $arFields;
			}
			?>
<div id="section">
		<div class="section tab-container section-sec">
            <div class="box-container">

                <div class="title">
                    <h2>Опции</h2>
                </div>

                <?
                $baseoptions_arr = array();
                foreach($arResult["PROPERTIES"]["BASEOPTIONS"]["VALUE"] as $baseoptions){
                  $baseoptions_arr[] = $baseoptions;
                }

                $arSelect = Array("ID", "NAME", "PROPERTY_OPTIONNAME");
                $arFilter = Array("IBLOCK_ID"=>14, "ID"=>$baseoptions_arr, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
                $baseoptions_arr_res = array();
                while($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $baseoptions_arr_res[$arFields["ID"]] = $arFields;
                }
                ?>

                <?
                $additionaloptions_arr = array();
                foreach($arResult["PROPERTIES"]["ADDITIONALOPTIONS"]["VALUE"] as $additionaloptions){
                  $additionaloptions_arr[] = $additionaloptions;
                }

                $arSelect = Array("ID", "NAME", "PROPERTY_OPTIONNAME");
                $arFilter = Array("IBLOCK_ID"=>15, "ID"=>$additionaloptions_arr, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
                $additionaloptions_arr_res = array();
                while($ob = $res->GetNextElement())
                {
                  $arFields = $ob->GetFields();
                  $additionaloptions_arr_res[$arFields["ID"]] = $arFields;
                }
                ?>

                <div class="standard-equipment-items">
                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Базовые опции</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <?foreach($baseoptions_arr_res as $baseoptions):?>
                                  <li><?=$baseoptions["PROPERTY_OPTIONNAME_VALUE"]?></li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>

                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Дополнительные опции</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <?foreach($additionaloptions_arr_res as $additionaloptions):?>
                                  <li><?=$additionaloptions["PROPERTY_OPTIONNAME_VALUE"]?></li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>

                <?=$arResult["PROPERTIES"]["TAB_COMPLECTS"]["~VALUE"]["TEXT"]?>

            </div>
        </div>
</div>
		<?}
	}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}

?>