<?
//if(isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == 'true'){
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  if(CModule::IncludeModule("iblock"))
  { 
    if($_REQUEST["ID"] != "")
    {
      $id = $_REQUEST["ID"];
      $arResult = array();
      $arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_*");
      $arFilter = Array("IBLOCK_ID"=>13, "ID"=>IntVal($id), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
      $res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCount"=>1), $arSelect);
      if($ob = $res->GetNextElement()){ 
        $arFields = $ob->GetFields();  
        $arResult = $arFields;
        $arProperties = $ob->GetProperties();
        $arResult["PROPERTIES"] = $arProperties;
      }

      $body_arr = array("sedan" => "седан", "hatchback" => "хэтчбек", "wagon" => "универсал");
      $body_arr_exist = array();?>

      <div id="section">
          <div id="characteristics" class="section tab-container section-sec">
            <div class="box-container">
                <div class="characteristics-navigation clearfix">
                  <span class="on-left">Кузов: </span>
                  <ul class="on-left clearfix">
                    <?$char_code = "";?>
                    <?foreach($body_arr as $key => $value):?>
                      <?if($value == $arResult["PROPERTIES"]["BODY"]["VALUE"]):?>
                        <?$char_code = $key;?>
                        <li class="on-left active"><a data-block="#<?=$key?>"><?=$value?></a></li>
                      <?endif;?>
                    <?endforeach;?>
                  </ul>
                </div>
                
                <div class="char-tabs-container" id="<?=$char_code?>">

                    <div class="body-arrangement bg" style="background-image: url(content/models/schema.jpg);"></div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Свойства модели</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="40%" />
                                    <col width="30%" />
                                    <col width="30%" />
                                </colgroup>
                                <tbody>
                                    <?foreach($arResult["PROPERTIES"] as $prop):?>
                                       <tr class="border-bottom">
                                          <td class="border-right"><?=$prop["NAME"]?></td>
                                          <td colspan="2"><?=$prop["VALUE"]?></td>
                                      </tr>
                                    <?endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?=$arResult["PROPERTIES"]["TAB_CHARACTERISTICS"]["~VALUE"]["TEXT"]?>
                    
                    <?if(($arResult["PROPERTIES"]["TAB_CHARACTERISTICS_FILES"]["VALUE"]) && ($arResult["PROPERTIES"]["TAB_DESCRIPTION_FILES"]["DESCRIPTION"])):?>
                      <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                          <div class="on-left">
                            <i class="fa fa-file-pdf-o"></i>
                          </div>
                          <div class="on-left">
                            <div class="pdf-link-url"><a href="<?=CFile::GetPath($arResult["PROPERTIES"]["TAB_CHARACTERISTICS_FILES"]["VALUE"])?>" download><?=$arResult["PROPERTIES"]["TAB_CHARACTERISTICS_FILES"]["DESCRIPTION"]?></a></div>
                            <div class="pdf-link-format">формат PDF</div>
                          </div>
                        </div>
                      </div>
                    <?endif;?>
                </div>
            </div>
        </div>
      </div>
    <?}
//  }
}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>