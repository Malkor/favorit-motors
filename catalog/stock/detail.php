<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("В наличии");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element",
	"catalog_stock_element",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "13",
		"ELEMENT_ID" => $_REQUEST["ID"],
		"ELEMENT_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"OFFERS_LIMIT" => "0",
		"TEMPLATE_THEME" => "blue",
		"DISPLAY_NAME" => "Y",
		"DETAIL_PICTURE_MODE" => "IMG",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"USE_VOTE_RATING" => "N",
		"USE_COMMENTS" => "N",
		"BRAND_USE" => "N",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"DISPLAY_COMPARE" => "N",
		"PRICE_CODE" => array(),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(),
		"ADD_TO_BASKET_ACTION" => array(0=>"BUY",),
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_COMPARE" => "Сравнить",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity"
	)
);?>
 <?/*$APPLICATION->IncludeComponent(
	"orion:ext.feedback.form", 
	"ext_feedback_form", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"SEND_MESSAGE" => "N",
		"USER_SEND_MESSAGE" => "N",
		"IBLOCK_TYPE" => "altasib_feedback",
		"IBLOCK_ID" => "21",
		"IBLOCK_ELEMENT_ID" => "",
		"SAVE_TO_IB" => "Y",
		"COMPONENT_ID" => "reviews_feedback_form",
		"FORM_CODE" => "",
		"USER_MESSAGE_ADD" => "Спасибо! Ваш отзыв отправлен на модераторацию.",
		"RESIZE_IMAGES" => "N",
		"DEFAULT_INPUT_SIZE" => "30",
		"MAX_FILE_SIZE" => "0",
		"INPUT_AS_PASSWORD" => "",
		"INPUT_AS_PASSWORD_CONFIRM" => "Y",
		"USE_CAPTCHA" => "N",
		"USE_CAPTCHA_REFRESH" => "Y",
		"PROPERTY_CODES" => array(
			0 => "NAME",
			1 => "PROP_THEME",
			2 => "PROP_TEXT",
		),
		"PROPERTY_CODES_REQUIRED" => array(
		),
		"GROUPS" => array(
		),
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_LEVELS" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"USE_TEXT_FOR_HTML" => array(
		),
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"FIELD_SELF_NAMES" => "N",
		"FIELD_ORDER" => "N",
		"FIELD_VALID" => "N",
		"FIELD_ERRMSG" => "N",
		"FIELD_PREDEF" => "N",
		"NEED_JQUERY" => "ORION_JQUERY",
		"FORM_NAME" => "фывфывфывф",
		"EFBF_FORM_WIDTH" => "",
		"DATA-TABLE-WIDTH" => "100%",
		"DATA-TABLE-COL1-WIDTH" => "40%",
		"DATA-TABLE-COL2-WIDTH" => "60%",
		"DATA-TABLE-LABEL-ALIGN-H" => "l-align",
		"DATA-TABLE-LABEL-ALIGN-V" => "c-valign",
		"FIELD_ERROR_POSITION" => "Y",
		"ERROR_MESSAGES_POSITION" => "UNDER",
		"AJAX_OPTION_ADDITIONAL" => "",
		"LIST_NOT_ESTABLISHED_PROP_THEME" => "Y"
	),
	false
);*/?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>