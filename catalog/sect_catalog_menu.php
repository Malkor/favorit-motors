<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="cat_nav">
	<a href="/catalog/stock/" class="cat_nav__link cat_nav__link--active">Автомобили в наличии</a>
	<a href="/catalog/catalog2.php" class="cat_nav__link">Подержанные автомобили</a>
	<a href="/catalog/order/" class="cat_nav__link cat_nav__link--last">Авто на заказ</a>
</div>