/**
 * Created by Znoy on 3/12/2015.
 */
$(document).ready(function(){
    // simple jMenu plugin called
    $("#jMenu").jMenu();

    // more complex jMenu plugin called
    $("#jMenu").jMenu({
        effects : {
            effectSpeedOpen : 300,
            effectTypeClose : false
        },
        animatedText : false
    });

    /*$('.menu-element').hover(function() {
            $(this).css({ 'background-color': 'rgba(255,255,255,0.5)'});
        }, function() {
            $(this).css({ 'background-color': 'rgba(0,0,0,0.4)'});
        }
    );*/

    $('.techCenter').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
            $(this).find('.img').css({ 'background-position': '0px -72px'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
            $(this).find('.img').css({ 'background-position': '0px 0px'});
        }
    );

    $('.testDrive').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
            $(this).find('.img').css({ 'background-position': '0px -216px'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
            $(this).find('.img').css({ 'background-position': '0px -144px'});
        }
    );

    $('.sellAuto').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
            $(this).find('.img').css({ 'background-position': '0px -596px'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
            $(this).find('.img').css({ 'background-position': '0px -526px'});
        }
    );

    $('.calculator').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
            $(this).find('.img').css({ 'background-position': '0px -360px'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
            $(this).find('.img').css({ 'background-position': '0px -288px'});
        }
    );

    $('.newAuto').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
        }
    );

    $('.usedAuto').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
        }
    );

    $('.commercialAuto').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
        }
    );

    $('.percents').hover(function() {
            $(this).css({ 'color': 'rgb(189,15,31)'});
            $(this).find('.img').css({ 'background-position': '0px -479px'});
        }, function() {
            $(this).css({ 'color': 'rgb(85,85,85)'});
            $(this).find('.img').css({ 'background-position': '0px -432px'});
        }
    );

    $('.menu-element').click(function() {
        $('.tab-bar-bg').css({ visibility: 'hidden' });
        $('#' + $(this).attr('data-link')).css({ visibility: 'visible' });
        $('.menu-element').css({ 'background-color': 'rgba(0,0,0,0.4)'});
        $(this).css({ 'background-color': 'rgba(255,255,255,0.5)'});
    });

    $('.menu-element').each(function() {
        if ($(this).attr('data-link') == 'promotionVolvo') {
            $(this).css({ 'background-color': 'rgba(255,255,255,0.5)'});
        }
    });

    $('.techCenter:before').css({ display: 'none' });


    /* Vetraz */

    var $price_slider;

    $('.select--styled-ui').selectmenu({
        width: function(){
            return $(this).parent().width();
        }
    });

    $( "#price-range" ).slider({
        range: true,
        min: 100,
        max: 3000,
        values: [ 779, 2244 ],
        slide: function( event, ui ) {
            $( "#price-first" ).html( ui.values[ 0 ] );
            $( "#price-second" ).html( ui.values[ 1 ] );
            $( "#price-val-first" ).val( ui.values[ 0 ] );
            $( "#price-val-second" ).val( ui.values[ 1 ] );
        }
    });

    $('#slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true
    });

    $('.location').hover(function() {
        $(this).find('.info').show();
    }, function() {
        $(this).find('.info').hide();
    });

    $price_slider = $( "#price-range" );
    $( "#price-first" ).html( $price_slider.slider( "values", 0 ) );
    $( "#price-second" ).html( $price_slider.slider( "values", 1 ) );
    $( "#price-val-first" ).val( $price_slider.slider( "values", 0 ) );
    $( "#price-val-second" ).val( $price_slider.slider( "values", 1 ) );

    $( ".big-filter" ).accordion({
        header: ".widget__title",
        heightStyle: "content"
    });

    if (window.location.href.indexOf('catalog3') != -1 || window.location.href.indexOf('catalog_brand') != -1) {
        $( ".big-filter" ).accordion( "option", "active", 1 );
    }

    $( ".slide-range" ).slider({
        range: true,
        min: 1,
        max: 1000,
        values: [ 1, 1000 ],
        slide: function( event, ui ) {
            var $parent = $(this).parents('.widget__content');
            $parent.find('.input-slider-l').val( ui.values[ 0 ] );
            $parent.find('.input-slider-r').val( ui.values[ 1 ] );
        }
    });
    
    $( ".slide-range" ).slider({
        range: true,
        min: 1,
        max: 1000,
        values: [ 1, 1000 ],
        slide: function( event, ui ) {
            var $parent = $(this).parents('.widget__content');
            $parent.find('.input-slider-l').val( ui.values[ 0 ] );
            $parent.find('.input-slider-r').val( ui.values[ 1 ] );
        }
    });
    
    $('.input-slider-l, .input-slider-r').on('blur', changeSliderValues);
    
    function changeSliderValues() {
        console.log(123);
        var leftval = $('.input-slider-l').val(),
            rightval = $('.input-slider-r').val();
        $('.input-slider-l').parents('.widget__content').find( ".slide-range" ).slider("values", [ leftval, rightval ]);
        
    }

    $('input[type="checkbox"]', '.marks__item').on('change', function(){

        var $parent = $(this).parents('.marks__item'),
            $selects = $parent.find('.select--styled-ui');

        $selects.selectmenu( "destroy" );

        $parent.children('.mark__subselect').toggle();

        $selects.selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });

    });

    $('.add-more-select').click(function(){

        var $parent = $(this).parent(),
            $selects = $parent.find('.select--styled-ui'),
            select_qauntity = $(this).data('quantity');

        $selects.selectmenu( "destroy" );

        $parent.find('.remove-select').before($parent.find('.select--styled-ui').first().clone());

        $parent.find('.select--styled-ui').selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });

        $(this).data('quantity', ++select_qauntity);

        if (select_qauntity > 1) $parent.find('.remove-select').show();

    });

    $('.widget__content').on('click', '.remove-select', function(){

        var $parent = $(this).parent(),
            $add_select = $parent.find('.add-more-select'),
            $selects = $parent.find('.select--styled-ui'),
            select_qauntity = $add_select.data('quantity');

        $selects.selectmenu( "destroy" );

        $selects.last().detach();

        $parent.find('.select--styled-ui').selectmenu({
            width: function(){
                return $(this).parent().width();
            }
        });

        $add_select.data('quantity', --select_qauntity);

        if (select_qauntity === 1) $(this).hide();

    });

    $('.add-more-mark-model').click(function(){

        var $parent = $(this).parents('.widget__allcontent'),
            models_quantity = $(this).data('quantity'),
            $clone;

        if (models_quantity < 3) {

            $parent.find('.select--styled-ui').selectmenu("destroy");

            $clone = $parent.children('.widget__content').first().clone();

            $clone.append('<div class="remove-link">х <span>Удалить</span></div>');

            $(this).parent().before($clone);

            $parent.find('.select--styled-ui').selectmenu({
                width: function () {
                    return $(this).parent().width();
                }
            });

            $(this).data('quantity', ++models_quantity);

            if (models_quantity === 3) $(this).parent().hide();

        }

    });

    $('.widget__allcontent').on('click', '.remove-link', function(){

        var $mark_model = $('.add-more-mark-model'),
            models_qauntity = $mark_model.data('quantity');

        $(this).parent().detach();

        $mark_model.data('quantity', --models_qauntity);

        if (models_qauntity === 2) $mark_model.parent().show();

    });

    $('.opener').click(function(){

        $(this).toggleClass('active').parent().children('ul').toggle();

    });

    var app = {
        scrolled: 0,
        footerOffset: 0,
        menuOffset: 0
    };

    function adjustFooterBar() {

        app.scrolled = window.pageYOffset || document.documentElement.scrollTop;
        app.footerOffset = $('.footer').offset().top;

        if (app.scrolled + window.innerHeight - 67 >= app.footerOffset) {
            $('.footer-menu-container').css({ bottom: app.scrolled + window.innerHeight - 67 - app.footerOffset });
        } else {
            $('.footer-menu-container').css({ bottom: 0 });
        }
    };

    adjustFooterBar();

    window.onscroll = function() {
        adjustFooterBar();
    };
});