<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("FAVOTIR MOTORS (каталог автомобилей на заказ)");?><?$arrFilter = array();?> <?$arFields = array();?> <?CModule::IncludeModule('iblock');?> <?if($_REQUEST["tag"] != ""):?> <?
    $arSelect = Array("ID", "IBLOCK_ID", "CODE", "PREVIEW_TEXT", "DETAIL_TEXT");
    $arFilter = Array("IBLOCK_ID"=>30, "CODE"=>$_REQUEST["tag"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arrFilter = array("PROPERTY_TAGS"=>$arFields["ID"]);
    }
    ?> <?endif?> <!-- Content --> <section id="content">
<div class="section section-gray section-sec section-top">
	<div class="box-container">
		<div class="post-title">
			<h1><?=$APPLICATION->ShowTitle();?></h1>
		</div>
		<div class="gray-text">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "header_text",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
	</div>
</div>
<div class="tabs-navigation section-gray catalog-page">
	<div class="box-container clearfix">
		<div class="on-left">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"inner_menu_catalog_order",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"ROOT_MENU_TYPE" => "catalog",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "catalog",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	)
);?>
		</div>
		<div class="on-right">
			<div class="catalog-filter-results">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.viewed.products",
	"catalog.viewed.products",
	Array(
		"COMPONENT_TEMPLATE" => "vertical",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "17",
		"SHOW_FROM_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"PAGE_ELEMENT_COUNT" => "15",
		"LINE_ELEMENT_COUNT" => "3",
		"TEMPLATE_THEME" => "blue",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"PRICE_CODE" => array(),
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"SHOW_PRODUCTS_17" => "Y",
		"SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
		"SECTION_ELEMENT_CODE" => "",
		"DEPTH" => "2",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE_17" => array(0=>"PRICE",1=>"",),
		"CART_PROPERTIES_17" => array(0=>"",1=>"",),
		"ADDITIONAL_PICT_PROP_17" => "",
		"LABEL_PROP_17" => "-",
		"SHOW_PRODUCTS_13" => "N",
		"PROPERTY_CODE_13" => array(0=>"",1=>"",),
		"CART_PROPERTIES_13" => array(0=>"",1=>"",),
		"ADDITIONAL_PICT_PROP_13" => "TAB_COMPLECTS_FILES",
		"LABEL_PROP_13" => "-"
	)
);?>
			</div>
		</div>
	</div>
</div>
<div class="section section-sec catalog-page">
	<div class="box-container clearfix">
		<div class="on-left">
			<div class="brand-items">
				 <!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="chevrolet" src="images/brands/chevrolet.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="citroen" src="images/brands/citroen.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="skoda" src="images/brands/skoda.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="seat" src="images/brands/seat.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="ford" src="images/brands/ford.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="volvo" src="images/brands/volvo.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="kia" src="images/brands/kia.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="peugeot" src="images/brands/peugeot.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="ssangyong" src="images/brands/ssangyong.png"> </a><!--
                    --><a href="#" class="animate-custom opacity-custom-hover brand-item"> <img alt="opel" src="images/brands/opel.png"> </a><!--
                -->
			</div>
			<div class="car-type-items">
				 <!--
                    --><a href="#" class="car-type-item animate-custom"> <span class="car-type-item-icon"> <img alt="icon" src="images/icons/type/blur/1.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/1.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Седан</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/2.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/2.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Хэтчбэк</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/3.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/3.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Универсал</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/4.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/4.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Кроссовер</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/5.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/5.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Внедорожник</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/6.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/6.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Купе</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/7.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/7.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Кабриолет</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/8.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/8.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Пикап</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/9.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/9.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Минивэн</span> </a><!--
                    --><a href="#" class="car-type-item"> <span class="car-type-item-icon animate-custom"> <img alt="icon" src="images/icons/type/blur/10.png" class="car-type-item-icon-blur animate-custom"> <img alt="icon" src="images/icons/type/hover/10.png" class="car-type-item-icon-hover animate-custom"> </span> <span class="car-type-item-text animate-custom">Ком. транс.</span> </a><!--
                -->
			</div>
		</div>
		<div class="on-right">
			<div class="catalog-filter">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter",
	"catalog_smart_filter_order",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => "17",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"TEMPLATE_THEME" => "blue",
		"FILTER_VIEW_MODE" => "vertical",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SAVE_IN_SESSION" => "N",
		"INSTANT_RELOAD" => "N",
		"PRICE_CODE" => "",
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "-",
		"SECTION_DESCRIPTION" => "-",
		"POPUP_POSITION" => "left"
	)
);?>
			</div>
		</div>
	</div>
</div>
<div class="section section-sec catalog-page padding">
	<div class="box-container clearfix">
		<div class="on-left">
			<div class="catalog-page-cars-block">
				<div class="title border-bottom">
					<h2 class="red-text">Предложение дня</h2>
				</div>
				<div class="new-auto-items three-in-line">
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                -->
				</div>
				<div class="bottom-part clearfix">
					<div class="on-right text-body">
 <a href="#">Смотреть все: 76</a>
					</div>
				</div>
			</div>
			<div class="catalog-page-cars-block">
				<div class="title border-bottom">
					<h2>Автомобили в наличии</h2>
				</div>
				<div class="new-auto-items three-in-line">
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                -->
				</div>
				<div class="bottom-part clearfix">
					<div class="on-right text-body">
 <a href="#">Смотреть все: 88</a>
					</div>
				</div>
			</div>
			<div class="catalog-page-cars-block">
				<div class="title border-bottom">
					<h2>Подержанные автомобили</h2>
				</div>
				<div class="new-auto-items three-in-line">
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                    -->
					<div class="new-auto-item">
						<div class="new-auto-item-image">
 <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
						</div>
						<div class="new-auto-item-name red-hover">
 <a href="#" class="animate-custom">KIA Sorento</a>
						</div>
						<div class="new-auto-item-desc">
							<div class="new-auto-item-desc-field">
								 2014, Luxe 2.0 AT
							</div>
							<div class="new-auto-item-desc-field">
								 2 л, бензин (150 л.с.) АКПП
							</div>
						</div>
						<div class="new-auto-item-info clearfix">
							<div class="on-left">
								<div class="new-auto-item-price-new">
									 1 019 900
								</div>
								<div class="new-auto-item-price-old">
									 1 320 000
								</div>
							</div>
							<div class="on-right">
 <a href="#" class="animate-custom gray-small-link">Подробнее</a>
							</div>
						</div>
					</div>
					 <!--
                -->
				</div>
				<div class="bottom-part clearfix">
					<div class="on-right text-body">
 <a href="#">Смотреть все: 420</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <!-- Seo Text -->
<div class="section section-sec padding">
	<div class="box-container">
		<div class="title">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "footer_title",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
		<div class="seo-text gray-text">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "footer_text",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
	</div>
</div>
 <!-- /Seo Text --> </section><!-- /Content -->
<?/*$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "catalog_section_order",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "17",
        "SECTION_ID" => $_REQUEST["SECTION_ID"],
        "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "ELEMENT_SORT_FIELD" => $sort_f,
        "ELEMENT_SORT_ORDER" => $sort_o,
        "ELEMENT_SORT_FIELD2" => "",
        "ELEMENT_SORT_ORDER2" => "",
        "FILTER_NAME" => "arrFilter",
        "INCLUDE_SUBSECTIONS" => "Y",
        "SHOW_ALL_WO_SECTION" => "Y",
        "HIDE_NOT_AVAILABLE" => "N",
        "PAGE_ELEMENT_COUNT" => $num_c,
        "LINE_ELEMENT_COUNT" => "3",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "OFFERS_LIMIT" => "5",
        "TEMPLATE_THEME" => "blue",
        "PRODUCT_SUBSCRIPTION" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_CLOSE_POPUP" => "N",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "SET_BROWSER_TITLE" => "Y",
        "BROWSER_TITLE" => "-",
        "SET_META_KEYWORDS" => "Y",
        "META_KEYWORDS" => "-",
        "SET_META_DESCRIPTION" => "Y",
        "META_DESCRIPTION" => "-",
        "ADD_SECTIONS_CHAIN" => "N",
        "SET_STATUS_404" => "N",
        "CACHE_FILTER" => "N",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRICE_CODE" => array(
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "CONVERT_CURRENCY" => "N",
        "BASKET_URL" => "/personal/basket.php",
        "USE_PRODUCT_QUANTITY" => "N",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRODUCT_PROPERTIES" => array(
        ),
        "ADD_TO_BASKET_ACTION" => "ADD",
        "DISPLAY_COMPARE" => "N",
        "PAGER_TEMPLATE" => "paginator",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "MESS_BTN_COMPARE" => "Сравнить",
        "AJAX_OPTION_ADDITIONAL" => "",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PICT_PROP" => "-",
        "LABEL_PROP" => "-"
    ),
    false
);*/?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.viewed.products",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "17",
		"SHOW_FROM_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"PAGE_ELEMENT_COUNT" => "15",
		"LINE_ELEMENT_COUNT" => "3",
		"TEMPLATE_THEME" => "blue",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"PRICE_CODE" => array(),
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"SHOW_PRODUCTS_17" => "Y",
		"SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
		"SECTION_ELEMENT_CODE" => "",
		"DEPTH" => "2",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE_17" => array("CARBRAND","CARFAMILY","CARMODEL","PRICE",""),
		"CART_PROPERTIES_17" => array("",""),
		"ADDITIONAL_PICT_PROP_17" => "",
		"LABEL_PROP_17" => "-",
		"SHOW_PRODUCTS_13" => "N",
		"PROPERTY_CODE_13" => array(0=>"",1=>"",),
		"CART_PROPERTIES_13" => array(0=>"",1=>"",),
		"ADDITIONAL_PICT_PROP_13" => "TAB_COMPLECTS_FILES",
		"LABEL_PROP_13" => "-"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>