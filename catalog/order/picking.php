<?if(isset($_SERVER['HTTP_X_PJAX']) && $_SERVER['HTTP_X_PJAX'] == 'true'){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



	if(CModule::IncludeModule("iblock"))
	{ 
		if($_REQUEST["ID"] != "")
		{
			print_r($_REQUEST["ID"]);
			$id = $_REQUEST["ID"];
			$arResult = array();
			$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID"=>28, "ID"=>IntVal($id), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(array(), $arFilter, false, array("nTopCount"=>1), $arSelect);
			if($ob = $res->GetNextElement()){ 
				$arProps = $ob->GetProperties();
				$arResult = $arProps["EQUIPMENTS"];
			}
			?>
<div id="section">
		<div class="section tab-container section-sec">
            <div class="box-container">

                <div class="title">
                    <h2>Опции</h2>
                </div>

                <div class="standard-equipment-items">
                    <div class="standard-equipment-item">
                        <div class="standard-equipment-title">
                            <h3>Базовые и дополнительные опции</h3>
                        </div>
                        <div class="standard-equipment-list">
                            <ul>
                                <?foreach($arResult["VALUE"] as $equipment):?>
                                  <li><?=$equipment;?></li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>

                <?=$arResult["PROPERTIES"]["TAB_COMPLECTS"]["~VALUE"]["TEXT"]?>

            </div>
        </div>
</div>
		<?}
	}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}

?>