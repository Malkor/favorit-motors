<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
global $USER;
$APPLICATION->IncludeComponent("bitrix:system.auth.form", "fm_auth_form", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"REGISTER_URL" => "/personal/registration.php",	// Страница регистрации
		"FORGOT_PASSWORD_URL" => "/personal/forgot.php",	// Страница забытого пароля
		"PROFILE_URL" => "/personal/index.php",	// Страница профиля
		"SHOW_ERRORS" => "N",	// Показывать ошибки
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>