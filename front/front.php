<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Front");
?>


    <!-- Content -->
    <section id="content">

        <!-- Super Slider -->
        <div class="super-slider front">

            <div class="front-slider">

                <div id="chevrolet-event" class="front-slider-item bg active" style="background-image: url(content/front/chevrolet.jpg); "></div>
                <div id="citroen-event" class="front-slider-item bg" style="background-image: url(content/front/citroen.jpg); "></div>
                <div id="peugeot-event" class="front-slider-item bg" style="background-image: url(content/front/peugeot.jpg); "></div>
                <div id="ford-event" class="front-slider-item bg" style="background-image: url(content/front/ford.jpg); "></div>
                <div id="kia-event" class="front-slider-item bg" style="background-image: url(content/front/kia.jpg); "></div>
                <div id="seat-event" class="front-slider-item bg" style="background-image: url(content/front/seat.jpg); "></div>
                <div id="skoda-event" class="front-slider-item bg" style="background-image: url(content/front/skoda.jpg); "></div>
                <div id="ssangyong-event" class="front-slider-item bg" style="background-image: url(content/front/ssangyong.jpg); "></div>
                <div id="volvo-event" class="front-slider-item bg" style="background-image: url(content/front/volvo.jpg); "></div>

            </div>

            <div class="box-container">

                <div class="left-side">

                    <ul class="event-slider-menu">

                        <li class="active"><a href="#" class="animate-custom" data-block="chevrolet-event" style="background-image: url(images/brands/small/chevrolet.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="citroen-event" style="background-image: url(images/brands/small/citroen.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="peugeot-event" style="background-image: url(images/brands/small/peugeot.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="ford-event" style="background-image: url(images/brands/small/ford.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="kia-event" style="background-image: url(images/brands/small/kia.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="opel-event" style="background-image: url(images/brands/small/opel.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="seat-event" style="background-image: url(images/brands/small/seat.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="skoda-event" style="background-image: url(images/brands/small/skoda.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="ssangyong-event" style="background-image: url(images/brands/small/ssangyong.png); "></a></li>
                        <li><a href="#" class="animate-custom" data-block="volvo-event" style="background-image: url(images/brands/small/volvo.png); "></a></li>

                    </ul>

                </div>

                <div class="right-side">

                    <div class="front-search-filter">

                        <div class="front-search-filter-navigation">
                            <ul class="front-search-filter-menu clearfix">
                                <li><a class="animate-custom" data-block="#front-search-new">Новые</a></li>
                                <li><a class="animate-custom" data-block="#front-search-exist">В наличии</a></li>
                                <li class="active"><a class="animate-custom" data-block="#front-search-old">С пробегом</a></li>
                            </ul>
                        </div>

                        <div id="front-search-new" class="front-search-filter-item">

                            <form>
                                <div class="front-search-item">
                                    <label for="front-search-new-mark"></label>
                                    <select id="front-search-new-mark">
                                        <option>Марка ...</option>
                                        <option>Марка 1</option>
                                        <option>Марка 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item">
                                    <label for="front-search-new-model"></label>
                                    <select id="front-search-new-model">
                                        <option>Модель ...</option>
                                        <option>Модель 1</option>
                                        <option>Модель 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item accordion animate-custom ui-slider-inner active">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Цена:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> p.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-new-start-price"></label>
                                                <input type="text" name="front-search-new-start-price" class="first-position" id="front-search-new-start-price" value="500000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-new-finish-price"></label>
                                                <input type="text" name="front-search-new-finish-price" class="second-position" id="front-search-new-finish-price" value="3000000" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item submit on-center">
                                    <input type="submit" name="front-search-new-submit" class="red-small-link animate-custom" value="Подобрать" />
                                </div>
                            </form>

                        </div>

                        <div id="front-search-exist" class="front-search-filter-item">

                            <form>
                                <div class="front-search-item">
                                    <label for="front-search-exist-mark"></label>
                                    <select id="front-search-exist-mark">
                                        <option>Марка ...</option>
                                        <option>Марка 1</option>
                                        <option>Марка 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item">
                                    <label for="front-search-exist-model"></label>
                                    <select id="front-search-exist-model">
                                        <option>Модель ...</option>
                                        <option>Модель 1</option>
                                        <option>Модель 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item accordion animate-custom with-border ui-slider-inner active">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Цена:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> p.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-exist-start-price"></label>
                                                <input type="text" name="front-search-exist-start-price" class="first-position" id="front-search-exist-start-price" value="500000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-exist-finish-price"></label>
                                                <input type="text" name="front-search-exist-finish-price" class="second-position" id="front-search-exist-finish-price" value="3000000" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item accordion animate-custom ui-slider-inner">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Год:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> г.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-exist-start-year"></label>
                                                <input type="text" name="front-search-exist-start-year" class="first-position" id="front-search-exist-start-year" value="2000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-exist-finish-year"></label>
                                                <input type="text" name="front-search-exist-finish-year" class="second-position" id="front-search-exist-finish-year" value="2015" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item submit on-center">
                                    <input type="submit" name="front-search-exist-submit" class="red-small-link animate-custom" value="Подобрать" />
                                </div>
                            </form>

                        </div>

                        <div id="front-search-old" class="front-search-filter-item">

                            <form>
                                <div class="front-search-item">
                                    <label for="front-search-old-mark"></label>
                                    <select id="front-search-old-mark">
                                        <option>Марка ...</option>
                                        <option>Марка 1</option>
                                        <option>Марка 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item">
                                    <label for="front-search-old-model"></label>
                                    <select id="front-search-old-model">
                                        <option>Модель ...</option>
                                        <option>Модель 1</option>
                                        <option>Модель 2</option>
                                    </select>
                                </div>
                                <div class="front-search-item accordion animate-custom with-border ui-slider-inner active">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Цена:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> p.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-old-start-price"></label>
                                                <input type="text" name="front-search-old-start-price" class="first-position" id="front-search-old-start-price" value="500000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-old-finish-price"></label>
                                                <input type="text" name="front-search-old-finish-price" class="second-position" id="front-search-old-finish-price" value="3000000" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item accordion animate-custom with-border ui-slider-inner">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Пробег:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> км.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-old-start-miles"></label>
                                                <input type="text" name="front-search-old-start-miles" class="first-position" id="front-search-old-start-miles" value="10000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-old-finish-miles"></label>
                                                <input type="text" name="front-search-old-finish-miles" class="second-position" id="front-search-old-finish-miles" value="100000" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item accordion animate-custom ui-slider-inner">
                                    <div class="front-search-item-title clearfix">
                                        <div class="front-search-item-title-label">Год:</div>
                                        <div class="front-search-item-title-data">
                                            <span class="first-label"></span>
                                            <span class="sec-label"> - </span>
                                            <span class="second-label"></span>
                                            <span class="type-label"> г.</span>
                                        </div>
                                    </div>
                                    <div class="front-search-item-accordion-content">
                                        <div class="front-search-item-section clearfix">
                                            <div class="front-search-item-first">
                                                <label for="front-search-old-start-year"></label>
                                                <input type="text" name="front-search-old-start-year" class="first-position" id="front-search-old-start-year" value="2000" />
                                            </div>
                                            <div class="front-search-item-second">
                                                <label for="front-search-old-finish-year"></label>
                                                <input type="text" name="front-search-old-finish-year" class="second-position" id="front-search-old-finish-year" value="2015" />
                                            </div>
                                        </div>
                                        <div class="ui-slider-container"></div>
                                    </div>
                                </div>
                                <div class="front-search-item submit on-center">
                                    <input type="submit" name="front-search-old-submit" class="red-small-link animate-custom" value="Подобрать" />
                                </div>
                            </form>

                        </div>

                    </div>

                </div>

                <div class="front-slider-text-items">

                    <div id="chevrolet-event-text" class="front-slider-text-item active">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="citroen-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="peugeot-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="ford-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="kia-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="seat-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="skoda-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="ssangyong-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                    <div id="volvo-event-text" class="front-slider-text-item">
                        <div class="front-slider-text-item-title">
                            <a href="#">Кликабельный заголовок</a>
                        </div>
                        <div class="front-slider-text-item-desc">Немного текста, который  будет рассказывать об основных преимуществах<br/>Переход в акции</div>
                    </div>

                </div>

            </div>
        </div><!-- /Super Slider -->

        <!-- Front Buttons -->
        <div class="front-buttons section section-sec section-gray">
            <div class="box-container">

                <div class="front-info-items clearfix">

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-icon"><i class="fa fa-wrench"></i></span>
                            <span class="front-info-item-text">Записаться в<br/>тех. центр</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-icon"><i class="fa fa-car"></i></span>
                            <span class="front-info-item-text">Пройти<br/>Тест-Драйв</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-icon"><i class="fa fa-thumbs-up"></i></span>
                            <span class="front-info-item-text">Продать авто<br/>выгодно</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-icon"><i class="fa fa-calculator"></i></span>
                            <span class="front-info-item-text">калькулятор<br/>каско</span>
                        </a>
                    </div>

                </div>

            </div>
        </div><!-- /Front Buttons -->

        <!-- Front Info -->
        <div class="front-info section">
            <div class="box-container">

                <div class="front-info-small-items clearfix">

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-text-icon">4500</span>
                            <span class="front-info-item-text">Новых<br/>автомобилей</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-text-icon">1500</span>
                            <span class="front-info-item-text">Автомобилей<br/>с пробегом</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-text-icon">500</span>
                            <span class="front-info-item-text">Коммерческих<br/>автомобилей</span>
                        </a>
                    </div>

                    <div class="front-info-item">
                        <a href="#" class="front-info-item-wrapper animate-custom clearfix">
                            <span class="front-info-item-icon"><i class="fa fa-rub"></i></span>
                            <span class="front-info-item-text">Рассчитать<br/>кредит</span>
                        </a>
                    </div>

                </div>

            </div>
        </div><!-- /Front Info -->

        <!-- Front Map -->
        <div class="front-map section">
            <div class="box-container clearfix">

                <div class="favorit-title clearfix">
                    <h2>Почему Favorit Motors</h2>
                </div>

                <div class="on-left">

                    <div class="about-company-items">
                        <div class="about-company-items-wrapper">
                            <div class="about-company-item clearfix">
                                <div class="about-company-item-icon"><i class="fa fa-check-square-o"></i></div>
                                <div class="about-company-item-text">Сертифицированные<br/>специалисты</div>
                            </div>
                            <div class="about-company-item clearfix">
                                <div class="about-company-item-icon"><i class="fa fa-check-square-o"></i></div>
                                <div class="about-company-item-text">Накопительные<br/>скидки и бонусы</div>
                            </div>
                            <div class="about-company-item clearfix">
                                <div class="about-company-item-icon"><i class="fa fa-check-square-o"></i></div>
                                <div class="about-company-item-text">Прямая связь<br/>с руководством</div>
                            </div>
                            <div class="about-company-item clearfix">
                                <div class="about-company-item-icon"><i class="fa fa-check-square-o"></i></div>
                                <div class="about-company-item-text">Шесть салонов с<br/>удобной доступностью</div>
                            </div>
                        </div>
                        <div class="about-company-link">
                            <a href="#">О компании</a>
                        </div>
                    </div>

                </div>

                <div class="on-right">

                    <div id="big-center-map">

                        <div class="tech-center tech-center-1 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                        <div class="tech-center tech-center-2 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                        <div class="tech-center tech-center-3 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                        <div class="tech-center tech-center-4 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                        <div class="tech-center tech-center-5 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                        <div class="tech-center tech-center-6 animate-custom">
                            <div class="tech-center-icon"></div>
                            <div class="tech-center-info">
                                <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                <div class="tech-center-info-worktime red-text">График работы:</div>
                                <div class="tech-center-info-worktime-line clearfix">
                                    <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                    <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                </div>
                                <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div><!-- /Front Map -->

        <div class="reviews-slider-block front">
            <div class="box-container">

                <div class="reviews-slider-title">
                    <h2>Отзывы</h2>
                </div>

                <div class="reviews-slider-header opacity-custom"><i class="fa fa-pencil"></i></div>

                <div class="reviews-slider swiper-container">

                    <div class="swiper-wrapper clearfix">

                        <div class="swiper-slide">

                            <div class="reviews-slider-item">
                                <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                <div class="reviews-slider-item-text">
                                    <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                </div>
                                <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                <div class="add-review-link">
                                    <a href="#" class="animate-custom opacity-custom">Оставить свой отзыв</a>
                                </div>
                            </div>

                        </div>

                        <div class="swiper-slide">

                            <div class="reviews-slider-item">
                                <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                <div class="reviews-slider-item-text">
                                    <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                </div>
                                <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                <div class="add-review-link">
                                    <a href="#" class="animate-custom opacity-custom">Оставить свой отзыв</a>
                                </div>
                            </div>

                        </div>

                        <div class="swiper-slide">

                            <div class="reviews-slider-item">
                                <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                <div class="reviews-slider-item-text">
                                    <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                </div>
                                <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                <div class="add-review-link">
                                    <a href="#" class="animate-custom opacity-custom">Оставить свой отзыв</a>
                                </div>
                            </div>

                        </div>

                        <div class="swiper-slide">

                            <div class="reviews-slider-item">
                                <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                <div class="reviews-slider-item-text">
                                    <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                </div>
                                <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                <div class="add-review-link">
                                    <a href="#" class="animate-custom opacity-custom">Оставить свой отзыв</a>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="swiper-pagination"></div>

                </div>

            </div>
        </div>

        <!-- Front Links -->
        <div class="front-links section padding section-sec">
            <div class="box-container clearfix">

                <div class="front-links-container">
                    <div class="front-links-inner">
                        <div class="front-links-title">
                            <h2>Рекомендации</h2>
                        </div>
                        <ul class="front-links-list">
                            <li><a href="#">Машина для девушки</a></li>
                            <li><a href="#">Обзор Chevrolet Cruze 2015</a></li>
                            <li><a href="#">Отличие КПП</a></li>
                        </ul>
                    </div>
                </div>

                <div class="front-links-container">
                    <div class="front-links-inner">
                        <div class="front-links-title">
                            <h2>Популярное</h2>
                        </div>
                        <ul class="front-links-list">
                            <li><a href="#">Каталог автомобилей в наличии</a></li>
                            <li><a href="#">Оставить отзыв на сотрудника</a></li>
                            <li><a href="#">Удаленное урегулирование убытков</a></li>
                        </ul>
                    </div>
                </div>

                <div class="front-links-container">
                    <div class="front-links-inner">
                        <div class="front-links-title">
                            <h2>Как выгодно продать авто?</h2>
                        </div>
                        <ul class="front-links-list">
                            <li><a href="#">Подготовка кузова автомобиля</a></li>
                            <li><a href="#">Как правильно мыть двигатель</a></li>
                            <li><a href="#">Уход за салоном</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div><!-- /Front Links -->

        <!-- Front About -->
        <div class="front-about section section-sec section-gray padding">
            <div class="box-container clearfix">

                <div class="on-left">
                    <div class="front-title">
                        <h2>О компании</h2>
                    </div>

                    <div class="body align-justify gray-text">
                        <p>ГК FAVORIT MOTORS демонстрирует принципиально новый подход к работе с клиентами. Она была первой и остается одной из самых активных и крупных фирм, которые предлагают возможность обратной связи. Каждый клиент имеет возможность посредством электронного блога общаться непосредственно с президентом компании. Такое нововведение привело к тому, что все пожелания клиентов своевременно удовлетворяются. Для ГК FAVORIT MOTORS механизм обратной связи по-настоящему важен, здесь это рассматривается как один из основных элементов эффективной работы, поскольку адекватное реагирование и корректное использование собранных сведений помогают значительно улучшить сервис. Успешный бизнес возможен, если посмотреть на него глазами клиента.</p>
                        <p>Социальный вектор ГК FAVORIT MOTORS заслуживает отдельного внимания. Внушительный по численности штат компании постоянно расширяется: привлекается все больше профессионалов, создаются новые рабочие места. Руководству благодаря четким подсчетам и правильной коммерческой политике удается это даже в периоды экономических кризисов. Для всех сотрудников создаются оптимальные условия труда с соблюдением и защитой трудовых прав. Такая социальная направленность не осталась незамеченной, и ГК FAVORIT MOTORS была удостоена национальной премии «Работа Года».</p>
                        <p>ГК FAVORIT MOTORS следит не только за количественными показателями, но и за уровнем квалификации своих сотрудников. Еще одна изюминка компании – собственный обучающий центр ААТ. Такая политика гарантирует ответственность ГК FAVORIT MOTORS за каждого своего сотрудника и его действия. Именно таким образом создается непоколебимый авторитет.</p>
                        <p>Все это позволило создать целую империю автоуслуг ГК FAVORIT MOTORS. </p>
                        <p>В этом году компания отпраздновала свое 20-летие на автомобильном рынке. Юбилей запомнился клиентам благодаря ряду акций и розыгрышу подарков. За двадцатилетнюю деятельность ГК FAVORIT MOTORS заняла свое место среди лидирующих автокомпаний и уверенно движется вперед. Постоянное развитие является залогом ее успеха.</p>
                    </div>

                </div>

                <div class="on-right align-left">
                    <div class="front-title">
                        <h2>Новости</h2>
                    </div>

                    <div class="front-news-items">
                        <div class="front-news-item">
                            <div class="front-news-item-date">06.03.2015</div>
                            <div class="front-news-item-title">
                                <a href="#" class="animate-custom red-hover">График работы в праздничные дни ГК FAVORIT MOTORS</a>
                            </div>
                        </div>
                        <div class="front-news-item">
                            <div class="front-news-item-date">04.03.2015</div>
                            <div class="front-news-item-title">
                                <a href="#" class="animate-custom red-hover">На Женевском автосалоне Volvo Cars раскрывает направление своего будущего развития</a>
                            </div>
                        </div>
                        <div class="front-news-item">
                            <div class="front-news-item-date">04.03.2015</div>
                            <div class="front-news-item-title">
                                <a href="#" class="animate-custom red-hover">Четвертый этап Volvo Ocean Race – после 20 дней борьбы тройку лидеров на финише разделили всего 8 минут</a>
                            </div>
                        </div>
                        <div class="front-news-item">
                            <div class="front-news-item-date">03.03.2015</div>
                            <div class="front-news-item-title">
                                <a href="#" class="animate-custom red-hover">Проект Volvo Cars по созданию системы обмена информацией между автомобилями открывает новые направления для повышения комфорта и безопасности</a>
                            </div>
                        </div>
                        <div class="front-news-item">
                            <div class="front-news-item-date">03.03.2015</div>
                            <div class="front-news-item-title">
                                <a href="#" class="animate-custom red-hover">Майкл Мальмстен назначен президентом Volvo Car Russia</a>
                            </div>
                        </div>
                    </div>

                    <div class="front-news-archive align-right">
                        <a href="#" class="gray-text">Архив новостей</a>
                    </div>

                </div>

            </div>
        </div><!-- /Front About -->

    </section><!-- /Content -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>