<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Rating");
?>

    <!-- Content -->
    <section id="content">

        <div id="master-rating" class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>Рейтинг сотрудников</h2>
                </div>

                <div class="master-rating-filter clearfix">
                    <div class="on-left">
                        <form class="clearfix">
                            <div class="filter-item">
                                <label for="master-rating-position">Должность:</label>
                                <select id="master-rating-position">
                                    <option>Выбрать</option>
                                    <option>Должность 1</option>
                                    <option>Должность 2</option>
                                </select>
                            </div>
                            <div class="filter-item">
                                <label for="master-rating-subdivision">Подразделение:</label>
                                <select id="master-rating-subdivision">
                                    <option>Выбрать</option>
                                    <option>Подразделение 1</option>
                                    <option>Подразделение 2</option>
                                </select>
                            </div>
                            <div class="filter-item">
                                <label for="master-rating-center">Дилерский центр:</label>
                                <select id="master-rating-center">
                                    <option>Выбрать</option>
                                    <option>Дилерский центр 1</option>
                                    <option>Дилерский центр 2</option>
                                </select>
                            </div>
                            <input type="submit" class="red-small-link animate-custom" name="master-rating-filter-submit" value="Показать" />
                        </form>
                    </div>
                    <div class="on-right align-left">
                        <form class="clearfix">
                            <label for="master-rating-search-text"><i class="fa fa-search"></i></label>
                            <input type="text" name="master-rating-search-text" id="master-rating-search-text" placeholder="Поиск" />
                            <input type="submit" class="red-small-link animate-custom" name="master-rating-search-submit" value="Найти" />
                        </form>
                    </div>
                </div>

                <div class="master-rating-info clearfix">
                    <div class="on-left clearfix two-sort">
                        <div class="sort-criteria up"><a href="#">Рейтинг</a></div>
                        <div class="sort-criteria down"><a href="#">Сотрудник</a></div>
                    </div>
                    <div class="on-right align-left">
                        <h2>Недавно с вами работали</h2>
                    </div>
                </div>

                <div class="master-rating-section clearfix">
                    <div class="on-left">
                        <div class="master-items">
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                            </div>
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar">
                                    <i class="fa fa-star"></i>
                                    <span>0</span>
                                </div>
                            </div>
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar negative">
                                    <i class="fa fa-star"></i>
                                    <span>-190</span>
                                </div>
                            </div>
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                            </div>
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                            </div>
                            <div class="master-item clearfix">
                                <div class="master-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(); "></a>
                                </div>
                                <div class="master-item-info">
                                    <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                    <div class="master-item-center"><a href="#" class="red-text">Томилино KIA</a></div>
                                    <div class="master-item-division">Сервис-бюро</div>
                                    <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                </div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                            </div>
                        </div>

                        <div class="pagination">

                            <ul class="pagination-list">
                                <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                                <li><a href="#" class="animate-custom">3</a></li>
                                <li><a class="animate-custom active">4</a></li>
                                <li><a href="#" class="animate-custom">5</a></li>
                                <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                                <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                            </ul>

                        </div>

                    </div>
                    <div class="on-right align-left">
                        <div class="your-master-items">
                            <div class="your-master-item">
                                <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                <div class="master-item-center"><a href="#" class="red-text">Плановое ТО 10 000</a></div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                                <div class="master-item-rate">
                                    <a href="#">Оценить</a>
                                </div>
                            </div>
                            <div class="your-master-item">
                                <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                <div class="master-item-center"><a href="#" class="red-text">Плановое ТО 10 000</a></div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                                <div class="master-item-rate">
                                    <a href="#">Оценить</a>
                                </div>
                            </div>
                            <div class="your-master-item">
                                <div class="master-item-name"><a href="#">Воробьев Сергей Владимирович</a></div>
                                <div class="master-item-position">Мастер-консультант слесарного производства</div>
                                <div class="master-item-center"><a href="#" class="red-text">Плановое ТО 10 000</a></div>
                                <div class="master-item-rating-bar positive">
                                    <i class="fa fa-star"></i>
                                    <span>+312</span>
                                </div>
                                <div class="master-item-rate">
                                    <a href="#">Оценить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section><!-- /Content -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>