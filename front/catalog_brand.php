<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("CATALOG ORDER NEW!!!");?>

    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>ГК FAVORIT MOTORS</h2>
                </div>

                <div class="gray-text">
                    <p>Наша компания за двацатилетнию историю накопила богатый опыт на авторынке Москвы. Мы являемся офциальным дилером а/м Опель с их безумными ОРС, старейшего корейского автоконцерна СанЙонг представлеными моделями К... и далее начинаем перечислять все наши марки и модели легко и просто пишем о том какие мы разностороние какой классный у нас персонал как мы рады развиваться и прочее. Доводим требуемый Антоном до длины в 500 символов. Sed ut perspiciatis, unde omnis iste Sed ut perspiciatis, unde Amen</p>
                </div>

            </div>
        </div>

        <div class="tabs-navigation section-gray catalog-page">
            <div class="box-container clearfix">

                <div class="on-left">
                    <ul class="tabs three-tabs clearfix">
                        <li><a href="#" class="animate-custom">Автомобили в наличии</a></li>
                        <li><a href="#" class="animate-custom">Подержанные автомобили</a></li>
                        <li class="active"><a href="#" class="animate-custom">Авто на заказ</a></li>
                    </ul>
                </div>

                <div class="on-right">

                    <div class="catalog-filter-results">
                        <span>Недавно просмотренные: [ <a href="#" class="red-text animate-custom">7</a> ]</span>
                    </div>

                </div>

            </div>
        </div>

        <div class="section section-sec catalog-page">

            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="post-title">
                        <h1>Каталог автомобилей</h1>
                    </div>

                </div>

                <div class="on-right">

                    <div class="catalog-filter">
                        <form>
                            <div class="catalog-filter-item accordion active">
                                <div class="catalog-filter-title animate-custom">Марки</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Основные параметры</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Динамика и мощность</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Расход</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Двигатель</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item">
                                <div class="catalog-filter-title animate-custom">Категории</div>
                                <div class="catalog-filter-container">
                                    <ul class="catalog-filter-list">
                                        <li><a href="#">Кроссоверы до 600 т.р.</a></li>
                                        <li><a href="#">Семейные</a></li>
                                        <li><a href="#">Для девушек</a></li>
                                        <li><a href="#">Экономичные</a></li>
                                        <li><a href="#">Для поездок на дачу</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

        <div class="section section-sec catalog-page padding">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="catalog-brand-items"><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                        --><div class="catalog-brand-item">
                            <div class="catalog-brand-item-container">
                                <div class="catalog-brand-item-image">
                                    <a href="#" class="animate-custom opacity-custom-hover bg-c" style="background-image: url(content/models/popup-car.png); "></a>
                                </div>
                                <div class="catalog-brand-item-name">
                                    <a href="#" class="animate-custom red-hover">Peugeot 308 CC</a>
                                </div>
                                <div class="catalog-brand-item-price">от <strong>1 414 000</strong> рублей</div>
                                <div class="catalog-brand-item-more">
                                    <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                                </div>
                                <div class="catalog-brand-item-hidden">
                                    <div class="catalog-brand-item-hidden-container">
                                        <div class="catalog-brand-item-hidden-item">Невероятный уровень безопасности в своём классе</div>
                                        <div class="catalog-brand-item-hidden-item">Расход 1л / 300 км</div>
                                        <div class="catalog-brand-item-hidden-item">Объём багажника 900 л</div>
                                    </div>
                                </div>
                            </div>
                        </div><!--

                    --></div>

                </div>

            </div>
        </div>

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="title">
                    <h2>Заголовок</h2>
                </div>

                <div class="seo-text gray-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>