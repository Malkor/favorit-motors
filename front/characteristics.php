<div class="box-container">

                    <div class="characteristics-navigation clearfix">
                        <span class="on-left">Кузов: </span>
                        <ul class="on-left clearfix">
                            <li class="on-left active"><a data-block="#sedan">седан</a></li>
                            <li class="on-left"><a data-block="#hatchback">хэчбэк</a></li>
                            <li class="on-left"><a data-block="#wagon">универсал</a></li>
                        </ul>
                    </div>

                    <div class="char-tabs-container" id="sedan">

                        <div class="body-arrangement sedan">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип топлива</td>
                                            <td colspan="2">бензиновый</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип двигателя</td>
                                            <td class="border-right">2.0 NU CVVL MPI</td>
                                            <td>2.4 Theta CVVT MPI</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Кол-во/расположение цилиндров</td>
                                            <td colspan="2">4, рядное</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Рабочий объем, см3</td>
                                            <td class="border-right">2000</td>
                                            <td>2400</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Количество клапанов</td>
                                            <td colspan="2">16</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                            <td class="border-right">150 (6500)</td>
                                            <td>180 (6000)</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                            <td class="border-right">196 @ 4800</td>
                                            <td>231 @ 4000</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Требования к топливу</td>
                                            <td colspan="2">бензин RON95</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип трансмиссии</td>
                                            <td class="border-right">MT</td>
                                            <td>AT</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип привода</td>
                                            <td colspan="2">передний</td>
                                        </tr>

                                        <tr class="border-bottom">
                                            <td class="border-right">Количество передач</td>
                                            <td colspan="2">6</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип кузова</td>
                                            <td colspan="3">седан</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Количество дверей/мест</td>
                                            <td colspan="3">4/5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                            <td colspan="3">4845 / 1830 / 1455</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Колесная база, мм</td>
                                            <td colspan="3">2795</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Колея (передняя, задняя), мм</td>
                                            <td colspan="3">1591-1601 / 1591-1601</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Дорожный просвет, мм</td>
                                            <td colspan="3">145</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Объём багажника, л (VDA)</td>
                                            <td colspan="3">505</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Подвеска (передняя/задняя)</td>
                                            <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тормоза (передние/задние)</td>
                                            <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                            <td class="border-right">1423/1535</td>
                                            <td class="border-right">1450/1550</td>
                                            <td>480/1580</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Разгон 0-100 км/ч, c</td>
                                            <td class="border-right">9.5</td>
                                            <td class="border-right">10.6</td>
                                            <td>9.5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Разгон 60-100 км/ч, c</td>
                                            <td class="border-right">9.8</td>
                                            <td class="border-right">5.7</td>
                                            <td>5.4</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальная скорость, км/ч</td>
                                            <td class="border-right">210</td>
                                            <td class="border-right">208</td>
                                            <td>210</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                            <td colspan="3">35,8</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Объём топливного бака, л</td>
                                            <td colspan="3">70</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Город, л/100км</td>
                                            <td class="border-right">9.3</td>
                                            <td class="border-right">10.3</td>
                                            <td>11.5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Трасса, л/100км</td>
                                            <td class="border-right">5.6</td>
                                            <td class="border-right">6.1</td>
                                            <td>6.2</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Смешанный, л/100км</td>
                                            <td class="border-right">7.0</td>
                                            <td class="border-right">7.6</td>
                                            <td>8.1</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="char-tabs-container" id="hatchback">

                        <div class="body-arrangement hatchback">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип топлива</td>
                                        <td colspan="2">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td class="border-right">2.0 NU CVVL MPI</td>
                                        <td>2.4 Theta CVVT MPI</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Кол-во/расположение цилиндров</td>
                                        <td colspan="2">4, рядное</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Рабочий объем, см3</td>
                                        <td class="border-right">2000</td>
                                        <td>2400</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество клапанов</td>
                                        <td colspan="2">16</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                        <td class="border-right">150 (6500)</td>
                                        <td>180 (6000)</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                        <td class="border-right">196 @ 4800</td>
                                        <td>231 @ 4000</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Требования к топливу</td>
                                        <td colspan="2">бензин RON95</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">MT</td>
                                        <td>AT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип привода</td>
                                        <td colspan="2">передний</td>
                                    </tr>

                                    <tr class="border-bottom">
                                        <td class="border-right">Количество передач</td>
                                        <td colspan="2">6</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип кузова</td>
                                        <td colspan="3">седан</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество дверей/мест</td>
                                        <td colspan="3">4/5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                        <td colspan="3">4845 / 1830 / 1455</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колесная база, мм</td>
                                        <td colspan="3">2795</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колея (передняя, задняя), мм</td>
                                        <td colspan="3">1591-1601 / 1591-1601</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Дорожный просвет, мм</td>
                                        <td colspan="3">145</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём багажника, л (VDA)</td>
                                        <td colspan="3">505</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подвеска (передняя/задняя)</td>
                                        <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормоза (передние/задние)</td>
                                        <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                        <td class="border-right">1423/1535</td>
                                        <td class="border-right">1450/1550</td>
                                        <td>480/1580</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 0-100 км/ч, c</td>
                                        <td class="border-right">9.5</td>
                                        <td class="border-right">10.6</td>
                                        <td>9.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 60-100 км/ч, c</td>
                                        <td class="border-right">9.8</td>
                                        <td class="border-right">5.7</td>
                                        <td>5.4</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная скорость, км/ч</td>
                                        <td class="border-right">210</td>
                                        <td class="border-right">208</td>
                                        <td>210</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                        <td colspan="3">35,8</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём топливного бака, л</td>
                                        <td colspan="3">70</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Город, л/100км</td>
                                        <td class="border-right">9.3</td>
                                        <td class="border-right">10.3</td>
                                        <td>11.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Трасса, л/100км</td>
                                        <td class="border-right">5.6</td>
                                        <td class="border-right">6.1</td>
                                        <td>6.2</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Смешанный, л/100км</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right">7.6</td>
                                        <td>8.1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="char-tabs-container" id="wagon">

                        <div class="body-arrangement wagon">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип топлива</td>
                                        <td colspan="2">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td class="border-right">2.0 NU CVVL MPI</td>
                                        <td>2.4 Theta CVVT MPI</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Кол-во/расположение цилиндров</td>
                                        <td colspan="2">4, рядное</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Рабочий объем, см3</td>
                                        <td class="border-right">2000</td>
                                        <td>2400</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество клапанов</td>
                                        <td colspan="2">16</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                        <td class="border-right">150 (6500)</td>
                                        <td>180 (6000)</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                        <td class="border-right">196 @ 4800</td>
                                        <td>231 @ 4000</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Требования к топливу</td>
                                        <td colspan="2">бензин RON95</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">MT</td>
                                        <td>AT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип привода</td>
                                        <td colspan="2">передний</td>
                                    </tr>

                                    <tr class="border-bottom">
                                        <td class="border-right">Количество передач</td>
                                        <td colspan="2">6</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип кузова</td>
                                        <td colspan="3">седан</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество дверей/мест</td>
                                        <td colspan="3">4/5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                        <td colspan="3">4845 / 1830 / 1455</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колесная база, мм</td>
                                        <td colspan="3">2795</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колея (передняя, задняя), мм</td>
                                        <td colspan="3">1591-1601 / 1591-1601</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Дорожный просвет, мм</td>
                                        <td colspan="3">145</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём багажника, л (VDA)</td>
                                        <td colspan="3">505</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подвеска (передняя/задняя)</td>
                                        <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормоза (передние/задние)</td>
                                        <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                        <td class="border-right">1423/1535</td>
                                        <td class="border-right">1450/1550</td>
                                        <td>480/1580</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 0-100 км/ч, c</td>
                                        <td class="border-right">9.5</td>
                                        <td class="border-right">10.6</td>
                                        <td>9.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 60-100 км/ч, c</td>
                                        <td class="border-right">9.8</td>
                                        <td class="border-right">5.7</td>
                                        <td>5.4</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная скорость, км/ч</td>
                                        <td class="border-right">210</td>
                                        <td class="border-right">208</td>
                                        <td>210</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                        <td colspan="3">35,8</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём топливного бака, л</td>
                                        <td colspan="3">70</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Город, л/100км</td>
                                        <td class="border-right">9.3</td>
                                        <td class="border-right">10.3</td>
                                        <td>11.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Трасса, л/100км</td>
                                        <td class="border-right">5.6</td>
                                        <td class="border-right">6.1</td>
                                        <td>6.2</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Смешанный, л/100км</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right">7.6</td>
                                        <td>8.1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>