<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>ГК FAVORIT MOTORS</h2>
                </div>

                <div class="gray-text">
                    <p>Наша компания за двацатилетнию историю накопила богатый опыт на авторынке Москвы. Мы являемся офциальным дилером а/м Опель с их безумными ОРС, старейшего корейского автоконцерна СанЙонг представлеными моделями К... и далее начинаем перечислять все наши марки и модели легко и просто пишем о том какие мы разностороние какой классный у нас персонал как мы рады развиваться и прочее. Доводим требуемый Антоном до длины в 500 символов. Sed ut perspiciatis, unde omnis iste Sed ut perspiciatis, unde Amen</p>
                </div>

            </div>
        </div>

        <div class="tabs-navigation section-gray catalog-page">
            <div class="box-container clearfix">

                <div class="on-left">
                    <ul class="tabs three-tabs clearfix">
                        <li><a href="#" class="animate-custom">Автомобили в наличии</a></li>
                        <li><a href="#" class="animate-custom">Подержанные автомобили</a></li>
                        <li class="active"><a href="#" class="animate-custom">Авто на заказ</a></li>
                    </ul>
                </div>

                <div class="on-right">

                    <div class="catalog-filter-results">
                        <span>Недавно просмотренные: [ <a href="#" class="red-text animate-custom">7</a> ]</span>
                    </div>

                </div>

            </div>
        </div>

        <div class="section section-sec catalog-page">

            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="post-title">
                        <h1>Каталог подержанных автомобилей</h1>
                    </div>

                </div>

                <div class="on-right">

                    <div class="catalog-filter">
                        <form>
                            <div class="catalog-filter-item accordion active">
                                <div class="catalog-filter-title animate-custom">Марки</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Основные параметры</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Динамика и мощность</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Расход</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item accordion">
                                <div class="catalog-filter-title animate-custom">Двигатель</div>
                                <div class="catalog-filter-container">

                                </div>
                            </div>
                            <div class="catalog-filter-item">
                                <div class="catalog-filter-title animate-custom">Категории</div>
                                <div class="catalog-filter-container">
                                    <ul class="catalog-filter-list">
                                        <li>
                                            <a href="#">Кроссоверы до 600 т.р.</a>
                                            <ul>
                                                <li><a href="#"></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Семейные</a></li>
                                        <li><a href="#">Для девушек</a></li>
                                        <li><a href="#">Экономичные</a></li>
                                        <li><a href="#">Для поездок на дачу</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

        <div class="section section-sec catalog-page padding">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="accessories-items">
                        <div class="accessories-item section-small border-top border-bottom featured clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 019 900 р.<span class="accessories-item-price-old">1 320 000 p.</span></span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom featured clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 019 900 р.<span class="accessories-item-price-old">1 320 000 p.</span></span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom featured clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 019 900 р.<span class="accessories-item-price-old">1 320 000 p.</span></span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg-c animate-custom opacity-custom-hover" href="#" style="background-image: url(content/models/popup-car.png);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Skoda Superb</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">Elegance 3.6 Дизель 4WD AT (260 л.с.)</a>
                                    <span class="on-right accessories-item-price">1 320 000 p.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>2010 г. в., Седан, Темно-синий металлик, пробег: 550.998 км, 4 владельца</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom gray-small-link" href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>

                    <div class="pagination">

                        <ul class="pagination-list">
                            <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                            <li><a href="#" class="animate-custom">3</a></li>
                            <li><a class="animate-custom active">4</a></li>
                            <li><a href="#" class="animate-custom">5</a></li>
                            <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                            <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                        </ul>

                        <div class="pagination-show-all"><a href="#" class="animate-custom">Показать все</a></div>

                    </div>

                </div>

            </div>
        </div>

        <!-- Seo Text -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="title">
                    <h2>Заголовок</h2>
                </div>

                <div class="seo-text gray-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?php include("footer.php"); ?>