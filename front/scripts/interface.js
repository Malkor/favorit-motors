var $ = jQuery.noConflict();
var speed = 250;

/* Page Ready Scripts */

/*** 1. Tabs Scripts. ***/
/*** 2. Table Accordion. ***/
/*** 3. Swiper Reviews Slider. ***/
/*** 4. Form Element Styler Activator. ***/
/*** 5. Characteristic Tabs. ***/
/*** 6. Profile Rotation Slider. ***/
/*** 7. ColorBox. ***/
/*** 8. Show/Hide Address Block. ***/
/*** 9. Super Slider Tabs. ***/
/*** 10. YouTube Activator. ***/
/*** 11. Body Tabs Inner Navigation. ***/
/*** 12. Body Sliders. ***/
/*** 13. Gallery Tabs Inner Navigation. ***/
/*** 14. Gallery Sliders. ***/
/*** 15. Colors Tabs Inner Navigation. ***/
/*** 16. Colors Sliders. ***/
/*** 17. Input Mask's. ***/
/*** 18. Custom Popup API. ***/
/*** 19. Popup Action Navigation. ***/
/*** 20. Popup Show/Hide Desc. ***/
/*** 21. Time Line Script. ***/
/*** 22. Big Map Popup Show/Hide. ***/
/*** 23. Front Slider Switch. ***/
/*** 24. Front Slider Filter Tabs. ***/
/*** 25. UI Slider. ***/
/*** 26. Front Slider Filter Accordion. ***/
/*** 27. Mobile Menu. ***/
/*** 28. Catalog Filter Accordion. ***/
/*** 29. Time Line Script. ***/
jQuery(document).ready(function(){
    var $ = jQuery.noConflict();

    /*** 0. Script Section. ***/

    moving_bar();
    footer();


    /*** 1. Tabs Scripts. ***/

    var tabs_menu = jQuery(".tabs");
    var tab_container = jQuery(".tab-container");

    if (tabs_menu.length && tab_container.length) {

        tab_container.hide();
        jQuery(tabs_menu.find('li.active').find('a').attr('data-block')).show();

        tabs_menu.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                tabs_menu.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                tab_container.hide();
                jQuery(selector).fadeIn(speed);
                if (jQuery(".reviews-slider").length) {
                    swiper.update();
                }
            }
        });
    }


    /*** 2. Table Accordion. ***/

    var table_collapsed = jQuery(".table-element.collapsed");

    table_collapsed.click(function(){
        if (jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
            jQuery(this).find(".table-title").removeClass('border-bottom');
            jQuery(this).find(".table-body").slideUp(speed);
        } else {
            jQuery(this).addClass('open');
            jQuery(this).find(".table-title").addClass('border-bottom');
            jQuery(this).find(".table-body").slideDown(speed);
        }
    });


    /*** 3. Swiper Reviews Slider. ***/

    var swiper = new Swiper('.reviews-slider', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop: true
    });


    /*** 4. Form Element Styler Activator. ***/

    jQuery('input, select').styler();


    /*** 5. Characteristic Tabs. ***/

    var char_tabs = jQuery(".characteristics-navigation");
    var char_tabs_container = jQuery(".char-tabs-container");

    if (char_tabs.length && char_tabs_container.length) {

        char_tabs_container.hide();
        jQuery(char_tabs.find('li.active').find('a').attr('data-block')).show();

        char_tabs.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                char_tabs.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                char_tabs_container.hide();
                jQuery(selector).fadeIn(speed);
            }
        });
    }


    /*** 6. Profile Rotation Slider. ***/

    var Profile_swiper = new Swiper('.rotation-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 5,
        loop: 'true',
        spaceBetween: 35,
        initialSlide: 2,
        coverflow: {
            rotate: 30,
            stretch: 0,
            depth: 65,
            modifier: 1,
            slideShadows : true
        },
        onInit: function() {
            colorbox_init();
        }
    });


    /*** 7. ColorBox. ***/

    function colorbox_init() {
        var colorbox = jQuery('a.colorbox');

        colorbox.colorbox({
            width: '80%',
            maxWidth: '1170px',
            current: '',
            previous: '',
            next: '',
            close: 'Закрыть'
        });
    }

    colorbox_init();


    /*** 8. Show/Hide Address Block. ***/

    var address_button = jQuery(".address-button");
    var address_map = jQuery(".address-map");
    var address_close = jQuery(".close-address-map");

    address_button.click(function(){
        if (address_map.hasClass('show')) {
            address_map.removeClass('show');
            address_map.slideUp(speed);
        } else {
            address_map.addClass('show');
            address_map.slideDown(speed);
        }
    });

    address_close.click(function(){
        address_map.removeClass('show');
        address_map.slideUp(speed);
    });


    /*** 9. Super Slider Tabs. ***/

    var super_tabs = jQuery(".super-slider-nav-tabs");
    var super_tabs_container = jQuery(".super-slider-container");

    if (super_tabs.length && super_tabs_container.length) {

        super_tabs_container.hide();
        jQuery(super_tabs.find('li.active').find('a').attr('data-block')).show();

        super_tabs.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                super_tabs.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                super_tabs_container.hide();
                jQuery(selector).fadeIn(speed);

                if (selector == "#body") {
                    setTimeout(function(){
                        body_sedan_slider.update(true);
                        body_hatchback_slider.update(true);
                        body_hatchback_slider.update(true);
                    },speed);
                }

                if (selector == "#gallery") {
                    setTimeout(function(){
                        gallery_exterior_Top.update(true);
                        gallery_exterior_Thumbs.update(true);
                        gallery_inter_Top.update(true);
                        gallery_inter_Thumbs.update(true);
                    },speed);
                }

                if (selector == "#colors") {
                    setTimeout(function(){
                        colors_all_slider.update(true);
                        colors_all_slider_thumbs.update(true);
                        colors_metallic_slider.update(true);
                        colors_metallic_slider_thumbs.update(true);
                        colors_nometallic_slider.update(true);
                        colors_nometallic_slider_thumbs.update(true);
                        colors_pearl_slider.update(true);
                        colors_pearl_slider_thumbs.update(true);
                        colors_combined_slider.update(true);
                        colors_combined_slider_thumbs.update(true);
                    },speed);
                }

            }
        });
    }

    /*** 10. YouTube Activator. ***/

    jQuery(".youtube").each(function() {

        jQuery(this).css('background-image', 'url(http://i.ytimg.com/vi/' + this.id + '/sddefault.jpg)');
        jQuery(this).append(jQuery('<div/>', {'class': 'play'}));

        jQuery(document).delegate('#'+this.id, 'click', function() {
            var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
            if (jQuery(this).data('params')) iframe_url+='&'+jQuery(this).data('params');

            var iframe = jQuery('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': jQuery(this).width(), 'height': jQuery(this).height() });

            jQuery(this).replaceWith(iframe);
        });
    });


    /*** 11. Body Tabs Inner Navigation. ***/

    var body_inner_tabs = jQuery(".body-navigation");
    var body_inner_tabs_container = jQuery(".body-navigation-container");

    if (body_inner_tabs.length && body_inner_tabs_container.length) {

        body_inner_tabs_container.hide();
        jQuery(body_inner_tabs.find('li.active').find('a').attr('data-block')).show();

        body_inner_tabs.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                body_inner_tabs.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                body_inner_tabs_container.hide();
                jQuery(selector).fadeIn(speed);

                if (selector == "#slider-sedan") {
                    body_sedan_slider.update(true);
                }

                if (selector == "#slider-hatchback") {
                    body_hatchback_slider.update(true);
                }

                if (selector == "#slider-wagon") {
                    body_wagon_slider.update(true);
                }
            }
        });
    }


    /*** 12. Body Sliders. ***/

    var body_sedan_slider = new Swiper('.body-sedan-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        centeredSlides: true,
        loop: true,
        onInit: function() {
            colorbox_init();
        }
    });

    var body_hatchback_slider = new Swiper('.body-hatchback-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        centeredSlides: true,
        loop: true,
        onInit: function() {
            colorbox_init();
        }
    });

    var body_wagon_slider = new Swiper('.body-wagon-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        centeredSlides: true,
        loop: true,
        onInit: function() {
            colorbox_init();
        }
    });


    /*** 13. Gallery Tabs Inner Navigation. ***/

    var gallery_inner_tabs = jQuery(".gallery-navigation");
    var gallery_inner_tabs_container = jQuery(".gallery-navigation-container");

    if (gallery_inner_tabs.length && gallery_inner_tabs_container.length) {

        gallery_inner_tabs_container.hide();
        jQuery(gallery_inner_tabs.find('li.active').find('a').attr('data-block')).show();

        gallery_inner_tabs.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                gallery_inner_tabs.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                gallery_inner_tabs_container.hide();
                jQuery(selector).fadeIn(speed);

                if (selector == "#slider-exterior") {
                    gallery_exterior_Top.update(true);
                    gallery_exterior_Thumbs.update(true);
                }

                if (selector == "#slider-interior") {
                    gallery_inter_Top.update(true);
                    gallery_inter_Thumbs.update(true);
                }
            }
        });
    }

    /*** 14. Gallery Sliders. ***/

    var gallery_inter_Top = new Swiper('.gallery-inter-top', {
        slidesPerView: 5,
        centeredSlides: true,
        loop: true
    });
    var gallery_inter_Thumbs = new Swiper('.gallery-inter-thumbs', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 5,
        loop: true,
        spaceBetween: 20,
        slideToClickedSlide: true,
        coverflow: {
            rotate: 35,
            stretch: 0,
            depth: 65,
            modifier: 1,
            slideShadows : true
        },
        onInit: function() {
            colorbox_init();
        }
    });
    gallery_inter_Top.params.control = gallery_inter_Thumbs;
    gallery_inter_Thumbs.params.control = gallery_inter_Top;

    var gallery_exterior_Top = new Swiper('.gallery-exterior-top', {
        slidesPerView: 5,
        centeredSlides: true,
        loop: true,
        onInit: function() {
            colorbox_init();
        }
    });
    var gallery_exterior_Thumbs = new Swiper('.gallery-exterior-thumbs', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 5,
        loop: true,
        spaceBetween: 20,
        slideToClickedSlide: true,
        coverflow: {
            rotate: 35,
            stretch: 0,
            depth: 65,
            modifier: 1,
            slideShadows : true
        },
        onInit: function() {
            colorbox_init();
        }
    });
    gallery_exterior_Top.params.control = gallery_exterior_Thumbs;
    gallery_exterior_Thumbs.params.control = gallery_exterior_Top;


    /*** 15. Colors Tabs Inner Navigation. ***/

    var colors_inner_tabs = jQuery(".colors-navigation");
    var colors_inner_tabs_container = jQuery(".colors-navigation-container");

    if (colors_inner_tabs.length && colors_inner_tabs_container.length) {

        colors_inner_tabs_container.hide();
        jQuery(colors_inner_tabs.find('li.active').find('a').attr('data-block')).show();

        colors_inner_tabs.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                colors_inner_tabs.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                colors_inner_tabs_container.hide();
                jQuery(selector).fadeIn(speed);

                if (selector == "#colors-all") {
                    colors_all_slider.update(true);
                    colors_all_slider_thumbs.update(true);
                }

                if (selector == "#colors-metallic") {
                    colors_metallic_slider.update(true);
                    colors_metallic_slider_thumbs.update(true);
                }

                if (selector == "#colors-nometallic") {
                    colors_nometallic_slider.update(true);
                    colors_nometallic_slider_thumbs.update(true);
                }

                if (selector == "#colors-pearl") {
                    colors_pearl_slider.update(true);
                    colors_pearl_slider_thumbs.update(true);
                }

                if (selector == "#colors-combined") {
                    colors_combined_slider.update(true);
                    colors_combined_slider_thumbs.update(true);
                }
            }
        });
    }


    /*** 16. Colors Sliders. ***/

    var colors_all_slider = new Swiper('.colors-all-slider', {
        slidesPerView: 1
    });

    var colors_all_slider_thumbs = new Swiper('.colors-all-slider-thumb', {
        spaceBetween: 30,
        centeredSlides: true,
        slidesPerView: 'auto',
        slideToClickedSlide: true
    });

    colors_all_slider.params.control = colors_all_slider_thumbs;
    colors_all_slider_thumbs.params.control = colors_all_slider;

    var colors_metallic_slider = new Swiper('.colors-metallic-slider', {
        slidesPerView: 1
    });

    var colors_metallic_slider_thumbs = new Swiper('.colors-metallic-slider-thumb', {
        spaceBetween: 30,
        centeredSlides: true,
        slidesPerView: 'auto',
        slideToClickedSlide: true
    });

    colors_metallic_slider.params.control = colors_metallic_slider_thumbs;
    colors_metallic_slider_thumbs.params.control = colors_metallic_slider;

    var colors_nometallic_slider = new Swiper('.colors-nometallic-slider', {
        slidesPerView: 1
    });

    var colors_nometallic_slider_thumbs = new Swiper('.colors-nometallic-slider-thumb', {
        spaceBetween: 30,
        centeredSlides: true,
        slidesPerView: 'auto',
        slideToClickedSlide: true
    });

    colors_nometallic_slider.params.control = colors_nometallic_slider_thumbs;
    colors_nometallic_slider_thumbs.params.control = colors_nometallic_slider;

    var colors_pearl_slider = new Swiper('.colors-pearl-slider', {
        slidesPerView: 1
    });

    var colors_pearl_slider_thumbs = new Swiper('.colors-pearl-slider-thumb', {
        spaceBetween: 30,
        centeredSlides: true,
        slidesPerView: 'auto',
        slideToClickedSlide: true
    });

    colors_pearl_slider.params.control = colors_pearl_slider_thumbs;
    colors_pearl_slider_thumbs.params.control = colors_pearl_slider;

    var colors_combined_slider = new Swiper('.colors-combined-slider', {
        slidesPerView: 1
    });

    var colors_combined_slider_thumbs = new Swiper('.colors-combined-slider-thumb', {
        spaceBetween: 30,
        centeredSlides: true,
        slidesPerView: 'auto',
        slideToClickedSlide: true
    });

    colors_combined_slider.params.control = colors_combined_slider_thumbs;
    colors_combined_slider_thumbs.params.control = colors_combined_slider;


    /*** 17. Input Mask's. ***/

    // Phone Mask
    jQuery('input.phone-mask').inputmask('+7 (999) 999-99-99');

    // Email Mask
    jQuery('input.email-mask').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });


    /*** 18. Custom Popup API. ***/

    var popup_button = jQuery("a.use-popup");
    var popup_shadow = jQuery(".popup-shadow");
    var popup_containers = jQuery(".popup");
    var popup_close = jQuery(".popup-close, .close-popup");

    popup_button.click(function(){
        var popup_ID = "#" + jQuery(this).attr('data-block');
        popup_containers.fadeOut(speed);
        jQuery(popup_ID).fadeIn(speed);
        popup_shadow.fadeIn(speed);
    });

    popup_shadow.click(function(){
        popup_shadow.fadeOut(speed);
        popup_containers.fadeOut(speed);
    });

    popup_close.click(function(){
        popup_shadow.fadeOut(speed);
        popup_containers.fadeOut(speed);
    });


    /*** 19. Popup Action Navigation. ***/

    var debug = false;

    var popup_action_container = jQuery(".popup-action");

    popup_action_container.each(function(){
        if (jQuery(this).hasClass('active')) {
            jQuery(this).show();
        }
    });

    var popup_action_navigation = jQuery(".navigation-buttons");

    popup_action_navigation.find('a').click(function(){

        var popup_action_line = jQuery(this).closest(".action-popup").find(".action-line");
        var popup_action_container = jQuery(this).closest(".action-popup").find(".popup-action");
        var popup_action_navigation = jQuery(this).closest(".action-popup").find(".navigation-buttons");
        var popup_action_text =jQuery(this).closest(".action-popup").find(".action-line-text");

        var active_section = Number(popup_action_line.find('ul').attr('data-block'));
        if (jQuery(this).hasClass('next')) {
            if (active_section < 5) {
                active_section++;
            }
        }
        if (jQuery(this).hasClass('prev')) {
            if (active_section > 1) {
                active_section--;
            }
        }
        if (debug) {console.log("active section = " + active_section);}
        popup_action_line.find('ul').attr('data-block',active_section);
        if (active_section == 1) {
            popup_action_navigation.find('a.prev').removeClass('active');
            popup_action_text.html('5 простых шагов и Вы у цели!');
        } else {
            popup_action_navigation.find('a.prev').addClass('active');
        }
        if (active_section == 2) {
            popup_action_text.html('Осталось 4 шага');
        }
        if (active_section == 3) {
            popup_action_text.html('Осталось 3 шага');
        }
        if (active_section == 4) {
            popup_action_text.html('Осталось 2 шага');
        }
        if (active_section == 5) {
            popup_action_navigation.find('a.next').removeClass('active');
            popup_action_navigation.find('input').addClass('active');
            popup_action_text.html('Осталося последний шаг');
        } else {
            popup_action_navigation.find('a.next').addClass('active');
            popup_action_navigation.find('input').removeClass('active');
        }
        for (var i = 1; i<=5; i++) {
            var active_line = ".action-line-" + i;
            if (i <= active_section) {
                jQuery(this).closest(".action-popup").find(active_line).addClass('active');
            } else {
                jQuery(this).closest(".action-popup").find(active_line).removeClass('active');
            }
        }
        popup_action_container.hide();
        var active_popup_action = ".popup-action-" + active_section;
        jQuery(this).closest(".action-popup").find(active_popup_action).fadeIn(speed);
    });


    /*** 20. Popup Show/Hide Desc. ***/

    jQuery(".show-hidden-desc").click(function(){
        jQuery(this).closest(".popup-action").find(".add-service-to-desc").slideDown(speed);
        jQuery(this).closest(".action-popup").find(".navigation-buttons").find(".block").show();
    });

    jQuery(".hide-hidden-desc").click(function(){
        jQuery(this).closest(".popup-action").find(".add-service-to-desc").slideUp(speed);
        jQuery(this).closest(".action-popup").find(".navigation-buttons").find(".block").hide();
    });


    /*** 20. Calendar. ***/

    jQuery(".datepicker").datepicker({
        defaultDate: 0,
        minDate: 0,
        firstDay: 1,
        dateFormat: "d M yy",
        hideIfNoPrevNext: true,
        showOtherMonths: true,
        dayNamesMin: [ "вс", "пн", "вт", "ср", "чт", "пт", "сб" ],
        monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
        monthNamesShort: [ "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря" ]

    });


    /*** 21. Time Line Script. ***/

    var time_line_button = jQuery(".time-line-button");
    var time_line_schema = jQuery(".time-line-schema");

    time_line_button.click(function(){
        var index, container;
        if (!jQuery(this).hasClass('active')) {
            time_line_schema.hide();
            time_line_button.removeClass('active');
            jQuery(this).addClass('active');
            index = jQuery(this).attr('data-block');
            container = jQuery(".time-line-schema-" + index);
            container.fadeIn(speed);
        } else {
            jQuery(this).removeClass('active');
            index = jQuery(this).attr('data-block');
            container = jQuery(".time-line-schema-" + index);
            container.fadeOut(speed);
        }
    });

    var time_line_list = jQuery(".time-line-list");
    var time_line_text = jQuery(".time-line-text");

    time_line_list.find('li').click(function(){
        var time = jQuery(this).html();
        var daytime = jQuery(this).closest('.time-line-schema').attr('data-block');
        time_line_list.find('li').removeClass('active');
        jQuery(this).addClass('active');
        jQuery(this).closest('.time-line-schema').hide();
        jQuery("#add-service-time").val(time);
        jQuery(".time-line-buttons").hide();
        jQuery(".time-line-text-daytime").html(daytime);
        jQuery(".time-line-text-time").html(time);
        time_line_text.fadeIn(speed);
    });

    time_line_text.find('i').click(function(){
        time_line_text.hide();
        jQuery(".time-line-buttons").fadeIn(speed);
    });


    /*** 22. Big Map Popup Show/Hide. ***/

    var center_container = jQuery(".tech-center");
    var center_conteiner_popup = jQuery(".tech-center-info");

    center_container.click(function(){
        var popup = jQuery(this).find(".tech-center-info");
        if (!jQuery(this).hasClass('show')) {
            center_conteiner_popup.fadeOut(speed);
            center_container.removeClass('show');
            popup.fadeIn(speed);
            jQuery(this).addClass('show');
        }
    });

    var close_center_container = jQuery(".tech-center-info-close");

    close_center_container.click(function(){
        setTimeout(function(){
            center_conteiner_popup.fadeOut(speed);
            center_container.removeClass('show');
        },10);
    });


    /*** 23. Front Slider Switch. ***/

    var front_slider_menu_item = jQuery(".event-slider-menu").find('li').find('a');
    var front_slider_item = jQuery(".front-slider-item");
    var front_slider_item_text = jQuery(".front-slider-text-item");

    front_slider_menu_item.click(function(){
        var selector = "#" + jQuery(this).attr('data-block');
        var selector_text = "#" + jQuery(this).attr('data-block') + "-text";
        if(!jQuery(this).parent('li').hasClass('active') && jQuery(selector).length) {
            front_slider_menu_item.parent('li').removeClass('active');
            jQuery(this).parent('li').addClass('active');
            front_slider_item.hide();
            jQuery(selector).fadeIn(speed);
            front_slider_item_text.hide();
            jQuery(selector_text).fadeIn(speed);
            return false;
        }
    });


    /*** 24. Front Slider Filter Tabs. ***/

    var filter_tabs_menu = jQuery(".front-search-filter-menu");
    var filter_tab_container = jQuery(".front-search-filter-item");

    if (filter_tabs_menu.length && filter_tab_container.length) {

        filter_tab_container.hide();
        jQuery(filter_tabs_menu.find('li.active').find('a').attr('data-block')).show();

        filter_tabs_menu.find('li').find('a').click(function(){
            if (!jQuery(this).parent('li').hasClass('active')) {
                var selector = jQuery(this).attr('data-block');
                filter_tabs_menu.find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                filter_tab_container.hide();
                jQuery(selector).fadeIn(speed);
            }
        });
    }


    /*** 25. UI Slider. ***/

    var ui_slider = jQuery(".ui-slider-container");

    ui_slider.each(function(){

        var ui_slider_container = jQuery(this).closest(".ui-slider-inner");

        var ui_slider_start = ui_slider_container.find(".first-position");
        var ui_slider_finish = ui_slider_container.find(".second-position");

        var ui_slider_min = Number(ui_slider_start.val());
        var ui_slider_max = Number(ui_slider_finish.val());

        var ui_slider_first_label = ui_slider_container.find(".first-label");
        var ui_slider_second_label = ui_slider_container.find(".second-label");

        var ui_slider_type_label = ui_slider_container.find(".type-label").html();

        jQuery(this).slider({
            range: true,
            min: ui_slider_min,
            max: ui_slider_max,
            values: [ui_slider_min , ui_slider_max],
            slide: function( event, ui ) {
                ui_slider_first_label.html(ui.values[ 0 ]);
                ui_slider_start.val(ui.values[ 0 ] + ui_slider_type_label);
                ui_slider_second_label.html(ui.values[ 1 ]);
                ui_slider_finish.val(ui.values[ 1 ]+ ui_slider_type_label);
            }
        });
        ui_slider_first_label.html(jQuery(this).slider( "values", 0 ));
        ui_slider_start.val(jQuery(this).slider( "values", 0 ) + ui_slider_type_label);
        ui_slider_second_label.html(jQuery(this).slider( "values", 1 ));
        ui_slider_finish.val(jQuery(this).slider( "values", 1 ) + ui_slider_type_label);

    });


    /*** 26. Front Slider Filter Accordion. ***/

    var front_slider_accordion = jQuery(".front-search-item.accordion");

    front_slider_accordion.click(function(){
        if (!jQuery(this).hasClass('active')) {
            var selector_old = jQuery(this).closest('form').find(".front-search-item.accordion.active");
            var selector_new = jQuery(this);

            selector_old.find(".front-search-item-accordion-content").slideUp(speed);
            selector_new.find(".front-search-item-accordion-content").slideDown(speed);

            setTimeout(function(){
                selector_old.removeClass('active');
                selector_new.addClass('active');
            },speed);
        }
    });


    /*** 27. Mobile Menu. ***/

    jQuery("#navigation").slicknav({
        label: '',
        duration: speed,
        prependTo: "#navigation"
    });


    /*** 28. Catalog Filter Accordion. ***/

    var catalog_slider_accordion = jQuery(".catalog-filter-item.accordion");

    catalog_slider_accordion.each(function(){
        if (!jQuery(this).hasClass('active')) {
            jQuery(this).find('.catalog-filter-container').hide();
        }
    });

    catalog_slider_accordion.find(".catalog-filter-title").click(function(){
        if (jQuery(this).parent('div').hasClass('active')) {
            jQuery(this).parent('div').find('.catalog-filter-container').slideUp(speed);
            jQuery(this).parent('div').removeClass('active');
        } else {
            jQuery(this).parent('div').find('.catalog-filter-container').slideDown(speed);
            jQuery(this).parent('div').addClass('active');
        }
    });


    /*** 29. Time Line UI Slider. ***/

    var time_ui_slider = jQuery(".test-drive-time-line");

    var time_ui_slider_min = jQuery(".test-drive-time-min").find('span').html().split(":");
    time_ui_slider_min = Number(time_ui_slider_min[0]);
    var time_ui_slider_max = jQuery(".test-drive-time-max").find('span').html().split(":");
    time_ui_slider_max = Number(time_ui_slider_max[0]);

    var time_ui_slider_label = jQuery(".test-drive-time-label");
    var time_ui_slider_label_value = time_ui_slider_label.html().split(":");
    time_ui_slider_label_value = Number(time_ui_slider_label_value[0]);
    var time_ui_slider_input = jQuery("#car-test-drive-time");

    console.log(time_ui_slider_min + " " + time_ui_slider_max + " " + time_ui_slider_label_value);

    time_ui_slider.each(function(){

        jQuery(this).slider({
            value: time_ui_slider_label_value,
            min: time_ui_slider_min,
            max: time_ui_slider_max - 1,
            step: 1,
            slide: function( event, ui ) {
                time_ui_slider_label.html(ui.value + ":00 - " + (ui.value+1) + ":00");
                time_ui_slider_label.css('left',(ui.value-time_ui_slider_min)/(time_ui_slider_max-1-time_ui_slider_min)*100 + "%");
                time_ui_slider_input.val(ui.value + ":00 - " + (ui.value+1) + ":00");
            }
        });
        time_ui_slider_label.html(jQuery(this).slider( "value" ) + ":00 - " + (jQuery(this).slider( "value" )+1) + ":00");
        time_ui_slider_input.val(jQuery(this).slider( "value" ) + ":00 - " + (jQuery(this).slider( "value" )+1) + ":00");
        time_ui_slider_label.css('left',(jQuery(this).slider( "value" )-time_ui_slider_min)/(time_ui_slider_max-1-time_ui_slider_min)*100 + "%");

    });

});


/* Page Load Scripts */

jQuery(window).load(function(){
    var $ = jQuery.noConflict();

    /*** 0. Script Section. ***/

    footer();

});


/* Page Resize Scripts */

jQuery(window).resize(function() {
    var $ = jQuery.noConflict();

    /*** 0. Script Section. ***/

    moving_bar();
    footer();

});


/* Page Scroll Functions */

jQuery(window).scroll(function() {
    var $ = jQuery.noConflict();

    /*** 0. Script Section. ***/

    moving_bar();

});


/*** Moving Bar ***/

function moving_bar() {

    var container = jQuery(".moving-bar");
    var footer = jQuery("footer");

    var position = footer.offset().top + (container.height() + 1);
    var scroll_position = jQuery(window).scrollTop();
    var indicator = (scroll_position + jQuery(window).height());

    if (indicator < position) {
        container.addClass('scroll');
    } else {
        container.removeClass('scroll');
    }

}


/*** Footer ***/

function footer() {
    var main_content = jQuery("#main-content");
    var footer = jQuery("#footer");

    if ((main_content.height() + footer.height()) < jQuery(window).height()) {
        main_content.css('min-height',jQuery(window).height() - footer.height());
    }
}