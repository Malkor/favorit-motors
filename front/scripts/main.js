var $ = jQuery.noConflict();

jQuery(document).ready(function(){
    var $ = jQuery.noConflict(),
        $tabsCancelBtn = jQuery('#addPersonCancel'),
        $tabsFirstBtn = jQuery('ul.tabs > li:first > a'),
        $tabsFirstBlock = $tabsFirstBtn.attr('data-block');

    $tabsCancelBtn.click(function(event) {
        event.preventDefault();

        $('#addPersonForm').trigger('reset');
        jQuery('.tab-container').hide();
        jQuery('ul.tabs > li').removeClass('active');
        $tabsFirstBtn.parent('li').addClass('active');
        jQuery($tabsFirstBlock).show();
    });

});