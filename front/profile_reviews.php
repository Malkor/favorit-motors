<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Profile Reviews");
?>


    <!-- Content -->
    <section id="content">

        <div class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>Отзывы</h2>
                </div>
            </div>
        </div>

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray">
                <div class="box-container">
                    <ul class="tabs four-tabs clearfix active-gray">
                        <li class="active"><a href="#" class="animate-custom">Мои отзывы</a></li>
                        <li><a href="#" class="animate-custom" >Блог президента компании</a></li>
                        <li><a href="#" class="animate-custom" >Отзывы о моем автомобиле</a></li>
                        <li><a href="#" class="animate-custom" >Написать новый отзыв</a></li>
                    </ul>
                </div>
            </div>

            <div class="section section-sec">
                <div class="box-container">

                    <div class="page-reviews-items">

                        <div class="reviews-item border-bottom">
                            <a href="#" class="reviews-item-comments animate-custom opacity-custom-hover">
                                <i class="fa fa-comments"></i>
                                <span>4</span>
                            </a>
                            <div class="reviews-item-header clearfix">
                                <div class="reviews-item-avatar">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                </div>
                                <div class="reviews-item-info">
                                    <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">Претензия по ремонту автомобиля</a></div>
                                    <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                    <div class="reviews-item-theme">Шумилова Мария Александровна</div>
                                    <div class="reviews-item-theme">Блог Президента компании</div>
                                </div>
                            </div>
                            <div class="reviews-item-desc">
                                <p>Меня зовут Шумилова Мария Александровна.  Я очень прошу Вас посодействовать в решении вопроса с ремонтом автомобиля (Шевроле Круз, рег.№ р531мм777). Автомобиль был приобретен в конце ноября 2014 года. И эксплуатировался строго в соответствии с правилами эксплуатации...</p>
                            </div>
                        </div>

                        <div class="reviews-item border-bottom child">

                            <div class="reviews-item border-bottom">
                                <a href="#" class="reviews-item-reply red-text">Ответить Николаю Петрову</a>
                                <div class="reviews-item-header clearfix">
                                    <div class="reviews-item-avatar">
                                        <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                    </div>
                                    <div class="reviews-item-info">
                                        <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">re: Претензия по ремонту автомобиля</a></div>
                                        <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                        <div class="reviews-item-theme">Николай Петров</div>
                                        <div class="reviews-item-theme">Блог Президента компании</div>
                                    </div>
                                </div>
                                <div class="reviews-item-desc">
                                    <p>Добрый день! На данный момент Ваш автомобиль тестируется после проведения ряда работ. В 19:00 с Вами свяжется старший мастер консультант и сообщит о результатах.</p>
                                </div>
                            </div>

                        </div>

                        <div class="reviews-item border-bottom">
                            <div class="reviews-item-header clearfix">
                                <div class="reviews-item-avatar">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                </div>
                                <div class="reviews-item-info">
                                    <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">Претензия по ремонту автомобиля</a></div>
                                    <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                    <div class="reviews-item-theme">Шумилова Мария Александровна</div>
                                    <div class="reviews-item-theme">Блог Президента компании</div>
                                </div>
                            </div>
                            <div class="reviews-item-desc">
                                <p>Меня зовут Шумилова Мария Александровна.  Я очень прошу Вас посодействовать в решении вопроса с ремонтом автомобиля (Шевроле Круз, рег.№ р531мм777). Автомобиль был приобретен в конце ноября 2014 года. И эксплуатировался строго в соответствии с правилами эксплуатации...</p>
                            </div>
                        </div>

                        <div class="reviews-item border-bottom">
                            <div class="reviews-item-rating">
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star active"></i>
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="reviews-item-header clearfix">
                                <div class="reviews-item-avatar">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                </div>
                                <div class="reviews-item-info">
                                    <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">Новая модель KIA RIO</a></div>
                                    <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                </div>
                            </div>
                            <div class="reviews-item-desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                            </div>
                        </div>

                        <div class="reviews-item border-bottom">
                            <a href="#" class="reviews-item-comments animate-custom opacity-custom-hover">
                                <i class="fa fa-comments"></i>
                                <span>4</span>
                            </a>
                            <div class="reviews-item-header clearfix">
                                <div class="reviews-item-avatar">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                </div>
                                <div class="reviews-item-info">
                                    <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">Претензия по ремонту автомобиля</a></div>
                                    <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                    <div class="reviews-item-theme">Шумилова Мария Александровна</div>
                                    <div class="reviews-item-theme">
                                        <span>Отзы о мастере <a href="#">Воробьев Сергей Владимирович</a></span>
                                        <span class="reviews-item-rating-small">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews-item-desc">
                                <p>Меня зовут Шумилова Мария Александровна.  Я очень прошу Вас посодействовать в решении вопроса с ремонтом автомобиля (Шевроле Круз, рег.№ р531мм777). Автомобиль был приобретен в конце ноября 2014 года. И эксплуатировался строго в соответствии с правилами эксплуатации...</p>
                            </div>
                        </div>

                        <div class="reviews-item border-bottom child">

                            <div class="reviews-item border-bottom">
                                <a href="#" class="reviews-item-reply red-text">Ответить Николаю Петрову</a>
                                <div class="reviews-item-header clearfix">
                                    <div class="reviews-item-avatar">
                                        <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                                    </div>
                                    <div class="reviews-item-info">
                                        <div class="reviews-item-name"><a class="animate-custom red-hover" href="#">re: Претензия по ремонту автомобиля</a></div>
                                        <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                        <div class="reviews-item-theme">Николай Петров</div>
                                        <div class="reviews-item-theme">Блог Президента компании</div>
                                    </div>
                                </div>
                                <div class="reviews-item-desc">
                                    <p>Добрый день! На данный момент Ваш автомобиль тестируется после проведения ряда работ. В 19:00 с Вами свяжется старший мастер консультант и сообщит о результатах.</p>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="pagination">

                        <ul class="pagination-list">
                            <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                            <li><a href="#" class="animate-custom">3</a></li>
                            <li><a class="animate-custom active">4</a></li>
                            <li><a href="#" class="animate-custom">5</a></li>
                            <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                            <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                        </ul>

                    </div>

                </div>
            </div>

        </div>


    </section><!-- /Content -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>