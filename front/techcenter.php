<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-gray section-sec section-top">
            <div class="box-container clearfix">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Адреса и режим работы технических центров</h1>
                    </div>

                    <div class="filter"><form id="models-works-filter" class="clearfix">

                            <div class="filter-item clearfix">
                                <label for="brand">Марка автомобиля:</label>
                                <select id="brand">
                                    <option>Все марки</option>
                                    <option>Марка 1</option>
                                    <option>Марка 2</option>
                                </select>
                            </div>

                            <div class="filter-item clearfix">
                                <label for="brand">Вид работ:</label>
                                <select id="brand">
                                    <option>Все виды</option>
                                    <option>Вид 1</option>
                                    <option>Вид 2</option>
                                </select>
                            </div>

                    </form></div>

                    <div class="techcenter-inner clearfix">

                        <div class="on-left">
                            <div class="body">
                                <ul>
                                    <li>Близкие звезды имеющие низкие</li>
                                    <li>Температуры.</li>
                                    <li>Астрономии создалось несколько странное</li>
                                    <li>Обстоятельство существование</li>
                                    <li>Близкие звезды имеющие низкие</li>
                                    <li>Температуры.</li>
                                    <li>Астрономии создалось несколько странное</li>
                                    <li>Обстоятельство существование</li>
                                    <li>Близкие звезды имеющие низкие</li>
                                    <li>Температуры.</li>
                                    <li>Астрономии создалось несколько странное</li>
                                    <li>Обстоятельство существование.</li>
                                </ul>
                            </div>
                        </div>

                        <div class="on-right">
                            <div id="big-center-map">

                                <div class="tech-center tech-center-1 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                                <div class="tech-center tech-center-2 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                                <div class="tech-center tech-center-3 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                                <div class="tech-center tech-center-4 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                                <div class="tech-center tech-center-5 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                                <div class="tech-center tech-center-6 animate-custom">
                                    <div class="tech-center-icon"></div>
                                    <div class="tech-center-info">
                                        <div class="tech-center-info-name red-text">FAVORIT MOTORS ВОСТОК Шоссе Энтузиастов</div>
                                        <div class="tech-center-info-address"><span class="red-text">М</span> Семеновская<br/>Москва, ул. Большая Семеновская, д. 42<br/>+7 (495) 786 25 25</div>
                                        <div class="tech-center-info-worktime red-text">График работы:</div>
                                        <div class="tech-center-info-worktime-line clearfix">
                                            <div class="tech-center-info-worktime-line-salone">Автосалон:<br/>ежедневно 09:00 - 22:00</div>
                                            <div class="tech-center-info-worktime-line-techcenter">Технический центр:<br/>ежедневно 07:00 - 22:00</div>
                                        </div>
                                        <div class="tech-center-info-more"><a href="#" class="gray-text">Подробнее</a></div>
                                        <div class="tech-center-info-close animate-custom opacity-custom-hover"><i class="fa fa-times"></i></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li class="active"><a href="#" class="animate-custom"><span>Технический центр</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Сервисные акции</span></a></li>
                            <li><a href="#" class="animate-custom"><span>ТО и слесарный ремонт</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Кузовной ремон</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Оригинальные запчасти</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Дополнительное оборудование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Регламент ТО</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Каталог оригинальных аксессуаров</span></a></li>
                        </ul>
                    </div>

                </div>

                <div class="accessories-button">
                    <a href="#" class="animate-custom red-small-link">On-line запись</a>
                </div>

            </div>
        </div>

        <div class="section section-sec padding">
            <div class="box-container">

                <div class="body">
                    <img src="content/accessories/body-image-1.jpg" alt="body-image" class="body-image show-on-right" />
                    <p>Купить автомобиль – это довольно просто. На рынке России сегодня множество компаний предлагают свои услуги. Но ведь задача выбрать не только продавца , но и надёжного сервисного и гарантийного партнёра на долгие годы. По статистике на сегодняшний день даже разводы в России вещь более частая чем смена автомобиля их владельцами . Поэтому ГК "FAVORIT MOTORS" в первую очередь рекомендует Вам обращать внимание именно на надёжность компании как сервисного партнёра. Как бы бережно ни осуществлялась эксплуатация автомобиля, без замены масел, технических жидкостей и расходных материалов не обойтись. Кроме того, от поломок и ДТП никто не застрахован. Задача ГК "FAVORIT MOTORS" как лидера авторынка России – сделать так, чтобы Ваш автомобиль служил Вам верой и правдой долгие годы. Мы являемся авторизованным техническим центром автомобилей марок CHEVROLET, CITROEN, VOLVO, KIA, OPEL, PEUGEOT, SKODA, SEAT и SSANGYONG. Наши дилерские центры могут предоставить полный комплекс услуг по техническому обслуживанию и ремонту транспортных средств. Предлагаем Вам записаться на сервис on-line, заполнив простую форму. Выбирайте удобный для Вас сервисный центр и время!</p>
                    <p class="red-text">Высококвалифицированные специалисты технического центра ГК "FAVORIT MOTORS", имеющие сертификаты производителей осуществляют:</p>
                    <ul>
                        <li>диагностику всех систем автомобиля;</li>
                        <li>периодическое техническое обслуживание авто;</li>
                        <li>гарантийный ремонт;</li>
                        <li>кузовной ремонт (от локального до исправления геометрии кузова на стенде);</li>
                        <li>ремонт ходовой части;</li>
                        <li>ремонт агрегатов автомобиля;</li>
                        <li>установку дополнительного оборудования;</li>
                        <li>восстановление геометрии кузова;</li>
                        <li>шиномонтаж;</li>
                        <li>развал-схождение колёс;</li>
                        <li>подбор редких запасных частей;</li>
                        <li>различные виды аэродинамических обвесов;</li>
                        <li>разработку уникального стиля для вашего автомобиля;</li>
                        <li>установку видео и аудио оборудования любой сложности;</li>
                        <li>оборудование автомобилей широчайшим спектром противоугонных комплексов;</li>
                        <li>специальные технические регламенты для корпоративного транспорта;</li>
                        <li>работы по снижению уровня шума в салонах автомобилей;</li>
                        <li>установка специального оборудования для автомобилей ФСО, ФСБ , ПОЛИЦИИ.</li>
                    </ul>
                    <p>Мы гарантируем вам высокое качество оказываемых услуг – все работники ГК "FAVORIT MOTORS" не только имеют профильное образование и большой опыт работы, но и регулярно проходят обучение для повышения своей квалификации у импортеров. Также каждый специалист  получает допуск на гарантийное обслуживание определенной марки автомобиля от производителя.</p>
                    <p>Мы понимаем, как тяжело находиться в состоянии неопределённости из-за поломки автомобиля. Именно поэтому делаем всё возможное, чтобы вы как можно быстрее получили полную информацию о неисправностях и сроках их устранения. С этой целью диагностика автомобиля в техническом центре ГК "FAVORIT MOTORS" проводится в кратчайшие сроки, а доставка запасных частей во все сервисные и кузовные цеха осуществляется с центрального склада по системе точно во-время. Собственная служба технического ассистанса обладающая контакт-центром и 14 эвакуаторами обеспечивает 24 часа в сутки их выезд на место поломки.</p>
                    <p>ГК "FAVORIT MOTORS" – это команда профессионалов своего дела.<br/>ГК "FAVORIT MOTORS" – это лучший дилер КИА в мире 2011-2012<br/>ГК "FAVORIT MOTORS" – это лучший дилер SKODA 2012<br/>ГК "FAVORIT MOTORS" – это лучший дилер по степени удовлетворённости клиентов CITROEN<br/>ГК "FAVORIT MOTORS" – это лучший дилер SsangYong 2009-2010-2011-2012<br/>ГК "FAVORIT MOTORS" – это неоднократный чемпион конкурсов профессионального мастерства среди дилеров России.<br/>ГК "FAVORIT MOTORS" – это привлекательные цены на техническое и сервисное обслуживание.<br/>ГК "FAVORIT MOTORS" – это лучший гарантийный сервис по мнению Фольксваген групп рус.<br/>ГК "FAVORIT MOTORS" – единственный дилер на рынке России предлагающий революционный стандарт обслуживания клиентов бесплатно.<br/>ГК "FAVORIT MOTORS" – это единственный дилер на рынке России , где каждый клиент может задать вопрос президенту группы онлайн и получить на него ответ в течении трёх часов.</p>
                    <p>Чтобы записаться на ТО, диагностику или ремонт вашего автомобиля, достаточно связаться с нами по телефону или оставить заявку, нажав на нужный бренд и перейдя на соответсвующий сайт. В обоих случаях вы сможете записаться на удобное для вас время. Будем рады встрече с вами!</p>
                </div>

                <div class="bottom-part on-center">
                    <a href="#" class="animate-custom red-small-link">Оставить отзыв о работе технического центра</a>
                </div>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>