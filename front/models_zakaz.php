<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Model Zakaz");
?>
    <!-- Content -->
    <section id="content">

        <!-- Super Slider -->
        <div class="super-slider">
            <div class="box-container">

                <div class="left-side">

                    <div class="super-slider-nav-tabs">
                        <ul>
                            <li class="active">
                                <a class="animate-custom" data-block="#body">
                                    <span class="circle-icon"><i class="fa fa-car animate-custom"></i></span>
                                    <span>кузов</span>
                                </a>
                            </li>
                            <li>
                                <a class="animate-custom" data-block="#gallery">
                                    <span class="circle-icon"><i class="fa fa-picture-o animate-custom"></i></span>
                                    <span>галерея</span>
                                </a>
                            </li>
                            <li>
                                <a class="animate-custom" data-block="#colors">
                                    <span class="circle-icon"><i class="fa fa-eyedropper animate-custom"></i></span>
                                    <span>цвета</span>
                                </a>
                            </li>
                            <li>
                                <a class="animate-custom" data-block="#video">
                                    <span class="circle-icon"><i class="fa fa-play animate-custom"></i></span>
                                    <span>видео</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="right-side">

                    <div class="this-car-info">
                        <div class="this-car-info-name">Skoda Fabia</div>
                        <div class="this-car-info-price"><span>1 890 000</span> р.</div>
                    </div>

                    <div class="this-car-some-links">
                        <ul>
                            <li><a href="#">Калькулятор КАСКО</a></li>
                            <li><a href="#">Рассчитать кредит</a></li>
                        </ul>
                    </div>

                    <div class="this-car-big-links">
                        <ul>
                            <li><a href="#">Конфигуратор</a></li>
                        </ul>
                    </div>

                    <div class="this-car-buttons-links">
                        <ul>
                            <li><a class="red-small-link animate-custom use-popup" data-block="test-drive">Тест-драйв</a></li>
                            <li><a class="green-small-link animate-custom" href="#">Заказать автомобиль</a></li>
                        </ul>
                    </div>

                    <div class="this-car-sell">
                        <a href="#" class="animate-custom clearfix">
                            <span class="circle-icon on-left"><i class="fa fa-thumbs-up animate-custom"></i></span>
                            <span class="on-right align-left">продать авто выгодно</span>
                        </a>
                    </div>

                </div>

                <div class="breadcrumbs clearfix">
                    <div class="on-left">
                        <ul class="breadcrumbs-list clearfix">
                            <li><a href="#">Каталог</a></li>
                            <li>/</li>
                            <li><a href="#">На заказ</a></li>
                            <li>/</li>
                            <li><a href="#">Skoda</a></li>
                            <li>/</li>
                            <li><a href="#">Fabia</a></li>
                        </ul>
                    </div>

                    <div class="addres-block on-right">
                        <div class="address-button animate-custom opacity-custom-hover">
                            <div class="address-button-icon bg" style="background-image: url(images/f.png); "></div>
                            <div class="address-button-text">Адреса салонов</div>
                        </div>
                        <div class="address-map">
                            <ul class="salon-list">
                                <li style="top: 70px; left: 90px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                                <li style="top: 100px; left: 225px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                                <li style="top: 90px; left: 175px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                                <li style="top: 185px; left: 235px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                                <li style="top: 185px; left: 120px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                                <li style="top: 195px; left: 155px;"><a href="#">
                                    <span class="salon-icon bg animate-custom" style="background-image: url(images/f.png); "></span>
                                    <span class="salon-name animate-custom red-small-link">Коптево</span>
                                </a></li>
                            </ul>

                            <div class="close-address-map"><i class="fa fa-times"></i>Закрыть</div>
                        </div>
                    </div>

                </div>

                <div class="super-slider-containers">

                    <div id="body" class="super-slider-container">

                        <div class="body-navigation tabs-inner-navigation">
                            <ul class="clearfix">
                                <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-sedan">Седан</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-hatchback">Хэтчбэк</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-wagon">Универсал</a></li>
                            </ul>
                        </div>

                        <div id="slider-sedan" class="body-navigation-container">

                            <div class="body-sedan-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/1.jpg); ">
                                        <a href="content/profile/slider/1.jpg" rel="gallery-sedan" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/2.jpg); ">
                                        <a href="content/profile/slider/2.jpg" rel="gallery-sedan" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/3.jpg); ">
                                        <a href="content/profile/slider/3.jpg" rel="gallery-sedan" class="colorbox"></a>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>

                        </div>

                        <div id="slider-hatchback" class="body-navigation-container">

                            <div class="body-hatchback-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                                        <a href="content/profile/slider/4.jpg" rel="gallery-hatchback" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-hatchback" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-hatchback" class="colorbox"></a>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>

                        </div>

                        <div id="slider-wagon" class="body-navigation-container">

                            <div class="body-wagon-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/slider/medium/1.jpg); ">
                                        <a href="content/models/slider/large/1.jpg" rel="gallery-wagon" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/slider/medium/2.jpg); ">
                                        <a href="content/models/slider/large/2.jpg" rel="gallery-wagon" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/slider/medium/3.jpg); ">
                                        <a href="content/models/slider/large/3.jpg" rel="gallery-wagon" class="colorbox"></a>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>

                        </div>

                    </div>

                    <div id="gallery" class="super-slider-container">

                        <div id="slider-exterior" class="gallery-navigation-container">

                            <div class="body-slider gallery-exterior-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <a href="content/profile/slider/8.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <a href="content/profile/slider/8.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-exterior" class="colorbox"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-container gallery-rotation-slider with-text gallery-exterior-thumbs">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>

                        </div>

                        <div id="slider-interior" class="gallery-navigation-container">

                            <div class="body-slider gallery-inter-top">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <a href="content/profile/slider/8.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <a href="content/profile/slider/8.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <a href="content/profile/slider/5.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <a href="content/profile/slider/6.jpg" rel="gallery-interior" class="colorbox"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-container gallery-rotation-slider with-text gallery-inter-thumbs">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                    <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                                        <div class="stider-text">
                                            <h3>Skoda Yeti</h3>
                                            <p>нового поколения выглядит более сурово благодаря измененной форме фар.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>

                        </div>

                        <div class="gallery-navigation tabs-inner-navigation">
                            <ul class="clearfix">
                                <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-exterior">Экстерьер</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#slider-interior">Интерьер</a></li>
                            </ul>
                        </div>

                    </div>

                    <div id="colors" class="super-slider-container">

                        <div class="colors-navigation tabs-inner-navigation">
                            <ul class="clearfix">
                                <li class="active"><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-all">Все</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-metallic">Металлик</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-nometallic">Неметаллик</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-pearl">Перламутр</a></li>
                                <li><a class="animate-custom opacity-custom-hover on-center" data-block="#colors-combined">Комбинированнй</a></li>
                            </ul>
                        </div>

                        <div id="colors-all" class="colors-navigation-container">

                            <div class="colors-all-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #000000;"></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #FFFFFF; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #CCCCCC; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #cc0000; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #0000cc; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #00cc00; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #944ccc; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #cc60a1; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #3fcccb; "></div>
                                </div>
                            </div>

                            <div class="colors-all-slider-thumb colors-thumb swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #000000"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #FFFFFF"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #CCCCCC"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #cc0000"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #0000cc"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #00cc00"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #944ccc"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #cc60a1"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #3fcccb"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="colors-metallic" class="colors-navigation-container">

                            <div class="colors-metallic-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #cc0000; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #0000cc; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #00cc00; "></div>
                                </div>
                            </div>

                            <div class="colors-metallic-slider-thumb colors-thumb swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #cc0000"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #0000cc"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #00cc00"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="colors-nometallic" class="colors-navigation-container">

                            <div class="colors-nometallic-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #000000;"></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #FFFFFF; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #CCCCCC; "></div>
                                </div>
                            </div>

                            <div class="colors-nometallic-slider-thumb colors-thumb swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #000000"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #FFFFFF"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #CCCCCC"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="colors-pearl" class="colors-navigation-container">

                            <div class="colors-pearl-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #944ccc; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #cc60a1; "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); background-color: #3fcccb; "></div>
                                </div>
                            </div>

                            <div class="colors-pearl-slider-thumb colors-thumb swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #944ccc"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #cc60a1"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="color-square animate-custom" style="background: #3fcccb"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="colors-combined" class="colors-navigation-container">

                            <div class="colors-combined-slider body-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); "></div>
                                    <div class="swiper-slide bg" style="background-image: url(content/models/Сolor_Skoda_Fabia.png); "></div>
                                </div>
                            </div>

                            <div class="colors-combined-slider-thumb colors-thumb swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide animate-custom">
                                        <div class="color-square" style="background: #FFFFFF"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide animate-custom">
                                        <div class="color-square" style="background: #FFFFFF"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                    <div class="swiper-slide animate-custom">
                                        <div class="color-square" style="background: #FFFFFF"></div>
                                        <div class="color-description">Оранжевый Terracota – cеребристая крыша (23sf35H)</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="video" class="super-slider-container">

                        <div class="youtube animate-custom opacity-custom-hover" id="O0SpFoDr1cc"></div>

                        <div class="text-element">
                            <div class="text-title border-bottom">
                                <h2>Тест–Драйв: Skoda Fabia</h2>
                            </div>
                            <div class="text-body">
                                <p>Настоящая выгода придется по вкусу каждому! Она идет на пользу вашему бюджету и не содержит ничего лишнего - только самое лучшее и свежее. Как и SKODA Fabia ограниченной серии Fresh, которую теперь можно приобрести без лишних трат. Ведь выгода на приобретение пакета дополнительного оборудования Fresh составляет сегодня 61%! Настоящая выгода придется по вкусу каждому! Она идет на пользу вашему бюджету и не содержит ничего лишнего - только самое лучшее и свежее. Как и SKODA Fabia ограниченной серии Fresh, которую теперь можно приобрести без лишних трат. Ведь выгода на приобретение пакета дополнительного оборудования Fresh составляет сегодня 61%!</p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div><!-- /Super Slider -->

        <!-- Cars -->
        <div id="models-cars" class="section section-sec">
            <div class="box-container">
                <div class="title border-bottom">
                    <h2>Skoda Fabia в наличии: <span class="red-text">91</span></h2>
                </div>

                <div class="new-auto-items four-in-line"><!--
                    --><div class="new-auto-item">
                        <div class="new-auto-item-image">
                            <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                        </div>
                        <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                        <div class="new-auto-item-desc">
                            <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                            <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                        </div>
                        <div class="new-auto-item-info clearfix">
                            <div class="on-left">
                                <div class="new-auto-item-price-new">1 019 900</div>
                                <div class="new-auto-item-price-old">1 320 000</div>
                            </div>
                            <div class="on-right">
                                <a href="#" class="animate-custom gray-small-link use-popup" data-block="event-info">Подробнее</a>
                            </div>
                        </div>
                    </div><!--
                    --><div class="new-auto-item">
                        <div class="new-auto-item-image">
                            <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                        </div>
                        <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                        <div class="new-auto-item-desc">
                            <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                            <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                        </div>
                        <div class="new-auto-item-info clearfix">
                            <div class="on-left">
                                <div class="new-auto-item-price-new">1 019 900</div>
                                <div class="new-auto-item-price-old">1 320 000</div>
                            </div>
                            <div class="on-right">
                                <a href="#" class="animate-custom gray-small-link use-popup" data-block="event-info">Подробнее</a>
                            </div>
                        </div>
                    </div><!--
                    --><div class="new-auto-item">
                        <div class="new-auto-item-image">
                            <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                        </div>
                        <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                        <div class="new-auto-item-desc">
                            <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                            <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                        </div>
                        <div class="new-auto-item-info clearfix">
                            <div class="on-left">
                                <div class="new-auto-item-price-new">1 019 900</div>
                                <div class="new-auto-item-price-old">1 320 000</div>
                            </div>
                            <div class="on-right">
                                <a href="#" class="animate-custom gray-small-link use-popup" data-block="event-info">Подробнее</a>
                            </div>
                        </div>
                    </div><!--
                    --><div class="new-auto-item">
                        <div class="new-auto-item-image">
                            <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/models/models.jpg);"></a>
                        </div>
                        <div class="new-auto-item-name red-hover"><a href="#" class="animate-custom">KIA Sorento</a></div>
                        <div class="new-auto-item-desc">
                            <div class="new-auto-item-desc-field">2014, Luxe 2.0 AT</div>
                            <div class="new-auto-item-desc-field">2 л, бензин (150 л.с.) АКПП</div>
                        </div>
                        <div class="new-auto-item-info clearfix">
                            <div class="on-left">
                                <div class="new-auto-item-price-new">1 019 900</div>
                                <div class="new-auto-item-price-old">1 320 000</div>
                            </div>
                            <div class="on-right">
                                <a href="#" class="animate-custom gray-small-link use-popup" data-block="event-info">Подробнее</a>
                            </div>
                        </div>
                    </div><!--
                --></div>

                <div class="bottom-part clearfix">
                    <div class="on-right"><a href="#">Смотреть все</a></div>
                </div>

            </div>
        </div><!-- /Cars -->

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray section-sec">
                <div class="box-container">
                    <ul class="tabs five-tabs clearfix">
                        <li class="active"><a class="animate-custom" data-block="#description">Описание</a></li>
                        <li><a class="animate-custom" data-block="#characteristics">Характеристики</a></li>
                        <li><a class="animate-custom" data-block="#picking">Коплектации и цены</a></li>
                        <li><a class="animate-custom" data-block="#reviews">Отзывы</a></li>
                        <li><a class="animate-custom" data-block="#stock">Акции</a></li>
                    </ul>
                </div>
            </div>

            <div id="description" class="section tab-container section-sec">
                <div class="box-container">
                    <div class="description-items"><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/1.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Седан КИА Оптима бизнес-класса был представлен публике на Международном автосалоне в Нью-Йорке и стал воплощением инновационного подхода компании KIA Motors к производству новых автомобилей. KIA Optima создан совместными усилиями немецких и американских конструкторских центров для замены KIA Magentis. Конструкторам удалось достичь оптимального баланса габаритов и пропорций благодаря внедрению универсальной платформы.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/2.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>КИА Оптима 2013-2014 года отличается превосходной динамикой благодаря использованию линейки новых силовых агрегатов — одновременно мощных и экономичных. Так, вы можете купить KIA Optima с бензиновым двигателем объемом 2,0 л и 2,4 л. Что касается трансмиссий, то автомобиль КИА Оптима комплектуется шестиступенчатой автоматической или механической коробкой передач. К слову, «автомат» оснащен системой ручного перехода с передачи на передачу Sportmatic.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/3.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Всем внешним видом KIA Optima производители словно хотели сказать: это динамичный и современный автомобиль для человека, который не стоит на месте. Агрессивная решетка радиатора, большие боковые фары заходящие на бока авто, широкий кузов — перед нами отличный представитель нового поколения автомобилей КИА, продажа которых с каждым годом становится все более популярной. Профиль кузова Киа Оптима очерчен хромированной дугой, продолжающейся от передних стоек к задним. </p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/4.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>В интерьере КИА Оптима все продумано для комфортной езды и не вызывает сомнений в том, что это бизнес-класс. Во всех комплектациях авто покупателям доступны лепестковые переключатели скоростей, разместившиеся за рулевым колесом. В базовой версии можно встретить мультиформатную аудиосистему с восемью динамиками и широкими возможностями подключения внешних устройств. Просторность салона достигается не только благодаря обилию свободного места, но и визуально, при помощи панорамного люка из солнцезащитного стекла.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/5.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Конструкторы KIA Optima не забыли и о безопасности. В базовой комплектации автомобиль оснащен шестью подушками, активными подголовниками передних сидений, системой крепления детского кресла и противоударными балками в дверях. В отдельных версиях можно встретить ABS, электронную систему стабилизации движения, систему помощи при экстренном торможении и многие другие полезные опции.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/6.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Купить KIA Optima New вы можете в автосалонах FAVORIT MOTORS. Мы предлагаем большое разнообразие комплектаций и цветовых решений, а также выгодные программы кредитования. Новый автомобиль КИА Оптима в салонах FAVORIT MOTORS — всегда выгодная покупка! </p>
                            </div>
                        </div><!--
                    --></div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="on-center"><a href="#" class="red-big-link animate-custom">Задать вопрос</a></div>
                    </div>

                </div>
            </div>

            <div id="characteristics" class="section tab-container section-sec">
                <div class="box-container">

                    <div class="characteristics-navigation clearfix">
                        <span class="on-left">Кузов: </span>
                        <ul class="on-left clearfix">
                            <li class="on-left active"><a data-block="#sedan">седан</a></li>
                            <li class="on-left"><a data-block="#hatchback">хэчбэк</a></li>
                            <li class="on-left"><a data-block="#wagon">универсал</a></li>
                        </ul>
                    </div>

                    <div class="char-tabs-container" id="sedan">

                        <div class="body-arrangement sedan">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип топлива</td>
                                            <td colspan="2">бензиновый</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип двигателя</td>
                                            <td class="border-right">2.0 NU CVVL MPI</td>
                                            <td>2.4 Theta CVVT MPI</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Кол-во/расположение цилиндров</td>
                                            <td colspan="2">4, рядное</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Рабочий объем, см3</td>
                                            <td class="border-right">2000</td>
                                            <td>2400</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Количество клапанов</td>
                                            <td colspan="2">16</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                            <td class="border-right">150 (6500)</td>
                                            <td>180 (6000)</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                            <td class="border-right">196 @ 4800</td>
                                            <td>231 @ 4000</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Требования к топливу</td>
                                            <td colspan="2">бензин RON95</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип трансмиссии</td>
                                            <td class="border-right">MT</td>
                                            <td>AT</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип привода</td>
                                            <td colspan="2">передний</td>
                                        </tr>

                                        <tr class="border-bottom">
                                            <td class="border-right">Количество передач</td>
                                            <td colspan="2">6</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тип кузова</td>
                                            <td colspan="3">седан</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Количество дверей/мест</td>
                                            <td colspan="3">4/5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                            <td colspan="3">4845 / 1830 / 1455</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Колесная база, мм</td>
                                            <td colspan="3">2795</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Колея (передняя, задняя), мм</td>
                                            <td colspan="3">1591-1601 / 1591-1601</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Дорожный просвет, мм</td>
                                            <td colspan="3">145</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Объём багажника, л (VDA)</td>
                                            <td colspan="3">505</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Подвеска (передняя/задняя)</td>
                                            <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тормоза (передние/задние)</td>
                                            <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                            <td class="border-right">1423/1535</td>
                                            <td class="border-right">1450/1550</td>
                                            <td>480/1580</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Разгон 0-100 км/ч, c</td>
                                            <td class="border-right">9.5</td>
                                            <td class="border-right">10.6</td>
                                            <td>9.5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Разгон 60-100 км/ч, c</td>
                                            <td class="border-right">9.8</td>
                                            <td class="border-right">5.7</td>
                                            <td>5.4</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Максимальная скорость, км/ч</td>
                                            <td class="border-right">210</td>
                                            <td class="border-right">208</td>
                                            <td>210</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                            <td colspan="3">35,8</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                        <tr class="border-bottom">
                                            <td class="border-right">Объём топливного бака, л</td>
                                            <td colspan="3">70</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Город, л/100км</td>
                                            <td class="border-right">9.3</td>
                                            <td class="border-right">10.3</td>
                                            <td>11.5</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Трасса, л/100км</td>
                                            <td class="border-right">5.6</td>
                                            <td class="border-right">6.1</td>
                                            <td>6.2</td>
                                        </tr>
                                        <tr class="border-bottom">
                                            <td class="border-right">Смешанный, л/100км</td>
                                            <td class="border-right">7.0</td>
                                            <td class="border-right">7.6</td>
                                            <td>8.1</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="char-tabs-container" id="hatchback">

                        <div class="body-arrangement hatchback">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип топлива</td>
                                        <td colspan="2">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td class="border-right">2.0 NU CVVL MPI</td>
                                        <td>2.4 Theta CVVT MPI</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Кол-во/расположение цилиндров</td>
                                        <td colspan="2">4, рядное</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Рабочий объем, см3</td>
                                        <td class="border-right">2000</td>
                                        <td>2400</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество клапанов</td>
                                        <td colspan="2">16</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                        <td class="border-right">150 (6500)</td>
                                        <td>180 (6000)</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                        <td class="border-right">196 @ 4800</td>
                                        <td>231 @ 4000</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Требования к топливу</td>
                                        <td colspan="2">бензин RON95</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">MT</td>
                                        <td>AT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип привода</td>
                                        <td colspan="2">передний</td>
                                    </tr>

                                    <tr class="border-bottom">
                                        <td class="border-right">Количество передач</td>
                                        <td colspan="2">6</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип кузова</td>
                                        <td colspan="3">седан</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество дверей/мест</td>
                                        <td colspan="3">4/5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                        <td colspan="3">4845 / 1830 / 1455</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колесная база, мм</td>
                                        <td colspan="3">2795</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колея (передняя, задняя), мм</td>
                                        <td colspan="3">1591-1601 / 1591-1601</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Дорожный просвет, мм</td>
                                        <td colspan="3">145</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём багажника, л (VDA)</td>
                                        <td colspan="3">505</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подвеска (передняя/задняя)</td>
                                        <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормоза (передние/задние)</td>
                                        <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                        <td class="border-right">1423/1535</td>
                                        <td class="border-right">1450/1550</td>
                                        <td>480/1580</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 0-100 км/ч, c</td>
                                        <td class="border-right">9.5</td>
                                        <td class="border-right">10.6</td>
                                        <td>9.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 60-100 км/ч, c</td>
                                        <td class="border-right">9.8</td>
                                        <td class="border-right">5.7</td>
                                        <td>5.4</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная скорость, км/ч</td>
                                        <td class="border-right">210</td>
                                        <td class="border-right">208</td>
                                        <td>210</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                        <td colspan="3">35,8</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём топливного бака, л</td>
                                        <td colspan="3">70</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Город, л/100км</td>
                                        <td class="border-right">9.3</td>
                                        <td class="border-right">10.3</td>
                                        <td>11.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Трасса, л/100км</td>
                                        <td class="border-right">5.6</td>
                                        <td class="border-right">6.1</td>
                                        <td>6.2</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Смешанный, л/100км</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right">7.6</td>
                                        <td>8.1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="char-tabs-container" id="wagon">

                        <div class="body-arrangement wagon">
                            <div class="body-arrangement-size length">4845</div>
                            <div class="body-arrangement-size height">1455</div>
                            <div class="body-arrangement-size width-one">1830</div>
                            <div class="body-arrangement-size width-two">1830</div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Двигатель</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип топлива</td>
                                        <td colspan="2">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td class="border-right">2.0 NU CVVL MPI</td>
                                        <td>2.4 Theta CVVT MPI</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Кол-во/расположение цилиндров</td>
                                        <td colspan="2">4, рядное</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Рабочий объем, см3</td>
                                        <td class="border-right">2000</td>
                                        <td>2400</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество клапанов</td>
                                        <td colspan="2">16</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная мощность, л.с. (об./мин.)</td>
                                        <td class="border-right">150 (6500)</td>
                                        <td>180 (6000)</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальный крутящий момент, Н•м (об./мин.)</td>
                                        <td class="border-right">196 @ 4800</td>
                                        <td>231 @ 4000</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Требования к топливу</td>
                                        <td colspan="2">бензин RON95</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="30%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">MT</td>
                                        <td>AT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип привода</td>
                                        <td colspan="2">передний</td>
                                    </tr>

                                    <tr class="border-bottom">
                                        <td class="border-right">Количество передач</td>
                                        <td colspan="2">6</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Трансмиссия</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип кузова</td>
                                        <td colspan="3">седан</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Количество дверей/мест</td>
                                        <td colspan="3">4/5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Габариты (длина/ширина/высота), мм</td>
                                        <td colspan="3">4845 / 1830 / 1455</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колесная база, мм</td>
                                        <td colspan="3">2795</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Колея (передняя, задняя), мм</td>
                                        <td colspan="3">1591-1601 / 1591-1601</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Дорожный просвет, мм</td>
                                        <td colspan="3">145</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём багажника, л (VDA)</td>
                                        <td colspan="3">505</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подвеска (передняя/задняя)</td>
                                        <td colspan="3">независимая, пружинная, типа Макферсон, со стабилизатором поперечной устойчивости/независимая, рычажно-пружинная, с телескопическими гидравлическими, со стабилизатором поперечной устойчивости</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормоза (передние/задние)</td>
                                        <td colspan="3">дисковые вентилируемые/ дисковые</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Снаряженная масса (мин/макс), кг</td>
                                        <td class="border-right">1423/1535</td>
                                        <td class="border-right">1450/1550</td>
                                        <td>480/1580</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Динамические характеристики</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 0-100 км/ч, c</td>
                                        <td class="border-right">9.5</td>
                                        <td class="border-right">10.6</td>
                                        <td>9.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Разгон 60-100 км/ч, c</td>
                                        <td class="border-right">9.8</td>
                                        <td class="border-right">5.7</td>
                                        <td>5.4</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Максимальная скорость, км/ч</td>
                                        <td class="border-right">210</td>
                                        <td class="border-right">208</td>
                                        <td>210</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тормозной путь 100-0 км/ч, м</td>
                                        <td colspan="3">35,8</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-element">
                            <div class="table-title border-bottom">
                                <h2>Расход топлива*</h2>
                            </div>
                            <div class="table-body">
                                <table>
                                    <colgroup>
                                        <col width="40%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объём топливного бака, л</td>
                                        <td colspan="3">70</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Город, л/100км</td>
                                        <td class="border-right">9.3</td>
                                        <td class="border-right">10.3</td>
                                        <td>11.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Трасса, л/100км</td>
                                        <td class="border-right">5.6</td>
                                        <td class="border-right">6.1</td>
                                        <td>6.2</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Смешанный, л/100км</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right">7.6</td>
                                        <td>8.1</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="text-element">
                            <div class="text-body">
                                <p>*Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                                <h2>Двигатель</h2>
                                <p>Автомобиль KIA Optima 2014-2015 оснащается бензиновым двигателем объемом 2,0 либо 2,4 литра. Силовые агрегаты, которые устанавливаются на модель, достаточно мощные (до 180 лошадиных сил).<br/>Технические характеристики KIA Optima с точки зрения динамики позволяют машине неплохо маневрировать в городских условиях. До 100 километров в час модель разгоняется за 10,5 секунды. С 60 до 100 км/ч авто разгоняется за 5,4 секунды. Максимальная скорость, которую может развивать Optima, составляет 210 километров</p>
                                <p>Технические характеристики Киа Оптима 2014-2015 соответствуют запросам самых требовательных автолюбителей. Автомобиль потребляет в городе около 11,5 литра топлива на 100 километров (при объеме двигателя в 2,4 литра). В смешанном цикле расход составляет 8,1 литра. Правда, на трассе эти цифры выглядят несколько скромнее – всего 6,2 литра на 100 километров. </p>
                                <h2>Трансмиссия</h2>
                                <p>Автомобиль KIA Optima подойдет как любителям автоматических, так и механических КПП. В обоих вариантах трансмиссии шестиступенчатые. Передний привод идеально подходит для городской езды, но испытывать на этой машине бездорожье не рекомендуется.</p>
                                <h2>Кузов</h2>
                                <p>Автомобили KIA Optima выпускаются в кузове седан. Габариты машины вполне соответствуют моделям бизнес-класса, который и представляет Optima. Ее длина –  4,85 м, ширина – 1,83 м и высота – 1,46 метра. Четырехдверный седан рассчитан на пять посадочных мест. </p>
                                <p>Клиренс у Optima равен 145 миллиметрам. Поэтому рекомендуется не заезжать на высокие бордюры и избегать больших ям на дорогах. Что касается объема багажного пространства, он у данной модели составляет 505 литров. Этого хватит, чтобы поместить туда все необходимое даже для длительной поездки.</p>
                            </div>
                        </div>

                        <div class="bottom-part clearfix">
                            <div class="pdf-link on-right clearfix align-left">
                                <div class="on-left">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <div class="on-left">
                                    <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                    <div class="pdf-link-format">формат PDF</div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div id="picking" class="section tab-container section-sec">
                <div class="box-container">

                    <div class="title">
                        <h2>Стандартное оборудование</h2>
                    </div>

                    <div class="standard-equipment-items">

                        <div class="standard-equipment-item">
                            <div class="standard-equipment-title">
                                <h3>Экстерьер</h3>
                            </div>
                            <div class="standard-equipment-list">
                                <ul>
                                    <li>Полноразмерное легкосплавное запасное колесо</li>
                                    <li>Cтеклоочистители "Aero blade"</li>
                                    <li>Омыватель фар</li>
                                    <li>Поворотный свет</li>
                                    <li>Светодиодные дневные ходовые огни (LED DRL)</li>
                                    <li>Дополнительный стоп-сигнал</li>
                                    <li>Электропривод складывания зеркал заднего вида и повторители указателя поворота</li>
                                </ul>
                            </div>
                        </div>

                        <div class="standard-equipment-item">
                            <div class="standard-equipment-title">
                                <h3>Интерьер</h3>
                            </div>
                            <div class="standard-equipment-list">
                                <ul>
                                    <li>Задние сиденья со спинками, складывающимися в соотношении 60/40</li>
                                    <li>Ручки дверей с отделкой хромом</li>
                                    <li>Ящик в переднем центральном подлокотнике</li>
                                    <li>Задний подлокотник с подстаканниками</li>
                                    <li>Охлаждаемый перчаточный ящик</li>
                                    <li>Прикуриватель и пепельница</li>
                                    <li>Сетка для крепления багажа</li>
                                </ul>
                            </div>
                        </div>

                        <div class="standard-equipment-item">
                            <div class="standard-equipment-title">
                                <h3>Безопасность</h3>
                            </div>
                            <div class="standard-equipment-list">
                                <ul>
                                    <li>Фронтальные подушки безопасности</li>
                                    <li>Боковые подушки и шторки безопасности</li>
                                    <li>Активные подголовники передних сидений</li>
                                    <li>Антиблокировочная система тормозов (ABS)</li>
                                    <li>Система предупреждения об экстренном торможении (ESS)</li>
                                    <li>Автоматическое запирание дверей при движении</li>
                                    <li>Крепление для детского кресла ISOFIX</li>
                                </ul>
                            </div>
                        </div>

                        <div class="standard-equipment-item">
                            <div class="standard-equipment-title">
                                <h3>Комфорт</h3>
                            </div>
                            <div class="standard-equipment-list">
                                <ul>
                                    <li>Круиз-контроль</li>
                                    <li>Адаптивные спортивные амортизаторы переменной жёсткости</li>
                                    <li>Функция "Эскорт" (Задержка выключения фар при закрывании замков)</li>
                                    <li>Передние и задние стеклоподъёмники с электроприводом и функцией Auto</li>
                                    <li>Раздельный климат-контроль</li>
                                    <li>Мультифункциональное рулевое колесо</li>
                                    <li>Регулировка рулевой колонки по высоте и по вылету</li>
                                    <li>Поясничный подпор водительского сиденья с электроприводом регулирово</li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="table-element">
                        <div class="table-title border-bottom">
                            <h2>Различающиеся опции</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Объем и мощность двигателя (л / л.с.)</td>
                                        <td class="border-right" colspan="4">2.0 / 150</td>
                                        <td colspan="3">2.4 / 180</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип двигателя</td>
                                        <td colspan="7">бензиновый</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Тип трансмиссии</td>
                                        <td class="border-right">6AT</td>
                                        <td colspan="6">6MT</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Ускорение 0-100 км/ч (сек.)</td>
                                        <td class="border-right">10.5</td>
                                        <td class="border-right" colspan="3">9.6</td>
                                        <td colspan="3">10.5</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Расход топлива комбинированный (л/100 км) ***</td>
                                        <td class="border-right">7.0</td>
                                        <td class="border-right" colspan="3">7.6</td>
                                        <td colspan="3">8.1</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Название комплектации</td>
                                        <td class="border-right" colspan="2">Comfort</td>
                                        <td class="border-right">Luxe</td>
                                        <td class="border-right">Prestige</td>
                                        <td class="border-right">Luxe</td>
                                        <td class="border-right">Prestige</td>
                                        <td>Premium</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">Код модели</td>
                                        <td class="border-right">TFS42G617</td>
                                        <td class="border-right" colspan="3">TFS42G61F</td>
                                        <td colspan="3">TFS4L661F</td>
                                    </tr>
                                    <tr class="border-bottom">
                                        <td class="border-right">OCN</td>
                                        <td class="border-right" colspan="2">D565</td>
                                        <td class="border-right">GI73</td>
                                        <td class="border-right">GJS9/GK26</td>
                                        <td class="border-right">GI77</td>
                                        <td class="border-right">GJT0/GK27</td>
                                        <td>GJT1/GK28</td>
                                    </tr>
                                    <tr class="border-bottom strong">
                                        <td class="border-right">Максимальная цена перепродажи на автомобили 2015 производственного года*</td>
                                        <td class="border-right">1 049 900</td>
                                        <td class="border-right">1 099 900</td>
                                        <td class="border-right">1 189 900</td>
                                        <td class="border-right">1 299 900</td>
                                        <td class="border-right">1 249 900</td>
                                        <td class="border-right">1 359 900</td>
                                        <td>1 459 900</td>
                                    </tr>
                                    <tr class="border-bottom strong">
                                        <td class="border-right">Максимальная цена перепродажи на автомобили 2014 производственного года*</td>
                                        <td class="border-right">989 900</td>
                                        <td class="border-right">1 039 900</td>
                                        <td class="border-right">1 129 900</td>
                                        <td class="border-right">1 239 900</td>
                                        <td class="border-right">1 189 900</td>
                                        <td class="border-right">1 299 900</td>
                                        <td>1 399 900</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element collapsed">
                        <div class="table-title animate-custom opacity-custom-hover">
                            <h2>Пакет <span class="red-text">«Теплые опции»</span></h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                    <tr class="border-bottom">
                                        <td class="border-right">Подогрев Руля</td>
                                        <td class="border-right">-</td>
                                        <td class="border-right">-</td>
                                        <td class="border-right">-</td>
                                        <td class="border-right">+</td>
                                        <td class="border-right">+</td>
                                        <td class="border-right">+</td>
                                        <td>+</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element collapsed">
                        <div class="table-title animate-custom opacity-custom-hover">
                            <h2>Экстерьер</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element collapsed">
                        <div class="table-title animate-custom opacity-custom-hover">
                            <h2>Интерьер</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element collapsed">
                        <div class="table-title animate-custom opacity-custom-hover">
                            <h2>Безопасность</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="table-element collapsed">
                        <div class="table-title animate-custom opacity-custom-hover">
                            <h2>Комфорт</h2>
                        </div>
                        <div class="table-body">
                            <table>
                                <colgroup>
                                    <col width="30%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                    <col width="10%" />
                                </colgroup>
                                <tbody>
                                <tr class="border-bottom">
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td class="border-right"></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="text-element">
                        <div class="text-body">
                            <p>* Цены на продукцию KIA. Сведения о ценах, содержащиеся на сайте, носят исключительно информационный характер. Указанные цены могут отличаться от действительных цен уполномоченных дилеров KIA. Для получения подробной информации об актуальных ценах на продукцию KIA обращайтесь к уполномоченным дилерам KIA. Приобретение любой продукции KIA осуществляется в соответствии с условиями индивидуального договора купли-продажи. </p>
                            <p>* Prices for KIA products. Information about prices, placed at this web-site, has only informational purposes. The indicated prices may be different from actual prices of authorized KIA dealers. To receive detailed information on actual prices for KIA products please refer to authorized KIA dealers. Purchase of any of KIA products is made according to provisions of individual sale and purchase contracts.</p>
                            <p>*** Данные по расходу топлива получены в стандартизованных условиях с применением специального измерительного оборудования. Реальный расход топлива может отличаться в силу воздействия различных объективных и субъективных факторов: влажности, давления и температуры окружающего воздуха, фракционного состава используемого топлива, рельефа местности, характеристик дорожного покрытия, скорости движения автомобиля, направления и скорости ветра, атмосферных осадков, давления в шинах а также их размерности, марки и модели, массы перевозимого груза (включая водителя и пассажиров) и манеры вождения (частота и интенсивность продольных и поперечных ускорений, средняя скорость).</p>
                            <h2>Цены на KIA Optima 2014-2015</h2>
                            <p>Новый седан бизнес-класса корейского производителя дебютировал на российском рынке в 2010 году. За прошедшее время Киа Оптима зарекомендовал себя как очень комфортный и представительный автомобиль. Богатое оснащение дополнительными опциями, мощные двигатели и высокий уровень безопасности — вот сильные стороны этой модели. Уже в базовой комплектации автомобиль оснащается раздельным климат-контролем, дистанционным управлением центральным замком и легкосплавными дисками. </p>
                            <p>Доступны два бензиновых двигателя (2,0 л и 2,4 л), на которые устанавливается механическая или автоматическая трансмиссия (6 ступеней). Механика доступна только в базовой комплектации, в остальных случаях устанавливается «автомат». Автомобиль поставляется в следующих комплектациях: Comfort, Luxe, Prestige и Premium. Они доступны как для базового двигателя (2,0 л), так и для самого мощного (2,4 л). Это позволяет водителям выбрать машину под свой темперамент и стиль вождения. Именно этого и ожидаешь от бизнес-класса — максимальный выбор и комфорт. Машина полностью соответствует своему классу. За безопасность отвечают антиблокировочная система, система курсовой устойчивости, интегрированная система активного управления, система помощи при трогании на подъеме, система контроля слепых зон, фронтальные и боковые подушки безопасности, ремни с преднатяжителями.</p>
                            <p>Комфорт обеспечивает современная аудиосистема с внешним сабвуфером и усилителем, система Drive Mode Select, приборная панель Supervision 4.3" и вентилируемые сидения. Получить более точную информацию о комплектациях и ценах на новый Киа Оптима 2014-2015 можно узнать из таблицы на данной странице. </p>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="reviews" class="section tab-container section-sec">

                <div class="reviews-slider-block">
                    <div class="box-container">

                        <div class="reviews-slider-header opacity-custom"><i class="fa fa-pencil"></i></div>

                        <div class="reviews-slider swiper-container">

                            <div class="swiper-wrapper">

                                <div class="swiper-slide">

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                </div>

                                <div class="swiper-slide">

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                </div>

                                <div class="swiper-slide">

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                    <div class="reviews-slider-item">
                                        <div class="reviews-slider-item-date">10.03.2015 - 13:54:46</div>
                                        <div class="reviews-slider-item-text">
                                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                                        </div>
                                        <div class="reviews-slider-item-author">Огородник Иван Михайлович</div>
                                    </div>

                                </div>

                            </div>

                            <div class="swiper-pagination"></div>

                        </div>

                    </div>
                </div>

                <div class="reviews-slider-form section section-gray section-sec">
                    <div class="box-container">
                        <div class="title">
                            <h2>Оставить свой отзыв</h2>
                        </div>

                        <p class="gray-text">Для того, чтобы оставить отзыв, Вам необходимо  <a href="#" class="animate-custom red-text">Зарегистрироваться</a>  или войдите через: <a class="fb social-icon-link" href="#"><i class="fa fa-facebook animate-custom"></i></a> <a class="vk social-icon-link" href="#"><i class="fa fa-vk animate-custom"></i></a></p>

                        <form id="reviews-form" class="page-form">

                            <div class="form-item">
                                <label for="review-theme">Выбирите тему отзыва:</label>
                                <select id="review-theme">
                                    <option>Продажа новых автомобилей</option>
                                    <option>Ещё пункт 1</option>
                                    <option>Ещё пункт 2</option>
                                    <option>Ещё пункт 3</option>
                                    <option>Ещё пункт 4</option>
                                </select>
                            </div>

                            <div class="form-item">
                                <label for="review-text">Текст Вашего сообщения:</label>
                                <textarea id="review-text"></textarea>
                            </div>

                            <div class="form-item clearfix">
                                <input type="submit" class="on-right animate-custom red-big-link" value="Оставить отзыв" />
                            </div>

                        </form>

                    </div>
                </div>

                <div class="section section-sec"></div>

            </div>

            <div id="stock" class="section tab-container section-sec">
                <div class="box-container">

                </div>
            </div>

        </div><!-- /Tabs Container -->

    </section><!-- /Content -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>