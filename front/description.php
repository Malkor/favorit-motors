 		<div class="box-container">
                    <div class="description-items"><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/1.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Седан КИА Оптима бизнес-класса был представлен публике на Международном автосалоне в Нью-Йорке и стал воплощением инновационного подхода компании KIA Motors к производству новых автомобилей. KIA Optima создан совместными усилиями немецких и американских конструкторских центров для замены KIA Magentis. Конструкторам удалось достичь оптимального баланса габаритов и пропорций благодаря внедрению универсальной платформы.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/2.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>КИА Оптима 2013-2014 года отличается превосходной динамикой благодаря использованию линейки новых силовых агрегатов — одновременно мощных и экономичных. Так, вы можете купить KIA Optima с бензиновым двигателем объемом 2,0 л и 2,4 л. Что касается трансмиссий, то автомобиль КИА Оптима комплектуется шестиступенчатой автоматической или механической коробкой передач. К слову, «автомат» оснащен системой ручного перехода с передачи на передачу Sportmatic.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/3.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Всем внешним видом KIA Optima производители словно хотели сказать: это динамичный и современный автомобиль для человека, который не стоит на месте. Агрессивная решетка радиатора, большие боковые фары заходящие на бока авто, широкий кузов — перед нами отличный представитель нового поколения автомобилей КИА, продажа которых с каждым годом становится все более популярной. Профиль кузова Киа Оптима очерчен хромированной дугой, продолжающейся от передних стоек к задним. </p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/4.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>В интерьере КИА Оптима все продумано для комфортной езды и не вызывает сомнений в том, что это бизнес-класс. Во всех комплектациях авто покупателям доступны лепестковые переключатели скоростей, разместившиеся за рулевым колесом. В базовой версии можно встретить мультиформатную аудиосистему с восемью динамиками и широкими возможностями подключения внешних устройств. Просторность салона достигается не только благодаря обилию свободного места, но и визуально, при помощи панорамного люка из солнцезащитного стекла.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/5.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Конструкторы KIA Optima не забыли и о безопасности. В базовой комплектации автомобиль оснащен шестью подушками, активными подголовниками передних сидений, системой крепления детского кресла и противоударными балками в дверях. В отдельных версиях можно встретить ABS, электронную систему стабилизации движения, систему помощи при экстренном торможении и многие другие полезные опции.</p>
                            </div>
                        </div><!--
                        --><div class="description-item">
                            <div class="description-item-image bg" style="background-image: url(content/models/desc/6.jpg)"></div>
                            <div class="description-item-info align-justify">
                                <p>Купить KIA Optima New вы можете в автосалонах FAVORIT MOTORS. Мы предлагаем большое разнообразие комплектаций и цветовых решений, а также выгодные программы кредитования. Новый автомобиль КИА Оптима в салонах FAVORIT MOTORS — всегда выгодная покупка! </p>
                            </div>
                        </div><!--
                    --></div>

                    <div class="bottom-part clearfix">
                        <div class="pdf-link on-right clearfix align-left">
                            <div class="on-left">
                                <i class="fa fa-file-pdf-o"></i>
                            </div>
                            <div class="on-left">
                                <div class="pdf-link-url"><a href="#">Загрузить брошюру</a></div>
                                <div class="pdf-link-format">формат PDF</div>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-part clearfix">
                        <div class="on-center"><a href="#" class="red-big-link animate-custom">Задать вопрос</a></div>
                    </div>

                </div>