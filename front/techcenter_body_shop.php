<?php include("header.php"); ?>

    <!-- Content -->
    <section id="content">

        <div id="accessories-header" class="section section-gray section-sec section-top">
            <div class="box-container clearfix" style="background-image: url(images/body-bg.png);">

                <div class="on-left">

                    <div class="title-post">
                        <h1>Дополнительное оборудование</h1>
                    </div>

                    <div class="body">

                        <ul>
                            <li>Окраска по фиксированной цене</li>
                            <li>Оптимальные сроки</li>
                            <li>Опытные специалисты</li>
                            <li>Скидки на запчасти</li>
                            <li>Программа лояльности</li>
                        </ul>

                    </div>

                </div>

                <div class="on-right">

                    <div class="tech-center-menu">
                        <ul class="tech-center-menu-list align-left">
                            <li><a href="#" class="animate-custom"><span>Технический центр</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Сервисные акции</span></a></li>
                            <li><a href="#" class="animate-custom"><span>ТО и слесарный ремонт</span></a></li>
                            <li class="active"><a href="#" class="animate-custom"><span>Кузовной ремон</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Оригинальные запчасти</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Дополнительное оборудование</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Регламент ТО</span></a></li>
                            <li><a href="#" class="animate-custom"><span>Каталог оригинальных аксессуаров</span></a></li>
                        </ul>
                    </div>

                </div>

                <div class="accessories-button">
                    <a href="#" class="animate-custom red-small-link">On-line запись</a>
                </div>

            </div>
        </div>

        <div class="section section-sec">
            <div class="box-container">

                <div class="body">
                    <img src="content/accessories/body-image-3.jpg" alt="body-image" class="body-image show-on-right" />
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam </p>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam </p>
                    <img src="content/accessories/body-image-4.jpg" alt="body-image" class="body-image show-on-left" />
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam </p>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam erat, sed diam voluptua. At avero eos et accusam et justo duo dolores et ea rebum. aStet clita kasd gubergren, no sea takimata sanctus est aLorem ipsum dolor sit amet. Lorem ipsum dolor sit aamet, consetetur sadipscing elitr, sed diam nonumy aeirmod tempor invidunt ut labore et dolore magna aaliquyam erat, sed diam voluptua. At vero eos et aaccusam et justo duo dolores et ea rebum. Stet clita akasd gubergren, no sea takimata sanctus est Lorem aipsum dolor sit amet.<br/>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, ased diam nonumy eirmod tempor invidunt ut labore et adolore magna aliquyam </p>
                </div>

                <div class="bottom-part on-center">
                    <a href="#" class="animate-custom red-small-link">Оставить отзыв о работе технического центра</a>
                </div>

            </div>
        </div>

    </section><!-- /Content -->

<?php include("footer.php"); ?>