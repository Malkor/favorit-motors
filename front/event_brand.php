<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Event Brand");
?>

    <!-- Content -->
    <section id="content">

        <div class="section-red">
            <div class="box-container">
                <div class="breadcrumbs-two">
                    <ul class="breadcrumbs-list">
                        <li>Каталог акций</li>
                        <li>/</li>
                    </ul>
                </div>
                <div class="title-gray">
                    <h1>Специальные предложения на покупку SKODA</h1>
                </div>
            </div>
        </div>

        <!-- Banners Header -->
        <div id="banner-header" class="section-sec section-gray">
            <div class="box-container clearfix">

                <div class="on-left">
                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/events/banner-1.jpg); "></a>
                </div>

                <div class="on-right">
                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/events/banner-2.jpg); "></a>
                    <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/events/banner-3.jpg); "></a>
                </div>

            </div>
        </div><!-- /Banners Header -->

        <!-- Events List -->
        <div class="section section-sec">
            <div class="box-container">

                <div class="event-list-items">

                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">

                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/slider/1.jpg);"></a>
                        </div>

                        <div class="accessories-item-info">
                            <div class="accessories-item-name">24 февраля 2015 – 24 марта 2015</div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover">ŠKODA Rapid</a>
                            </div>
                            <div class="event-list-item-desc">
                                <p>Выгода до 125 000 руб. при покупке SKODA в декабре! Воспользуйтесь зимней выгодой до 125 000 рублей на приобретение автомобиля SKODA в салонах официального дилера Автопрага.</p>
                            </div>
                        </div>

                        <div class="accessories-item-button">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>

                    </div>

                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">

                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/slider/1.jpg);"></a>
                        </div>

                        <div class="accessories-item-info">
                            <div class="accessories-item-name">24 февраля 2015 – 24 марта 2015</div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover">ŠKODA Rapid</a>
                            </div>
                            <div class="event-list-item-desc">
                                <p>Выгода до 125 000 руб. при покупке SKODA в декабре! Воспользуйтесь зимней выгодой до 125 000 рублей на приобретение автомобиля SKODA в салонах официального дилера Автопрага.</p>
                            </div>
                        </div>

                        <div class="accessories-item-button">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>

                        <div class="event-list-item-mark red-event">
                            <div class="event-list-item-mark-icon animate-custom"><i class="fa fa-exclamation"></i></div>
                            <div class="event-list-item-mark-text">Количество автомобилей ограничено</div>
                        </div>

                    </div>

                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">

                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/slider/1.jpg);"></a>
                        </div>

                        <div class="accessories-item-info">
                            <div class="accessories-item-name">24 февраля 2015 – 24 марта 2015</div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover">ŠKODA Rapid</a>
                            </div>
                            <div class="event-list-item-desc">
                                <p>Выгода до 125 000 руб. при покупке SKODA в декабре! Воспользуйтесь зимней выгодой до 125 000 рублей на приобретение автомобиля SKODA в салонах официального дилера Автопрага.</p>
                            </div>
                        </div>

                        <div class="accessories-item-button">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>

                        <div class="event-list-item-mark">
                            <div class="event-list-item-mark-icon animate-custom"><i class="fa fa-rub"></i></div>
                            <div class="event-list-item-mark-text">Кредитование</div>
                        </div>

                    </div>

                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">

                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/slider/1.jpg);"></a>
                        </div>

                        <div class="accessories-item-info">
                            <div class="accessories-item-name">24 февраля 2015 – 24 марта 2015</div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover">ŠKODA Rapid</a>
                            </div>
                            <div class="event-list-item-desc">
                                <p>Выгода до 125 000 руб. при покупке SKODA в декабре! Воспользуйтесь зимней выгодой до 125 000 рублей на приобретение автомобиля SKODA в салонах официального дилера Автопрага.</p>
                            </div>
                        </div>

                        <div class="accessories-item-button">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>

                        <div class="event-list-item-mark">
                            <div class="event-list-item-mark-icon animate-custom"><i class="fa fa-umbrella"></i></div>
                            <div class="event-list-item-mark-text">Страхование</div>
                        </div>

                    </div>

                    <div class="event-list-item accessories-item section-small border-top border-bottom clearfix">

                        <div class="accessories-item-image">
                            <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/slider/1.jpg);"></a>
                        </div>

                        <div class="accessories-item-info">
                            <div class="accessories-item-name">24 февраля 2015 – 24 марта 2015</div>
                            <div class="event-list-item-name">
                                <a href="#" class="animate-custom red-hover">ŠKODA Rapid</a>
                            </div>
                            <div class="event-list-item-desc">
                                <p>Выгода до 125 000 руб. при покупке SKODA в декабре! Воспользуйтесь зимней выгодой до 125 000 рублей на приобретение автомобиля SKODA в салонах официального дилера Автопрага.</p>
                            </div>
                        </div>

                        <div class="accessories-item-button">
                            <a href="#" class="animate-custom gray-small-link">Подробнее</a>
                        </div>

                    </div>

                </div>

                <div class="bottom-part clearfix">
                    <a class="on-right" href="#">Архив акций</a>
                </div>

            </div>
        </div><!-- /Events List -->

        <!-- Event Deals -->
        <div class="section section-sec section-gray">
            <div class="box-container medium">

                <div class="event-deal-items"><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-wrench"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Сервисные<br/>акции</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-car"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог подержанных<br/>авто со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-rub"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Кредитные<br/>предложения</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-umbrella"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Страхование<br/>Genius</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-file-text-o"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог автомобилей<br/>в наличии со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-expand"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Выгода при сдаче<br/>авто в Trade-In</span>
                    </a><!--
                --></div>

            </div>
        </div><!-- /Event Deals -->

        <!-- Phone -->
        <div id="add-your-phone" class="section section-sec padding">
            <div class="box-container medium">

                <div class="text on-center">
                    <p>Оставьте свой телефон и мы перезвоним вам в течении 15 минут</p>
                </div>

                <div class="filter"><form id="event" class="clearfix">

                    <div class="filter-item clearfix">
                        <label for="event-name" class="on-left">Ваше имя:</label>
                        <input id="event-name" class="on-left" type="text" />
                    </div>

                    <div class="filter-item on-center clearfix">
                        <label for="event-phone" class="on-left">Телефон:</label>
                        <input id="event-phone" class="on-left phone-mask" type="text" />
                    </div>

                    <div class="filter-item align-right">
                        <input type="submit" class="animate-custom red-small-link" value="Персональное предложение" />
                    </div>

                </form></div>

            </div>
        </div><!-- /Phone -->

        <!-- Seo Text -->
        <div class="section section-sec section-gray padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>