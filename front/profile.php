<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Reviews");
?>

    <!-- Content -->
    <section id="content">

        <!-- Cars -->
        <div id="profile-cars" class="section section-gray section-sec section-top">
            <div class="box-container">
                <div class="title">
                    <h2>Мои автомобили</h2>
                </div>

                <div class="filter"><form id="auto-filter" class="clearfix">

                        <div class="filter-item">
                            <label for="my-auto"></label>
                            <select id="my-auto">
                                <option>Volvo S60</option>
                                <option>Машина 1</option>
                                <option>Машина 2</option>
                            </select>
                        </div>

                        <div class="filter-item on-center">
                            <i class="fa fa-star"></i> <span class="gray-text">Автомобиль по умолчанию</span>
                        </div>

                        <div class="filter-item align-right">
                            <i class="fa fa-pencil"></i> <a href="#">Пробег и регистрационные данные</a>
                        </div>

                </form></div>

                <div class="rotation-slider swiper-container">

                    <div class="swiper-wrapper">
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/1.jpg); ">
                            <a href="content/profile/slider/1.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/2.jpg); ">
                            <a href="content/profile/slider/2.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/3.jpg); ">
                            <a href="content/profile/slider/3.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/4.jpg); ">
                            <a href="content/profile/slider/4.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/5.jpg); ">
                            <a href="content/profile/slider/5.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/6.jpg); ">
                            <a href="content/profile/slider/6.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/7.jpg); ">
                            <a href="content/profile/slider/7.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                        <div class="swiper-slide bg" style="background-image: url(content/profile/slider/8.jpg); ">
                            <a href="content/profile/slider/8.jpg" rel="gallery" class="colorbox"></a>
                        </div>
                    </div>

                    <!-- Add Arrows -->
                    <div class="swiper-button-next opacity-custom-hover animate-custom"></div>
                    <div class="swiper-button-prev opacity-custom-hover animate-custom"></div>

                </div>

                <div class="bottom-part clearfix">
                    <div class="on-right"><i class="icon fa fa-plus"></i><a href="#">Загрузить еще фото</a></div>
                </div>

            </div>
        </div><!-- /Cars -->

        <!-- Service -->
        <div id="profile-service" class="section section-sec">
            <div class="box-container">
                <div class="clearfix">

                    <div class="left-part">
                        <div class="title-gray">
                            <h2>Обслуживание</h2>
                        </div>

                        <div class="border-bottom">
                            <a class="red-big-link animate-custom use-popup" data-block="add-service">Записаться в тех. центр</a>
                        </div>

                        <div class="section-small border-bottom">
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">В процессе</div>
                                <div class="service-progress-item-status"><a href="#">Слесарные работы</a></div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">Готово</div>
                                <div class="service-progress-item-status"><a href="#">Оценить работу</a></div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">Запланированно</div>
                                <div class="service-progress-item-status">Записаться на ТО</div>
                            </div>
                        </div>

                        <div class="section-small border-top">
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">Готово</div>
                                <div class="service-progress-item-status on-left right-margin">Плановое ТО 10 000</div>
                                <div class="service-progress-item-date on-left">23 марта 2015</div>
                                <div class="service-progress-item-rating on-right left-margin">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-pencil animate-custom"></i>
                                </div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">Готово</div>
                                <div class="service-progress-item-status on-left right-margin">Плановое ТО 10 000</div>
                                <div class="service-progress-item-date on-left">23 марта 2015</div>
                                <div class="service-progress-item-rating on-right left-margin">
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-pencil animate-custom"></i>
                                </div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-name on-left right-margin">Готово</div>
                                <div class="service-progress-item-status on-left right-margin">Плановое ТО 10 000</div>
                                <div class="service-progress-item-date on-left">23 марта 2015</div>
                                <div class="service-progress-item-rating on-right left-margin">
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star active"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-pencil animate-custom"></i>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="right-part">
                        <div class="title-gray">
                            <h2>Персональные акции</h2>
                        </div>

                        <div class="event-items">
                            <div class="event-item section-small border-top border-bottom clearfix">
                                <div class="event-item-image">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/event.jpg);"></a>
                                </div>
                                <div class="event-item-info">
                                    <div class="accessories-item-name">
                                        <a class="animate-custom red-hover" href="#">Тормозные диски GRIZZLY</a>
                                    </div>
                                    <div class="event-item-desc">
                                        <p>Тормозные диски специально разработаны для машин класса C таких как Volvo S60</p>
                                    </div>
                                    <div class="event-item-bar clearfix">
                                        <div class="on-left event-item-price">20 000 р.</div>
                                        <div class="on-right event-item-button">
                                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="event-item section-small border-top border-bottom clearfix">
                                <div class="event-item-image">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/event.jpg);"></a>
                                </div>
                                <div class="event-item-info">
                                    <div class="accessories-item-name">
                                        <a class="animate-custom red-hover" href="#">Тормозные диски GRIZZLY</a>
                                    </div>
                                    <div class="event-item-desc">
                                        <p>Тормозные диски специально разработаны для машин класса C таких как Volvo S60</p>
                                    </div>
                                    <div class="event-item-bar clearfix">
                                        <div class="on-left event-item-price">20 000 р.</div>
                                        <div class="on-right event-item-button">
                                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="event-item section-small border-top border-bottom clearfix">
                                <div class="event-item-image">
                                    <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/event.jpg);"></a>
                                </div>
                                <div class="event-item-info">
                                    <div class="accessories-item-name">
                                        <a class="animate-custom red-hover" href="#">Тормозные диски GRIZZLY</a>
                                    </div>
                                    <div class="event-item-desc">
                                        <p>Тормозные диски специально разработаны для машин класса C таких как Volvo S60</p>
                                    </div>
                                    <div class="event-item-bar clearfix">
                                        <div class="on-left event-item-price">20 000 р.</div>
                                        <div class="on-right event-item-button">
                                            <a class="animate-custom red-small-link" href="#">Заказать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="bottom-part clearfix">
                    <a class="on-left" href="#">История обслуживания</a>
                    <a class="on-right" href="#">Все акции</a>
                </div>

            </div>
        </div><!-- /Service -->

        <!-- Insurance -->
        <div id="profile-insurance" class="section section-gray section-sec">
            <div class="box-container">

                <div class="clearfix">

                    <div class="left-part">
                        <div class="title-gray">
                            <h2>Страхование</h2>
                        </div>

                        <div>
                            <a class="red-big-link animate-custom" href="#">Сообщить о страховом случае</a>
                        </div>

                        <div class="section-small border-top">
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-type on-left right-margin">ОСАГО</div>
                                <div class="service-progress-item-date on-left right-margin">23 марта 2015</div>
                                <div class="service-progress-item-link on-left"><a href="#">Продлить</a></div>
                                <div class="service-progress-item-company on-right left-margin"><a href="#">ООО "Страховщик"</a></div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-type on-left right-margin">КАСКО</div>
                                <div class="service-progress-item-date on-left right-margin">23 марта 2015</div>
                                <div class="service-progress-item-link on-left"><a class="use-popup" data-block="car-insurance">Расчитать стоимость</a></div>
                            </div>
                        </div>

                    </div>

                    <div class="right-part">
                        <div class="title-gray">
                            <h2>Оценка автомобиля</h2>
                        </div>

                        <div class="clearfix">
                            <a class="on-left red-big-link animate-custom use-popup" data-block="car-sell">Продать автомобиль</a>
                            <div class="on-left your-auto-cost">за <strong>520 000</strong> р.</div>
                            <div class="on-right you-auto-info">
                                <div class="your-auto-cost-date">цена на 23 марта</div>
                                <div class="your-auto-cost-recost"><a href="#">Оценить заново</a></div>
                            </div>
                        </div>

                        <div class="section-small border-top">
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-date on-left right-margin">Стоимость на 10 марта 2015</div>
                                <div class="service-progress-item-price on-left">580 000 р.</div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-date on-left right-margin">Стоимость на 11 марта 2015</div>
                                <div class="service-progress-item-price on-left">620 000 р.</div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-date on-left right-margin">Стоимость на 12 марта 2015</div>
                                <div class="service-progress-item-price on-left">660 000 р.</div>
                            </div>
                            <div class="service-progress-item clearfix">
                                <div class="service-progress-item-date on-left right-margin">Стоимость на 13 марта 2015</div>
                                <div class="service-progress-item-price on-left">120 000 р.</div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="clearfix">

                    <div class="left-part">

                        <div class="bottom-part">
                            <a href="#">История страхования</a>
                        </div>

                    </div>

                    <div class="right-part">

                        <div class="bottom-part">
                            <a href="#">История оценки</a>
                        </div>

                    </div>

                </div>

            </div>
        </div><!-- /Insurance -->

        <!-- Reviews -->
        <div id="profile-reviews" class="section section-sec">
            <div class="box-container">
                <div class="title-gray title-with-link">
                    <h2>Отзывы</h2>
                    <div class="title-link"><a class="animate-custom red-big-link" href="#">Написать Президенту компании</a></div>
                </div>

                <div class="reviews-items">
                    <div class="reviews-item border-bottom">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">Сегодня 13:54:46</div>
                                <div class="reviews-item-status positive">позитивный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                        <div class="reviews-item-vote clearfix">
                            <div class="on-right">
                                <span class="reviews-item-vote-label">Вам помог этот отзыв: </span>
                                <span class="reviews-item-vote-yes"><a class="animate-custom" href="#">Да</a></span>
                                <span class="reviews-item-vote-no"><a class="animate-custom" href="#">Нет</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="reviews-item border-bottom">
                        <div class="reviews-item-rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="reviews-item-header clearfix">
                            <div class="reviews-item-avatar">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(); "></a>
                            </div>
                            <div class="reviews-item-info">
                                <div class="reviews-item-date">02.04.2015 13:54:46</div>
                                <div class="reviews-item-status negative">негативный</div>
                                <div class="reviews-item-user"><a class="animate-custom red-hover" href="#">Огородник Иван Михайлович</a></div>
                                <div class="reviews-item-theme">Преобретение автомобиля</div>
                            </div>
                        </div>
                        <div class="reviews-item-desc">
                            <p>Благодарность менеджеру Андрею Иванову SSANGYONG на Коптевкой<br/>9 февраля приобрел автомобиль в Favorit Motors. Был приятно удивлен работой менеджера Иванова Андрея, последнее время редко встретишь менеджера, который не только говорит, а слушает и слышит. Андрей помог нам определиться по автомобилю по его комплектации, оперативно реагировал на все вводные, всегда был вежлив, слова всегда сходились с делом. Один словом - Андрей ПРОФЕССИОНАЛ, что мы лично прочувствовали с первых минут знакомства до его звонка после покупки авто.</p>
                        </div>
                        <div class="reviews-item-vote clearfix">
                            <div class="on-right">
                                <span class="reviews-item-vote-label">Вам помог этот отзыв: </span>
                                <span class="reviews-item-vote-yes"><a class="animate-custom" href="#">Да</a></span>
                                <span class="reviews-item-vote-no"><a class="animate-custom" href="#">Нет</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bottom-part">
                    <a href="#">Все отзывы</a>
                </div>

            </div>
        </div><!-- /Reviews -->

        <!-- News -->
        <div id="profile-news" class="section section-gray section-sec">
            <div class="box-container">
                <div class="title-gray">
                    <h2>Новости компании</h2>
                </div>

                <div class="profile-news-items">
                    <div class="profile-news-item border-bottom">
                        <div class="profile-news-item-date">Сегодня</div>
                        <div class="profile-news-item-title"><a class="animate-custom red-hover" href="#"><h3>Новая модель KIA RIO </h3></a></div>
                        <div class="profile-news-item-desc">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                        </div>
                    </div>
                    <div class="profile-news-item border-bottom">
                        <div class="profile-news-item-date">12 мая 2015</div>
                        <div class="profile-news-item-title"><a class="animate-custom red-hover" href="#"><h3>Что делать если не заводится газель в мороз?</h3></a></div>
                        <div class="profile-news-item-desc">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                        </div>
                    </div>
                    <div class="profile-news-item border-bottom">
                        <div class="profile-news-item-date">11 мая 2015</div>
                        <div class="profile-news-item-title"><a class="animate-custom red-hover" href="#"><h3>Работа сервисного центра в вопросах и ответах</h3></a></div>
                        <div class="profile-news-item-desc">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                        </div>
                    </div>
                </div>

                <div class="bottom-part">
                    <a href="#">Все новости</a>
                </div>

            </div>
        </div><!-- /News -->

        <!-- Tabs Container -->
        <div class="section-tabs">
            <div class="tabs-navigation section-gray">
                <div class="box-container">
                    <ul class="tabs four-tabs clearfix">
                        <li class="active"><a class="animate-custom" data-block="#accessories">Аксессуары</a></li>
                        <li><a class="animate-custom" data-block="#parts">Запчасти</a></li>
                        <li><a class="animate-custom" data-block="#options">Комплектация</a></li>
                        <li><a class="animate-custom" data-block="#characteristics">Технические характеристики</a></li>
                    </ul>
                </div>
            </div>

            <div id="accessories" class="section tab-container section-sec">
                <div class="box-container">

                    <div class="filter"><form id="accessories-filter" class="clearfix">

                            <div class="filter-item clearfix">
                                <label for="brand">Выбирите марку:</label>
                                <select id="brand">
                                    <option>Все марки</option>
                                    <option>Марка 1</option>
                                    <option>Марка 2</option>
                                </select>
                            </div>

                            <div class="filter-item clearfix">
                                <label for="sorting">Сортировать по:</label>
                                <select id="sorting">
                                    <option>Умолчанию</option>
                                    <option>Параметр 1</option>
                                    <option>Параметр 1</option>
                                </select>
                            </div>

                            <div class="filter-item clearfix">
                                <div class="on-left">
                                    <input id="lets-search" type="submit" class="hidden" value="" />
                                    <label for="lets-search"></label>
                                </div>
                                <div class="on-right">
                                    <label for="search-text"></label>
                                    <input id="search-text" type="text" class="large" placeholder="Поиск по каталогу аксессуаров" />
                                </div>
                            </div>

                    </form></div>

                    <div class="accessories-items">
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/accessories.jpg);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Автохолодильники</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">iSky iREF-18</a>
                                    <span class="on-right accessories-item-price">19 900 р.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>Холодильник работает как от сети автомобиля (12V), так и от стандартной сети в 220 V. Даже если пикник затянулся, а подзарядки нет, вы можете быть спокойными – аккумулятор холода поддерживает температуру внутри пластикового контейнера до нескольких часов</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Заказать</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/accessories.jpg);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Автохолодильники</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">iSky iREF-18</a>
                                    <span class="on-right accessories-item-price">19 900 р.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>Холодильник работает как от сети автомобиля (12V), так и от стандартной сети в 220 V. Даже если пикник затянулся, а подзарядки нет, вы можете быть спокойными – аккумулятор холода поддерживает температуру внутри пластикового контейнера до нескольких часов</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Заказать</a>
                            </div>
                        </div>
                        <div class="accessories-item section-small border-top border-bottom clearfix">
                            <div class="accessories-item-image">
                                <a class="bg animate-custom opacity-custom-hover" href="#" style="background-image: url(content/profile/accessories.jpg);"></a>
                            </div>
                            <div class="accessories-item-info">
                                <div class="accessories-item-category">
                                    <a class="animate-custom red-hover" href="#">Автохолодильники</a>
                                </div>
                                <div class="accessories-item-name clearfix">
                                    <a class="on-left animate-custom red-hover" href="#">iSky iREF-18</a>
                                    <span class="on-right accessories-item-price">19 900 р.</span>
                                </div>
                                <div class="accessories-item-desc">
                                    <p>Холодильник работает как от сети автомобиля (12V), так и от стандартной сети в 220 V. Даже если пикник затянулся, а подзарядки нет, вы можете быть спокойными – аккумулятор холода поддерживает температуру внутри пластикового контейнера до нескольких часов</p>
                                </div>
                            </div>
                            <div class="accessories-item-button">
                                <a class="animate-custom red-small-link" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>

                    <div class="pagination">

                        <ul class="pagination-list">
                            <li><a href="#" class="animate-custom pagination-prev"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#" class="animate-custom pagination-first">Первая</a></li>
                            <li><a href="#" class="animate-custom">3</a></li>
                            <li><a class="animate-custom active">4</a></li>
                            <li><a href="#" class="animate-custom">5</a></li>
                            <li><a href="#" class="animate-custom pagination-last">Последняя</a></li>
                            <li><a href="#" class="animate-custom pagination-next"><i class="fa fa-angle-right"></i></a></li>
                        </ul>

                        <div class="pagination-show-all"><a href="#" class="animate-custom">Показать все</a></div>

                    </div>

                    <div class="mail-as bottom-part on-center">
                        <span>Не нашли то, что искали?</span> <i class="fa fa-envelope"></i> <a href="#" class="animate-custom red-link">Напишите нам</a>
                    </div>

                </div>
            </div>

            <div id="parts" class="section tab-container section-sec">
                <div class="box-container">

                </div>
            </div>

            <div id="options" class="section tab-container section-sec">
                <div class="box-container">

                </div>
            </div>

            <div id="characteristics" class="section tab-container section-sec">
                <div class="box-container">

                </div>
            </div>

        </div><!-- /Tabs Container -->

    </section><!-- /Content -->

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>