<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Event Selected");
?>

    <!-- Content -->
    <section id="content">

        <div class="section-red">
            <div class="box-container">
                <div class="breadcrumbs-two">
                    <ul class="breadcrumbs-list">
                        <li><a href="#">Каталог акций</a></li>
                        <li>/</li>
                        <li><a href="#">Cпцеиальные предложения на покупку SKODA</a></li>
                    </ul>
                </div>
                <div class="title-gray clearfix event-section">
                    <div class="on-left"><h1>SKODA. Успейте купить!</h1></div>
                    <div class="on-right"><h2 class="align-left">Адреса салонов:</h2></div>
                </div>
            </div>
        </div>

        <!-- Banners Header -->
        <div id="banner-header" class="section-sec section-gray">
            <div class="box-container clearfix">

                <div class="on-left bg" style="background-image: url(content/events/banner-1.jpg); "></div>

                <div class="on-right">
                    <div class="event-address-items align-left">

                        <div class="event-address-item"><span class="red-text">м.</span> Войковская, Москва, ул. Коптевская, д. 69-71</div>

                        <div class="event-address-item"><span class="red-text">м.</span> Семеновская, Москва, ул. Большая Семеновская, д.42</div>

                        <div class="event-address-item"><span class="red-text">м.</span> Новогиреево, Выхино, Москва, МКАД, 2 км, внешний радиус, г. Реутов</div>

                        <div class="event-address-item"><span class="red-text">м.</span> Лермонтовский проспект, Московская обл., г. Люберцы, Рязанское шоссе, д. 46</div>

                        <div class="event-address-item"><span class="red-text">м.</span> Юго-Западная, Москва, Ленинский проспект, д. 146</div>

                        <div class="event-address-item"><span class="red-text">м.</span> Южная, Пражская, Москва, Дорожный 1-й проезд, д.4</div>

                    </div>
                </div>

            </div>
        </div><!-- /Banners Header -->

        <!-- Events Body -->
        <div class="section section-sec padding">
            <div class="box-container">

                <div class="table-element">
                    <div class="table-body">
                        <table>
                            <tbody>
                            <tr class="border-bottom">
                                <th class="align-left">Модель</th>
                                <th class="align-left">Комплектация</th>
                                <th class="align-left">Цвет</th>
                                <th>Цена</th>
                                <th>Со скидкой</th>
                                <th>Выгода</th>
                                <th></th>
                            </tr>
                            <tr class="border-bottom">
                                <td class="align-left nowrap">SKODA Rapid</td>
                                <td class="align-left">Rapid Elegance 1.4 (122HP) DSG 7 пакет 8, защита, лиски 16, тонировка Sunset,передние спортивные сиденья , датчик дождя и света</td>
                                <td class="align-left">Черный перламутр с белой крышей</td>
                                <td class="nowrap"><strong>929 800</strong></td>
                                <td class="nowrap"><strong>879 800</strong></td>
                                <td class="nowrap red-text"><strong>50 000</strong></td>
                                <td><strong><a class="green-text use-popup" data-block="book-car">Зарезервировать</a></strong></td>
                            </tr>
                            <tr class="border-bottom">
                                <td class="align-left nowrap">SKODA Rapid</td>
                                <td class="align-left">Rapid Elegance 1.4 (122HP) DSG 7 пакет 8, защита, лиски 16, тонировка Sunset,передние спортивные сиденья , датчик дождя и света</td>
                                <td class="align-left">Черный перламутр с белой крышей</td>
                                <td class="nowrap"><strong>929 800</strong></td>
                                <td class="nowrap"><strong>879 800</strong></td>
                                <td class="nowrap red-text"><strong>50 000</strong></td>
                                <td><strong><a class="green-text use-popup" data-block="book-car">Зарезервировать</a></strong></td>
                            </tr>
                            <tr class="border-bottom">
                                <td class="align-left nowrap">SKODA Rapid</td>
                                <td class="align-left">Rapid Elegance 1.4 (122HP) DSG 7 пакет 8, защита, лиски 16, тонировка Sunset,передние спортивные сиденья , датчик дождя и света</td>
                                <td class="align-left">Черный перламутр с белой крышей</td>
                                <td class="nowrap"><strong>929 800</strong></td>
                                <td class="nowrap"><strong>879 800</strong></td>
                                <td class="nowrap red-text"><strong>50 000</strong></td>
                                <td><strong><a class="green-text use-popup" data-block="book-car">Зарезервировать</a></strong></td>
                            </tr>
                            <tr class="border-bottom">
                                <td class="align-left nowrap">SKODA Rapid</td>
                                <td class="align-left">Rapid Elegance 1.4 (122HP) DSG 7 пакет 8, защита, лиски 16, тонировка Sunset,передние спортивные сиденья , датчик дождя и света</td>
                                <td class="align-left">Черный перламутр с белой крышей</td>
                                <td class="nowrap"><strong>929 800</strong></td>
                                <td class="nowrap"><strong>879 800</strong></td>
                                <td class="nowrap red-text"><strong>50 000</strong></td>
                                <td><strong><a class="green-text use-popup" data-block="book-car">Зарезервировать</a></strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-element">
                    <div class="text-body">
                        <p>Уважаемые клиенты, получайте только положительные эмоции от покупки ŠKODA Octavia с выгодой до 200 000 рублей* Воспользуйтесь нашим предложением, покупая в кредит по ставке от 11% годовых, задействовав программу "Утилизации/Trade-In" и приобретая пакеты опций на выгодных условиях!<br/>Получайте удовольствие от путешествий, коллекционируйте незабываемые впечатления! Наслаждайтесь поездками за рулём Вашего нового автомобиля ŠKODA Octavia!<br/>Спешите воспользоваться предложением и стать обладателем идеального автомобиля </p>
                        <img src="content/events/banner-4.jpg" alt="event-image" />
                        <p>Акция действительна с 01.03.2015 по 31.03.2015. Количество автомобилей ограничено. Подробности акции уточняйте у менеджеров отдела продаж по телефону 8 (495) 308 91 27.</p>
                        <p>Ждем Вас во всех дилерских центрах Автопрага:<br/>Автопрага Юг – 1-й Дорожный проезд, д. 4;<br/>Автопрага Северо-Запад – ул. Коптевская, д.71.</p>
                    </div>
                </div>

                <div class="prev-event clearfix">

                    <div class="on-left">
                        <a href="#" class="animate-custom opacity-custom-hover bg" style="background-image: url(content/events/banner-2.jpg); "></a>
                    </div>

                    <div class="on-right align-left">
                        <p>*Основные условия кредитования ООО «Фольксваген Банк РУС» (далее «Банк») в рамках специальной акции «ŠKODA(Octavia, Rapid, Yeti)» на новые автомобили ŠKODAOctavia, ŠKODARapid, ŠKODAYeti: валюта кредита – рубли РФ; сумма кредита от 120 тыс. до 4 млн. рублей. Срок кредита – от 12 до 36 месяцев. Минимальный первоначальный взнос (далее – «ПВ») – 15% от стоимости автомобиля и страховых премий, в случае их включения в сумму кредита. Процентная ставка при предоставлении полного пакета документов составит: при сроке 12 месяцев – 11% годовых, при сроке от 13 до 24 месяцев – 16% годовых, при сроке от 25 до 36 месяцев – 18% годовых.Обеспечение по кредиту – залог приобретаемого автомобиля. Часть кредита может быть направлена на оплату страхования КАСКО и/или иных программ страхования, сумма страховой премии по которым зависит от перечня страхуемых рисков, выбранной Клиентом программы и суммы кредита. Обязательно страхование от рисков утраты, угона и ущерба на срок не менее одного года, без условия о рассрочке платежа страховой премии и без возможности выплаты страхового возмещения по калькуляции. Срок действия акции с 01.03.2015 г. по 31.03.2015 г. Условия предоставления кредита действительны на 01.03.2015 г. и могут быть изменены Банком. Информация по телефону Банка: 8-800-700-75-57 (звонок по России бесплатный). Лицензия ЦБ РФ № 3500, 117485, г. Москва, ул. Обручева, д.30/1, стр.1. www.vwbank.ru. </p>
                    </div>

                </div>

                <div class="bottom-part event-bottom-links clearfix">
                    <div class="on-right"><a href="#">Все акции Skoda</a></div>
                    <div class="on-right on-center"><a href="#" class="red-small-link animate-custom">Унать подробности акции</a></div>
                </div>

            </div>
        </div><!-- /Events Body -->

        <!-- Event Deals -->
        <div class="section section-sec section-gray">
            <div class="box-container medium">

                <div class="event-deal-items"><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-wrench"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Сервисные<br/>акции</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-car"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог подержанных<br/>авто со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-rub"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Кредитные<br/>предложения</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-umbrella"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Страхование<br/>Genius</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-file-text-o"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Каталог автомобилей<br/>в наличии со скидкой</span>
                    </a><!--
                    --><a href="#" class="event-deal-item clearfix">
                        <span class="event-deal-item-icon on-left animate-custom"><i class="fa fa-expand"></i></span>
                        <span class="event-deal-item-text on-left animate-custom">Выгода при сдаче<br/>авто в Trade-In</span>
                    </a><!--
                --></div>

            </div>
        </div><!-- /Event Deals -->

        <!-- Phone -->
        <div id="add-your-phone" class="section section-sec padding">
            <div class="box-container medium">

                <div class="text on-center">
                    <p>Оставьте свой телефон и мы перезвоним вам в течении 15 минут</p>
                </div>

                <div class="filter"><form id="event" class="clearfix">

                    <div class="filter-item clearfix">
                        <label for="event-name" class="on-left">Ваше имя:</label>
                        <input id="event-name" class="on-left" type="text" />
                    </div>

                    <div class="filter-item on-center clearfix">
                        <label for="event-phone" class="on-left">Телефон:</label>
                        <input id="event-phone" class="on-left phone-mask" type="text" />
                    </div>

                    <div class="filter-item align-right">
                        <input type="submit" class="animate-custom red-small-link" value="Персональное предложение" />
                    </div>

                </form></div>

            </div>
        </div><!-- /Phone -->

        <!-- Seo Text -->
        <div class="section section-sec section-gray padding">
            <div class="box-container">

                <div class="seo-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                </div>

            </div>
        </div><!-- /Seo Text -->

    </section><!-- /Content -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>